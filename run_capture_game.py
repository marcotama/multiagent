# -*- coding: utf-8 -*-
"""
Runs episodes of Q-learning in Pacman.

@author: Marco Tamassia
"""
import argparse
import random
from os import makedirs
from os.path import exists, dirname
from sys import exit
import ast
import numpy as np
import inspect
import pprint

from pacman_ctf.layout import get_layout
from pacman_ctf.capture_games_runner import GamesRunner
from reinf.exploration_strategies import ExplorationParams
from util.util2 import pickle_save, pickle_load
from util.timing import Interval


def load_agent(pacman, no_graphics):
    import pkgutil
    from importlib import import_module
    agentsModuleNames = [name for _, name, _ in pkgutil.iter_modules(['pacman_ctf/agents'])]
    for agentsModuleName in agentsModuleNames:
        if not agentsModuleName.endswith('gents'):
            continue
        agentsModule = import_module("pacman_ctf.agents." + agentsModuleName)
        if pacman in dir(agentsModule):
            if no_graphics and agentsModule == 'keyboard_agents.py':
                raise Exception('Using the keyboard requires graphics (not text display)')
            return getattr(agentsModule, pacman)
    raise Exception('The agent %s is not specified in any *gents.py.' % pacman)


def fill_defaults(settings):
    if 'features' not in settings:
        settings['features'] = [
            'best_way_to_food',
            'best_way_to_invader',
            'best_way_to_scared_defender',
            'best_way_to_non_scared_defender',
            #'best_way_to_capsule',
            'best_way_to_home',
            #'distance_to_food',
            #'distance_to_invader',
            #'distance_to_scared_defender',
            #'distance_to_non_scared_defender',
            #'distance_to_capsule',
            'am_I_attacking',
            'am_I_scared',
            #'is_teammate_attacking',
            'am_I_carrying_enough_food_to_win',
            'closest_entity'
        ]
        settings['q_table_features'] = [
            'best_way_to_food',
            'best_way_to_invader',
            'best_way_to_scared_defender',
            'best_way_to_non_scared_defender',
            #'best_way_to_capsule',
            'best_way_to_home',
            #'distance_to_food',
            #'distance_to_invader',
            #'distance_to_scared_defender',
            #'distance_to_non_scared_defender',
            #'distance_to_capsule',
            'am_I_attacking',
            'am_I_scared',
            #'is_teammate_attacking',
            'am_I_carrying_enough_food_to_win',
            'closest_entity'
        ]
    if 'gamma' not in settings:
        settings['gamma'] = 0.97
    if 'alpha' not in settings:
        settings['alpha'] = 0.1
    if 'lmbda' not in settings:
        settings['lmbda'] = 0.3
    if 'eta' not in settings:
        settings['eta'] = 5
    if 'hashing_algorithm' not in settings:
        settings['hashing_algorithm'] = 'UNH'
    if 'hashing_memory' not in settings:
        settings['hashing_memory'] = 0
    if 'trace' not in settings:
        settings['trace'] = 'accumulating'
    if 'n_tiles' not in settings:
        settings['n_tiles'] = 5
    if 'n_tilings' not in settings:
        settings['n_tilings'] = 5
    if 'init_q' not in settings:
        settings['init_q'] = 0.0
    if 'operator' not in settings:
        settings['operator'] = 'bellman'
    if 'operator_alpha' not in settings:
        settings['operator_alpha'] = 0.2
    if 'early_termination' not in settings:
        settings['early_termination'] = True
    if 'depth' not in settings:
        settings['depth'] = 3
    if 'exploration_strategy' not in settings:
        settings['exploration_strategy'] = 'EpsilonGreedy'
    if 'epsilon' not in settings:
        settings['epsilon'] = 0.1
    if 'epsilon_ci' not in settings:
        settings['epsilon_ci'] = 0.05
    if 'cp' not in settings:
        settings['cp'] = 2.0
    if 'significance' not in settings:
        settings['significance'] = 0.5
    if 'annealing_variable' not in settings:
        settings['annealing_variable'] = 'visits'
    if 'init_temperature' not in settings:
        settings['init_temperature'] = 0.05
    if 'start_descent' not in settings:
        settings['start_descent'] = 10
    if 'end_descent' not in settings:
        settings['end_descent'] = 20
    if 'do_not_override_exploration_strategy' not in settings:
        settings['do_not_override_exploration_strategy'] = True
    if 'projector' not in settings:
        settings['projector'] = 'tile_coder'
    if 'state_action_encoder' not in settings:
        settings['state_action_encoder'] = 'tabular'
    if 'scales' not in settings:
        settings['scales'] = 'log'
    if 'threshold' not in settings:
        settings['threshold'] = 0.2
    if 'evaluation_cycle' not in settings:
        settings['evaluation_cycle'] = 5
    if 'prob_attack' not in settings:
        settings['prob_attack'] = 0.8
    if 'prob_scared_flee' not in settings:
        settings['prob_scared_flee'] = 0.8
    if 'disable_primitive_actions' not in settings:
        settings['disable_primitive_actions'] = True
    return settings


def instantiate_agent (agentIndex, settings):
    settings['index'] = agentIndex

    if 'add_options_from_file' not in settings or settings['add_options_from_file'] == '':
        settings['options'] = []
    else:
        settings['options'] = pickle_load(settings['add_options_from_file'])

    # noinspection PyProtectedMember
    explSettings = {key: settings[key] if key in settings else None for key in ExplorationParams._fields}
    exploration_params = ExplorationParams(**explSettings)
    if 'load_agent' in settings and settings['load_agent']:
        agent = pickle_load(settings['load_agent'])
        if not settings['do_not_override_exploration_strategy']:
            agent.set_exploration_strategy(exploration_params)
    else:
        ag_cls = load_agent(settings['controller'], True)
        params = inspect.signature(ag_cls.__init__).parameters

        # if the desired class wants exploration_params, mdpParams or mdpOptionsParams, create them
        additionalParams = {
            'exploration_params': ExplorationParams,
        }
        for key, params_cls in additionalParams.items():
            if key in params:
                try:
                    settings[key] = params_cls(**{key: settings[key] for key in params_cls._fields})
                except KeyError as e:
                    raise ValueError("Missing parameter: %s" % e.args)

        agent = ag_cls(**{key: settings[key] for key in params if key != 'self'})

    return agent


def create_agent(index, name, settings_string, layout, quiet):
    settings = ast.literal_eval(settings_string)
    settings = fill_defaults(settings)
    settings['layout'] = layout
    if not quiet:
        print("%s settings:" % name)
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(settings)
        print()
    return instantiate_agent(index, settings)


def save_agents(path, agents):
    if dirname(path) != '' and not exists(dirname(path)):
        makedirs(dirname(path))
    pickle_save(path, agents)


def doStuff ():
    parser = argparse.ArgumentParser(
        description='Run a multi-episode experiment')
    parser.add_argument(
        '-b1',
        dest='b1',
        type=str,
        help='Parameters for the controller of agent Blue 1. The value should be a string '
             'representing a Python dictionary. A tentative list of values is:\n'
             '- name: an identifier for the agent\n'
             '- load_agent: file from which to load a previously saved agent\n'
             '- do_not_override_exploration_strategy: whether to override the loaded exploration strategy when using load_agent\n'
             '- controller: the controller for the agent (one of KeyboardAgent, QLearningAgent, MultiQLearningAgent, '
             'ConservativePacman, AggressivePacman, FSMPacman; not supported yet: MultiAgentSearchPacman, '
             'MinimaxPacman, AlphaBetaPacman, ExpectimaxPacman, MCTSAgent)\n'
             '- features: features to use for Q-learning agents (as a list); options are:\n'
             '  - self_coordinates\n'
             '  - self_score\n'
             '  - legal_moves\n'
             '  - pacman_coordinates\n'
             '  - <entity>-<attribute>'
             '  - nearest[<number>]-<entity>-<attribute>'
             '  - agent[<number>]-<attribute>, '
             '  - shortest_path_distance_to_nearest-<entity>-via-<direction>'
             'where entity can be any of:\n'
             '    - ghost\n'
             '    - scared_ghost\n'
             '    - nonscared_ghost\n'
             '    - capsule\n'
             '    - food\n'
             '    - escape_junction (not supported yet)\n'
             '    - safe_escape_junction\n'
             'attribute can be any of:\n'
             '    - coordinates\n'
             '    - shortest_path_distance\n'
             '    - shortest_path_direction\n'
             'direction can be any of:\n'
             '    - north\n'
             '    - south\n'
             '    - east\n'
             '    - west\n'
             'number defines either n for the n-th nearest entity or the agent index'
             ' - features_sets: features to use for multi-abstraction Q-Learning (as a list of lists); options are '
             'the same format of the argument for features; if an empty list is passed, normal Q-learning is used'
             '- for Q-learning and GQ:\n'
             '  - alpha: learning parameter for Q-learning\n'
             '  - gamma: discount factor for the MDPs'
             '- for Q-learning only:\n'
             '  - init_q: initial value for Q-table entries\n'
             '  - operator: operator to be used for Q-learning updates; can be bellman, consistent-bellman, '
             '- for GQ only:\n'
             '  - eta: learning parameter for second set of weights\n'
             '  - gamma: discount factor for the MDPs'
             'advantage-learning, persistent-advantage-learning or lazy (see Bellemare et al., 2015)\n'
             '  - operator_alpha: alpha parameter for non-standard operators\n'
             '- for MDPs w/ options:\n'
             '  - add_options_from_file: options to be loaded (a file location)\n'
             '  - early_termination: whether or not options should be terminated early if chosen action is not the best one\n'
             '- for exploration strategies:\n'
             '  - explStrategy: exploration strategy to be used (one of EpsilonGreedyPolicy, EpsilonCIGreedy, UCT, '
             'AnnealingEpsilonGreedy, AnnealingEpsilonCIGreedy, Softmax, GreedyPolicy, Random)\n'
             '  - epsilon: exploration probability for epsilon-greedy strategy\n'
             '  - epsilon_ci: CI exploration probability for epsilon-CI-greedy strategy\n'
             '  - cp: UCT exploration strategy coefficient\n'
             '  - init_temperature: initial temperature for Softmax strategy\n'
             '  - annealing_variable: variable to consider for annealing schedules; can be visits or episodes\n'
             '  - start_descent: qualitatively, where to start the descent of the sigmoid schedule for strategies with'
             'annealing (i.e. AnnealingEpsilonGreedy, AnnealingCIEpsilonGreedy and Softmax)\n'
             '  - end_descent: qualitatively, where to end the descent of the sigmoid schedule\n'
             '  - significance: significance value for t-tests (for multi-abstraction Q-Learning\n'
             '- depth: depth for tree search (for relevant agents, such as MinimaxPacman etc.)')
    parser.add_argument(
        '-b2',
        dest='b2',
        type=str,
        help='Parameters for the controller of agent Blue 2. See -b1 for acceptable values.')
    parser.add_argument(
        '-r1',
        dest='r1',
        type=str,
        help='Parameters for the controller of agent Red 1. See -b1 for acceptable values.')
    parser.add_argument(
        '-r2',
        dest='r2',
        type=str,
        help='Parameters for the controller of agent Red 2. See -b1 for acceptable values.')
    parser.add_argument(
        '--episodes',
        type=int,
        default=1,
        help='episodes to be run', )
    parser.add_argument(
        '--test-greedily',
        action='store_true',
        help='whether to test without exploration strategy (runs 2x episodes)')
    parser.add_argument(
        '--quiet',
        action='store_true',
        help='whether to keep output to a minimum')
    parser.add_argument(
        '--graphics',
        type=str,
        default='null',
        choices=['null', 'text', 'full'],
        help='whether to show non-interactive games')
    parser.add_argument(
        '--graphics-after',
        type=int,
        default=0,
        help='if specified, this many episodes will be run without graphics')
    parser.add_argument(
        '--frame-time',
        type=float,
        default=0.0,
        help='time to delay between frames')
    parser.add_argument(
        '--random-seed',
        type=int,
        default=None,
        help='seed for random functions')
    parser.add_argument(
        '--save-agents',
        type=str,
        default=False,
        help='file where to save the agent')
    parser.add_argument(
        '--load-agents',
        type=str,
        default=False,
        help='file to load the agent from')
    parser.add_argument(
        '--save-logs',
        type=str,
        default=False,
        help='file where to save the logs of the games')
    parser.add_argument(
        '--do-not-run-if-log-file-exists',
        dest="do_not_run_if_log_file_exists",
        action='store_true',
        help='if this is set and the file where to save the log exists, the ' +
             'program will return immediately, without executing')
    parser.add_argument(
        '-l',
        '--layout',
        type=str,
        default='defaultCapture',
        help=("the layout file from which to load the " +
              "map layout"))
    parser.add_argument(
        '--save-moves-histories',
        type=str,
        default=None,
        help="where to save moves history; no history is saved if argument is not provided")
    parser.add_argument(
        '--show-countdown',
        action='store_true',
        help='whether to show a countdown between graphical games')

    args = parser.parse_args()

    if args.save_logs and args.do_not_run_if_log_file_exists:
        if exists(args.save_logs):
            exit(0)


    r1_agent, b1_agent, r2_agent, b2_agent = None, None, None, None
    if args.load_agents:
        path = args.load_agents
        if dirname(path) != '' and not exists(dirname(path)):
            makedirs(dirname(path))
        agents = pickle_load(path)
        r1_agent, b1_agent, r2_agent, b2_agent = agents

    layout = get_layout(args.layout)

    if args.r1 or r1_agent is None:
        r1_agent = create_agent(0, "Red 1", args.r1, layout, args.quiet)
    if args.b1 or b1_agent is None:
        b1_agent = create_agent(1, "Blue 1", args.b1, layout, args.quiet)
    if args.r2 or r2_agent is None:
        r2_agent = create_agent(2, "Red 2", args.r2, layout, args.quiet)
    if args.b2 or b2_agent is None:
        b2_agent = create_agent(3, "Blue 2", args.b2, layout, args.quiet)

    if not args.quiet:
        print("Other settings:")
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint({k: v for k, v in vars(args).items() if k not in ('r1', 'r2', 'b1','b2')})
        print()


    games_runner = GamesRunner(
        blue_agents=[b1_agent, b2_agent],
        red_agents=[r1_agent, r2_agent],
        layout=layout,
        episodes=args.episodes,
        test_greedily=args.test_greedily,
        quiet=args.quiet,
        graphics=args.graphics,
        graphics_after=args.graphics_after,
        frame_time=args.frame_time,
        save_moves_histories=args.save_moves_histories,
        show_countdown=args.show_countdown
    )

    if args.random_seed is not None:
        random.seed(args.random_seed)
        np.random.seed(args.random_seed)

    periodic_fun = Interval(600, save_agents, args.save_agents, (r1_agent, b1_agent, r2_agent, b2_agent))
    if args.save_agents:
        periodic_fun.start()
    try:
        episodes_data, moves_histories = games_runner.test()
        if args.save_logs:
            pickle_save(args.save_logs, episodes_data)
        if args.save_moves_histories:
            pickle_save(args.save_moves_histories, moves_histories)
    finally:
        if args.save_agents:
            periodic_fun.stop()
            save_agents(args.save_agents, (r1_agent, b1_agent, r2_agent, b2_agent))


if __name__ == '__main__':
    doStuff()