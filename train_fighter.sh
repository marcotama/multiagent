#!/usr/bin/env bash

python3 -u -O train_fighter.py \
  --save-agent "ql.controller" \
  --controller '{"controller": "QLearningAgent"}' \
  --log-paths /media/space/fightingice/FightingICE/log/replay/*MctsAi*MctsAi*.json \
  --limit 100 \
&> "trainer.log"
