#!/usr/bin/env bash

echo -e "
import pickle
from pacman_ctf.options.capture_options import GoToFood, GoToCapsule, GoToInvader, GoToScaredDefender, GoToHome, AvoidInvader, AvoidNonScaredDefender
from util.translator import Translator
termProb = 0.2
with open('capture_options.pkl','wb') as f:
    options = [
        GoToFood(termProb),
        #GoToCapsule(termProb),
        GoToInvader(termProb),
        GoToScaredDefender(termProb),
        GoToHome(termProb),
        AvoidInvader(termProb),
        AvoidNonScaredDefender(termProb)
    ]
    pickle.dump(options,f)
" | python3 \
|| exit 1

python \
  run_capture_game.py \
  -b1 '{"controller": "QLearningAgent", "add_options_from_file": "capture_options.pkl", "early_termination": False}' \
  -b2 '{"controller": "QLearningAgent", "add_options_from_file": "capture_options.pkl", "early_termination": False}' \
  -r1 '{"controller": "OffensiveReflexAgent"}' \
  -r2 '{"controller": "DefensiveReflexAgent"}' \
  --episodes 10000 \
  --save-agents agents.pkl
