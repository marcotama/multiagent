# -*- coding: utf-8 -*-
"""
Analyze logs to check performance of learning algorithms

@author: Marco Tamassia
"""
import argparse
import ast
from collections import OrderedDict
from itertools import cycle, chain
from typing import List, Dict, Tuple

from util.util2 import pickle_load, pickle_save
from util.data_manipulation import normalize, get_averages, filter_groups, transpose, load_eps_data, load_games_data, pairwise_stat_analyses, generate_csv
from util.plots import get_colors, smooth_curves, parse_insets_params, calc_plots_args, calc_fill_between_args, save_plot, save_stackplot, save_boxplot


fields_info = {
    'outcome': {
        'human'   : 'Victories to games ratio',
        'include_non-learning_agents': True
    },
    'score': {
        'human'   : 'Score',
        'include_non-learning_agents': True
    },
    'movesCount': {
        'human'   : 'Moves count',
        'include_non-learning_agents': True
    },
    'statesCount': {
        'human'   : 'Explored states',
        'include_non-learning_agents': True
    },
    'stateActionPairsCount': {
        'human'   : 'Explored state-action pairs',
        'include_non-learning_agents': True
    },
    'executionTime': {
        'human'   : 'Execution time',
        'include_non-learning_agents': False
    },
    'eatenGhostsCount': {
        'human'   : 'Eaten ghosts',
        'include_non-learning_agents': True
    },
    'eatenFoodCount': {
        'human'   : 'Eaten food',
        'include_non-learning_agents': True
    },
    'eatenCapsulesCount': {
        'human'   : 'Eaten capsules',
        'include_non-learning_agents': True
    }
}





def load_data(args):
    try:
        nl_avg_by_ep, l_avg_by_ep, l_avg_by_rep, l_avg_by_rep_lim = pickle_load(args.outputFolder + 'analysis.cache')
    except FileNotFoundError:
        nl_avg_by_ep = OrderedDict()
        for demos_folder in args.demosFolders:
            nl_avg_by_ep.update(load_games_data(demos_folder, list(fields_info.keys())))
        l_avg_by_ep, l_avg_by_rep, l_avg_by_rep_lim = load_eps_data(args.inputFolder, args.desiredGroups, args.minEpisode, args.maxEpisode, list(fields_info.keys()))
        if args.saveCache:
            pickle_save(args.outputFolder + 'analysis.cache', (nl_avg_by_ep, l_avg_by_ep, l_avg_by_rep, l_avg_by_rep_lim))
    return nl_avg_by_ep, l_avg_by_ep, l_avg_by_rep, l_avg_by_rep_lim


def generate_plots(
    args: argparse.Namespace,
    data: Tuple[Dict[str, Dict[str, List[float]]], Dict[str, Dict[str, List[float]]]]
):
    nlad, lad = data

    insets = parse_insets_params(args.insets)
    insets2 = parse_insets_params(args.insets2)

    figure_specs = {
        'xscale':   'linear',
        'yscale':   'linear',
        'xlabel':   'Episodes',
        'width_in':  7.0,
        'height_in': 7.0
    }

    # Prepare learning agents data
    reps = max(gd['count'] for gd in lad.values())
    lad = filter_groups(lad, args.desiredGroups, args.desiredNames)
    lad = transpose(lad, list(fields_info.keys()))
    nlad = filter_groups(nlad, args.desiredGroups, args.desiredNames)
    nlad = get_averages(nlad, deeper_keys=['abstractionsUsages'], copy_keys=['count'])
    nlad = transpose(nlad, list(fields_info.keys()))

    for field, data in fields_info.items():
        human = data['human']
        include_nla = data['include_non-learning_agents']
        lad_f = smooth_curves(lad[field], args.window_size)
        max_x = max(len(c) for c in lad_f.values())
        nlad_f = OrderedDict((l, c.repeat(max_x)) for l, c in nlad[field].items())
        color_cycler = cycle(args.colors)
        lad_pa = calc_plots_args(curves=lad_f, colors=color_cycler, line_style='-', limit_x=args.limitX)
        if include_nla:
            nlad_pa = calc_plots_args(curves=nlad_f, colors=color_cycler, line_style='--', limit_x=args.limitX)
            pa = OrderedDict(chain(lad_pa.items(), nlad_pa.items()))
        else:
            pa = lad_pa
        fs = dict(figure_specs.items(), title=human + ", avg over %d reps" % reps, ylabel=human)
        save_plot(
            plot_args=pa,
            figure_specs=fs,
            show=False,
            file_path=args.outputFolder + str(field),
            file_formats=args.plotTypes,
            legend_args=args.legendArgs,
            insets=insets
        )

    if 'abstractionsUsages' in lad:
        curves = lad['abstractionsUsages']
        curves = OrderedDict(sorted(curves.items(), key=lambda label_curve: len(label_curve[0])))
        curves = normalize(curves)
        lad_fba = calc_fill_between_args(curves=curves, colors=args.colors, limit_x=args.limitX)
        fs = dict(figure_specs.items(), title='Abstractions usage', ylabel='Decisions')
        save_stackplot(
            fill_between_args=lad_fba,
            figure_specs=fs,
            show=False,
            file_path=args.outputFolder + 'abstractionsUsages',
            file_formats=args.plotTypes,
            insets=insets2
        )


def parse_arguments ():
    parser = argparse.ArgumentParser(
        description='Analyze data from experiments')
    parser.add_argument(
        '-d',
        '--demos-folders',
        dest='demosFolders',
        type=str,
        nargs='+',
        default=[],
        help='file to load')
    parser.add_argument(
        '-i',
        '--input-folder',
        dest='inputFolder',
        type=str,
        help='file to load')
    parser.add_argument(
        '-o',
        '--output-folder',
        dest='outputFolder',
        type=str,
        help='folder to save outputs')
    parser.add_argument(
        '-ws',
        '--window-size',
        type=int,
        default=50,
        help='sliding windows size')
    parser.add_argument(
        '-lx',
        '--limit-x',
        dest='limitX',
        type=int,
        default=3000,
        help='x axis maximum value')
    parser.add_argument(
        '-ot',
        '--output-type',
        dest='outputType',
        type=str,
        nargs='+',
        choices=['plots', 'qtable', 'csv', 'agents', 'abstractions_usage'],
        default='log',
        help='type of output')
    parser.add_argument(
        '-pt',
        '--plot-type',
        dest='plotTypes',
        type=str,
        default='PNG,PDF',
        help='format for plots; multiple inputs can be given comma-separated')
    parser.add_argument(
        '--min-episode',
        dest='minEpisode',
        type=int,
        default=1000,
        help='first episode used when summing total reward')
    parser.add_argument(
        '--max-episode',
        dest='maxEpisode',
        type=int,
        default=1000,
        help='last episode used when summing total reward')
    parser.add_argument(
        '--legend-args',
        dest='legendArgs',
        type=str,
        default=None,
        help='a dict containing arguments for legend positioning; it will be passed as **kwargs upon function call')
    parser.add_argument(
        '-s',
        '--save-cache',
        dest='saveCache',
        action='store_true',
        help='whether to cache plot data for faster future access')
    parser.add_argument(
        '-dg',
        '--desired-groups',
        dest='desiredGroups',
        type=str,
        nargs='+',
        default=None,
        help='restrict plots to the specified groups')
    parser.add_argument(
        '-dn',
        '--desired-names',
        dest='desiredNames',
        type=str,
        nargs='+',
        default=None,
        help='rename desired groups')
    parser.add_argument(
        '-c',
        '--colors',
        type=str,
        nargs='+',
        default=None,
        help='colors')
    parser.add_argument(
        '-is',
        '--insets',
        type=str,
        nargs='+',
        default=[],
        help='insets for each group; eg: "x1=2800,x2=3000,y1=1450,y2=1500,zf=2.5,loc=2,zf=4,' +
             'loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2"')
    parser.add_argument(
        '-is2',
        '--insets2',
        type=str,
        nargs='+',
        default=[],
        help='insets for stacked plots; eg: "x1=2800,x2=3000,y1=1450,y2=1500,zf=2.5,loc=2,zf=4,' +
             'loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2"')
    parser.add_argument(
        '--xscale',
        type=str,
        choices=['linear', 'log'],
        default='linear',
        help='scale for x axis')

    args = parser.parse_args()
    args.plotTypes = args.plotTypes.split(",")
    args.legendArgs = None if args.legendArgs is None else ast.literal_eval(args.legendArgs)
    return args


def do_stuff():
    args = parse_arguments()
    if args.colors is None:
        args.colors = get_colors(len(args.desiredGroups))
    nl_avg_by_ep, l_avg_by_ep, l_avg_by_rep, l_avg_by_rep_lim = load_data(args) # (Non)Learning Agent Data Average By Episode/Repetition
    if 'plots' in args.outputType:
        generate_plots(args, (nl_avg_by_ep, l_avg_by_ep))
    if 'csv' in args.outputType:
        generate_csv(l_avg_by_rep, args.outputFolder + "cumulative.csv", args.maxEpisode, 'score')
    pairwise_stat_analyses(l_avg_by_rep, args.outputFolder + "stat_analysis_all_%s.txt", args.maxEpisode, list(fields_info.keys()))
    pairwise_stat_analyses(l_avg_by_rep_lim, args.outputFolder + "stat_analysis_lim_%s.txt", args.maxEpisode, list(fields_info.keys()))
    save_boxplot(l_avg_by_rep, args.desiredGroups, args.desiredNames, args.outputFolder + "box_plot_all", args.plotTypes, 'score', 'Average score per episode')
    save_boxplot(l_avg_by_rep_lim, args.desiredGroups, args.desiredNames, args.outputFolder + "box_plot_limited", args.plotTypes, 'score', 'Average score per episode')


if __name__ == '__main__':
    do_stuff()