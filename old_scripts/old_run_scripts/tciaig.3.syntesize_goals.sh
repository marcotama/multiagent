#!/usr/bin/env bash

# Read params
EXPERIMENT_DIR="$1"
VALUE_ITERATION_THRESHOLD="$2"

# Setup dirs
GOALS_FOLDER="${EXPERIMENT_DIR}Goals/"
MODELS_FOLDER="${EXPERIMENT_DIR}Models/"
DEMOS_FOLDER="${EXPERIMENT_DIR}Demos/"
HUMAN_DEMOS_FOLDER="${EXPERIMENT_DIR}HumanDemos/"

# Backup code
mkdir -p ${GOALS_FOLDER}
tar -zcf "${GOALS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Do stuff
echo "Extracting goals from computer demos"
parallel \
  --xapply \
  --header : \
  python3 syntesize_goals.py \
    --use-sink-state \
    --threshold ${VALUE_ITERATION_THRESHOLD} \
    --model-file ${MODELS_FOLDER}learned.model \
    --output-file ${GOALS_FOLDER}{ALGORITHM_NAME}.goals \
    --demos-files ${DEMOS_FOLDER}{ALGORITHM_NAME}'/*'.demo \
  '&>' ${GOALS_FOLDER}{ALGORITHM_NAME}.out \
  ::: ALGORITHM_NAME $( \ls -d ${DEMOS_FOLDER}*/ | xargs -L1 basename )

echo "Extracting goals from human demos"
parallel \
  --xapply \
  --header : \
  python3 syntesize_goals.py \
    --use-sink-state \
    --threshold ${VALUE_ITERATION_THRESHOLD} \
    --model-file ${MODELS_FOLDER}learned.model \
    --output-file ${GOALS_FOLDER}{HUMAN_NAME}.goals \
    --demos-files ${HUMAN_DEMOS_FOLDER}{HUMAN_NAME}'/*'.demo \
  '&>' ${GOALS_FOLDER}{HUMAN_NAME}.out \
  ::: HUMAN_NAME $( \ls -d ${HUMAN_DEMOS_FOLDER}*/ | xargs -L1 basename )
