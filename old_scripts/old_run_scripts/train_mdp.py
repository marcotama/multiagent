# -*- coding: utf-8 -*-
"""
Entry point to train an agent with demons.

@author: Marco Tamassia
"""
import argparse
import ast
from pacman.mdp_trainer import MDPTrainer
from util.util2 import pickle_load, pickle_save


def parse_arguments ():

    parser = argparse.ArgumentParser(
        description='Train an agent using demos.')
    parser.add_argument(
        '--alpha',
        type=float,
        default=0.1,
        help='learning parameter for Q-learning')
    parser.add_argument(
        '--gamma',
        type=float,
        default=0.97,
        help='discount factor for the MDPs')
    parser.add_argument(
        '--init-q-value',
        dest="initQ",
        type=float,
        default=1,
        help='initial value for Q-table entries')
    parser.add_argument(
        '--load-agent',
        dest="loadAgent",
        type=str,
        default=False,
        help='file where to load the learning agent')
    parser.add_argument(
        '--save-agent',
        dest="saveAgent",
        type=str,
        required=True,
        help='file where to save the learning agent')
    parser.add_argument(
        '--features',
        dest='featuresStr',
        type=str,
        required=True,
        help=
             'features to use for Q-learning agents (as a string representing a Python list); options are:\n'
             '  - pacman_coordinates\n'
             '  - [nearest-]<entity>-<mapping>, where entity can be any of:\n'
             '    - ghost\n'
             '    - scaredGhost\n'
             '    - nonscaredGhost\n'
             '    - capsule\n'
             '    - food\n'
             '    - escapeJunction (not supported yet)\n'
             '    - safeEscapeJunction\n'
             '    and mapping can be any of:\n'
             '    - euclideanDistance\n'
             '    - angularDirection\n'
             '    - coordinates\n'
             '    - shortestPathDistance\n'
             '    - shortestPathDirection\n'
    )
    parser.add_argument(
        '-P',
        action='store_true',
        help='whether to learn the P table')
    parser.add_argument(
        '-Q',
        action='store_true',
        help='whether to learn the Q table')
    parser.add_argument(
        '-S',
        action='store_true',
        help='whether to learn the Q stats table')
    parser.add_argument(
        '-N',
        action='store_true',
        help='whether to learn the N table')
    parser.add_argument(
        '-R',
        action='store_true',
        help='whether to learn the R table')
    parser.add_argument(
        '-T',
        action='store_true',
        help='whether to learn the T table')
    parser.add_argument(
        '-H',
        action='store_true',
        help='whether to store the history')
    parser.add_argument(
        '--demo-files',
        dest='demoFiles',
        type=str,
        nargs='+',
        default=None,
        help='demo files')
    args = parser.parse_args()
    return args


def doStuff():
    args = parse_arguments()

    args.features = ast.literal_eval(args.features)

    trainer = MDPTrainer(args.features, args.gamma, args.alpha, args.initQ, args.Q, args.N, args.S, args.P, args.R, args.T, args.H)
    if args.loadAgent:
        trainer.agent = pickle_load(args.loadAgent)

    for i, file in enumerate(args.demoFiles):
        print("Processing demo file %d of %d" % (i+1, len(args.demoFiles)), end='\r')
        trainer.feedDemoFile(file)

    pickle_save(args.saveAgent, trainer.getAgent())


if __name__ == '__main__':
    doStuff()
