#!/usr/bin/env bash

if [[ $# -ne 1 ]]
then
	TIMESTAMP=`date +%Y-%m-%d`
	EXPERIMENT_DIR="experiments/exp-${TIMESTAMP}__bellemare_operators/"
else
	EXPERIMENT_DIR=$1
fi

[ -e "${EXPERIMENT_DIR}" ] || mkdir -p ${EXPERIMENT_DIR}
echo "Experiment folder: $EXPERIMENT_DIR"

EXPERIMENT_ID=`date +%Y-%m-%d__%H.%M.%S`





# ------ parameters ------
FEATURES="['.FdDr','.FdDs:','.NScGhDr','.NScGhDs:','.CpDr','.CpDs:','.ScGhDr','.ScGhDs:']"
EPISODES=3000
REPETITIONS=200
OPERATOR_ALPHA=0.8
AGENT_NAMES="bellman consistent-bellman advantage-learning persistent-advantage-learning lazy"
N_AGENTS=5 # number of agents, keep it consistent
LAYOUT="mediumClassic"
# ------------------------





WORKERS_PARALLEL="4/ubuntu@118.138.244.8,4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"


# Setup dirs
TESTS_FOLDER="${EXPERIMENT_DIR}Tests/"
RESULTS_FOLDER="${EXPERIMENT_DIR}Results/"
TESTS_DIRS=`for AGENT_NAME in ${AGENT_NAMES}; do echo ${TESTS_FOLDER}${AGENT_NAME}/; done`


# Backup code
mkdir -p ${TESTS_FOLDER}
tar -zcf "${TESTS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Setup parallelization
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`
WORKING_DIR=/tmp/tciaig_${EXPERIMENT_ID}
TAR_PATH=/tmp/code.tar.gz

# Cleanup
# parallel --nonall -S $WORKERS rm -rf $WORKING_DIR $TAR_PATH

# Setup
tar -czf "code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/
parallel --nonall -S ${WORKERS} rm -rf ${WORKING_DIR} ${TAR_PATH}
parallel scp code.tar.gz {}:/tmp ::: `echo ${WORKERS} | sed 's/,/ /g'`
parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${TAR_PATH} -C ${WORKING_DIR}
# List of experiments already processed
for AGENT_NAME in ${AGENT_NAMES}; do
    mkdir -p ${TESTS_FOLDER}${AGENT_NAME}
    touch ${TESTS_FOLDER}${AGENT_NAME}/skip.list
    (cd ${TESTS_FOLDER}${AGENT_NAME} && ls *.log) > ${TESTS_FOLDER}${AGENT_NAME}/skip.list 2> /dev/null
    parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}/${TESTS_FOLDER}${AGENT_NAME}
    parallel scp ${TESTS_FOLDER}${AGENT_NAME}/skip.list {}:${WORKING_DIR}/${TESTS_FOLDER}${AGENT_NAME} ::: `echo ${WORKERS} | sed 's/,/ /g'`
done

# Run tests with the different operators; highly parallelizable due to the high number of repetitions.
echo "Running tests"
parallel --header : mkdir -p {DIR} ::: DIR ${TESTS_DIRS}

# Do stuff
parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${WORKING_DIR} \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.log \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.agent \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.out \
  --xapply \
  --header : \
  --ungroup \
  mkdir -p ${TESTS_FOLDER}{AGENT_NAME} ';' \
  cat ${TESTS_FOLDER}{AGENT_NAME}/skip.list '|' grep -Fq exp{REP}.log '||' \
  python3 -u -O run_pacman_game.py \
    --layout ${LAYOUT} \
    --episodes ${EPISODES} \
    --random-seed {REP} \
    --controller "\"{'controller': 'QLearningAgent', 'explStrategy': 'AnnealingEpsilonGreedy', 'epsilon': 0.1, 'startDescent': 500, 'endDescent': 1500, 'features': "${FEATURES}", 'operator': '"{OPERATOR}"', 'operatorAlpha': "${OPERATOR_ALPHA}", 'saveAgent': '"${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.agent"'}\"" \
    --save-logs ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.log \
  "&>" ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.out \
  ::: REP $(for i in `seq ${REPETITIONS}`; do printf ${i}" %.0s" $(seq ${N_AGENTS} ) ; done) \
  ::: AGENT_NAME ${AGENT_NAMES} \
  ::: OPERATOR ${AGENT_NAMES}


echo "All done"
