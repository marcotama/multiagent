#!/usr/bin/env bash

LOGS_DIR='tmp/logs'
LAYOUT='smallClassic'
EPISODES=5000







python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs ${LOGS_DIR}/qvsas_vs_gq_2.log \
  --controller \
    '{"controller": "QValueSensitiveActionSelectionAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'qvsas_2.agent", "opponent_index": 1}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs qvsas)_2.agent"}' \
  &> "${LOGS_DIR}/qvsas_vs_gq_2.out" &

exit



python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs ${LOGS_DIR}/qvsas_vs_gq_1.log \
  --controller \
    '{"controller": "QValueSensitiveActionSelectionAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'qvsas_1.agent", "opponent_index": 1}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs qvsas)_1.agent"}' \
  &> "${LOGS_DIR}/qvsas_vs_gq_1.out" &

exit







STANDARD_TERMINATION_PROBABILITY=0.1
echo -e "
import pickle
from pacman_2p.options.pacman_options import GoToClosestCapsule, GoToClosestFood, GoToClosestScaredGhost, AvoidGhost
from util.translator import Translator
stateTranslator = Translator()
actionTranslator = Translator()
options = [
    GoToClosestCapsule(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
    GoToClosestFood(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
    GoToClosestScaredGhost(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
    AvoidGhost(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY})
]
with open('Subramanian.options','wb') as f:
    pickle.dump((options, stateTranslator, actionTranslator),f)
" | python3 \
|| exit 1

mkdir -p "${LOGS_DIR}"




python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs "${LOGS_DIR}/ar_vs_gq.log" \
  --controller \
    '{"controller": "AggressivePacman"}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs cr).agent"}' \
  &> "${LOGS_DIR}/ar_vs_gq.out" &


python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs "${LOGS_DIR}/cr_vs_gq.log" \
  --controller \
    '{"controller": "ConservativePacman"}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs ar).agent"}' \
  &> "${LOGS_DIR}/cr_vs_gq.out" &


python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs "${LOGS_DIR}/ql_vs_gq.log" \
  --controller \
    '{"controller": "QLearningAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'ql.agent"}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs ql).agent"}' \
  &> "${LOGS_DIR}/ql_vs_gq.out" &


python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs "${LOGS_DIR}/csas_vs_gq.log" \
  --controller \
    '{"controller": "ChallengeSensitiveActionSelectionAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'csas.agent", "evaluation_cycle": 5, "threshold": 40, "opponent_index": 1}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs csas).agent"}' \
  &> "${LOGS_DIR}/csas_vs_gq.out" &


python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs ${LOGS_DIR}/csaswo_vs_gq.log \
  --controller \
    '{"controller": "ChallengeSensitiveActionSelectionWithOptionsAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'csaswo.agent", "evaluation_cycle": 5, "threshold": 40, "add_options_from_file": "Subramanian.options", "opponent_index": 1}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs csaswo).agent"}' \
  &> "${LOGS_DIR}/csaswo_vs_gq.out" &


python3 -u -O run_pacman_2p_game.py \
  --random-seed 1 \
  --layout "${LAYOUT}" \
  --episodes "${EPISODES}" \
  --save-logs ${LOGS_DIR}/qvsas_vs_gq.log \
  --controller \
    '{"controller": "QValueSensitiveActionSelectionAgent", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.1, "gamma": 0.97, "exploration_strategy": "AnnealingEpsilonGreedy", "epsilon": 0.1, "annealing_variable": "episodes", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'qvsas.agent", "opponent_index": 1}' \
    '{"controller": "GQPacman", "features": [".FdDr",".FdDs:",".NScGhDr",".NScGhDs:",".CpDr",".CpDs:",".ScGhDr",".ScGhDs:"], "alpha": 0.003, "gamma": 0.97, "eta": 0.1, "lmbda": 0.4, "epsilon": 0.1, "hashing_algorithm": "UNH", "hashing_memory": 50, "n_tiles": 5, "n_tilings": 5, "projector": "tile_coder", "scales": "log", "start_descent": 500, "end_descent": 2000, "save_agent": "'${LOGS_DIR}/'gq(vs qvsas).agent"}' \
  &> "${LOGS_DIR}/qvsas_vs_gq.out" &
