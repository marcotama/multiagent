# -*- coding: utf-8 -*-
"""
Entry point to goal-oriented options.

@author: Marco Tamassia
"""
import argparse
import ast

from reinf.options.options_generator import OptionsGenerator
from util.util2 import pickle_load, pickle_save

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Learn options from saved games.')

    parser.add_argument(
        '--goals-file',
        dest="goalsFile",
        type=str,
        required=True,
        help='file with the extracted goals')
    parser.add_argument(
        '--model-file',
        dest="modelFile",
        type=str,
        required=True,
        help='file with the MDP model')
    parser.add_argument(
        '--output-file',
        dest='outputFile',
        type=str,
        help='file where to store the generated options'
    )
    parser.add_argument(
        '--save-log',
        dest='saveLog',
        type=str,
        default=False,
        help='file where to save the log'
    )
    parser.add_argument(
        '--how-many',
        dest='howMany',
        type=int,
        default=None,
        help='file where to save the log'
    )
    parser.add_argument(
        '--relevant-features',
        dest='relevantFeatures',
        type=str,
        required=True,
        help=
             'features to use for Q-learning agents (as a string representing a Python list); options are:\n'
             '  - pacman_coordinates\n'
             '  - [nearest-]<entity>-<mapping>, where entity can be any of:\n'
             '    - ghost\n'
             '    - scaredGhost\n'
             '    - nonscaredGhost\n'
             '    - capsule\n'
             '    - food\n'
             '    - escapeJunction (not supported yet)\n'
             '    - safeEscapeJunction\n'
             '    and mapping can be any of:\n'
             '    - euclideanDistance\n'
             '    - angularDirection\n'
             '    - coordinates\n'
             '    - shortestPathDistance\n'
             '    - shortestPathDirection\n'
    )
    parser.add_argument(
        '--threshold',
        type=float,
        default=0.1,
        help=('threshold for value iteration (max change between iterations ' +
              'before the algorithm terminates)'))
    parser.add_argument(
        '--reward-peak',
        dest='rewardPeak',
        type=float,
        default=50,
        help='peak in the reward function used in building options')
    parser.add_argument(
        '--default-termination-probability',
        dest='defaultTermProb',
        type=float,
        default=0.1,
        help='termination probability for states that are not goal states')
    parser.add_argument(
        '--use-sink-state',
        dest='useSinkState',
        action='store_true',
        help='whether or not to use a sink state in the model for options policy generation')

    def do_stuff():
        args = parser.parse_args()
        args.relevantFeatures = ast.literal_eval(args.relevantFeatures)

        agent, goalsCounters = pickle_load(args.goalsFile)  # type: PacmanQLearningAgent, List[Dict[T.StateCode, int]]

        og = OptionsGenerator(goalsCounters,
                              agent.qAgent,
                              args.relevantFeatures,
                              args.rewardPeak,
                              args.defaultTermProb,
                              args.threshold)
        options = og.get_top_n_options(args.howMany, args.useSinkState)
        stateTranslator = agent.qAgent.stateTranslator
        actionTranslator = agent.qAgent.actionTranslator
        pickle_save(args.outputFile, (options, stateTranslator, actionTranslator))
        if args.saveLog:
            og.print_to(args.saveLog)

    do_stuff()