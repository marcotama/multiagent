#!/usr/bin/env bash
FEATURES_NEW="FdDr,GhDr,GhDs,#Cp,ScGhDr,#ScGh"
FEATURES_OLD="nearest-food-shortestPathDirection,capsule-counter,nearest-nonscaredGhost-shortestPathDirection,nearest-nonscaredGhost-shortestPathDistance:2,nearest-scaredGhost-shortestPathDirection,scaredGhost-counter"
EPISODES=1
EXPLORATION_STRATEGY="EpsilonGreedy"

python3 -u -O run_pacman_game.py \
  --controller QLearningAgent \
  --exploration-strategy ${EXPLORATION_STRATEGY} \
  --episodes ${EPISODES} \
  --features "${FEATURES_NEW}" \
  --save-agent new.agent \
  --save-logs new.log \
&> new.out &

cd old_multiagent

python3 -O runExperiment.py \
  --controller QLearningAgent \
  --exploration-strategy ${EXPLORATION_STRATEGY} \
  --episodes ${EPISODES} \
  --features "${FEATURES_OLD}" \
  --save-agent ../old.agent \
  --save-logs ../old.log \
&> ../old.out &

cd ..

