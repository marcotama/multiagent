#!/usr/bin/env bash

# This script manages the execution of one or more configurations of a script, repeatedly, in a parallel
# fashion. Multiple executions are sent to remote workers to which is possible a non-interactive SSH access.

# Terminology:
#  - experiments class: the set of all the experiments, past and future, that test the same script; when you run this
#      script, you are adding one experiment to its class
#  - experiment: execution of a script, possibly with multiple configurations, possibly repeated multiple times for
#      statistical purposes
#  - configuration: all the parameters used to launch an algorithm; the script can be launched multiple times with
#      different random seeds to obtain different samples from the population of results (seed passed as a parameter)
#  - repetition: a single run of an algorithm with a specific configuration and a specific random seed

EXPERIMENT_CLASS='approx_q_parameters'

if [[ $# -ne 1 ]]
then
	TIMESTAMP=`date +%Y-%m-%d`
	EXPERIMENT_DIR="experiments/exp-${TIMESTAMP}__${EXPERIMENT_CLASS}"
else
	EXPERIMENT_DIR=$1
fi

[ -e "${EXPERIMENT_DIR}" ] || mkdir -p ${EXPERIMENT_DIR}
echo "Experiment folder: $EXPERIMENT_DIR"

EXPERIMENT_ID=`date +%Y-%m-%d__%H.%M.%S`



# Dynamic Role Assignment using General Value Functions
# lambda = 0.8 both in answer and question functions
# epsilon = 0.05
# alpha = 1/289, 1/449, 1/609
# eta = 0.001

# Toward Off-Policy Learning Control with Function Approximation
# gamma = 0.99
# alpha = 0.05
# beta = alpha * eta = 0.25 -> eta = 5

# Horde
# lambda = 0.4
# alpha = 0.3
# beta = alpha * eta = 0.00001 -> eta = 1/30000 = 0.0000333...

# ------ parameters ------
cat > configurations.json <<END_OF_JSON
{
    "repetitions": 100,
    "configurations": {
        "alpha": [0.003],
        "eta": [0.1],
        "hashing_memory": [0],
        "gamma": [0.97],
        "lmbda": [0.4],
        "layout": ["smallClassic"],
        "episodes": [3000],
        "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs"]],
        "start_descent": [500],
        "end_descent": [2500],
        "n_tiles": [5],
        "n_tilings": [1,2,3,5],
        "scales": ["log"]
    }
}
END_OF_JSON
# ------------------------
# "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs",".ScGhTm"]]
python gen_runs_parameters.py configurations.json



WORKERS_PARALLEL="4/ubuntu@118.138.244.8,4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"


# Setup dirs
REMOTE_WORKING_DIR="/tmp/${EXPERIMENT_CLASS}_${EXPERIMENT_ID}"
TESTS_FOLDER="${EXPERIMENT_DIR}/Tests"
TESTS_DIRS=`cat configurations.txt`
mkdir -p "${TESTS_FOLDER}/"


# Backup code
LOCAL_TAR_PATH="${TESTS_FOLDER}/code.tar.gz"
REMOTE_TAR_PATH="${REMOTE_WORKING_DIR}/code.tar.gz"
tar -zcf "${LOCAL_TAR_PATH}" \
    *.py \
    *.sh \
    *.txt \
    *HumanDemos*/ \
    bash_utils/ \
    http_server/ \
    layouts/ \
    pacman/ \
    pacman_2p/ \
    reinf/ \
    taxi/ \
    tests/ \
    uploads/ \
    util/ \
    windows_utils/


# Config parallelization
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`

# Remove remote working directory, if already existing
parallel --verbose --nonall -S ${WORKERS} rm -rf ${REMOTE_WORKING_DIR} ${REMOTE_TAR_PATH}
# Create remote working directory
parallel --verbose --nonall -S ${WORKERS} mkdir -p ${REMOTE_WORKING_DIR}
# Copy code remotely
parallel scp "${LOCAL_TAR_PATH}" {}:"${REMOTE_WORKING_DIR}" ::: `echo ${WORKERS} | sed 's/,/ /g'`
# Extract code on the remote worker
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${REMOTE_TAR_PATH} -C ${REMOTE_WORKING_DIR}
# Generate a list of configuration already processed, based on existing .log files
while read CONFIG_NAME; do
    mkdir -p "${TESTS_FOLDER}/${CONFIG_NAME}"
    (cd "${TESTS_FOLDER}/${CONFIG_NAME}" && ls *.log) > "${TESTS_FOLDER}/${CONFIG_NAME}/skip.list" 2> /dev/null
done < configurations.txt


echo "Running tests"

# Do stuff
parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${REMOTE_WORKING_DIR} \
  --return ${TESTS_FOLDER}/{config_id}/exp{repetition}.log \
  --return ${TESTS_FOLDER}/{config_id}/exp{repetition}.agent \
  --return ${TESTS_FOLDER}/{config_id}/exp{repetition}.out \
  --transferfile ${TESTS_FOLDER}/{config_id}/skip.list \
  --xapply \
  --header : \
  --colsep '\t' \
  --ungroup \
  cat ${TESTS_FOLDER}/{config_id}/skip.list '|' grep -Fq exp{repetition}.log '||' \
  python3 -u -O run_pacman_game.py \
    --layout {layout} \
    --episodes {episodes} \
    --random-seed {repetition} \
    --controller '\{\"controller\":\"LinearQLAgent\",\"gamma\":{gamma},\"alpha\":{alpha},\"eta\":{eta},\"lmbda\":{lmbda},\"start_descent\":{start_descent},\"end_descent\":{end_descent},\"n_tiles\":{n_tiles},\"n_tilings\":{n_tilings},\"scales\":\"{scales}\",\"hashing_algorithm\":\"UNH\",\"hashing_memory\":{hashing_memory},\"features\":{features},\"save_agent\":\"'${TESTS_FOLDER}'/{config_id}/exp{repetition}.agent\"\}' \
    --save-logs ${TESTS_FOLDER}/{config_id}/exp{repetition}.log \
  "&>" ${TESTS_FOLDER}/{config_id}/exp{repetition}.out \
  :::: runs.tsv


echo "All done"
