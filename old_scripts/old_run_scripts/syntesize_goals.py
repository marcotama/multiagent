# -*- coding: utf-8 -*-
"""
Entry point to learn goals from demos.

@author: Marco Tamassia
"""
import argparse
from typing import List, Tuple

from pacman.layout import getLayout
from reinf.options.goals_extractor import GoalsExtractor
from util.util2 import pickle_load, pickle_save
from pacman.agents.q_learning_agents import QLearningAgent
from pacman.demos_converter import PacmanDemosConverter
from pacman.game import Direction
from pacman.layout import Layout

if __name__ == '__main__':
    def do_stuff():

        parser = argparse.ArgumentParser(
            description='Learn options from saved games.')

        parser.add_argument(
            '--demos-files',
            dest="demosFiles",
            type=str,
            nargs='+',
            required=True,
            help='file with the demos')
        parser.add_argument(
            '--model-file',
            dest="modelFile",
            type=str,
            required=True,
            help='file with the model')
        parser.add_argument(
            '--output-file',
            dest='outputFile',
            type=str,
            default=False,
            help='file where to store extracted subgoals')
        parser.add_argument(
            '-l',
            '--layout',
            type=str,
            default='mediumClassic',
            help="the layout file from which to load the map layout")
        parser.add_argument(
            '--threshold',
            type=float,
            default=0.1,
            help='value iteration error threshold')
        parser.add_argument(
            '--reward-peak',
            dest='rewardPeak',
            type=float,
            default=50,
            help='reward for single-peaked reward function peaks')
        parser.add_argument(
            '-q',
            '--quiet',
            action='store_true',
            help='whether or not to suppress output')
        parser.add_argument(
            '--use-sink-state',
            dest='useSinkState',
            action='store_true',
            help='whether or not to use a sink state in the model for best action computation in goals discovery')

        args = parser.parse_args()
        args.layout = getLayout(args.layout)

        agent = pickle_load(args.modelFile)  # type: QLearningAgent
        demos = sum((pickle_load(demos_file) for demos_file in args.demosFiles), [])  # type: List[Tuple[Layout, List[Tuple[int, Direction]]]]

        demosConverter = PacmanDemosConverter(agent)
        traces = demosConverter.demosToMDPTraces(demos)

        goals_extractor = GoalsExtractor(agent.get_mdp(), args.threshold, args.rewardPeak, args.useSinkState)
        goals = goals_extractor.extractSubgoalsFromTraces(traces)

        pickle_save(args.outputFile, (agent, goals))
        if not args.quiet:
            print("Goals saved in %s." % args.outputFile)
            print()
            print(goals)

    do_stuff()