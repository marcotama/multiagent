#!/usr/bin/env bash

# Read params
EXPERIMENT_DIR="$1"
RELEVANT_FEATURES="$2"
HOW_MANY_OPTIONS="$3"
STANDARD_TERMINATION_PROBABILITY="$4"

# Setup dirs
OPTIONS_FOLDER="${EXPERIMENT_DIR}Options/"
GOALS_FOLDER="${EXPERIMENT_DIR}Goals/"
MODELS_FOLDER="${EXPERIMENT_DIR}Models/"

# Backup code
mkdir -p ${OPTIONS_FOLDER}
tar -zcf "${OPTIONS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Do stuff
echo "Generating options"
parallel \
  --xapply \
  --header : \
  python3 -O generate_options.py \
    --goals-file ${GOALS_FOLDER}{AGENT_NAME}.goals \
    --model-file learned.model \
    --relevant-features "\"${RELEVANT_FEATURES}\"" \
    --how-many ${HOW_MANY_OPTIONS} \
    --default-termination-probability ${STANDARD_TERMINATION_PROBABILITY} \
    --output-file ${OPTIONS_FOLDER}{AGENT_NAME}Options.options \
    --save-log ${OPTIONS_FOLDER}{AGENT_NAME}Options.info \
  "&>" ${OPTIONS_FOLDER}{AGENT_NAME}Options.log \
  ::: AGENT_NAME $( \ls -d ${GOALS_FOLDER}*.goals | xargs -L1 basename -s .goals )

# Create an empty options list
NO_OPTIONS_FILE="${EXPERIMENT_DIR}Options/NoOptions.options"
echo -e "
import pickle
from util.translator import Translator
with open('${NO_OPTIONS_FILE}','wb') as f:
    pickle.dump(([], Translator(), Translator()),f)
" | python3 > "${EXPERIMENT_DIR}Options/NoOptions.log"

SUBRAMANIAN_OPTIONS_FILE="${EXPERIMENT_DIR}Options/SubramanianOptions.options"
echo -e "
import pickle
from pacman.options.pacman_options import GoToClosestCapsule, GoToClosestFood, GoToClosestScaredGhost, AvoidGhost
from util.translator import Translator
stateTranslator = Translator()
actionTranslator = Translator()
with open('${SUBRAMANIAN_OPTIONS_FILE}','wb') as f:
    options = [
        GoToClosestCapsule(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
        GoToClosestFood(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
        GoToClosestScaredGhost(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY}),
        AvoidGhost(stateTranslator, actionTranslator, ${STANDARD_TERMINATION_PROBABILITY})
    ]
    pickle.dump((options, stateTranslator, actionTranslator),f)
" | python3 \
|| exit 1

exit

## Create a list with randomized options
#RANDOM_OPTIONS_FILE="${EXPERIMENT_DIR}Options/RandomOptions.options"
#echo -e "
#import pickle
#from reinf.option import RandomActionOption
#from util.translator import Translator
#with open('${RANDOM_OPTIONS_FILE}','wb') as f:
#    options = [RandomActionOption('_random~%d' % i, ${STANDARD_TERMINATION_PROBABILITY}) for i in range(5)]
#    pickle.dump((options, Translator(), Translator()),f)
#" | python3 > "${EXPERIMENT_DIR}Options/RandomOptions.log"
#
## Create a list with an option for each action, each mimicking an action
#CONSTANT_OPTIONS_FILE="${EXPERIMENT_DIR}Options/ConstantOptions.options"
#echo -e "
#import pickle
#from reinf.option import ConstantActionOption
#from util.translator import Translator
#with open('${CONSTANT_OPTIONS_FILE}','wb') as f:
#    options = [ConstantActionOption('_constant~%d' % i, i, ${STANDARD_TERMINATION_PROBABILITY}) for i in range(5)]
#    pickle.dump((options, Translator(), Translator()),f)
#" | python3 > "${EXPERIMENT_DIR}Options/ConstantOptions.log"