#!/usr/bin/env bash

# Read params
EXPERIMENT_DIR="$1"
EPISODES="$2"
REPETITIONS="$3"
FEATURES="$4"
WORKERS_PARALLEL="$5"
EXPERIMENT_ID="$6"
LAYOUT="$7"

# Setup dirs
TESTS_FOLDER="${EXPERIMENT_DIR}Tests/"
RESULTS_FOLDER="${EXPERIMENT_DIR}Results/"
OPTIONS_FOLDER="${EXPERIMENT_DIR}Options/"
OPTIONS_SETS=`ls ${OPTIONS_FOLDER}*.options | xargs -n 1 basename | sed s/.options//`
OPTIONS_FILES=`for a in $( (echo "${OPTIONS_SETS}") ); do echo ${OPTIONS_FOLDER}${a}; done`
TESTS_DIRS=`for a in $( (echo "${OPTIONS_SETS}") ); do echo ${TESTS_FOLDER}${a}/; done`
N_OPTIONS_SETS=`echo ${OPTIONS_SETS} | wc -w`


# Backup code
mkdir -p ${TESTS_FOLDER}
tar -zcf "${TESTS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Setup parallelization
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`
WORKING_DIR=/tmp/tciaig_${EXPERIMENT_ID}
TAR_PATH=/tmp/code.tar.gz

# Cleanup
# parallel --nonall -S $WORKERS rm -rf $WORKING_DIR $TAR_PATH

# Setup
tar -czf "code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/
parallel --nonall -S ${WORKERS} rm -rf ${WORKING_DIR} ${TAR_PATH}
parallel scp code.tar.gz {}:/tmp ::: `echo ${WORKERS} | sed 's/,/ /g'`
parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${TAR_PATH} -C ${WORKING_DIR}
parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}/${EXPERIMENT_DIR}Options
parallel scp ${OPTIONS_FOLDER}*.options {}:${WORKING_DIR}/${EXPERIMENT_DIR}Options ::: `echo ${WORKERS} | sed 's/,/ /g'`
# List of experiments already processed
for AGENT_NAME in $( \ls -d ${OPTIONS_FOLDER}*.options | xargs -L1 basename -s .options ); do
    mkdir -p ${TESTS_FOLDER}${AGENT_NAME}
    touch ${TESTS_FOLDER}${AGENT_NAME}/skip.list
    (cd ${TESTS_FOLDER}${AGENT_NAME} && ls *.log) > ${TESTS_FOLDER}${AGENT_NAME}/skip.list 2> /dev/null
    parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}/${TESTS_FOLDER}${AGENT_NAME}
    parallel scp ${TESTS_FOLDER}${AGENT_NAME}/skip.list {}:${WORKING_DIR}/${TESTS_FOLDER}${AGENT_NAME} ::: `echo ${WORKERS} | sed 's/,/ /g'`
done

# Run tests with the different sets of options and with no options; highly parallelizable due to the high number of repetitions.
echo "Running tests"
parallel --header : mkdir -p {DIR} ::: DIR ${TESTS_DIRS}

# Do stuff
parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${WORKING_DIR} \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.log \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.agent \
  --return ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.out \
  --xapply \
  --header : \
  --ungroup \
  mkdir -p ${TESTS_FOLDER}{AGENT_NAME} ';' \
  cat ${TESTS_FOLDER}{AGENT_NAME}/skip.list '|' grep -Fq exp{REP}.log '||' \
  python3 -u -O run_pacman_game.py \
    --layout ${LAYOUT} \
    --episodes ${EPISODES} \
    --random-seed {REP} \
    --controller "\"{'controller': 'QLearningAgent', 'explStrategy': 'AnnealingEpsilonGreedy', 'epsilon': 0.1, 'startDescent': 500, 'endDescent': 1500, 'features': "${FEATURES}", 'addOptionsFromFile': '"${OPTIONS_FOLDER}{AGENT_NAME}.options"', 'saveAgent': '"${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.agent"'}\"" \
    --save-logs ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.log \
  "&>" ${TESTS_FOLDER}{AGENT_NAME}/exp{REP}.out \
  ::: REP $(for i in `seq ${REPETITIONS}`; do printf ${i}" %.0s" $(seq $( \ls -d ${OPTIONS_FOLDER}*.options | wc -l ) ) ; done) \
  ::: AGENT_NAME $( \ls -d ${OPTIONS_FOLDER}*.options | xargs -L1 basename -s .options )
