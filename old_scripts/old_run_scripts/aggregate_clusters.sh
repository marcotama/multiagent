#!/usr/bin/env bash

#Read params
EXPERIMENT_DIR=$1
P_TRESHOLD=$2

#Setup dirs
AGENTS="ConservativePacman AggressivePacman"
ABSTRACTIONS_FOLDER="${EXPERIMENT_DIR}Abstractions/"
ABSTRACTIONS_FILES="${ABSTRACTIONS_FOLDER}ConservativeAgent ${ABSTRACTIONS_FOLDER}AggressiveAgent"
GOALS_FOLDER="${EXPERIMENT_DIR}Goals/"
GOALS_FILES="${GOALS_FOLDER}ConservativeAgent ${GOALS_FOLDER}AggressiveAgent"

#Backup code
mkdir -p ${ABSTRACTIONS_FOLDER}
tar -zcf "${ABSTRACTIONS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

#Do stuff
echo "Abstracting goals"
parallel \
  --xapply \
  --header : \
  python3 -O aggregate_clusters.py \
    --p-threshold ${P_TRESHOLD} \
    --goals-file {GOALS_FILE}.goals \
    --output-file {ABSTRACTIONS_FILE}.abstractions \
  "&>" {ABSTRACTIONS_FILE}.txt \
  ::: ABSTRACTIONS_FILE ${ABSTRACTIONS_FILES} \
  ::: GOALS_FILE ${GOALS_FILES}
