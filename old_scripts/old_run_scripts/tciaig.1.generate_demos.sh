#!/usr/bin/env bash

# Read params
EXPERIMENT_DIR="$1"
N_DEMOS="$2"
WORKERS_PARALLEL="$3"
AGENTS_FULL="$4"
AGENTS_SHORT="$5"
EXPERIMENT_ID="$6"
LAYOUT="$7"

# Setup dirs
DEMOS_FOLDER="${EXPERIMENT_DIR}Demos/"
DEMOS_DIRS=`for a in $( (echo "${AGENTS_SHORT}") ); do echo ${DEMOS_FOLDER}${a}; done`

# Backup code
mkdir -p ${DEMOS_FOLDER}
tar -zcf "${DEMOS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Setup parallelization
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`
WORKING_DIR=/tmp/tciaig_${EXPERIMENT_ID}
TAR_PATH=/tmp/code.tar.gz

# Setup
tar -czf "code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/
parallel --nonall -S ${WORKERS} rm -rf ${WORKING_DIR} ${TAR_PATH}
parallel scp code.tar.gz {}:/tmp ::: `echo ${WORKERS} | sed 's/,/ /g'`
parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${TAR_PATH} -C ${WORKING_DIR}

# Do stuff
echo "Generating $N_DEMOS games for each intelligence"
parallel --header : mkdir -p {DIR} ::: DIR ${DEMOS_DIRS}
parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${WORKING_DIR} \
  --return {DEMOS_DIR}/{AGENT}.demo \
  --return {DEMOS_DIR}/{AGENT}.out \
  --return {DEMOS_DIR}/{AGENT}.log \
  --xapply \
  --header : \
  --ungroup \
  mkdir -p {DEMOS_DIR} ';' \
  '[' -e {DEMOS_DIR}/{AGENT}.demo ']' '||' \
  python3 -u -O run_pacman_game.py \
    --layout ${LAYOUT} \
    --save-moves-histories {DEMOS_DIR}/{AGENT}.demo \
    --save-logs {DEMOS_DIR}/{AGENT}.log \
    --random-seed 0 \
    --episodes ${N_DEMOS} \
    --controller "{'controller': '"{AGENT}"'}" \
  '&>' {DEMOS_DIR}/{AGENT}.out \
  ::: AGENT ${AGENTS_FULL} \
  ::: DEMOS_DIR ${DEMOS_DIRS}
