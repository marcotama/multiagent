# -*- coding: utf-8 -*-
"""
Runs episodes of Q-learning in Taxi.

@author: Marco Tamassia
"""
import argparse
import os
import random
import sys
import inspect
from os import makedirs
from os.path import exists, dirname
from typing import List, Iterable

import numpy as np

from taxi.game import Layout
from taxi.agents.q_learning_agents import QLearningAgent, MultiQLearningAgent
from taxi.games_runner import GamesRunner
from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from util.util2 import pickle_load, pickle_save
import pkgutil
from importlib import import_module

explorationStrategies = ['EpsilonGreedy',
                         'EpsilonCIGreedy',
                         'UCT',
                         'AnnealingEpsilonGreedy',
                         'AnnealingEpsilonCIGreedy',
                         'Softmax',
                         'Greedy',
                         'Random']


def loadAgent(pacman, noGraphics):
    agentsModuleNames = [name for _, name, _ in pkgutil.iter_modules(['taxi/agents'])]
    for agentsModuleName in agentsModuleNames:
        if not agentsModuleName.endswith('gents'):
            continue
        agentsModule = import_module("taxi.agents." + agentsModuleName)
        if pacman in dir(agentsModule):
            if noGraphics and agentsModule == 'keyboard_agents.py':
                raise Exception('Using the keyboard requires graphics (not text display)')
            return getattr(agentsModule, pacman)
    raise Exception('The agent %s is not specified in any *Agents.py.' % pacman)


def runGames(args):
    # In this function, NamedTuples are created automatically by copying necessary fields from the variable settings
    settings = vars(args)
    settings['name'] = args.explStrategy

    if args.addOptionsFromFile == '':
        settings['options'], stateTranslator, actionTranslator = [], None, None
    else:
        settings['options'], stateTranslator, actionTranslator = pickle_load(args.addOptionsFromFile)

    # noinspection PyProtectedMember
    explParams = ExplorationParams(**{key: settings[key] for key in ExplorationParams._fields})
    if settings['loadAgent']:
        taxi = pickle_load(settings['loadAgent'])
        if not args.doNotOverrideExplorationStrategy:
            taxi.set_exploration_strategy(explParams)
    elif settings['controller'] == 'QLearningAgent':
        taxi = createTaxiQLearningAgent(
            featuresSets=settings['featuresSets'],
            features=settings['features'],
            explParams=explParams,
            alpha=settings['alpha'],
            gamma=settings['gamma'],
            init_q=settings['init_q'],
            operator=settings['operator'],
            operator_alpha=settings['operator_alpha'],
            options=settings['options'],
            early_termination=settings['early_termination']
        )
        if stateTranslator is not None:
            taxi.qAgent.stateTranslator = stateTranslator
        if actionTranslator is not None:
            taxi.qAgent.actionTranslator = actionTranslator
    else:

        cls = loadAgent(settings['controller'], True)
        params =  inspect.signature(cls.__init__).parameters
        taxi = cls(**{key: settings[key] for key in params if key not in ('self','index')})

    gamesRunner = GamesRunner(
        taxi=taxi,
        layout=settings['layout'],
        episodes=settings['episodes'],
        testGreedily=settings['testGreedily'],
        quiet=settings['quiet'],
        graphics=settings['graphics'],
        graphicsAfter=settings['graphicsAfter'],
        frameTime=settings['frameTime'])
    try:
        epsData, movesHistories = gamesRunner.test()
        if args.saveLogs:
            pickle_save(args.saveLogs, epsData)
        if args.saveMovesHistories:
            pickle_save(args.saveMovesHistories, movesHistories)
    finally:
        if args.saveAgent:
            try:
                gamesRunner.taxi.featuresExtractor.clear_cache()
            except NameError:
                pass
            pickle_save(args.saveAgent, gamesRunner.taxi)


def createTaxiQLearningAgent(
    features: List[str],
    featuresSets: List[List[str]],
    explParams: ExplorationParams,
    alpha: float,
    gamma: float,
    init_q: float,
    operator: str,
    operator_alpha: float,
    options: Iterable[Option],
    early_termination: bool,
):
    if featuresSets:
        taxi = MultiQLearningAgent(
            featuresSets=featuresSets,
            explParams=explParams,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )
    else:
        taxi = QLearningAgent(
            features=features,
            explParams=explParams,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )
    return taxi


def doStuff():
    default_features = ['ClDr','DsDr']

    parser = argparse.ArgumentParser(
        description='Run a multi-episode experiment')
    parser.add_argument(
        '--controller',
        type=str,
        help='games will be run with the taxi controlled by this agent')
    parser.add_argument(
        '--episodes',
        type=int,
        default=1,
        help='episodes to be run',)
    parser.add_argument(
        '--alpha',
        type=float,
        default=0.1,
        help='learning parameter for Q-learning')
    parser.add_argument(
        '-es',
        '--exploration-strategy',
        dest="explStrategy",
        type=str,
        default='EpsilonGreedy',
        choices=explorationStrategies,
        help='whether to show non-interactive games')
    parser.add_argument(
        '--test-greedily',
        dest="testGreedily",
        action='store_true',
        help='whether to test without exploration strategy (runs 2x episodes)')
    parser.add_argument(
        '--init-q-value',
        dest="initQ",
        type=float,
        default=1,
        help='initial value for Q-table entries')
    parser.add_argument(
        '--uct-coefficient',
        dest="cp",
        type=float,
        default=2,
        help='UCT exploration strategy coefficient')
    parser.add_argument(
        '--epsilon',
        type=float,
        default=0.1,
        help='exploration probability for epsilon-greedy strategy')
    parser.add_argument(
        '--epsilon-ci',
        dest="epsilonCI",
        type=float,
        default=0.05,
        help='CI exploration probability for epsilon-CI-greedy strategy')
    parser.add_argument(
        '--init-temperature',
        dest="initTemperature",
        type=float,
        default=0.05,
        help='initial temperature for Softmax strategy')
    parser.add_argument(
        '--annealing-variable',
        dest="annealingVariable",
        type=str,
        default='episodes',
        choices=['visits','episodes'],
        help='variable to consider for annealing schedules')
    parser.add_argument(
        '--start-descent',
        dest="startDescent",
        type=float,
        default=10.0,
        help=('qualitatively, where to start the descent of the sigmoid schedule' +
              'for strategies with annealing (i.e. AnnealingEpsilonGreedy, ' +
              'AnnealingCIEpsilonGreedy and Softmax)'))
    parser.add_argument(
        '--end-descent',
        dest="endDescent",
        type=float,
        default=20.0,
        help=('qualitatively, where to end the descent of the sigmoid schedule' +
              'for strategies with annealing (i.e. AnnealingEpsilonGreedy, ' +
              'AnnealingCIEpsilonGreedy and Softmax)'))
    parser.add_argument(
        '--quiet',
        action='store_true',
        help='whether to keep output to a minimum')
    parser.add_argument(
        '--gamma',
        type=float,
        default=0.97,
        help='discount factor for the MDPs')
    parser.add_argument(
        '--add-options',
        dest="addOptions",
        type=str,
        default="",
        help='options to be added to q-learner agents (comma separated)')
    parser.add_argument(
        '--add-options-from-file',
        dest="addOptionsFromFile",
        type=str,
        default="",
        help='file with options to be added to q-learner agents')
    parser.add_argument(
        '--options-early-termination',
        dest="earlyTerm",
        action='store_true',
        help='whether or not options should be terminated early if chosen ' +
             'action is not the best one')
    parser.add_argument(
        '--graphics',
        type=str,
        default='null',
        choices=['null', 'text', 'full'],
        help='whether to show non-interactive games')
    parser.add_argument(
        '--graphics-after',
        dest="graphicsAfter",
        type=int,
        default=0,
        help='if specified, this many episodes will be run without graphics')
    parser.add_argument(
        '--frame-time',
        dest="frameTime",
        type=float,
        default=0.1,
        help='time to delay between frames')
    parser.add_argument(
        '--load-agent',
        dest="loadAgent",
        type=str,
        default=False,
        help='file where to load the learning agent')
    parser.add_argument(
        '--save-agent',
        dest="saveAgent",
        type=str,
        default=False,
        help='file where to save the learning agent')
    parser.add_argument(
        '--random-seed',
        dest="randomSeed",
        type=int,
        default=None,
        help='seed for random functions')
    parser.add_argument(
        '--save-logs',
        dest="saveLogs",
        type=str,
        default=False,
        help='file where to save the logs of the games')
    parser.add_argument(
        '--do-not-run-if-log-file-exists',
        dest="doNotRunIfLogFileExists",
        action='store_true',
        help='if this is set and the file where to save the log exists, the ' +
             'program will return immediately, without executing')
    parser.add_argument(
        '-l',
        '--layout',
        type=str,
        default='mediumClassic',
        help=("the layout file from which to load the " +
              "map layout"))
    parser.add_argument(
        '--features',
        dest='featuresStr',
        type=str,
        default=','.join(default_features),
        help='features to use, comma separated; options are: '
              'pacman_coordinates, [nearest-]<entity>-<mapping>, where '
              'entity can be any of [ghost, scaredGhost, nonscaredGhost, '
              'capsule, food, escapeJunction, safeEscapeJunction] and '
              'mapping can be any of [euclideanDistance, '
              'angularDirection, coordinates, shortestPathDistance, '
              'shortestPathDirection].\n'
              'After each feature, it is possible to specify a base for a '
              'floor(log(feature,base)) mapping by separating it with a '
              'colon.\n'
              'Example: remainingFood:2,nearest-ghost-shortestPathDirection')
    parser.add_argument(
        '--features-sets',
        dest='featuresSetsStr',
        type=str,
        default='[]',
        help='features to use for multi-Q-Learning; semicolon separated '
             'sequences with the same format of the argument for '
             '--features; if "[]" is passed, normal Q-learning is used')
    parser.add_argument(
        '--significance',
        type=float,
        default=0.01,
        help='significance value for t-tests')
    parser.add_argument(
        '--do-not-override-exploration-strategy',
        dest='doNotOverrideExplorationStrategy',
        action='store_true',
        help="whether or not to override the exploration strategy of the "
             "loaded agent - if the agent is not loaded, this is ignored")
    parser.add_argument(
        '--save-moves-histories',
        dest='saveMovesHistories',
        type=str,
        default=None,
        help="where to save moves history; no history is save if argument is not provided")
              

    args = parser.parse_args()
    
    if args.randomSeed is not None:
        random.seed(args.randomSeed)
        np.random.seed(args.randomSeed)

    # Make necessary paths
    path = args.saveAgent
    if path and dirname(path) != '' and not exists(dirname(path)):
        makedirs(dirname(path))
    path = args.saveLogs
    if path and dirname(path) != '' and not exists(dirname(path)):
        makedirs(dirname(path))

    if args.saveLogs and args.doNotRunIfLogFileExists:
        if os.path.exists(args.saveLogs):
            sys.exit(0)

    if args.featuresStr == "[]":
        args.features = []
    else:
        args.features = args.featuresStr.split(',')
    if args.featuresSetsStr == "[]":
        args.featuresSets = []
    else:
        args.featuresSets = [fs.split(',') for fs in args.featuresSetsStr.split(';')]
    args.layout = Layout.get_default()
    runGames(args)

if __name__ == '__main__':
    doStuff()