#!/usr/bin/env bash

if [[ $# -ne 1 ]]
then
	TIMESTAMP=`date +%Y-%m-%d`
	EXPERIMENT_DIR="exp-${TIMESTAMP}__ecai/"
else
	EXPERIMENT_DIR=$1
fi
if [[ -e "${EXPERIMENT_DIR}" ]]; then
	echo "Using existing experiment folder: $EXPERIMENT_DIR"
else
	mkdir -p ${EXPERIMENT_DIR}
	echo "New experiment folder created: $EXPERIMENT_DIR"
fi

tar -zcf "${EXPERIMENT_DIR}/run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/

# Options testing parameters
EPISODES=30
REPETITIONS=5

TESTS_FOLDER="${EXPERIMENT_DIR}Tests/"
RESULTS_FOLDER="${EXPERIMENT_DIR}Results/"

# Variables for distributing experiments
WORKERS_PARALLEL="4/marco@131.170.27.93,16/ubuntu@131.170.250.69,4/ubuntu@118.138.244.8,4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`
EXPERIMENT_ID=`date +%Y-%m-%d__%H.%M.%S`
WORKING_DIR=/tmp/ecai_${EXPERIMENT_ID}
TAR_PATH=/tmp/code.tar.gz

echo "Remote working directory: ${WORKING_DIR}"

# Cleanup
# parallel --nonall -S $WORKERS rm -rf $WORKING_DIR $TAR_PATH

# Setup
tar -czf "code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/
parallel --nonall -S ${WORKERS} rm -rf ${WORKING_DIR} ${TAR_PATH}
parallel scp code.tar.gz {}:/tmp ::: `echo ${WORKERS} | sed 's/,/ /g'`
parallel --nonall -S ${WORKERS} mkdir -p ${WORKING_DIR}
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${TAR_PATH} -C ${WORKING_DIR}

# Run tests with the different sets of options and with no options; highly parallelizable due to the high number of repetitions.
echo "Running tests"
parallel --header : mkdir -p {DIR} ::: DIR ${TESTS_DIRS}

parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${WORKING_DIR} \
  --return {TEST_DIR}rep{REP}.log \
  --return {TEST_DIR}rep{REP}.agent \
  --return {TEST_DIR}rep{REP}.txt \
  --xapply \
  --header : \
  --ungroup \
  --retries 10 \
  mkdir -p {TEST_DIR} ';' \
  python3 -u -O run_pacman_game.py \
    --layout mediumClassic \
    --controller QLearningAgent \
    --episodes "$EPISODES" \
    --random-seed {REP} \
    --save-agent {TEST_DIR}rep{REP}.agent \
    --save-logs {TEST_DIR}rep{REP}.log \
    --exploration-strategy {EXPL_STRATEGY} \
    --epsilon {EPSILON} \
    --epsilon-ci {EPSILON_CI} \
    --significance {SIGNIFICANCE} \
    --init-temperature 100 \
    --annealing-variable "episodes" \
    --start-descent $((1 * $EPISODES / 3)) \
    --end-descent   $((2 * $EPISODES / 3)) \
    --features {FEATURES} \
    --features-sets {FEATURES_SETS} \
  '&>' {TEST_DIR}rep{REP}.txt \
  ::: TEST_DIR \
    "${TESTS_FOLDER}MultiQ-AEGCI(.05,.05)-sig(0.1)/" \
    "${TESTS_FOLDER}MultiQ-AEGCI(.05,.05)-sig(0.2)/" \
    "${TESTS_FOLDER}MultiQ-AEGCI(.05,.05)-sig(0.5)/" \
    "${TESTS_FOLDER}MultiQ-AEGCI(.05,.05)-sig(0.9)/" \
    "${TESTS_FOLDER}MultiQ-AEG(.1)/" \
    "${TESTS_FOLDER}SingleQ[Fd,Gh]-AEG(.1)/" \
    "${TESTS_FOLDER}SingleQ[Fd,Gh,ScGh]-AEG(.1)/" \
    "${TESTS_FOLDER}SingleQ[Fd,Gh,ScGh,Cp]-AEG(.1)/" \
    "${TESTS_FOLDER}SingleQ[Fd,Gh,ScGh,Cp]-AEGCI(.05,.05)/" \
  ::: FEATURES \
    "[]" \
    "[]" \
    "[]" \
    "[]" \
    "[]" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs" \
  ::: FEATURES_SETS \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs;FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs;FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs;FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs;FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs;FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs;FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs;FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs;FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    ".FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs,.CpDr,.CpDs;FdDr,.FdDs,.NScGhDr,.NScGhDs,.ScGhDr,.ScGhDs;FdDr,.FdDs,.NScGhDr,.NScGhDs" \
    "[]" \
    "[]" \
    "[]" \
    "[]" \
  ::: SIGNIFICANCE \
    "0.1" \
    "0.2" \
    "0.5" \
    "0.9" \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.0" \
  ::: EXPL_STRATEGY \
    "AnnealingEpsilonCIGreedy" \
    "AnnealingEpsilonCIGreedy" \
    "AnnealingEpsilonCIGreedy" \
    "AnnealingEpsilonCIGreedy" \
    "AnnealingEpsilonGreedy" \
    "AnnealingEpsilonGreedy" \
    "AnnealingEpsilonGreedy" \
    "AnnealingEpsilonGreedy" \
    "AnnealingEpsilonCIGreedy" \
  ::: EPSILON \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.1" \
    "0.1" \
    "0.1" \
    "0.1" \
    "0.0" \
  ::: EPSILON_CI \
    "0.1" \
    "0.1" \
    "0.1" \
    "0.1" \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.0" \
    "0.1" \
  ::: REP $(for i in `seq ${REPETITIONS}`; do printf ${i}" %.0s" $(seq 9) ; done)

echo "Nothing else to do"
