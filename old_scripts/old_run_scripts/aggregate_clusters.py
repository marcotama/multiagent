import argparse
from util.util2 import pickle_load, pickle_save
from pacman.agents.q_learning_agents import QLearningAgent
from typing import List, Dict
from util.typing2 import MyTypes as T
from reinf.options.goals_clusterer import ClustersDetector

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Analyzes goals and detects clusters.')

    parser.add_argument(
        '--goals-file',
        dest="goalsFile",
        type=str,
        required=True,
        help='file with the extracted goals')
    parser.add_argument(
        '--output-file',
        dest='outputFile',
        type=str,
        help='file where to store the generated abstractions')
    parser.add_argument(
        '--p-threshold',
        dest='pThreshold',
        type=float,
        default=0.05,
        help='Threshold for p-value of statistical tests used for clustering')
    parser.add_argument(
        '-q',
        '--quiet',
        action='store_true',
        help='whether or not to suppress output')

    args = parser.parse_args()
    agent, goalsCounters = pickle_load(args.goalsFile)  #type: QLearningAgent,List[Dict[T.StateCode, int]]
    cd = ClustersDetector(goalsCounters, agent.qAgent.stateTranslator, args.pThreshold, args.quiet)
    cd.aggregate()
    pickle_save(args.outputFile, cd)
    if not args.quiet:
        print(cd)
