#!/usr/bin/env bash

if [[ $# -ne 1 ]]
then
	TIMESTAMP=`date +%Y-%m-%d`
	EXPERIMENT_DIR="experiments/exp-${TIMESTAMP}__tciaig/"
else
	EXPERIMENT_DIR=$1
fi

[ -e "${EXPERIMENT_DIR}" ] || mkdir -p ${EXPERIMENT_DIR}
echo "Experiment folder: $EXPERIMENT_DIR"

EXPERIMENT_ID=`date +%Y-%m-%d__%H.%M.%S`

N_DEMOS=1000
FEATURES="['.FdDr','.FdDs:','.NScGhDr','.NScGhDs:','.CpDr','.CpDs:','.ScGhDr','.ScGhDs:']"
RELEVANT_FEATURES="['.FdDs:','.NScGhDs:','.CpDs:','.ScGhDs:']"
VALUE_ITERATION_THRESHOLD=0.5
HOW_MANY_OPTIONS=4
STANDARD_TERMINATION_PROBABILITY=0.2
EPISODES=3000
REPETITIONS=200
AGENTS_FULL="ConservativePacman AggressivePacman"
AGENTS_SHORT="Conservative Aggressive"
LAYOUT="mediumClassic"

WORKERS_PARALLEL="4/marco@131.170.27.93"
[ -e "${EXPERIMENT_DIR}Demos" ] \
&& echo "Demos already in place" \
|| bash tciaig.1.generate_demos.sh \
  "${EXPERIMENT_DIR}" \
  "${N_DEMOS}" \
  "${WORKERS_PARALLEL}" \
  "${AGENTS_FULL}" \
  "${AGENTS_SHORT}" \
  "${EXPERIMENT_ID}" \
  "${LAYOUT}" \
|| exit

[ -e "${EXPERIMENT_DIR}HumanDemos" ] || ln -s "$( pwd )/Pacman_HumanDemos_${LAYOUT}/" "${EXPERIMENT_DIR}HumanDemos"
[ -e "${EXPERIMENT_DIR}Models" ] \
&& echo "Models already in place" \
|| bash tciaig.2.train_mdp.sh \
  "${EXPERIMENT_DIR}" \
  "${FEATURES}" \
|| exit

[ -e "${EXPERIMENT_DIR}Goals" ] \
&& echo "Goals already in place" \
|| bash tciaig.3.syntesize_goals.sh \
  "${EXPERIMENT_DIR}" \
  "${VALUE_ITERATION_THRESHOLD}" \
|| exit

[ -e "${EXPERIMENT_DIR}Options" ] \
&& echo "Options already in place" \
|| bash tciaig.4.generate_options.sh \
  "${EXPERIMENT_DIR}" \
  "${RELEVANT_FEATURES}" \
  "${HOW_MANY_OPTIONS}" \
  "${STANDARD_TERMINATION_PROBABILITY}" \
|| exit

#WORKERS_PARALLEL="16/ubuntu@131.170.250.69,4/marco@131.170.27.93"
#WORKERS_PARALLEL="4/marco@131.170.27.93,16/ubuntu@131.170.250.69,4/ubuntu@118.138.244.8,4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"
WORKERS_PARALLEL="4/ubuntu@118.138.244.8,4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"

bash tciaig.5.test_options.sh \
"${EXPERIMENT_DIR}" \
"${EPISODES}" \
"${REPETITIONS}" \
"${FEATURES}" \
"${WORKERS_PARALLEL}" \
"${EXPERIMENT_ID}" \
 "${LAYOUT}" \
|| exit

echo "All done"
