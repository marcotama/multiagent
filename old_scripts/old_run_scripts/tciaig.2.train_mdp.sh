#!/usr/bin/env bash

#Read params
EXPERIMENT_DIR="$1"
FEATURES="$2"

#Setup dirs
MODELS_FOLDER="${EXPERIMENT_DIR}Models/"
DEMOS_FOLDER="${EXPERIMENT_DIR}Demos/"
HUMAN_DEMOS_FOLDER="${EXPERIMENT_DIR}HumanDemos/"

#Backup code
mkdir -p ${MODELS_FOLDER}
tar -zcf "${MODELS_FOLDER}run_with_this_code.tar.gz" *.py *.sh *.txt pacman/ reinf/ util/ layouts/


# All models are the same, trained using demos from all agents
echo "Training MDP from demos"
python3 -u -O train_mdp.py \
  --save-agent "${MODELS_FOLDER}learned.model" \
  --features "\"${FEATURES}\"" \
  --demo-files \
    ${HUMAN_DEMOS_FOLDER}*/*.demo \
    ${DEMOS_FOLDER}*/*.demo \
  -P -R -S -N -T \
&> "${MODELS_FOLDER}learned.log"
