import os
import pickle
import random
import tarfile
import tempfile
import time
import timeit
from contextlib import redirect_stdout

from taxi.data_structures import EpisodeData
from taxi.agents.keyboard_agents import KeyboardAgent
from taxi.game import TaxiGraphics, Layout, ClassicGameRules
from util.http import upload_file
from util.gui import yes_no_dialog, message_dialog

LOGS_HOST = 'http://118.138.244.72:8000'
PLAYER_ID_FILE = 'player.code'
PLAYER_DATA_FILE = 'taxi_games.data'
REQUIRED_GAMES = 50

def run_game(display, layout):
    rules = ClassicGameRules()
    taxi = KeyboardAgent()
    display.master.bind("<Down>", taxi.goNorth)
    display.master.bind("<Up>", taxi.goSouth)
    display.master.bind("<Right>", taxi.goEast)
    display.master.bind("<Left>", taxi.goWest)
    display.master.bind("u", taxi.pickup)
    display.master.bind("d", taxi.putdown)
    game = rules.newGame(layout, taxi, display, quiet=True)
    time.sleep(1.5)
    start = timeit.default_timer()
    game.run()
    stop = timeit.default_timer()
    total_time = stop - start

    data = EpisodeData(
        episode=1,
        outcomeStr='WON' if game.state.isOver() else 'CRASHED',
        outcome=1 if game.state.isOver() else 0,
        score=game.state.getScore(),
        movesCount=game.numMoves,
        statesCount=None,
        stateActionPairsCount=None,
        abstractionsUsage=None,
        executionTime=total_time
    )
    record = game.state.data.layout, game.moveHistory
    print("Episode #{episode}, {outcomeStr}, score: {score}, moves: {movesCount}".format(**data._asdict()))

    return data, record


def run_and_log_game(layout, moves_file_path, eps_file_path):
    display = TaxiGraphics()
    epData, movesHistory = run_game(display, layout)
    with open(eps_file_path, 'wb') as f:
        pickle.dump([epData], f)
    with open(moves_file_path, 'wb') as f:
        pickle.dump([movesHistory], f)
    return epData


def get_player_id():
    if os.path.isfile(PLAYER_ID_FILE):
        with open(PLAYER_ID_FILE, 'rb') as f:
            _, player_id = pickle.load(f)
    else:
        player_id = ''.join(random.choice('0123456789ABCDEF') for _ in range(16))
        with open(PLAYER_ID_FILE, 'wb') as f:
            pickle.dump(("Please, don't do this...", player_id), f)

    return player_id


def log_game(epData):
    if os.path.isfile(PLAYER_DATA_FILE):
        with open(PLAYER_DATA_FILE, 'rb') as f:
            epsData = pickle.load(f)
    else:
        epsData = []
    epsData.append(epData)
    with open(PLAYER_DATA_FILE, 'wb') as f:
        pickle.dump(epsData, f)


def get_stats():
    if os.path.isfile(PLAYER_DATA_FILE):
        with open(PLAYER_DATA_FILE, 'rb') as f:
            epsData = pickle.load(f)
        stats = {
            'games': len(epsData),
            'victories': sum(epData.outcome for epData in epsData),
            'avg_score': int(round(sum(epData.score for epData in epsData) / len(epsData)))
        }
    else:
        stats = {
            'games': 0,
            'victories': 0,
            'avg_score': 0
        }
    return stats


def run_and_log_and_upload_game():
    # Create temp files
    temp_files_path = {}
    temp_files_descriptor = {}
    for key in ['demo','log','out','tar']:
        descriptor, path = tempfile.mkstemp()
        temp_files_path[key] = path
        temp_files_descriptor[key] = descriptor

    # Run
    with open(temp_files_path['out'], 'w') as f:
        with redirect_stdout(f):
            epData = run_and_log_game(Layout.get_default(), temp_files_path['demo'], temp_files_path['log'])
            log_game(epData)

    # Upload files
    player_id = get_player_id()
    session_id = ''.join(random.choice('0123456789ABCDEF') for _ in range(16))
    arc_prefix = '/taxi/' + player_id + '/' + session_id
    with tarfile.open(temp_files_path['tar'], "w:gz") as tar:
        for key in ['demo','log','out']:
            tar.add(temp_files_path[key], arc_prefix+'.'+key)
    upload_file(
        url=LOGS_HOST,
        file_path=temp_files_path['tar'],
        file_name='taxi_' + player_id + '_' + session_id + '.tar.gz'
    )

    for key in ['demo','log','out','tar']:
        os.close(temp_files_descriptor[key])
        os.remove(temp_files_path[key])


QUERY_MESSAGE = """Upload succeeded!
Do you want to play another game?

Games played: {games}
Games won: {victories}
Average score: {avg_score}

"""
MORE_GAMES_MESSAGE = """It would be great if you could play a bit more (we can only use your data if you play 50 games)."""
ENOUGH_GAMES_MESSAGE = """You played all the games we needed, that's great!
But... if you want to play more, no one will judge you ;)"""
SURVEY_LINK = 'https://rmit.au1.qualtrics.com/SE/?SID=SV_5aJ4Vqy0yMHAYuN&player_id={player_id}'
THANKS_MESSAGE = """The final step is to take a 1 minute survey (click below to copy the link).

We really appreciate your time and hope you enjoyed science!"""
def do_record():
    survey_link = SURVEY_LINK.format(player_id=get_player_id())
    while True:
        run_and_log_and_upload_game()
        stats = get_stats()
        message = QUERY_MESSAGE.format(**stats)
        if stats['games'] < REQUIRED_GAMES:
            message += MORE_GAMES_MESSAGE.format(required_games=REQUIRED_GAMES)
        else:
            message += ENOUGH_GAMES_MESSAGE
        if not yes_no_dialog(message, 'Another game?'):
            message_dialog(THANKS_MESSAGE.format(survey_link=survey_link), "Thanks for playing!", survey_link, 'Copy URL')
            break

if __name__ == '__main__':
    do_record()