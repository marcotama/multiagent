# -*- mode: python -*-

block_cipher = None


a = Analysis(['record_pacman_game.py'],
             pathex=['/home/marco/Desktop/multiagent'],
             binaries=None,
             datas=None,
             hiddenimports=['pycurl'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='record_pacman_game',
          debug=False,
          strip=False,
          upx=True,
          console=False )
