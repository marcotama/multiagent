#!/usr/bin/env bash

PLAYER_ID="$1"
GAMES="$2"

LAYOUT="mediumClassic"
DEMOS_FOLDER="Pacman_HumanDemos_${LAYOUT}/${PLAYER_ID}"
SESSION_ID=`date +%s%N | md5sum | head -c 10`

[ -e "${DEMOS_FOLDER}" ] || mkdir -p ${DEMOS_FOLDER}

python3 -u -O run_pacman_game.py \
  --layout "${LAYOUT}" \
  --save-moves-histories "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.demo" \
  --save-logs "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.log" \
  --episodes "${GAMES}" \
  --graphics full \
  --frame-time 0.15 \
  --controller '\{\"controller\":\"KeyboardAgent\"\}' \
2>&1 | tee "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.out"