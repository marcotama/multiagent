#!/usr/bin/env bash

PLAYER_ID="$1"
GAMES="$2"

DEMOS_FOLDER="Taxi_HumanDemos/${PLAYER_ID}"
LAYOUT="mediumClassic"
SESSION_ID=`date +%s%N | md5sum | head -c 10`

[ -e "${DEMOS_FOLDER}" ] || mkdir -p ${DEMOS_FOLDER}

python3 -u -O run_taxi_game.py \
  --controller KeyboardAgent \
  --layout "${LAYOUT}" \
  --save-moves-histories "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.demo" \
  --save-logs "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.log" \
  --random-seed 0 \
  --episodes "${GAMES}" \
  --graphics full \
  --frame-time 0.05 \
2>&1 | tee "${DEMOS_FOLDER}/${PLAYER_ID}_${SESSION_ID}.out"