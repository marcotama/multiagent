#!/usr/bin/env bash

FORMAT="pdf"


mkdir -p "${1}Results/"
python3 -O analyze_single_player_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI[.05,.05]-sig[0.1]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.2]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.25]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.30]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.35]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.40]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.45]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.5]" \
    "MultiQ-AEGCI[.05,.05]-sig[0.9]" \
  -dn \
    "$\\alpha=0.10$" \
    "$\\alpha=0.20$" \
    "$\\alpha=0.25$" \
    "$\\alpha=0.30$" \
    "$\\alpha=0.35$" \
    "$\\alpha=0.40$" \
    "$\\alpha=0.45$" \
    "$\\alpha=0.50$" \
    "$\\alpha=0.90$" \

mkdir -p "${1}Results/plot_many_lines"
mv ${1}Results/*.${FORMAT} "${1}Results/plot_many_lines/"
exit


python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.05,.05)-sig(0.9)" \
    "MultiQ-AEG(.1)" \
    "SingleQ[Fd,Gh,ScGh,Cp]-AEGCI(.05,.05)" \
    "SingleQ[Fd,Gh,ScGh,Cp]-AEG(.1)" \
  -dn \
    "MQL with Ann. EpsilonCI-Greedy(.05,.05)" \
    "MQL with Ann. Epsilon-Greedy(.1)" \
    "QL with Ann. EpsilonCI-Greedy(.05,.05)" \
    "QL with Ann. Epsilon-Greedy(.1)" \
  -c \
    "darkgoldenrod" \
    "deepskyblue" \
    "red" \
    "dimgrey"

mkdir -p "${1}Results/plot6"
mv ${1}Results/*.${FORMAT} "${1}Results/plot6/"

exit


mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.05,.05)-sig(0.1)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.2)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.5)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.9)" \
  -dn \
    "$\\alpha=0.1$" \
    "$\\alpha=0.2$" \
    "$\\alpha=0.5$" \
    "$\\alpha=0.9$" \
  -c \
    "green" \
    "darkred" \
    "indigo" \
    "darkgoldenrod" \
  -is \
    "plot=scores,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1380,y2=1575,zf=4,loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=outcomes,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=0.84,y2=0.94,zf=4,loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1.05,y2=1.15,zf=3.0,loc=3,bbx=340,bby=100,loc1=2,loc2=4,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1.90,y2=2.00,zf=3.0,loc=3,bbx=340,bby=200,loc1=2,loc2=4,lw=2"

mkdir -p "${1}Results/plot4"
mv ${1}Results/*.${FORMAT} "${1}Results/plot4/"

python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "SingleQ[Fd,Gh]-AEG(.1)" \
    "SingleQ[Fd,Gh,ScGh]-AEG(.1)" \
    "SingleQ[Fd,Gh,ScGh,Cp]-AEG(.1)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.5)" \
  -dn \
    "QL with Fd,Gh" \
    "QL with Fd,Gh,ScGh" \
    "QL with Fd,Gh,ScGh,Cp" \
    "MQL with Fd,Gh[,ScGh[,Cp]]" \
  -c \
    "mediumblue" \
    "darkorange" \
    "lightseagreen" \
    "darkgoldenrod"

mkdir -p "${1}Results/plot5"
mv ${1}Results/*.${FORMAT} "${1}Results/plot5/"





mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.1,.1)-sig(0.1)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.2)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.5)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.9)" \
  -dn \
    "$\\alpha=0.1$" \
    "$\\alpha=0.2$" \
    "$\\alpha=0.5$" \
    "$\\alpha=0.9$" \
  -is \
    "plot=scores,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1440,y2=1600,zf=4,loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=outcomes,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=0.83,y2=0.93,zf=4,loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1.23,y2=1.31,zf=3.5,loc=3,bbx=390,bby=170,loc1=1,loc2=3,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=2.11,y2=2.19,zf=3.5,loc=3,bbx=390,bby=205,loc1=2,loc2=4,lw=2"


mkdir -p "${1}Results/plot1"
mv ${1}Results/*.${FORMAT} "${1}Results/plot1/"

python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "SingleQ[Fd,Gh]-AEG(.1)" \
    "SingleQ[Fd,Gh,ScGh]-AEG(.1)" \
    "SingleQ[Fd,Gh,ScGh,Cp]-AEG(.1)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.5)" \
  -dn \
    "QL with Fd,Gh" \
    "QL with Fd,Gh,ScGh" \
    "QL with Fd,Gh,ScGh,Cp" \
    "MQL with Fd,Gh[,ScGh[,Cp]]"

mkdir -p "${1}Results/plot2"
mv ${1}Results/*.${FORMAT} "${1}Results/plot2/"


python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.1,.1)-sig(0.5)" \
    "MultiQ-AEG(.1)" \
  -dn \
    "MQL with Ann. EpsilonCI-Greedy" \
    "MQL with Ann. Epsilon-Greedy"

mkdir -p "${1}Results/plot3"
mv ${1}Results/*.${FORMAT} "${1}Results/plot3/"


mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -r \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.05,.05)-sig(0.1)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.2)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.5)" \
    "MultiQ-AEGCI(.05,.05)-sig(0.9)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.1)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.2)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.5)" \
    "MultiQ-AEGCI(.1,.1)-sig(0.9)" \
  -dn \
    "\$\\epsilon=.05,\\alpha=0.1\$" \
    "\$\\epsilon=.05,\\alpha=0.2\$" \
    "\$\\epsilon=.05,\\alpha=0.5\$" \
    "\$\\epsilon=.05,\\alpha=0.9\$" \
    "\$\\epsilon=.1,\\alpha=0.1\$" \
    "\$\\epsilon=.1,\\alpha=0.2\$" \
    "\$\\epsilon=.1,\\alpha=0.5\$" \
    "\$\\epsilon=.1,\\alpha=0.9\$"

mkdir -p "${1}Results/plot0"
mv ${1}Results/*.${FORMAT} "${1}Results/plot0/"


mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  -ep "$2" \
  -ws "$3" \
  -pt "${FORMAT}" \
  -ot plots \
  -dg \
    "MultiQ-AEGCI(.0,.1)-sig(0.1)" \
    "MultiQ-AEGCI(.0,.1)-sig(0.2)" \
    "MultiQ-AEGCI(.0,.1)-sig(0.5)" \
    "MultiQ-AEGCI(.0,.1)-sig(0.9)" \
  -dn \
    "$\\alpha=0.1$" \
    "$\\alpha=0.2$" \
    "$\\alpha=0.5$" \
    "$\\alpha=0.9$" \
  -c \
    "green" \
    "darkred" \
    "indigo" \
    "darkgoldenrod" \
  -is \
    "plot=scores,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1270,y2=1420,zf=4,loc=3,bbx=400,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=outcomes,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=0.8,y2=0.92,zf=4,loc=3,bbx=380,bby=120,loc1=2,loc2=4,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=0.95,y2=1.10,zf=3.0,loc=3,bbx=340,bby=100,loc1=2,loc2=4,lw=2" \
    "plot=eaten_ghosts,x1="$(($2-2*$3-1000))",x2="$(($2-2*$3))",y1=1.32,y2=1.47,zf=3.0,loc=3,bbx=340,bby=200,loc1=2,loc2=4,lw=2"


mkdir -p "${1}Results/plot7"
mv ${1}Results/*.${FORMAT} "${1}Results/plot7/"