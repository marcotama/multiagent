#!/usr/bin/env bash

mkdir -p "${1}Results/"
python3 -O analyze_single_player_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  --limit-x "$2" \
  --min-episode 2700 \
  --max-episode 3000 \
  -ws "$3" \
  -pt PNG,PDF \
  -ot plots csv \
  -s \
  -dg \
    "bellman" \
    "consistent-bellman" \
    "advantage-learning" \
    "persistent-advantage-learning" \
    "lazy" \
  -dn \
    "Bellman op." \
    "Consistent Bellman op." \
    "Advantage Learning op." \
    "Persistent Advantage Learning op." \
    "Lazy op."
