#!/usr/bin/env bash

mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -d "${1}Demos/" \
     "${1}HumanDemos/" \
  -o "${1}Results/" \
  --limit-x "$2" \
  --min-episode 2700 \
  --max-episode 3000 \
  -ws "$3" \
  -pt PNG,PDF \
  -ot plots csv \
  -s \
  -dg \
    "NoOptions" \
    "AggressiveOptions" \
    "ConservativeOptions" \
    "MarcoOptions" \
    "WilliamOptions" \
    "SubramanianOptions" \
    "Aggressive" \
    "Conservative" \
    "Marco" \
    "William" \
  -dn \
    "QL standard" \
    "QL + aggr. options" \
    "QL + cons. options" \
    "QL + P1 options" \
    "QL + P2 options" \
    "QL + Subr. options" \
    "Aggressive" \
    "Conservative" \
    "P1" \
    "P2"
