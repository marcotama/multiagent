#!/usr/bin/env bash

mkdir -p "${1}Results/"
python3 -O analyze_single_player_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  --limit-x "$2" \
  --min-episode 1700 \
  --max-episode 2000 \
  --legend-args '{"loc": "upper center", "fontsize": "small", "ncol": 3, "bbox_to_anchor": (0.5, -0.1)}' \
  -ws "$3" \
  -pt PNG,PDF \
  -ot plots csv \
  -dg \
    n_tilings_is_1 \
    n_tilings_is_2 \
    n_tilings_is_3 \
    n_tilings_is_5 \
  -dn \
    n_tilings_is_1 \
    n_tilings_is_2 \
    n_tilings_is_3 \
    n_tilings_is_5 \




