#!/usr/bin/env bash

mkdir -p "${1}Results/"
python3 -O analyze_single_player_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  --limit-x "$2" \
  --min-episode 2700 \
  --max-episode 3000 \
  --legend-args '{"loc": "upper center", "fontsize": "small", "ncol": 3, "bbox_to_anchor": (0.5, -0.1)}' \
  -ws "$3" \
  -pt PNG,PDF \
  -ot plots csv \
  -dg \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp), Dr(.Fd .ScGh .NScGh .Cp)" \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp, .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(10)" \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp, .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(20)" \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp, .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(50)" \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp, .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(100)" \
    "Linear Q-learning with Ds:(.Fd .ScGh .NScGh .Cp, .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(200)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp), Dr(.Fd .ScGh .NScGh .Cp)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(10)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(20)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(50)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(100)" \
    "GQ with Ds:(.Fd .ScGh .NScGh .Cp .ScGhSpDs), Dr(.Fd .ScGh .NScGh .Cp), Tm(.ScGh) - hashing_memory(200)" \
    "Tabular Q-learning with Ds:(.Fd .ScGh .NScGh .Cp), Dr(.Fd .ScGh .NScGh .Cp)" \
  -dn \
    "LQL basic" \
    "LQL basic + spawn + timer  -  HM(10)" \
    "LQL basic + spawn + timer  -  HM(20)" \
    "LQL basic + spawn + timer  -  HM(50)" \
    "LQL basic + spawn + timer  -  HM(100)" \
    "LQL basic + spawn + timer  -  HM(200)" \
    "GQ basic" \
    "GQ basic + spawn + timer  -  HM(10)" \
    "GQ basic + spawn + timer  -  HM(20)" \
    "GQ basic + spawn + timer  -  HM(50)" \
    "GQ basic + spawn + timer  -  HM(100)" \
    "GQ basic + spawn + timer  -  HM(200)" \
    "QL basic" \




