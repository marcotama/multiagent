import sys
import argparse
from typing import Optional
from py4j.java_gateway import JavaGateway, GatewayParameters, CallbackServerParameters
from fighting_ice.agents.common import FightingController
from util.util2 import pickle_load, pickle_save
from util.loading import load_class


def fill_defaults (settings):
    import json
    with open('defaults.json', 'r') as f:
        defaults = json.load(f)

    # common defaults
    for parameter_name, default_value in defaults.items():
        if parameter_name not in settings:
            settings[parameter_name] = default_value

    # game-specific defaults
    if 'features' not in settings:
        settings['features'] = [
            "players_x_distance()",
            "opponent_is_attacking()"
        ]
    if 'q_table_features' not in settings:
        settings['q_table_features'] = [
            "players_x_distance()",
            "opponent_is_attacking()"
        ]
    return settings


def instantiate_controller(settings_str) -> FightingController:
    """
    Instantiate a controller according to the specified settings.
    :param settings_str: a string with the class name of the controller or encoding a Python dictionary literal.
    If the string represents a Python dictionary literal, its values will be used as settings for the controller.
    Settings include
    - 'controller': [mandatory] the class name of the controller
    - 'load_agent': [optional] a string with the path of a pickled instance of the class
    fighting_ice.agents.commonFightingController.
    - 'add_options_from_file': [optional] a string with the path of a pickled list of instances of the class
    reinf.options.option.Option.
    'do_not_override_exploration_strategy': [optional] a boolean indicating whether, after loading a controller with
    'load_from_file', the exploration strategy of such agent should be overridden. If the 'load_from_file' parameter
    is not specified, this is ignored.
    - any other entry will be passed to the constructor of the class of the agent. If the 'load_from_file' parameter
    is specified, all other entries are ignored.
    :return: an instance of the class fighting_ice.agents.commonFightingController.
    :raises ValueError if 'settings_str' encodes a Python literal that is not a dictionary.
    :raises FileNotFoundError if 'load_from_file' is specified but does not point to a file.
    :raises ModuleNotFoundError if 'load_from_file' is not specified and no module implementing the specified
    controller is found.
    """
    import inspect
    import ast
    from reinf.exploration_strategies import ExplorationParams

    try:
        settings = ast.literal_eval(settings_str)
        if not isinstance(settings, dict):
            raise ValueError("Settings for controller not understood: %s" % str(settings))
        settings = fill_defaults(settings)
    except ValueError:
        settings = {'controller': settings_str}

    # Load options
    if 'add_options_from_file' not in settings or settings['add_options_from_file'] == '':
        settings['options'] = []
    else:
        settings['options'] = pickle_load(settings['add_options_from_file'])

    # Exploration params
    expl_params_kwargs = {key: settings[key] if key in settings else None for key in ExplorationParams._fields}
    settings['exploration_params'] = ExplorationParams(**expl_params_kwargs)

    if 'load_from_file' in settings and settings['load_from_file']:
        # Load instance from file
        agent = pickle_load(settings['load_from_file'])
        if not isinstance(agent, FightingController):
            raise ValueError("The file indicated does not contain an instance of FightingController.")
        if hasattr(agent, 'set_exploration_strategy') and not settings['do_not_override_exploration_strategy']:
            agent.set_exploration_strategy(settings['exploration_params'])
    else:
        # Create new instance
        controller_class = load_class(settings['controller'], 'fighting_ice.agents') # type: Optional[FightingController]
        constructor_params = inspect.signature(controller_class.__init__).parameters
        agent = controller_class(**{key: settings[key] for key in constructor_params if key != 'self'})

    return agent


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Run a multi-episode experiment')
    parser.add_argument(
        '-n'
        '--number-of-games',
        dest='number_of_games',
        type=int,
        default=1,
        help='Number of games to play.')
    parser.add_argument(
        '-p1'
        '--player-1',
        dest='player_1',
        type=str,
        required=True,
        help="Settings for the AI that will control player 1."
             "- If the argument can be interpreted as a Python dictionary, the string associated with the key "
             "'controller' will be used as the name of the controller class, which will be looked up in all of the "
             "modules in the fighting_ice.agents package. All other entries in the dictionary will be passed to the "
             "agent if they are part of the signature of the __init__ method of the controller class."
             "- Otherwise, the argument will be interpreted as the name of a jar package (without the .jar part) "
             "in the engine's data/ai/ folder or the name of a class in one of the modules in the fighting_ice.agents "
             "package.")
    parser.add_argument(
        '-p2'
        '--player-2',
        dest='player_2',
        type=str,
        required=True,
        help="Settings for the AI that will control player 2. The argument should follow the same directions as for -p1.")
    parser.add_argument(
        '-c1'
        '--character-1',
        dest='character_1',
        type=str,
        choices=['ZEN', 'GARNET', 'LUD', 'KFM'],
        help="Name of the character played by player 1. This can be 'ZEN', 'GARNET', 'LUD' or 'KFM'.")
    parser.add_argument(
        '-c2'
        '--character-2',
        dest='character_2',
        type=str,
        choices=['ZEN', 'GARNET', 'LUD', 'KFM'],
        help="Name of the character played by player 2. This can be 'ZEN', 'GARNET', 'LUD' or 'KFM'.")
    parser.add_argument(
        '-p'
        '--port',
        dest='port',
        type=int,
        default=4242,
        help='Number of games to play.')
    parser.add_argument(
        '--save-controller-1',
        type=str,
        default=False,
        help='file where to save the controller of player 1')
    parser.add_argument(
        '--save-controller-2',
        type=str,
        default=False,
        help='file where to save the controller of player 2')
    parser.add_argument(
        '-log',
        action='store_true',
        help='enables logging on both Java and Python')
    return parser.parse_args()


class GamesRunner:
    def __init__(self):
        self.args = parse_args()

        # Initialize communication channel with Java
        self.gateway = JavaGateway(
            gateway_parameters=GatewayParameters(port=self.args.port),
            callback_server_parameters=CallbackServerParameters(port=0)
        )
        python_port = self.gateway.get_callback_server().get_listening_port()
        self.gateway.java_gateway_server.resetCallbackClient(self.gateway.java_gateway_server.getCallbackClient().getAddress(), python_port)
        self.manager = self.gateway.entry_point

        if self.args.log:
            import logging
            logger = logging.getLogger("py4j")
            logger.setLevel(logging.DEBUG)
            logger.addHandler(logging.StreamHandler())
            self.gateway.jvm.py4j.GatewayServer.turnLoggingOn()

        self.controller_1 = None
        self.controller_2 = None
        self.controller_1_name = None
        self.controller_2_name = None
        self.load_controllers()

        # Run the games
        try:
            print(self.controller_1)
            self.run_all_games()
        finally:
            if self.args.save_controller_1:
                pickle_save(self.args.save_controller_1, self.controller_1)
            if self.args.save_controller_2:
                pickle_save(self.args.save_controller_2, self.controller_2)

        # Close communication channel with Java
        self.close_gateway()


    def load_controllers(self):
        try:
            self.controller_1 = instantiate_controller(self.args.player_1)
            self.controller_1_name = type(self.controller_1).__name__ + "_1"
            self.controller_1.set_gateway(self.gateway)
        except (ValueError, FileNotFoundError, ModuleNotFoundError):
            import traceback
            traceback.print_exc()
            self.controller_1_name = self.args.player_1

        try:
            self.controller_2 = instantiate_controller(self.args.player_2)
            self.controller_2_name = type(self.controller_2).__name__ + "_2"
            self.controller_2.set_gateway(self.gateway)
        except (ValueError, FileNotFoundError, ModuleNotFoundError):
            self.controller_2_name = self.args.player_2


    def register_ais(self):
        if self.controller_1 is not None:
            self.manager.registerAI(self.controller_1_name, self.controller_1)
        if self.controller_2 is not None:
            self.manager.registerAI(self.controller_2_name, self.controller_2)


    def run_one_game(self):
        self.register_ais()
        game = self.manager.createGame(self.args.character_1, self.args.character_2,
                                       self.controller_1_name, self.controller_2_name)
        self.manager.runGame(game)

        sys.stdout.flush()

    def run_all_games(self):
        for i in range(self.args.number_of_games):
            print("Starting game %d" % (i + 1))
            self.run_one_game()
            print("Game %d finished" % (i + 1))

    def close_gateway(self):
        self.gateway.close_callback_server()
        self.gateway.close()

if __name__ == '__main__':
    GamesRunner()