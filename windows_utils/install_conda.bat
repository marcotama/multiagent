# Tentative script to setup conda, pyinstaller etc. on Windows (in RMIT)
# To be tested!


Miniconda3-latest-Windows-x86_64.exe /S

# Open a terminal
H:
cd Configuration\Desktop\multiagent\windows_utils
xcopy .condarc C:\Users\e15442\.condarc*
set PATH "%PATH%;C:\Users\e15442\AppData\Local\Continuum\Miniconda3\Scripts"
setx PATH "%PATH%;C:\Users\e15442\AppData\Local\Continuum\Miniconda3\Scripts"

# Open a new terminal
H:
cd Configuration\Desktop\multiagent
conda create --name py35 python=3.5 --yes
activate py35
conda install pywin32 --yes
pip install pefile pycurl PyInstaller setuptools==19.2 --proxy proxy.rmit.edu.au:8080 --ignore-installed
pyinstaller --noconsole -F record_pacman_game.py
pyinstaller --noconsole -F record_taxi_game.py
xcopy trusted-certs.crt dist\trusted-certs.crt*
