#!/usr/bin/env bash

archive="`date +"%Y-%m-%d"` - snapshot - multiagent.tar.gz"
tar -zcf "$archive" \
    .git/ \
    *.py \
    *.sh \
    *.txt \
    *HumanDemos*/ \
    bash_utils/ \
    http_server/ \
    layouts/ \
    pacman/ \
    pacman_2p/ \
    reinf/ \
    taxi/ \
    tests/ \
    uploads/ \
    util/ \
    windows_utils/ \

for d in ~/backup-destinations/*/ ; do
  echo "[BACKED UP] -> " ${d}
  mkdir -p "${d}multiagent/"
  cp "$archive" "${d}multiagent/"
done
rm "$archive"
