"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

policy.py

Created on: Jul 18, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https:#github.com/samindaa/RLLib
"""

from abc import abstractmethod
import random
import scipy.stats

from reinf.policy.common import Policy
from typing import List, Dict
from util.typing2 import MyTypes as T
from util.vector import Vector, DenseVector, Vectors, SparseVector
from util.math2 import Boundedness, Range

class ContinuousPolicy(Policy):
    """
    Abstract class for policies over continuous actions.
    """

    @abstractmethod
    def compute_grad_log(self, phis: Dict[T.ActionCode, T.StateActionAsVector], action: T.ActionCode) -> float: pass

    @abstractmethod
    def parameters(self) -> DenseVector: pass


class NormalDistribution(ContinuousPolicy):  # TODO rename NormallyDistributedContinuousPolicy

    def __init__(self, actions: List[T.ActionCode], init_mean: float, init_stddev: float, n_features: int):
        self.actions = actions
        self.initial_mean = init_mean
        self.initial_stddev = init_stddev
        self.sigma2 = 0.0
        self.mean = 0.0
        self.stddev = 0.0
        self.mean_step = 0.0
        self.stddev_step = 0.0
        self.u_mean = DenseVector(n_features)
        self.u_stddev = DenseVector(n_features)
        self.grad_mean = SparseVector(n_features)
        self.grad_stddev = SparseVector(n_features)
        self.x = SparseVector(n_features)
        self.multiu = Vectors()
        self.multigrad = Vectors()
        self.default_action = 0

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        # N(mu,var) for single action, single representation only
        assert len(phi) == 1 and len(self.actions) == 1
        self.x[:] = phi[self.actions[self.default_action]]
        self.mean = self.u_mean @ self.x + self.initial_mean
        self.stddev = DenseVector.exp(self.u_stddev @ self.x) * self.initial_stddev + 10e-8
        assert Boundedness.check_value(self.stddev)
        self.sigma2 = self.stddev * self.stddev
        assert Boundedness.check_value(self.sigma2)

    def _pi(self, a: T.ActionCode) -> float:
        return scipy.stats.norm(self.mean, self.stddev).pdf(0)

    def _sample_action(self) -> T.ActionCode:
        self.actions[self.default_action] = random.gauss() * self.stddev + self.mean
        return self.actions[self.default_action]

    def sample_best_action(self) -> T.ActionCode:
        return self._sample_action()

    def update_step(self, action: T.ActionCode):
        self.mean_step = (action - self.mean) / self.sigma2
        self.stddev_step = (action - self.mean) * (action - self.mean) / self.sigma2 - 1.0

    def compute_grad_log(self, phi: Dict[T.ActionCode, T.StateActionAsVector], action: T.ActionCode) -> Vector:
        assert len(phi) == 1 and len(self.actions) == 1
        self.update_step(action)
        self.x[:] = phi[self.actions[self.default_action]]
        self.grad_mean[:] = self.x * self.mean_step
        self.grad_stddev[:] = self.x * self.stddev_step
        return self.grad_mean, self.grad_stddev

    def parameters(self) -> Vector:
        return self.u_mean, self.u_stddev


class NormalDistributionScaled(NormalDistribution):

    def __init__(self, actions: List[T.ActionCode], init_mean: float, init_stddev: float, n_features: int):
        super().__init__(actions, init_mean, init_stddev, n_features)

    def update_step(self, action: T.ActionCode):
        self.mean_step = (action - self.mean)
        self.stddev_step = (action - self.mean) * (action - self.mean) - self.sigma2


class NormalDistributionSkewed(NormalDistribution):

    def __init__(self, actions: List[T.ActionCode], init_mean: float, init_stddev: float, n_features: int):
        super().__init__(actions, init_mean, init_stddev, n_features)

    def update_step(self, action: T.ActionCode):
        self.mean_step = (action - self.mean)
        self.stddev_step = (action - self.mean) * (action - self.mean) / self.sigma2 - 1.0


class ScaledPolicyDistribution(ContinuousPolicy):

    def __init__(self, actions: List[T.ActionCode], policy: ContinuousPolicy, policy_range: Range, problem_range: Range):
        self.actions = actions
        self.policy = policy
        self.policy_range = policy_range
        self.problem_range = problem_range
        self.a_t = T.ActionCode(0)
        self.a_t.append(0.0)

    # From (c, a, min(), max()) to (0, a', -1, 1)
    @staticmethod
    def _normalize(rng: Range, a: float) -> float:
        return (a - rng.center()) / (rng.length() / 2.0)

    # From (0, a', -1, 1) to (c, a, min(), max())
    @staticmethod
    def _scale(rng: Range, a: float) -> float:
        return (a * rng.length() / 2.0) + rng.center()

    def _problem_to_policy(self, problem_action: float) -> T.ActionCode:
        normalized_action = self._normalize(self.problem_range, problem_action)
        scaled_action = self._scale(self.policy_range, normalized_action)
        self.a_t[0] = scaled_action
        return self.a_t

    def _policy_to_problem(self, policy_action: float) -> T.ActionCode:
        normalized_action = self._normalize(self.policy_range, policy_action)
        scaled_action = self._scale(self.problem_range, normalized_action)
        self.a_t[0] = scaled_action
        return self.a_t

    def _update(self, phis: Dict[T.ActionCode, T.StateActionAsVector]):
        self.policy._update(phis)

    def _pi(self, a: T.ActionCode) -> float:
        return self.policy._pi(self._problem_to_policy(a))

    def _sample_action(self) -> T.ActionCode:
        self.actions[0] = self._policy_to_problem(self.policy._sample_action())
        return self.actions[0]

    def sample_best_action(self) -> T.ActionCode:
        return self._sample_action()

    def compute_grad_log(self, phis: Dict[T.ActionCode, T.StateActionAsVector], action: T.ActionCode) -> float:
        return self.policy.compute_grad_log(phis, self._problem_to_policy(action))

    def parameters(self) -> DenseVector:
        return self.policy.parameters()
