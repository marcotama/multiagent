"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

policy.py

Created on: Jul 18, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https:#github.com/samindaa/RLLib
"""
import math
import random

from reinf.policy.common import Policy
from reinf.policy.discrete import DiscreteActionPolicy
from reinf.policy.continuous import ContinuousPolicy
from typing import List, Dict
from util.typing2 import MyTypes as T
from util.vector import DenseVector, Vectors, SparseVector
from util.math2 import Boundedness, random_weighted


class StochasticPolicy(DiscreteActionPolicy):

    def __init__(self, actions: List[T.ActionCode]):
        super().__init__()
        self.random = random
        self.actions = actions
        self.distribution = DenseVector(len(actions))

    def _update(self, phis: Dict[T.ActionCode, T.StateActionAsVector]):
        pass

    def _pi(self, a: T.ActionCode) -> float:
        return self.distribution[a]

    def _sample_action(self) -> T.ActionCode:
        assert Boundedness.check_distribution(self.distribution)
        rand = random.random()
        tot = 0.0
        for a in self.actions:
            tot += self.distribution[a]
            if tot >= rand:
                return a
        return self.actions[len(self.actions) - 1]

    def sample_best_action(self) -> T.ActionCode:
        return self._sample_action()


class BoltzmannDistribution(StochasticPolicy, ContinuousPolicy):

    def __init__(self, actions, n_features):
        super().__init__(actions)
        self.avg = SparseVector(n_features)
        self.grad = SparseVector(n_features)
        self.u = DenseVector(n_features)
        self.multiu = Vectors()
        self.multigrad = Vectors()

        # Parameter setting
        self.multiu.append(self.u)
        self.multigrad.append(self.grad)

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        assert len(self.actions) == len(phi)
        self.distribution.clear()
        self.avg.clear()
        tot = 0.0
        # The exponential function may become very large and overflow.
        # Therefore, multiply top and bottom of the hypothesis by the same
        # constant without changing the output.
        max_value = 0.0
        for action in self.actions:
            tmp = self.u @ phi[action]
            if tmp > max_value:
                max_value = tmp

        assert Boundedness.check_value(max_value)
        for action in self.actions:
            a_id = action
            self.distribution[a_id] = DenseVector.exp(self.u @ phi[action]) - max_value
            assert Boundedness.check_value(self.distribution[a_id])
            tot += self.distribution[a_id]
            self.avg += self.distribution[a_id] * phi[action]

        self.distribution /= tot
        if __debug__:
            for action in self.actions:
                assert Boundedness.check_value(self.distribution[action])

        self.avg *= 1.0 / tot

    def compute_grad_log(self, phi: Dict[T.ActionCode, T.StateActionAsVector], action: T.ActionCode) -> DenseVector:
        self.grad[:] = phi[action] - self.avg
        return self.multigrad

    def parameters(self) -> DenseVector:
        return self.multiu

    def _pi(self, a: T.ActionCode) -> float:
        return self._pi(a)

    def _sample_action(self) -> T.ActionCode:
        return self._sample_action()

    def sample_best_action(self) -> T.ActionCode:
        return self.sample_best_action()

class BoltzmannDistributionPerturbed(Policy):

    def __init__(self, actions: List[T.ActionCode], u: DenseVector, epsilon: float, perturbation: float):
        self.random = random
        self.actions = actions
        self.u = u
        self.distribution = DenseVector(len(actions))
        self.epsilon = epsilon
        self.perturbation = perturbation

    def _update(self, phis: Dict[T.ActionCode, T.StateActionAsVector]):
        assert len(self.actions) == len(phis)
        self.distribution.clear()
        tot = 0.0
        # The exponential function may become very large and overflow.
        # Therefore, multiply top and bottom of the hypothesis by the same
        # constant without changing the output.
        max_value = max(self.u @ phis[a] for a in self.actions)

        for a in self.actions:
            a_id = a
            perturb = self.perturbation if random.random() < self.epsilon else 0.0
            self.distribution[a_id] = math.exp(self.u @ phis[a]) + perturb - max_value
            assert Boundedness.check_value(self.distribution[a_id])
            tot += self.distribution[a_id]

        self.distribution /= tot
        if __debug__:
            for a in self.actions:
                assert Boundedness.check_value(self.distribution[a])

    def _pi(self, action: T.ActionCode) -> float:
        return self.distribution[action]

    def _sample_action(self) -> T.ActionCode:
        return random_weighted(self.distribution)

    def sample_best_action(self) -> T.ActionCode:
        return self._sample_action()


