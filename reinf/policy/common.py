"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

policy.py

Created on: Jul 18, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https:#github.com/samindaa/RLLib
"""

from abc import abstractmethod

from typing import Dict, Optional, Union, Iterable
from util.typing2 import MyTypes as T
from reinf.predictor import Predictor


class Policy:

    def __init__(self, predictor: Predictor):
        self.predictor = predictor

    @abstractmethod
    def _update(self, phis: Dict[T.ActionCode, T.StateActionAsVector]):
        pass

    @abstractmethod
    def _pi(self, a: T.ActionCode) -> float:
        pass

    @abstractmethod
    def _sample_action(self) -> T.ActionCode:
        pass

    def pi (self, a: Union[Iterable[T.ActionCode],T.ActionCode], phis: Optional[Dict[T.ActionCode, T.StateActionAsVector]]) -> float:
        if phis is not None:
            self._update(phis)
        return self._pi(a)

    def pis (self, phis: Optional[Dict[T.ActionCode, T.StateActionAsVector]]) -> Dict[T.ActionCode, float]:
        if phis is not None:
            self._update(phis)
        return {_a: self._pi(_a) for _a in phis}

    def sample_action (self, phis: Optional[Dict[T.ActionCode, T.StateActionAsVector]]) -> T.ActionCode:
        if phis is not None:
            self._update(phis)
        return self._sample_action()

    def tick (self) -> None:
        pass