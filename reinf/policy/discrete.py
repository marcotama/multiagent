import math

from abc import abstractmethod
from operator import itemgetter
from typing import Dict

from reinf.policy.common import Policy
from reinf.predictor import Predictor
from util.math2 import random_weighted
from util.typing2 import MyTypes as T
from functools import partial


def linear_interpolation(x, x0, y0, x1, y1):
    return y0 + (x - x0) * (y1 - y0) / (x1 - x0)

def rectilinear_interpolation(x, x0, y0, x1, y1):
    if x < x0:
        return y0
    if x > x1:
        return y1
    return linear_interpolation(x, x0, y0, x1, y1)

def generate_rectilinear_schedule(x0, y0, x1, y1):
    return partial(rectilinear_interpolation, x0=x0,y0=y0,x1=x1,y1=y1)


class DiscreteActionPolicy(Policy):

    def __init__(self, predictor: Predictor):
        super().__init__(predictor)
        self.distribution = {}

    @abstractmethod
    def _update(self, phis: Dict[T.ActionCode, T.StateActionAsVector]):
        pass

    def _pi(self, a: T.ActionCode) -> float:
        return self.distribution[a] if a in self.distribution else 0.0

    def _sample_action(self) -> T.ActionCode:
        return random_weighted(self.distribution)


class SoftMax(DiscreteActionPolicy):
    """
    Policy that chooses an action with a probability that increases exponentially with its predicted value.
    """

    def __init__(self, predictor: Predictor, temperature: float=1.0):
        super().__init__(predictor)
        self.temperature = temperature

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        self.distribution = {}
        tot = 0.0
        # The exponential function may become very large and overflow.
        # Therefore, multiply top and bottom of the hypothesis by the same
        # constant without changing the output.
        q = {a: self.predictor.predict(phi[a]) for a in phi}
        max_value = max(q.values())

        for a in phi:
            self.distribution[a] = math.exp(q[a] - max_value) / self.temperature
            tot += self.distribution[a]

        for a in phi:
            self.distribution[a] /= tot


class RandomPolicy(DiscreteActionPolicy):
    """
    Policy choosing uniformly among the legal actions.
    """

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        self.distribution = {a: 1.0 / len(phi) for a in phi}


class RandomBiasPolicy(DiscreteActionPolicy):
    """
    A random policy that, with probability 50%, chooses the same action as before, if legal.
    If previous action is not legal or on the first query, chooses uniformly at random.
    """

    def __init__(self, predictor: Predictor):
        super().__init__(predictor)
        self.previous_action = None

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        if self.previous_action is None or self.previous_action not in phi:
            self.distribution = {a: 1.0 / len(phi) for a in phi}
        else:
            self.distribution = {a: 0.5 / (len(phi) - 1) for a in phi if a != self.previous_action}
            self.distribution[self.previous_action] = 0.5

    def _sample_action(self) -> T.ActionCode:
        choice = super()._sample_action()
        self.previous_action = choice
        return choice


class GreedyPolicy(DiscreteActionPolicy):
    """
    A policy that always chooses the best action
    """

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        q = {a: self.predictor.predict(phi[a]) for a in phi}
        best_action = max(q.items(), key=itemgetter(1))[0]
        self.distribution = {best_action: 1.0}


class EpsilonGreedyPolicy(DiscreteActionPolicy):
    """
    A policy choosing the (predicted) best action with probability 1 - epsilon, or uniformly at random with probability
    epsilon.
    """

    def __init__(self, predictor: Predictor, epsilon: float):
        super().__init__(predictor)
        self.epsilon = epsilon

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        q = {a: self.predictor.predict(phi[a]) for a in phi}
        best_action = max(q.items(), key=itemgetter(1))[0]
        self.distribution = {a: self.epsilon / len(phi) for a in phi}
        self.distribution[best_action] += (1 - self.epsilon)


class AnnealingEpsilonGreedyPolicy(DiscreteActionPolicy):
    """
    A policy choosing the (predicted) best action with probability 1 - epsilon, or uniformly at random with probability
    epsilon. Epsilon decreases over time with a rectilinear schedule: it is constant for the first <start_descent> ticks
    and starts decreasing linearly afterwards, reaching 0.0 after a total of <end_descent> ticks.
    """

    def __init__(self, predictor: Predictor, epsilon: float, start_descent: float, end_descent: float):
        super().__init__(predictor)
        self.epsilon = epsilon
        self.time = 0
        self.epsilon_schedule = generate_rectilinear_schedule(start_descent, epsilon, end_descent, 0.0)

    def _update(self, phi: Dict[T.ActionCode, T.StateActionAsVector]):
        q = {a: self.predictor.predict(phi[a]) for a in phi}
        best_action = max(q.items(), key=itemgetter(1))[0]
        self.distribution = {a: self.epsilon_schedule(self.time) / len(phi) for a in phi}
        self.distribution[best_action] += (1 - self.epsilon)

    def tick(self):
        self.time += 1
