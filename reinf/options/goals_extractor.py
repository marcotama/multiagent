# -*- coding: utf-8 -*-
"""
Algorithms to extract goals from demos.

@author: Marco Tamassia
"""
from collections import Counter
import numpy as np
from typing import List, AbstractSet, Tuple, Dict
from util.typing2 import MyTypes as T
from util.util2 import weightedChoicesNoRepetition
from functools import lru_cache, partial

from reinf.dynamic_programming import valueIteration, deriveQFromV
from reinf.mdp import MDP


def getQ(
    gamma: float,
    P: T.NumpyPTable,
    threshold: float,
    rPeak: float,
    useSinkState: bool,
    goals: AbstractSet[T.StateCode]
) -> T.NumpyQTable:
    """
    Computes the optimal state-action value function induced by a reward function (that is zero everywhere except in
    some specified goals states) and a model

    :param gamma: [float] discount factor for value iteration
    :param P: [ndarray (SxAxS)] transition probabilities
    :param threshold: [float] value iteration threshold
    :param rPeak: [float] reward peak for goals,
    :param useSinkState: [bool] whether or not to use a modified model with a sink state
    :param goals: [frozenset] states whose reward is not zero
    :return type: [ndarray (SxA)] the state-action value function
    """
    if useSinkState:
        # Make a copy of P with an additional sink state
        P_ = np.zeros((P.shape[0] + 1, P.shape[1], P.shape[2] + 1))
        P_[:-1,:,:-1] = P[:,:,:]
        P_[goals,:,:] = 0.0
        P_[goals,:,-1] = 1.0
        P_[-1,:,:] = 0.0
        P_[-1,:,-1] = 1.0
        # Create R with peaks and sink state
        R = np.zeros(P.shape[0] + 1)
    else:
        P_ = P
        R = np.zeros(P.shape[0])
    R[goals] = rPeak
    # Find Q
    V = valueIteration(R, P_, gamma, threshold)
    Q = deriveQFromV(V, P_)
    if useSinkState:
        return Q[:-1,:]
    else:
        return Q


def getQMemoizer(
    gamma: float,
    P: T.NumpyPTable,
    threshold: float,
    rPeak: float,
    useSinkState: bool
) -> T.NumpyQTable:
    """
    Computes the optimal state-action value function induced by a reward function (that is zero everywhere except in
    some specified goals states) and a model (as given, but modified so that goal states end up in a sink state)

    :param gamma: [float] discount factor for value iteration
    :param P: [ndarray (SxAxS)] transition probabilities
    :param threshold: [float] value iteration threshold
    :param rPeak: [float] reward peak for goals
    :param useSinkState: [bool] whether or not to use a modified model with a sink state
    :return type: [Callable[[AbstractSet[T.StateCode]],ndarray (SxA)] a function that computes the state-action value
        function with the specified parameters and using memoization
    """
    memoizer = lru_cache(maxsize=None)
    fun = partial(getQ, gamma, P, threshold, rPeak, useSinkState)
    return memoizer(fun)


class GoalsExtractor:
    def __init__(
        self,
        mdp: MDP,
        threshold: float,
        rPeak: float,
        useSinkState: bool
    ) -> None:
        """
        Initializes the object.
        Notice that transition probabilities are expected only for primitive
        actions.
        """
        self.qMemoizer = getQMemoizer(mdp.gamma, mdp.get_p_table_by_counts(), threshold, rPeak, useSinkState)

    def getOptimalAction(
        self,
        goals: AbstractSet[T.StateCode],
        s: T.StateCode
    ) -> T.Action:
        """
        Computes the optimal action in state s assuming a multi-peaked reward function with non-zeros in goal states.
        Uses memoization.

        :param goals: [frozenset] states whose reward is not zero
        :param s: [int] current state
        """
        Q = self.qMemoizer(goals)
        if s >= Q.shape[0]:
            return None
        action = Q[s, :].argmax()
        return action

    def _extractSubgoalsFromStateActionPairsSequence(
        self,
        sap_seq: List[Tuple[T.StateCode, T.ActionCode]],
        count: str='uses'
    ) -> List[T.StateCode]:
        """
        Given a demonstration, extracts subgoals.
        """
        cg = next(reversed(sap_seq))[0]  # Current goal
        ps = None  # Previous state
        subgoals = [cg]
        for s, a in reversed(sap_seq):
            ba = self.getOptimalAction(cg, s)  # Best action
            if ba != a:
                cg = s
                if count == 'discoveries':
                    subgoals.append(ps)
            if count == 'uses':
                subgoals.append(ps)
            ps = s
        subgoals = [g for g in reversed(subgoals) if g is not None]
        return subgoals

    def _extractSubgoalsFromStateActionPairsSequences(
        self,
        sap_seqs: List[List[Tuple[T.StateCode, T.ActionCode]]]
    ) -> Dict[T.StateCode, int]:
        goals = Counter()
        for sap_seq in sap_seqs:
            goals.update(self._extractSubgoalsFromStateActionPairsSequence(sap_seq))
        return goals

    def extractSubgoalsFromTraces(
        self,
        traces: List[T.Trace]
    ) -> Dict[T.StateCode, int]:
        goals = Counter()
        for trace in traces:
            sap_seq = [(s,a) for s, _, a, _ in trace if s is not None]
            goals.update(self._extractSubgoalsFromStateActionPairsSequence(sap_seq))
        return goals

    @staticmethod
    def stochasticallySelectGoals(
            goals: List[T.State],
        topN: int
    ) -> List[T.State]:
        goals = Counter(goals)
        topN = min(topN, len(goals))
        topGoals = weightedChoicesNoRepetition(goals, topN)
        return topGoals