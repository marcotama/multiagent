# -*- coding: utf-8 -*-
"""
Classes to aggregate goals and create goal-oriented options.

@author: Marco Tamassia
"""
from collections import defaultdict
from typing import List, Optional, Iterable, Dict, Tuple

import numpy as np
from tabulate import tabulate

from reinf.dynamic_programming import valueIteration, deriveQFromV
from reinf.q_learning import QLearningAgent
from reinf.options.option import NumpyfiedQOption
from util.translator import Aggregator
from util.typing2 import MyTypes as T


class OptionsGenerator:
    def __init__(
        self,
        goalsCounter: Dict[T.StateCode, int],
        agent: QLearningAgent,
        relevantFeatures: List[str],
        rewardPeak: float,
        defaultTermProb: float,
        threshold: float
    ) -> None:
        self.states = {}
        self.goalsCounter = goalsCounter
        self.gamma = agent.mdp.gamma
        self.rewardPeak = rewardPeak
        self.defaultTermProb = defaultTermProb
        self.threshold = threshold
        self.P = agent.mdp.numpify_p_table()
        self.aggregator = Aggregator(relevantFeatures,
                                     agent.state_translator)
        self.sorted_agg_goals = None
        self.agg_goals_counter = None
        self.options, self.counts, self.agents = [], [], []

    def build_options(
        self,
        limit: Optional[int],
        use_sink_state: bool
    ) -> Tuple[List[NumpyfiedQOption], List[int], List[int]]:
        agg_goals_counter = defaultdict(int)
        for g, c in self.goalsCounter.items():
            aggregator = self.aggregator.aggregate(g)
            agg_goals_counter[aggregator] += c
        nS = self.P.shape[0]
        nA = self.P.shape[1]

        options = [] # type: List[NumpyfiedQOption]
        counts  = [] # type: List[int]
        agg_goals = [] # type: List[int]

        # Choose aggregate goals
        self.sorted_agg_goals = sorted(agg_goals_counter, key=lambda x: agg_goals_counter[x], reverse=True) # type: List[int]
        if limit is None:
            sorted_agg_goals = self.sorted_agg_goals
        else:
            limit = min(limit, len(self.sorted_agg_goals))
            sorted_agg_goals = self.sorted_agg_goals[:limit]

        for i, agg_goal in enumerate(sorted_agg_goals):

            goals = self.aggregator.reverse(agg_goal)
            # Make a copy of P with an additional sink state
            if use_sink_state:
                P_ = np.zeros((nS+1, nA, nS+1))
                P_[:-1,:,:-1] = self.P[:,:,:]
                P_[goals,:,:] = 0.0
                P_[goals,:,-1] = 1.0
                P_[-1,:,:] = 0.0
                P_[-1,:,-1] = 1.0
                # Create R with peaks and sink state
                R = np.zeros(nS+1)
            else:
                P_ = self.P
                R = np.zeros(nS)
            R[goals] = self.rewardPeak
            # Find Q
            V = valueIteration(R, P_, self.gamma, self.threshold)
            Q = deriveQFromV(V, P_)
            # Termination probabilities
            TP = np.ones(nS)
            sources = self.get_sources(goals)
            TP[sources] = self.defaultTermProb
            TP[goals] = 1.0
            # Build option
            if use_sink_state:
                o = NumpyfiedQOption('~learned%d' % i, Q[:-1,:], TP)
            else:
                o = NumpyfiedQOption('~learned%d' % i, Q, TP)
            options.append(o)
            counts.append(agg_goals_counter[agg_goal])
            agg_goals.append(agg_goal)
        self.agg_goals_counter = agg_goals_counter
        return options, counts, agg_goals

    def get_all_options(
        self,
        useSinkState: bool
    ) -> List[NumpyfiedQOption]:
        self.options, self.counts, self.agents = self.build_options(limit=False, use_sink_state=useSinkState)
        return self.options

    def get_top_n_options(
        self,
        n: int,
        useSinkState: bool
    ) -> List[NumpyfiedQOption]:
        self.options, self.counts, self.agents = self.build_options(limit=n, use_sink_state=useSinkState)
        return self.options

    def get_aggregate_sources(self, ag):
        peaks = self.aggregator.reverse(ag)
        sources = self._get_one_step_sources(peaks)
        aggSources = [self.aggregator.aggregate(s) for s in sources]
        return set(aggSources)

    def get_sources(
        self,
        dest: Iterable[T.StateCode]
    ) -> List[T.StateCode]:
        oldLen = -1
        sources = set(dest)
        while len(sources) - oldLen > 0:
            oldLen = len(sources)
            sources |= self._get_one_step_sources(sources)
        return list(sources)

    def _get_one_step_sources(self, dest: Iterable[T.StateCode]):
        sources = np.where(self.P[:, :, list(dest)].sum(axis=1) > 0)[0]
        return set(sources)

    def print_to(self, file: str, n: Optional[int]=None) -> None:
        if n is None:
            top = self.sorted_agg_goals
        else:
            n = min(n, len(self.sorted_agg_goals))
            top = self.sorted_agg_goals[:n]
        sortedRelevantFeatures = sorted(self.aggregator.relevantFeatures)
        dicts = {ag: self.aggregator.aggrTranslator.getDict(ag) for ag in top}
        featDicts = {ag: [d[f] for f in sortedRelevantFeatures]
                     for ag, d in dicts.items()}
        rows = [featDicts[ag] + [self.agg_goals_counter[ag]] for ag in top]
        headers = sortedRelevantFeatures + ["Counter"]
        tbGoals = tabulate(rows, headers, tablefmt="grid")
        with open(file, 'w') as f:
            print("Top (aggregated) goals:", file=f)
            print(file=f)
            print(tbGoals, file=f)

            for o, ag in zip(self.options, self.agents):
                print(file=f)
                print(file=f)
                print("Info for option %s with goal %s" %
                      (o.name, dicts[ag]), file=f)
                print("Agg. states that can reach it:", file=f)
                aggSources = sorted(self.get_aggregate_sources(ag))
                print("\n".join([str(aggS) for aggS in aggSources]), file=f)
                print(file=f)
                print(str(o), file=f)