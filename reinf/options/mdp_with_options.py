from reinf.mdp import MDP
from typing import Optional, List, Iterable, Callable, Union, Dict, Tuple, Set
from reinf.options.option import Option
from reinf.exploration_strategies import ExplorationStrategy, sampleMax
from collections import defaultdict
from util.util2 import defaultdict2, defaultdict3
from functools import partial
from copy import copy
from util.typing2 import HashedState, Action, OptionName
from tabulate import tabulate


class MDPWithOptions(MDP):
    def __init__(
        self,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
        disable_primitive_actions: bool
    ) -> None:
        """
        Initializes the MDP.
        """
        super().__init__(
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha
        )
        self.options = {o.name: o for o in options} # type: Dict[str, Option]
        self.stack = OptionsStack(getattr(self, 'option_terminated'), self.options, gamma)
        self.early_termination = early_termination
        self.disable_primitive_actions = disable_primitive_actions
        self.qOTable = defaultdict2(partial(float, self.init_q))  # type: Dict[HashedState, Dict[OptionName, float]]
        self.pOTable = defaultdict3(float)  # type: Dict[HashedState, Dict[OptionName, Dict[HashedState, float]]]
        self.nOTable = defaultdict2(int)  # type: Dict[HashedState, Dict[OptionName, int]]
        self.qOStats = defaultdict(partial(copy, (0, 0.0, 0.0)))  # type: Dict[Tuple[HashedState, OptionName], Tuple[float, float, float]]

    def get_q_and_n_tables(
        self
    ) -> Tuple[Dict[HashedState, Dict[Union[Action, OptionName], float]], Dict[HashedState, Dict[Union[Action, OptionName], int]]]:
        """
        Returns the Q- and N-tables, which include actions and options.
        """
        qTable = self.qTable.copy()  # type: Dict[HashedState, Dict[Union[Action, OptionName], float]]
        nTable = self.nTable.copy()  # type: Dict[HashedState, Dict[Union[Action, OptionName], int]]
        for s in self.qOTable:
            for o in self.qOTable[s]:
                qTable[s][o] = self.qOTable[s][o]
                nTable[s][o] = self.nOTable[s][o]
        return qTable, nTable

    def get_q_row(
        self,
        hashed_state: HashedState,
        legal_actions: Iterable[Union[Action, OptionName]]
    ) -> Dict[Union[Action, OptionName], float]:
        """
        Returns the Q-table row for the given state, which include
        actions and options.
        :param hashed_state: state code
        :param legal_actions: legal actions
        """
        qRow = {}
        for a in legal_actions:
            qRow[a] = self.qTable[hashed_state][a]
        for o in self.options:
            qRow[o] = self.qOTable[hashed_state][o]
        return qRow

    def get_n_row(
        self,
        hashed_state: HashedState,
        legal_actions: Iterable[Union[Action, OptionName]]
    ) -> Dict[Union[Action, OptionName], int]:
        """
        Returns the N-table row for the given state, which include
        actions and options.
        :param hashed_state: state code
        :param legal_actions: legal actions
        """
        nRow = {}
        for a in legal_actions:
            nRow[a] = self.nTable[hashed_state][a]
        for o in self.options:
            nRow[o] = self.nOTable[hashed_state][o]
        return nRow

    def get_q_stats_row(
            self,
            hashed_state: HashedState,
            legal_state: Iterable[Union[Action, OptionName]]
    ) -> Dict[Union[Action, OptionName], Tuple[int, float, float]]:
        """
        Returns the N-table row for the given state, which include
        actions and options.
        :param hashed_state: state code
        :param legal_state: legal actions
        """
        qStatsRow = {}
        for a in legal_state:
            qStatsRow[a] = self.qStats[(hashed_state, a)]
        for o in self.options:
            qStatsRow[o] = self.qOStats[(hashed_state, o)]
        return qStatsRow

    def get_non_initiable_options(
            self,
            state: HashedState
    ) -> Set[OptionName]:
        """
        Returns a set containing the non-initiable options.
        :param state: state code
        """
        nonInitiable = [n for n, o in self.options.items()
                        if not o.isInitiable(state)]
        return set(nonInitiable)

    def choose_action(
            self,
            hashed_state: HashedState,
            exploration_strategy: ExplorationStrategy,
            legal_actions: Iterable[Action]
    ) -> Action:
        """
        Returns an action according to the exploration strategy. If
        la is None, all _known_ actions will be considered.
        :param hashed_state: state code
        :param exploration_strategy: exploration strategy
        :param legal_actions: legal actions
        """
        lao = set(self.options) - self.get_non_initiable_options(hashed_state)
        if not self.disable_primitive_actions:
            lao |= set(legal_actions)

        # Iteratively terminate options until any or none remains active
        self.stack.stochasticTermination(hashed_state, lao)

        if self.stack.isEmpty():
            qRow = self.get_q_row(hashed_state, lao)
            nRow = self.get_n_row(hashed_state, lao)
            qStatsRow = self.get_q_stats_row(hashed_state, lao)
            chosen = self.choose_by_q_row(exploration_strategy, qRow, nRow, qStatsRow)
            self.stack.push(chosen, hashed_state, lao)
        lao |= set(legal_actions)

        while self.stack.isTopAnOption():
            top = self.stack.getTop()
            active = set(self.stack.getActiveOptions())
            _lao = lao - active
            chosen = top.getAction(hashed_state, _lao)
            self.stack.push(chosen, hashed_state, _lao)
        chosen = self.stack.pop(hashed_state, lao)

        if self.early_termination:
            Q, initQ = self.qTable, self.init_q
            maxQ = max([Q[hashed_state][a] if a in Q[hashed_state] else initQ for a in legal_actions])
            if maxQ > Q[hashed_state][chosen]:
                self.stack.terminateAll(hashed_state, lao)
                chosen = sampleMax({a: Q[hashed_state][a] if a in Q[hashed_state] else initQ for a in legal_actions})

        return chosen


    def _count_option(
            self,
            hashed_state: HashedState,
            option_name: OptionName
    ) -> None:
        """
        Updates internal counters that keep track of knowledge statistics.
        """
        self.nOTable[hashed_state][option_name] += 1

    def learn(
        self,
        hashed_state: HashedState,
        legal_actions: Iterable[Action],
        action: Action,
        reward: float,
        hashed_new_state: Optional[HashedState],
        new_legal_actions: Iterable[Action],
        own_decision: bool=True
    ) -> None:
        """
        Updates internal data structures in light of the information acquired.
        :param hashed_state: state code
        :param legal_actions: legal actions code (for state s)
        :param action: action code
        :param reward: reward
        :param hashed_new_state: new state code
        :param new_legal_actions: legal actions code (for state ns)
        :param own_decision: whether the decision was made by this agent (has implications when using options)
        """
        # Update usual data structures
        super().learn(hashed_state, legal_actions, action, reward, hashed_new_state, new_legal_actions)
        if own_decision:
            # Update cumulative rewards for options on the stack
            self.stack.addReward(reward)
            # If there is no next state, terminate all options on the stack
            if hashed_new_state is None:
                self.stack.terminateAll()

    def option_terminated(
        self,
        hashed_state: HashedState,
        legal_actions_and_options: Iterable[Union[Action, OptionName]],
        options: OptionName,
        reward: float,
        new_state: Optional[HashedState],
        new_legal_actions_and_options: Optional[Iterable[Union[Action, OptionName]]],
        option_steps: int=1
    ) -> None:
        """
        Updates internal data structures in light of the information acquired.
        :param hashed_state: state code
        :param legal_actions_and_options: legal actions and options code (for state s)
        :param options: option code
        :param reward: reward
        :param new_state: new state code
        :param new_legal_actions_and_options: legal actions and options code (for state ns)
        :param option_steps: number of steps
        """
        # Update statistics
        self._count_option(hashed_state, options)
        # Update Q-table
        self._updateQOTable(hashed_state, legal_actions_and_options, options, reward, new_state, new_legal_actions_and_options, option_steps)
        # Update Q-statistics
        self._updateQOStatistics(hashed_state, options)
        # Update model
        self._updatePOTable(hashed_state, options, new_state, option_steps)

    def _updateQOStatistics(
        self,
        hashed_state: HashedState,
        option_name: OptionName
    ) -> None:
        key = (hashed_state, option_name)
        if key not in self.qOStats:
            self.qOStats[key] = (0, 0.0, 0.0)
        n, m, v = self.qOStats[key]
        q = self.qOTable[hashed_state][option_name]
        n += 1
        e = q - m
        m += e / n
        if n > 1:
            v = (v * (n - 2) + e * (q - m)) / (n - 1)
        else:
            v = 0.0
        self.qOStats[key] = (n, m, v)

    def _updateQOTable(
        self,
        hashed_state: HashedState,
        legal_actions_and_options: Iterable[Union[Action, OptionName]],
        option_name: OptionName,
        reward: float,
        new_state: Optional[HashedState],
        new_legal_actions_and_options: Optional[Iterable[Union[Action, OptionName]]],
        option_steps: int=1
    ) -> None:
        """
        Updates the Q-table using Q-learning.
        """
        QO = self.qOTable
        q = QO[hashed_state][option_name]
        if new_state is None:
            new_q = reward
        else:
            max_next_q = max(self.get_q_row(new_state, new_legal_actions_and_options).values())
            tq = reward + self.gamma ** option_steps * max_next_q
            if self.bellman_operator:
                new_q = tq
            elif self.consistent_bellman_operator:
                # if new state is the same, use the same action instead of the best
                new_q = tq if new_state != hashed_state else q
            elif self.advantage_learning_operator:
                # propagate current action gap
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions_and_options).values())
                new_q = tq - self.operator_alpha * (max_cur_q - q)
            elif self.persistent_advantage_learning_operator:
                # propagate current action gap unless repeating this action in next state is a good choice
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions_and_options).values())
                new_q = max(tq - self.operator_alpha * (max_cur_q - q),
                            reward + self.gamma * QO[new_state][option_name])
            elif self.lazy_operator:
                # only update Q if it would change the policy
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions_and_options).values())
                new_q = q if q <= tq <= self.operator_alpha * max_cur_q + (1 - self.operator_alpha) * q else tq
            else:
                raise Exception("No operator selected")  # cannot happen

        QO[hashed_state][option_name] += self.alpha * (new_q - q)

    def _updatePOTable(
        self,
        hashed_state: HashedState,
        option_name: OptionName,
        new_state: Optional[HashedState],
        option_steps: int=1
    ) -> None:
        """
        Updates transition probabilities model.
        """

        P = self.pOTable
        alpha = self.alpha
        gamma = self.gamma
        # Update transition probabilities model
        if new_state not in P[hashed_state][option_name]:
            P[hashed_state][option_name][new_state] = 0
        for x in P[hashed_state][option_name]:
            delta = int(x == new_state)
            P[hashed_state][option_name][x] += alpha * (gamma ** option_steps * delta - P[hashed_state][option_name][x])
        # No normalization because this is not just a probability but, rather,
        # the probability times gamma ** k, so to keep track also of how much
        # the transition is delayed with respect to gamma. No need to keep
        # these values separated-ly since they would be multiplied together in
        # Bellman equations (and the likes) anyway.

    def __str__(self) -> str:
        result = "Number of states: %d\n" % len(self.states)
        result += "Number of actions: %d\n" % len(self.actions)
        actions = sorted(self.actions)
        options = sorted(self.options)
        rows = []
        Q, N = self.qTable, self.nTable
        QO, NO = self.qOTable, self.nOTable
        for s in self.qTable:
            if s is None:
                continue
            row = [str(s)]
            for a in actions:
                row.append("%+.2f (%d)" % (Q[s][a], N[s][a]))
            for o in options:
                row.append("%+.2f (%d)" % (QO[s][o], NO[s][o]))
            if len(self.qTable[s]) == 0:
                row.append('-')
            else:
                qRow = self.get_q_row(s, actions + options)
                row.append(max(qRow, key=qRow.get))
            rows.append(row)
        result += "\n\nQ (N) + Greedy policy:\n"
        result += tabulate(rows, headers=['State'] + actions + options + ['Best action'])
        return result


class OptionsStack:
    """
    Implement a stack of active options. Handles cumulative reward calculation
    and options termination.
    """
    def __init__(
        self,
        updateFunction: Callable[
            [
                HashedState,
                OptionName,
                float,
                Optional[HashedState],
                Optional[Iterable[Union[Action, OptionName]]],
                int
            ], None],
        options: Dict[str, Option],
        gamma: float
    ) -> None:
        self.update_function = updateFunction
        self.options = options
        self.gamma = gamma
        self.stack = []  # List[Union[Action, OptionName]]

    def push(
        self,
        action_or_option_name: Union[Action, OptionName],
        hashed_state: HashedState,
        lao: Iterable[Union[Action, OptionName]]
    ) -> None:
        entry = OptionsStackEntry(action_or_option_name, hashed_state, lao, self.gamma)
        self.stack.append(entry)

    def pop(
        self,
        new_hashed_state: Optional[HashedState]=None,
        new_actions_and_options: Optional[Iterable[Union[Action, OptionName]]]=None
    ) -> Union[Action, OptionName]:
        top = self.stack.pop()
        if new_actions_and_options is None:
            new_actions_and_options = []
        if top.name in self.options:
            init_state = top.initState
            init_legal_actions_and_options = top.initLegalActions
            option_steps = top.steps
            cumulative_reward = top.cumReward
            option_name = top.name
            self.update_function(init_state, init_legal_actions_and_options, option_name, cumulative_reward,
                                 new_hashed_state, new_actions_and_options, option_steps)
        return top.name

    def isTopAnOption(self) -> bool:
        return self.stack[-1].name in self.options

    def getTop(self) -> Option:
        return self.options[self.stack[-1].name]

    def stochasticallyTerminateTop(self, hashed_state: HashedState) -> bool:
        if len(self.stack) > 0:
            top = self.getTop()
            return self.options[top.name].stochasticallyTerminate(hashed_state)
        else:
            return False

    def stochasticTermination(
            self,
            hashed_state: Optional[HashedState]=None,
            legal_actions_and_options: Optional[Iterable[Union[Action, OptionName]]]=None
    ) -> None:
        while self.stochasticallyTerminateTop(hashed_state):
            self.pop(hashed_state, legal_actions_and_options)  # Handles option termination

    def terminateAll(
        self,
        hashed_state: Optional[HashedState]=None,
        legal_actions_and_options: Optional[Iterable[Union[Action, OptionName]]]=None
    ) -> None:
        while len(self.stack) > 0:
            self.pop(hashed_state, legal_actions_and_options)

    def addReward(
        self,
        reward: float
    ) -> None:
        for entry in self.stack:
            entry.addReward(reward)

    def isActive(self, option_name: OptionName) -> bool:
        return option_name in [entry.name for entry in self.stack]

    def getActiveOptions(self) -> List[OptionName]:
        """
        Returns the set of names of options that are in stack.
        """
        active = [entry.name for entry in self.stack]
        return active

    def isEmpty(self) -> bool:
        return len(self.stack) == 0


class OptionsStackEntry:
    def __init__(
        self,
        name: OptionName,
        init_hashed_state: HashedState,
        init_legal_actions: Iterable[Union[Action, OptionName]],
        gamma: float
    ) -> None:
        self.name = name
        self.initState = init_hashed_state
        self.initLegalActions = init_legal_actions
        self.cumReward = 0.0
        self.steps = 0
        self.gamma = gamma

    def addReward(
        self,
        reward: float
    ) -> None:
        self.cumReward += reward * self.gamma ** self.steps
        self.steps += 1
