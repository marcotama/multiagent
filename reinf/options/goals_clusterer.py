# -*- coding: utf-8 -*-
"""
Detects clusters using ANOVA test.

@author: Marco Tamassia
"""
from itertools import combinations
from typing import Dict, List, Set, Iterable

import numpy as np
from util.typing2 import MyTypes as T
from util.util2 import matchTuples
from scipy.stats import friedmanchisquare
from tabulate import tabulate

from util.translator import Translator


class ClustersDetector:
    def __init__(
        self,
        samples: List[Dict[T.StateCode, int]],
        translator: Translator,
        pThreshold: float,
        quiet: bool
    ):
        self.samples = samples
        self.origTranslator = translator
        self.pThreshold = pThreshold
        self.tuplesToRepr = {}
        self.quiet = quiet
        states = self.origTranslator.codeToDict.values()
        self.features = set(next(iter(states)).keys())

    def __str__(
        self
    ):
        representatives = set()
        for r in self.tuplesToRepr.values():
            representatives.add(r)
        result = ''
        for r in representatives:
            rows = []
            for t, tr in sorted(self.tuplesToRepr.items(), key=lambda x: repr(x)):
                if r == tr:
                    rows.append([v for f, v in sorted(t, key=lambda x: x[0])])
            if len(rows) > 1:
                result += "\n".join([str(t) for t in r]) + "\n"
                result += tabulate(rows)
                result += "\n\n\n"
        return result
            

    def aggregate(
        self
    ):
        states = self.origTranslator.codeToDict.values()
        tuples = [tuple(sorted(s.items())) for s in states]
        
        # Init dictionary mapping states (tuples of features) -> representatives
        for st in tuples:
            self.tuplesToRepr[st] = st
        # Init list of states to check
        toCheck = tuples
        # While some states need to be checked, keep trying aggregations
        while len(toCheck) > 0:
            toCheck = self.aggregateStates(toCheck)

    def aggregateStates(
        self,
        tuplesToCheck: Set[T.FeatureValuesTuple]
    ):
        toCheckAgain = set()
        # For each state to be checked
        for tup in tuplesToCheck:
            # Try aggregations by eliminating one feature
            # Only the best aggregation - in terms of p-value - is returned
            subtup = self.aggregateStateWithStatTest(tup)
            # If an aggregation is successful, mark the tuple for re-check
            if subtup is not None:
                toCheckAgain.add(subtup)
        return toCheckAgain


    def printJoining(self, matches, subtup, pValue=None, dist=None):
        if not self.quiet:
            print("JOINING %d tuples" % len(matches))
            rows = []
            for f in sorted(self.features):
                features = [f for f, _ in subtup]
                values = [v for _, v in subtup]
                if f in features:
                    i = features.index(f)
                    v = values[i]
                    rows.append([f, v])
                else:
                    rows.append([f, '*'])
            print(tabulate(rows))
            if pValue is not None:
                print("p-value: %f" % pValue)
            if dist is not None:
                print("dist: %f" % dist)
            print("---")
                
    def aggregateStateWithStatTest(
        self,
        tup
    ):
        info = []
        # For each feature in the (abstracted) state
        for f, _ in tup:
            # Compute the abstraction resulting from removing f
            subtup = tuple(((tf, tv) for tf, tv in tup if tf != f))
            # Find all the abstracted states that match subtup
            matches = matchTuples(subtup, self.tuplesToRepr.values())
            # Compute the likelihood of the cluster
            p = self.clusterLikelihood(matches)
            # Save the results - the one with the highest likelihood is kept, if any
            info.append((p, subtup, matches))
        # Base case - if the state was so abstracted it was empty, return None
        if len(info) == 0:
            return None
        # Find the best feature to abstract (lowest p-value)
        iMax = info.index(min(info, key=lambda i: i[0]))
        p, subtup, matches = info[iMax]
#        if len(matches) > 5:
#            print(p)
        if len(matches) > 1 and p < self.pThreshold:
            for m in matches:
                self.tuplesToRepr[m] = subtup
            self.printJoining(matches, subtup, pValue=p)
            return subtup
        else:
            return None

    def clusterLikelihood(
        self,
        elements: Iterable[T.FeatureValuesTuple]
    ):
        """
        Computes the probability that saying the given elements do not form a
        cluster is wrong. In other words, returns the p-value of the
        statistical test used to check the existence of the cluster.
        :param elements: elements to be evaluated
        """
        hashToCode = self.origTranslator.hashToCode
        codes = [hashToCode[e] for e in elements if e in hashToCode]
        samples = [[s[i] for s in self.samples] for i in codes]
        samples = [np.array(s) for s in samples if any([v > 0 for v in s])]
#        if len(samples) > 5:
#            print(len(samples), [np.mean(s) > 0.1 for s in samples])
        if len(samples) > 3 and any([np.mean(s) > 0.1 for s in samples]):
            _, p = friedmanchisquare(*samples)
        else:
            p = 1
        return p

    def clusterDistance(
        self,
        elements: Iterable[T.FeatureValuesTuple]
    ):
        """
        Computes the probability that saying the given elements do not form a
        cluster is wrong. In other words, returns the p-value of the
        statistical test used to check the existence of the cluster.
        :param elements: elements to be evaluated
        """
        hashToCode = self.origTranslator.hashToCode
        codes = [hashToCode[e] for e in elements if e in hashToCode]
        samples = []
        for code in codes:
            sample = self.samples[code]
            tot = sum(sample.values())
            sampleDistribution = {k: float(v) / tot for k, v in sample.items()}
            samples.append(sampleDistribution)
        dists = [0.0]
        for distA, distB in combinations(samples, 2):
            dists.append(self.bhattacharyyaDistance(distA, distB))
        return np.max(dists)
                
    def aggregateStateWithDistMeasure(
        self,
        tup
    ):
        info = []
        for f, _ in tup:
            subtup = tuple(((tf, tv) for tf, tv in tup if tf != f))
            matches = matchTuples(subtup, self.tuplesToRepr.values())
            d = self.clusterDistance(matches)
            if len(matches) >= 2:
                info.append((d, subtup, matches))
        if len(info) == 0:
            return None
        iMin = info.index(min(info, key=lambda i: i[0]))
        d, subtup, matches = info[iMin]
        if len(matches) > 1 and d < 0.5:
            for m in matches:
                self.tuplesToRepr[m] = subtup
            self.printJoining(self.quiet, matches, subtup, dist=d)
            return subtup
        else:
            return None
            

    @staticmethod
    def bhattacharyyaDistance(
        distA: Dict[T.FeatureValues, float],
        distB: Dict[T.FeatureValues, float],
    ):
        dist = 0
        keys = set(list(distA.keys()) + list(distB.keys()))
        for k in keys:
            vA = distA[k] if k in distA else 0
            vB = distB[k] if k in distB else 0
            dist += np.sqrt(vA * vB)
        if dist == 0.0:
            dist = float('Inf')
        else:
            dist = -np.log(dist)
        return dist
