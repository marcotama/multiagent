# -*- coding: utf-8 -*-
"""
Implementation of some kinds of options.

@author: Marco Tamassia
"""
import random
#import numpy as np
from abc import abstractmethod
from typing import Optional, Iterable, Union

#from tabulate import tabulate

from reinf.exploration_strategies import Greedy
from util.typing2 import HashedState, Action, OptionName


class Option:
    """
    A general policy, as defined in Sutton et al., 1999.
    """
    def __init__(
        self,
        name: OptionName,
        initSet: Optional[Iterable]
    ):

        self.name = name
        self.initSet = initSet

    @abstractmethod
    def _policy(
        self,
        s: HashedState,
        legalActions: Iterable[Action]
    ) -> Action:
        pass

    @abstractmethod
    def _termProbs(
        self,
        s: HashedState
    ) -> float:
        pass

    def isInitiable(
        self,
        s: HashedState
    ):
        return self.initSet is None or s in self.initSet

    def getAction(
        self,
        s: HashedState,
        lao: Iterable[Union[Action, OptionName]]
    ):
        action = self._policy(s, lao)
        return action

    def stochasticallyTerminate(
        self,
        state
    ):
        prob = self._termProbs(state)
        r = random.random()
        return r <= prob


class UniformTerminationProbabilityOption(Option):
    def __init__(self, name, initSet, tProb):
        super().__init__(name, initSet)
        self.tProb = tProb

    @abstractmethod
    def _policy(
        self,
        s: HashedState,
        legalActions: Iterable[Action]
    ) -> Action:
        pass

    def _termProbs(
        self,
        s: HashedState
    ):
        return self.tProb


# TODO: re-implement me
# class NumpyfiedQOption(Option):
#     """
#     An option whose policy is to be greedy on a given numpyfied Q-table.
#     """
#
#     def __init__(
#         self,
#         name: OptionName,
#         Q: np.ndarray,
#         termProbsValues: np.ndarray,
#     ) -> None:
#         self.Q = Q
#         self.termProbsValues = termProbsValues
#         self.greedyStrategy = Greedy()
#         self.states = list(range(Q.shape[0]))
#         self.actions = list(range(Q.shape[1]))
#         super().__init__(name,
#                          [s for s in self.states
#                           if self.termProbsValues[s] < 1.0])
#
#     def _policy(
#         self,
#         s: HashedState,
#         la: Iterable[Action]
#     ) -> Action:
#         # noinspection PyTypeChecker
#         if s is None or s >= len(self.Q):
#             a = random.sample(la, 1)[0]
#         else:
#             qs = {a: self.Q[s][a]
#                   for a in la
#                   if isinstance(a, Action)}
#             a = self.greedyStrategy.decide(qRow=qs)
#         return a
#
#     def _termProbs(
#         self,
#         s: Optional[HashedState]
#     ) -> float:
#         # noinspection PyTypeChecker
#         if s is None or s >= len(self.termProbsValues):
#             prob = 1.0
#         else:
#             prob = self.termProbsValues[s]
#         return prob
#
#     def __str__(
#         self
#     ) -> str:
#         rows = []
#         Q = self.Q
#         for s in self.states:
#             if s is None:
#                 continue
#             if self.termProbsValues[s] == 1.0:
#                 continue
#             rows.append([s] + Q[s, :].tolist())
#
#         tb = tabulate(rows, ['State'] + [str(a) for a in self.actions])
#         nameInfo = "Q-table of option %s" % self.name + "\n\n"
#         return nameInfo + tb


class ConstantActionOption(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.
    """

    def __init__(
        self,
        name: OptionName,
        action: Action,
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.action = action
        self.greedyStrategy = Greedy()
        super().__init__(name, None)

    def _policy(
        self,
        s: HashedState,
        la: Iterable[Action]
    ) -> Action:
        if self.action in la:
            return self.action
        else:
            return random.choice([a for a in la if isinstance(a,Action)])

    def _termProbs(
        self,
        s: HashedState
    ) -> float:
        return self.tProb


class RandomActionOption(Option):
    """
    An option whose policy is to choose a random *primitive* action among the legal ones.
    """

    def __init__(
        self,
        name: OptionName,
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.greedyStrategy = Greedy()
        super().__init__(name, None)

    def _policy(
        self,
        s: HashedState,
        la: Iterable[Action]
    ) -> Action:
        return random.choice([a for a in la if isinstance(a,Action)])

    def _termProbs(
        self,
        s: HashedState
    ) -> float:
        return self.tProb
