from util.vector import DenseVector, Vector, SparseVector
from util.trace import Trace
from typing import List, Callable, Optional, Dict
from util.state_action_encoder import StateActionEncoder
from reinf.predictor import Predictor, LearnerPredictor
from reinf.controller import LearnerController
from reinf.policy.common import Policy
from util.functions import StateActionNewStateFn


class GQ(LearnerPredictor):
    """
    Gradient Temporal-Difference learning algorithm that uses eligibility traces and learns off-policy.
    Ref: Maei, Hamid Reza, and Richard S. Sutton. "GQ (λ): A general gradient algorithm for temporal-difference
    prediction learning with eligibility traces." Proceedings of the Third Conference on Artificial General
    Intelligence. Vol. 1. 2010.
    Link: https://webdocs.cs.ualberta.ca/~sutton/papers/maei-sutton-10.pdf

    Ref: White, Adam, and Richard S. Sutton. "GQ (λ) Quick Reference Guide." (2014).
    Link: https://webdocs.cs.ualberta.ca/~sutton/GQlambda.pdf
    """

    def __init__(
        self,
        vec_size: int,
        alpha: float,
        eta: float,
        trace_factory: Callable[[], Trace],
        target_factory: Callable[[Predictor], Policy],
        state_action_encoder: StateActionEncoder,
        gamma_fn: StateActionNewStateFn,
        lambda_fn: StateActionNewStateFn,
        interest_fn: StateActionNewStateFn,
        reward_fn: Optional[StateActionNewStateFn]=None
    ):

        self.alpha = alpha  # step-size parameter for learning theta
        self.eta = eta  # relative step-size parameter for learning w (alpha * eta)
        self.e = trace_factory(vec_size)  # eligibility trace vector
        self.target = target_factory(self)

        self.theta = DenseVector(vec_size)  # learned weights of the linear approximation
        self.w = DenseVector(vec_size)  # secondary set of learned weights
        self.state_action_encoder = state_action_encoder

        self.lambda_fn = lambda_fn
        self.gamma_fn = gamma_fn
        self.interest_fn = interest_fn
        self.reward_fn = reward_fn

        self.phi_bar_tp1 = SparseVector(state_action_encoder.out_vec_size)

    def learn(self, x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t, phis: Optional[Dict[int, Vector]]=None):
        if phis is None:
            phis = self.state_action_encoder.state_actions(x_t, la_t)
        rho_t = self.target.pi(a_t, phis) / bpi_t
        phis_bar = self.state_action_encoder.state_actions(x_tp1, la_tp1)
        if len(phis_bar) > 0:
            pis = self.target.pis(phis_bar)
            self.phi_bar_tp1.clear()
            for a in phis_bar:
                self.phi_bar_tp1 += phis_bar[a] * pis[a]

        lambda_t = self.lambda_fn(x_t, a_t, x_tp1)
        gamma_t = self.gamma_fn(x_t, a_t, x_tp1)
        interest_t = self.interest_fn(x_t, a_t, x_tp1)
        if r_t is None:
            r_t = self.reward_fn(x_t, a_t, x_tp1)
        self._learn(phi=phis[a_t], phi_bar=self.phi_bar_tp1, gamma=gamma_t, lmbda=lambda_t, r=r_t, rho=rho_t, I=interest_t)

    # noinspection PyTypeChecker
    def _learn(self, phi: Vector, phi_bar: Vector, gamma: float, lmbda: float, r: float, rho: float, I: float):
        delta = r + gamma * self.theta @ phi_bar - self.theta @ phi
        self.e = rho * self.e + I * phi
        self.theta += self.alpha * (delta * self.e - gamma * (1 - lmbda) * (self.w @ self.e) * phi_bar)
        self.w += self.alpha * self.eta * (delta * self.e - (self.w @ phi) * phi)
        self.e *= gamma * lmbda

    def end_of_episode(self):
        self.e.clear()
        self.target.tick()

    def reset(self):
        self.e.clear()
        self.theta.clear()
        self.w.clear()

    def predict(self, phi_sa: Vector) -> float:
        return self.theta @ phi_sa

    def save(self, file_path: str):
        self.theta.save(file_path)

    def load(self, file_path: str):
        self.theta.load(file_path)

    def get_weights(self) -> Vector:
        return self.theta

    def compute_value_function(self, x: Vector, la: List[int]) -> float:
        phis = self.state_action_encoder.state_actions(x, la)
        self.target._update(phis)
        v_s = 0.0
        # V(s) = \sum_{a \in A} \pi(s,a) * Q(s,a)
        for a in phis:
            v_s += self.target._pi(a) * self.predict(phis[a])
        return v_s

    def sample_action(self, x: Vector, la: List[int]):
        self.target.sample_action(self.state_action_encoder.state_actions(x, la))



# Gradient descent control
class GreedyGQ(LearnerController):

    def __init__(
        self,
        alpha: float,
        eta: float,
        trace_factory: Callable[[], Trace],
        target_factory: Callable[[Predictor], Policy],
        behavior_factory: Callable[[Predictor], Policy],
        actions: List[int],
        state_action_encoder: StateActionEncoder,
        gamma_fn: StateActionNewStateFn,
        lambda_fn: StateActionNewStateFn,
        interest_fn: StateActionNewStateFn,
        reward_fn: Optional[StateActionNewStateFn]=None
    ):

        self.gq = GQ(
            vec_size=state_action_encoder.vector_size(),
            alpha=alpha,
            eta=eta,
            trace_factory=trace_factory,
            target_factory=target_factory,
            state_action_encoder=state_action_encoder,
            gamma_fn=gamma_fn,
            lambda_fn=lambda_fn,
            interest_fn=interest_fn,
            reward_fn=reward_fn
        )
        self.behavior = behavior_factory(self.gq)

        self.rho_t = 0.0
        self.actions = actions
        self.state_action_encoder = state_action_encoder

    # noinspection PyTypeChecker
    def learn(self, x_t, la_t, a_t, x_tp1, la_tp1, r_t) -> Vector:
        phis = self.state_action_encoder.state_actions(x_t, la_t)
        bpi_t = self.behavior.pi(a_t, phis)
        self.gq.learn(x_t=x_t, la_t=la_t, a_t=a_t, x_tp1=x_tp1, la_tp1=la_tp1, bpi_t=bpi_t, r_t=r_t, phis=phis)

    def propose_greedy_action(self, x: Vector, la: List[int]) -> int:
        return self.gq.target.sample_action(self.state_action_encoder.state_actions(x, la))

    def propose_exploratory_action(self, x: Vector, la: List[int]) -> int:
        return self.behavior.sample_action(self.state_action_encoder.state_actions(x, la))

    def compute_value_function(self, x: Vector, la: List[int]) -> float:
        return self.gq.compute_value_function(x, la)

    def save(self, file_path: str):
        self.gq.save(file_path)

    def load(self, file_path: str):
        self.gq.load(file_path)

    def end_of_episode(self):
        self.gq.end_of_episode()
        self.behavior.tick()
