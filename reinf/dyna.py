import numpy as np
import queue
from scipy.sparse import csr_matrix
from typing import List, Callable, Union

from reinf.predictor import LearnerPredictor
from reinf.controller import LearnerController
from reinf.policy.common import Policy
from reinf.policy.discrete import GreedyPolicy
from util.vector import Vector


# Gradient descent control
class DynaMGPS(LearnerPredictor):
    """
    Linear Dyna with McMahan-Gordon (MG) Prioritized Sweeping.

    Ref: Sutton, Richard S., et al. "Dyna-style planning with linear function approximation and prioritized sweeping." arXiv preprint arXiv:1206.3285 (2012).
    Link: https://arxiv.org/abs/1206.3285v1
    """

    def __init__(
        self,
        vec_size: int,
        alpha: float,
        actions: List[int],
        gamma: float
    ):
        self.vec_size = vec_size
        self.alpha = alpha
        self.gamma = gamma
        self.actions = actions
        self.F_a = {a: np.zeros((vec_size, vec_size)) for a in actions}  # forward model
        self.b_a = {a: np.zeros((vec_size, 1)) for a in actions}  # reward model
        self.theta = np.zeros((vec_size, 1))
        self.budget_p = 100

    # noinspection PyTypeChecker
    def learn(self, x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t) -> Vector:
        x = x_t.data.T
        x_prime = x_tp1.data.T

        delta = r_t + self.gamma * self.theta.T @ x_prime - self.theta.T @ x
        delta = delta[0,0]
        self.theta += self.alpha * delta * x
        self.F_a[a_t] += self.alpha * (x_prime - self.F_a[a_t] @ x) @ x.T
        self.b_a[a_t] += self.alpha * (r_t - (self.b_a[a_t].T @ x)[0,0]) * x

        p_queue = queue.PriorityQueue()
        for i in range(self.vec_size):
            x_t_i = x[i]
            if x_t_i != 0:
                p_queue.put((-np.abs(delta * x_t_i), i))

        p = self.budget_p
        while not p_queue.empty():
            if p <= 0:
                break
            p -= 1

            _, i = p_queue.get()
            for j in range(self.vec_size):
                if any(self.F_a[a][i,j] != 0 for a in la_t):
                    delta = max(self.b_a[a][j] + self.gamma * self.theta.T @ self.F_a[a][:,j] for a in la_t) - self.theta[j]
                    self.theta[j] += self.alpha * delta
                    p_queue.put((-np.abs(delta), j))

    def predict(self, xa_pair):
        x, a = xa_pair
        x = x.data.T
        return self.b_a[a].T @ x + self.gamma * self.theta.T @ self.F_a[a] @ x


class DynaMGPSControl(LearnerController):
    def __init__(
        self,
        vec_size: int,
        alpha: float,
        actions: List[int],
        gamma: float,
        policy_factory: Callable[[LearnerPredictor], Policy]
    ):
        self.dyna = DynaMGPS(
            vec_size=vec_size,
            alpha=alpha,
            actions=actions,
            gamma=gamma
        )
        self.greedy = GreedyPolicy(self.dyna)
        self.policy = policy_factory(self.dyna)

    def phis(self, x, la):
        return {a: (x, a) for a in la}

    def learn (self, x_t: Vector, la_t: List[int], a_t: int, x_tp1: Vector, la_tp1: List[int], r_t: float):
        self.dyna.learn(x_t=x_t, la_t=la_t, a_t=a_t, x_tp1=x_tp1, la_tp1=la_tp1, bpi_t=0.0, r_t=r_t)

    def propose_exploratory_action (self, x: Vector, la: List[int]) -> int:
        return self.policy.sample_action(self.phis(x, la))

    def propose_greedy_action (self, x: Vector, la: List[int]) -> int:
        return self.greedy.sample_action(self.phis(x, la))

    def compute_value_function(self, x: Vector, la: List[int]) -> float:
        phis = self.phis(x, la)
        self.policy._update(phis)
        v_s = 0.0
        # V(s) = \sum_{a \in A} \pi(s,a) * Q(s,a)
        for a in phis:
            v_s += self.policy._pi(a) * self.dyna.predict(phis[a])
        return v_s

    def end_of_episode(self):
        self.policy.tick()
