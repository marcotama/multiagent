from abc import abstractmethod
from util.delegation import delegate_attrs
from reinf.mdp import MDP


from typing import Iterable, Optional, List

from util.typing2 import State, Action

from reinf.options.option import Option
from reinf.options.mdp_with_options import MDPWithOptions


@delegate_attrs('mdp', [
    'get_q_and_n_tables',
    'end_of_episode',
])
class MDPAgent:
    def __init__(
        self,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
        disable_primitive_actions: bool
    ):
        if options:
            self.mdp = MDPWithOptions(
                alpha=alpha,
                gamma=gamma,
                init_q=init_q,
                operator=operator,
                operator_alpha=operator_alpha,
                options=options,
                early_termination=early_termination,
                disable_primitive_actions=disable_primitive_actions
            )
        else:
            self.mdp = MDP(
                alpha=alpha,
                gamma=gamma,
                init_q=init_q,
                operator=operator,
                operator_alpha=operator_alpha
            )

    def __str__(self):
        return str(self.mdp)

    @abstractmethod
    def get_action(
        self,
        state: State,
        legal_actions: Iterable[Action]
    ):
        pass

    @abstractmethod
    def learn(
        self,
        state: State,
        legal_actions: List[Action],
        action: Action,
        reward: float,
        new_state: Optional[State],
        new_legal_actions: List[Action]
    ):
        pass

    def get_states_count(self):
        return len(self.mdp.qTable)

    def get_state_action_pairs_count(self):
        return sum(len(qRow) for qRow in self.mdp.qTable.values())

    def get_mdp(self):
        return self.mdp