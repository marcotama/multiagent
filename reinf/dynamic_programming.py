# -*- coding: utf-8 -*-
"""
Standard dynamic programming algorithms for Reinforcement Learning.
All the algorithms work on Numpy arrays; therefore, to use states and actions
representations that are different than subsequent positive integers, you
should wrap these functions with encoders and decoders.

@author: Marco Tamassia
"""
import numpy as np

from util.typing2 import MyTypes as T


def deriveQFromV(
    V: T.NumpyVTable,
    P: T.NumpyPTable
) -> T.NumpyQTable:
    """
    Computes the state-action value function induced by the state value
    function.

    Parameters
    ----------
    V : 1-dimensional numpy array of size |S|
            The state value function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.

    Returns
    -------
    Q :  2-dimensional numpy array of size |S|x|A|
            The state-action value function induced by the state value
            function.
    """
    nS, n_A = (P.shape[0], P.shape[1])
    Q = np.zeros([nS, n_A])
    for s in range(nS):
        Q[s, :] = np.dot(P[s, :, :], V)
    return Q


def derivePolicyFromV(
    V: T.NumpyVTable,
    P: T.NumpyPTable
) -> T.NumpyPolicy:
    """
    Computes the policy induced by the state value function.

    Parameters
    ----------
    V : 1-dimensional numpy array of size |S|
            The state value function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.

    Returns
    -------
    pi : 1-dimensional numpy array of size |S|
            The policy induced by the state value function.
    """
    nS = P.shape[0]
    policy = np.zeros(nS, dtype=int)
    for s in range(nS):
        expectations = np.dot(P[s, :, :], V)
        policy[s] = expectations.argmax()
    return policy


def derivePolicyFromQ(
    Q: T.NumpyQTable
) -> T.NumpyPolicy:
    """
    Computes the policy induced by the state-action value function.

    Parameters
    ----------
    Q : 2-dimensional numpy array of size |S|x|A|
            The state-action value function.

    Returns
    -------
    pi : 1-dimensional numpy array of size |S|
            The policy induced by the state-action value function.
    """
    nS = Q.shape[0]
    policy = np.zeros(nS, dtype=int)
    for s in range(nS):
        policy[s] = Q[s, :].argmax()
    return policy


def deriveVFromQ(
    Q: T.NumpyQTable
) -> T.NumpyVTable:
    """
    Computes the state value function induced by the state-action value
    function.

    Parameters
    ----------
    Q : 2-dimensional numpy array of size |S|x|A|
            The state-action value function.

    Returns
    -------
    V : 1-dimensional numpy array of size |S|
            The state value function.
    """
    nS = Q.shape[0]
    V = np.zeros(nS)
    for s in range(nS):
        V[s] = Q[s, :].max()
    return V


def expectationStep(
    pi: T.NumpyPolicy,
    old_V: T.NumpyVTable,
    V: T.NumpyVTable,
    R: T.NumpyRTable,
    P: T.NumpyArray3D,
    g: float
):
    """
    Applies Bellman equations for value function estimation.
    The computation is performed in place, using old_V values and storing the
    result in V.

    Parameters
    ----------
    pi : 1-dimensional numpy array of size |S|
            The policy.
    old_V : 1-dimensional numpy array of size |S|
            The current value function.
    V : 1-dimensional numpy array of size |S|
            A container for the result.
    R : 1-dimensional numpy array of size |S|
            The reward function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.
    g : float in [0,1]
            The discount factor.
    """
    nS = R.shape[0]
    S = np.arange(nS)
    Tpi = P[S, pi, :]
    np.copyto(V, R + g * np.dot(Tpi, old_V))


def greedyStep(
    old_V: T.NumpyVTable,
    V: T.NumpyVTable,
    R: T.NumpyRTable,
    P: T.NumpyArray3D,
    g: float
):
    """
    Applies Bellman equations for maximum value function estimation.
    The computation is performed in place, using old_V values and storing the
    result in V.

    Parameters
    ----------
    old_V : 1-dimensional numpy array of size |S|
            The current value function.
    V : 1-dimensional numpy array of size |S|
            A container for the result.
    R : 1-dimensional numpy array of size |S|
            The reward function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.
    g : float in [0,1]
            The discount factor.
    """
    max_values = np.dot(P, old_V).max(axis=1)  # type: np.ndarray
    np.copyto(V, R + g * max_values)


def policyEvaluationByIteration(
    pi: T.NumpyPolicy,
    R: T.NumpyRTable,
    P: T.NumpyArray3D,
    g: float,
    threshold: float=0.01
) -> T.NumpyVTable:
    """
    Computes the state value function induced by the policy pi using Bellman
    equations iteratively.

    Parameters
    ----------
    pi : 1-dimensional numpy array of size |S|
            The policy to be evaluated.
    R : 1-dimensional numpy array of size |S|
            The reward function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.
    g : float in [0,1]
            The discount factor.
    threshold : float in [0,1]
            The threshold for value iteration to stop iterating.

    Returns
    -------
    V : 1-dimensional numpy array of size |S|
            The policy induced by the value function.
    """

    V = np.copy(R)

    old_V = np.copy(V)

    error = float("inf")
    while error > threshold:
        old_V, V = (V, old_V)
        expectationStep(pi, old_V, V, R, P, g)
        error = np.linalg.norm(V - old_V)

    return V


def policyIteration(
    R: T.NumpyRTable,
    P: T.NumpyArray3D,
    g: float,
    threshold: float=0.01
) -> T.NumpyVTable:
    """
    Computes the optimal state value function for the given MDP using Policy
    Iteration algorithm (Howard, 1960).

    Parameters
    ----------
    R : 1-dimensional numpy array of size |S|
            The reward function.
    P : 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.
    g: float in [0,1]
            The discount factor.
    threshold : float in [0,1]
            The threshold for the norm-2 difference between successive values
            of V.

    Returns
    -------
    V : 1-dimensional numpy array of size |S|
            The value function.
    """
    V = R
    pi = derivePolicyFromV(V, P)
    old_V = np.copy(V)

    error = float("inf")
    while error > threshold:
        old_V, V = (V, old_V)
        V = policyEvaluationByIteration(pi, R, P, g, old_V)
        pi = derivePolicyFromV(V, P)
        error = np.linalg.norm(V - old_V)

    return V


def valueIteration(
    R: T.NumpyRTable,
    P: T.NumpyArray3D,
    g: float,
    threshold: float=0.01,
    V: T.NumpyVTable=None
) -> T.NumpyVTable:
    """
    Computes the optimal state value function for the given MDP using Value
    Iteration algorithm (Bellman, 1957).

    Parameters
    ----------
    R: 1-dimensional numpy array of size |S|
            The reward function.
    P: 3-dimensional numpy array of size |S|x|A|x|S|
            The transition probability matrix.
    g: float in [0,1]
            The discount factor.
    threshold: float in [0,1]
            The threshold for the norm-2 difference between successive values
            of V.
    V: 1-dimensional numpy array of size |S|
            Optional initial values for the value function.

    Returns
    -------
    V : 1-dimensional numpy array of size |S|
            The value function.
    """
    if V is None:
        V = np.copy(R)
    old_V = np.copy(V)
    error = float("inf")
    while error > threshold:
        old_V, V = (V, old_V)
        greedyStep(old_V, V, R, P, g)
        error = np.linalg.norm(V - old_V)
    return V
