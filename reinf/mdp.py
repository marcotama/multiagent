# -*- coding: utf-8 -*-
"""
Class representing environment knowledge. Implements the learning algorithms
for Q-values, rewards and transition probabilities. The internal representation
is a dictionary. Therefore, instances of this class can be updated with new
experience.

States are expected to be dictionaries.

@author: Marco Tamassia
"""

from collections import defaultdict
from copy import copy
from functools import partial
from typing import Optional, List, Iterable, Union, Dict, Tuple, Set

import numpy as np
from tabulate import tabulate

from reinf.exploration_strategies import ExplorationStrategy
from util.typing2 import HashedState, Action, OptionName
from util.util2 import defaultdict2, defaultdict3



class MDP:

    def __init__(
        self,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
    ) -> None:
        """
        Initializes the MDP.
        """
        self.gamma = gamma
        self.init_q = init_q
        self.baseAlpha = alpha
        self.alpha = alpha
        self.scale_alpha(1)
        self.qTable = defaultdict2(partial(float, self.init_q))  # type: Dict[HashedState, Dict[Action, float]]
        self.rTable = defaultdict2(float)  # type: Dict[HashedState, Dict[Action, float]]
        self.pTable = defaultdict3(float)  # type: Dict[HashedState, Dict[Action, Dict[HashedState, float]]]
        self.nTable = defaultdict2(int)  # type: Dict[HashedState, Dict[Action, int]]
        self.tTable = defaultdict3(int)  # type: Dict[HashedState, Dict[Action, Dict[HashedState, int]]]
        self.qStats = defaultdict(partial(copy, (0, 0.0, 0.0)))  # type: Dict[Tuple[HashedState, Action], Tuple[int, float, float]]
        self.states = set()  # type: Set[HashedState]
        self.actions = set()  # type: Set[Action]
        self.totalActions = 0
        self.episodeActions = 0
        self.episodes = 0
        self.history = []  # type: List[Tuple[HashedState, Iterable[Action], Action, float, HashedState, Iterable[Action]]]
        self.learn_q = True
        self.learn_r = True
        self.learn_p = True
        self.learn_n = True
        self.learn_t = True
        self.learn_q_stats = True
        self.save_history = True

        # Setup operator (with boolean variables for efficiency)
        # see Bellemare, Marc G., et al. "Increasing the action gap: New operators for reinforcement learning."
        # Proceedings of the AAAI Conference on Artificial Intelligence. 2016.
        self.bellman_operator = False
        self.consistent_bellman_operator = False
        self.advantage_learning_operator = False
        self.persistent_advantage_learning_operator = False
        self.lazy_operator = False
        if operator == 'bellman':
            self.bellman_operator = True
        elif operator == 'consistent-bellman':
            self.consistent_bellman_operator = True
        elif operator == 'advantage-learning':
            self.advantage_learning_operator = True
        elif operator == 'persistent-advantage-learning':
            self.persistent_advantage_learning_operator = True
        elif operator == 'lazy':
            self.lazy_operator = True
        else:
            raise Exception("Unknown operator: %s" % operator)
        self.operator_alpha = operator_alpha

    def scale_alpha(
        self,
        factor: float
    ) -> None:
        self.alpha = self.baseAlpha * factor

    def __str__(self) -> str:
        result = "Number of states: %d\n" % len(self.states)
        result += "Number of actions: %d\n" % len(self.actions)
        actions = sorted(self.actions)
        rows = []
        Q, N = self.qTable, self.nTable
        for s in self.qTable:
            if s is None:
                continue
            row = [str(s)]
            for a in actions:
                row.append("%.2f (%d)" % (Q[s][a], N[s][a]))
            if len(self.qTable[s]) == 0:
                row.append('-')
            else:
                row.append(max(self.qTable[s], key=self.qTable[s].get))
            rows.append(row)
        result += "\n\nQ (N) + Greedy policy:\n"
        result += tabulate(rows, headers=['State'] + actions + ['Best action'])
        return result

    def get_q_and_n_tables(self) -> Tuple[Dict[HashedState, Dict[Action, float]], Dict[HashedState, Dict[Action, int]]]:
        """
        Returns the Q- and N-tables.
        """
        return self.qTable, self.nTable

    def get_q_row(
            self,
            hashed_state: HashedState,
            legal_actions: Iterable[Action]
    ) -> Dict[Union[Action, OptionName], float]:
        """
        Returns the Q-table row for the given state, which include
        actions and options.
        :param hashed_state: state code
        :param legal_actions: legal actions
        """
        qRow = {}
        for a in legal_actions:
            qRow[a] = self.qTable[hashed_state][a]
        return qRow

    def get_n_row(
            self,
            hashed_state: HashedState,
            legal_actions: Iterable[Action]
    ) -> Dict[Union[Action, OptionName], int]:
        """
        Returns the N-table row for the given state, which include
        actions and options.
        :param hashed_state: state code
        :param legal_actions: legal actions
        """
        nRow = {}
        for a in legal_actions:
                nRow[a] = self.nTable[hashed_state][a]
        return nRow

    def learn(
        self,
        hashed_state: HashedState,
        legal_actions: Iterable[Action],
        action: Action,
        reward: float,
        hashed_new_state: Optional[HashedState],
        new_legal_actions: Iterable[Action],
        own_decision: bool=True
    ) -> None:
        """
        Updates internal knowledge.
        :param hashed_state: state code
        :param legal_actions: legal actions code (for state `state`)
        :param action: action code
        :param reward: reward
        :param hashed_new_state: next state code
        :param new_legal_actions: legal actions code (for state `new_state`)
        :param own_decision: whether the decision was made by this agent (has implications when using options)
        """
        self.states.add(hashed_state)
        self.states.add(hashed_new_state)
        self.actions.add(action)
        self.totalActions += 1
        self.episodeActions += 1
        # Update history
        if self.save_history:
            self.history.append((hashed_state, legal_actions, action, reward, hashed_new_state, new_legal_actions))
        # Update N-table
        if self.learn_n:
            self.nTable[hashed_state][action] += 1
        # Update T-table
        if self.learn_t:
            self.tTable[hashed_state][action][hashed_new_state] += 1
        # Update Q-table
        if self.learn_q:
            self._update_q_table(hashed_state, legal_actions, action, reward, hashed_new_state, new_legal_actions)
        # Update Q-stats-table
        if self.learn_q_stats:
            self._update_q_statistics(hashed_state, action)
        # Update P-table
        if self.learn_p:
            self._update_p_table(hashed_state, action, hashed_new_state)
        # Update R-table
        if self.learn_r:
            self._update_r_table(hashed_state, action, reward)

    def _update_q_table(
        self,
        hashed_state: HashedState,
        legal_actions: Iterable[Action],
        action: Action,
        reward: float,
        new_state: Optional[HashedState],
        new_legal_actions: Iterable[Action]
    ) -> None:
        """
        Updates the Q-table using Q-learning.
        """
        Q = self.qTable
        q = Q[hashed_state][action]
        if new_state is None:
            new_q = reward
        else:
            max_next_q = max(self.get_q_row(new_state, new_legal_actions).values())
            tq = reward + self.gamma * max_next_q
            if self.bellman_operator:
                new_q = tq
            elif self.consistent_bellman_operator:
                # if new state is the same, use the same action instead of the best
                new_q = tq if new_state != hashed_state else q
            elif self.advantage_learning_operator:
                # propagate current action gap
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions).values())
                new_q = tq - self.operator_alpha * (max_cur_q - q)
            elif self.persistent_advantage_learning_operator:
                # propagate current action gap unless repeating this action in next state is a good choice
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions).values())
                new_q = max(tq - self.operator_alpha * (max_cur_q - q),
                            reward + self.gamma * Q[new_state][action])
            elif self.lazy_operator:
                # only update Q if it would change the policy
                max_cur_q = max(self.get_q_row(hashed_state, legal_actions).values())
                new_q = q if q <= tq <= self.operator_alpha * max_cur_q + (1 - self.operator_alpha) * q else tq
            else:
                raise Exception("No operator selected") # should not happen
        Q[hashed_state][action] += self.alpha * (new_q - q)

    def _update_q_statistics(
            self,
            hashed_state: HashedState,
            action: Action
    ) -> None:
        key = (hashed_state, action)
        if key not in self.qStats:
            self.qStats[key] = (0, 0.0, 0.0)
        n, m, v = self.qStats[key]
        q = self.qTable[hashed_state][action]
        n += 1
        e = q - m
        m += e / n
        if n > 1:
            v = (v * (n - 2) + e * (q - m)) / (n - 1)
        else:
            v = 0.0
        self.qStats[key] = (n, m, v)

    def _update_r_table(
            self,
            hashed_state: HashedState,
            action: Action,
            reward: float
    ) -> None:
        """
        Updates reward and transition probabilities models.
        """

        R = self.rTable
        alpha = self.alpha
        # Update reward model
        R[hashed_state][action] += alpha * (reward - R[hashed_state][action])

    def _update_p_table(
            self,
            hashed_state: HashedState,
            action: Action,
            new_state: Optional[HashedState]
    ) -> None:
        """
        Updates reward and transition probabilities models.
        """

        P = self.pTable
        alpha = self.alpha
        gamma = self.gamma
        # Update transition probabilities model
        if new_state not in P[hashed_state][action]:
            P[hashed_state][action][new_state] = 0
        for x in P[hashed_state][action]:
            delta = int(x == new_state)
            P[hashed_state][action][x] += alpha * (gamma * delta - P[hashed_state][action][x])

    def end_of_episode(self) -> None:
        """
        Updates internal information concerning the current episode.
        """
        self.episodeActions = 0
        self.episodes += 1

    def choose_by_q_row(
            self,
            exploration_strategy: ExplorationStrategy,
            q_row: Dict[Action, float],
            n_row: Dict[Action, int],
            q_stats_row: Dict[Union[Action, OptionName], Tuple[float, float, float]]
    ) -> Action:
        a = exploration_strategy.decide(qRow=q_row, nRow=n_row, qStatsRow=q_stats_row, eps=self.episodes)
        return a

    def choose_action(
        self,
        hashed_state: HashedState,
        exploration_strategy: ExplorationStrategy,
        legal_actions: Iterable[Action]
    ) -> Action:
        """
        Returns an action according to the exploration strategy. If
        la is None, all _known_ actions will be considered.
        :param hashed_state: state code
        :param exploration_strategy: exploration strategy
        :param legal_actions: legal actions
        """
        Q, N, QS, init_q = self.qTable, self.nTable, self.qStats, self.init_q
        q_row = {a: Q[hashed_state][a] if a in Q[hashed_state] else init_q for a in legal_actions}
        n_row = {a: N[hashed_state][a] if a in N[hashed_state] else 0 for a in legal_actions}
        q_stats_row = {a: QS[(hashed_state, a)] if (hashed_state, a) in QS else (0, 0.0, 0.0) for a in legal_actions}
        choice = self.choose_by_q_row(exploration_strategy, q_row, n_row, q_stats_row)
        return choice

    def get_p_table_by_counts(self):
        nS, nA = len(self.states), len(self.actions)
        P = np.zeros([nS, nA, nS])
        Tt = self.tTable
        for s in Tt:
            for a in Tt[s]:
                for ns in Tt[s][a]:
                    P[s, a, ns] = Tt[s][a][ns]
                P[s, a, :] /= P[s, a, :].sum()
        return P

    def numpify_p_table(self, fill_missing=False) -> np.ndarray:
        """
        Returns the P table in the form of a 3D Numpy array accessible by using P[s, a, ns].
        :param fill_missing: if True, state-action pairs never tried are filled with uniform probabilities;
            if False (default), they are filled with zeros.
        """
        P_ = self.pTable
        nS, nA = (len(self.states), len(self.actions))

        P = np.zeros([nS, nA, nS])
        for s in P_:
            for a in P_[s]:
                for ns in P_[s][a]:
                    if ns is None:
                        continue
                    P[s, a, ns] = P_[s][a][ns]
                tot = P[s, a, :].sum()
                if tot != 0:
                    P[s, a, :] /= tot
                elif fill_missing:
                    P[s, a, :] = 1.0 / nS
        return P

    def numpify_q_table(self) -> np.ndarray:
        """
        Returns the Q table in the form of a 2D Numpy array accessible by using
        Q[s, a].
        """
        Q_ = self.qTable
        nS, nA = (len(self.states), len(self.actions))

        Q = np.zeros([nS, nA])
        for s in Q_:
            for a in Q_[s]:
                Q[s, a] = Q_[s][a]
        return Q

    def numpify_n_table(self) -> np.ndarray:
        """
        Returns the N table in the form of a 2D Numpy array accessible by using
        N[s, a].
        """
        N_ = self.qTable
        nS, nA = (len(self.states), len(self.actions))

        N = np.zeros([nS, nA])
        for s in N_:
            for a in N_[s]:
                N[s, a] = N_[s][a]
        return N

    def numpify_r_table(self) -> np.ndarray:
        """
        Returns the R table in the form of a 1D Numpy array accessible by using
        R[s].
        """
        R_ = self.qTable
        nS = len(self.states)

        R = np.zeros([nS])
        for s in R_:
            R[s] = R_[s]
        return R

    def draw_graph(self, filename: str) -> None:
        import networkx as nx
        P = self.pTable
        G = nx.MultiDiGraph()
        for s in P:
            for a in P[s]:
                for ns in P[s][a]:
                    if (s != 100) or P[s][a][ns] < 0.01:
                        continue
                    G.add_edge(s, ns, key=a,
                               attr_dict={'label': "(%s, %.2f)" % (a, P[s][a][ns])})
        pG = nx.to_pydot(G)
        pG.set_sep("+50,50")
        pG.set_overlap('scalexy')
        pG.set_nodesep('1.0')
        with open(filename, 'w') as f:
            f.write(pG.to_string())
        # dot -Tps model.gv -o model.pdf
