# -*- coding: utf-8 -*-
"""
Q-Learning classes that deal with factored states (i.e., states that are expressed as dictionaries).

@author: Marco Tamassia
"""


from typing import Iterable, Optional, List

from util.typing2 import State, HashedState, Action
from util.util2 import hash_dictionary

from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from reinf.options.mdp_with_options import MDPWithOptions
from reinf.common import Explorator, Learner
from reinf.mdp_agent import MDPAgent
from tabulate import tabulate




class QLearningAgent(MDPAgent, Learner, Explorator):
    def __init__(
        self,
        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Optional[Iterable[Option]]=None,
        early_termination: bool=True,
        disable_primitive_actions: bool=True,
        q_table_features: Optional[List[str]]=None
    ):
        if options is None:
            options = []

        if q_table_features is None:
            q_table_features = features

        self.features = sorted(features)
        self.q_table_features = sorted(q_table_features)
        super().__init__(
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        Learner.__init__(self)
        Explorator.__init__(self, exploration_params)
        self.last_hashed_state = None # type: Optional[State]

        if options is not None:
            for option in options:
                if hasattr(option, 'get_all_state_features'):
                    option.get_all_state_features = getattr(self, 'get_last_hashed_state')

    def hash(self, state: State) -> HashedState:
        self.last_hashed_state = state
        if state is None:
            return None
        else:
            state = {k: v for k, v in state.items() if k in self.q_table_features}
            return hash_dictionary(state)

    def get_last_hashed_state(self):
        return self.last_hashed_state

    def get_action(
        self,
        state: State,
        legal_actions: Iterable[Action]
    ):
        exploration_strategy = self.get_expl_strategy()
        action = self.mdp.choose_action(self.hash(state), exploration_strategy, legal_actions)
        return action

    def learn(
        self,
        state: State,
        legal_actions: List[Action],
        action: Action,
        reward: float,
        new_state: Optional[State],
        new_legal_actions: List[Action],
        own_decision: bool=True
    ):
        """
        Updates internal knowledge.
        :param state: state code
        :param legal_actions: legal actions code (for state `state`)
        :param action: action code
        :param reward: reward
        :param new_state: next state code
        :param new_legal_actions: legal actions code (for state `new_state`)
        :param own_decision: whether the decision was made by this agent (has implications when using options)
        """
        if self.isLearning():
            self.mdp.learn(self.hash(state), legal_actions, action, reward, self.hash(new_state), new_legal_actions, own_decision)

    def __str__(self):
        featuresRepr = {f: "F%d" % i if len(f) >= 8 else None
                        for i, f in enumerate(self.q_table_features)}
        featuresReprPairs = [(f, featuresRepr[f]) for f in self.q_table_features if featuresRepr[f] is not None]
        features_list = [f if featuresRepr[f] is None else featuresRepr[f] for f in self.q_table_features]
        actions = sorted(self.mdp.actions)

        mdp = self.get_mdp()
        if isinstance(mdp, MDPWithOptions):
            actions += sorted(mdp.options)


        pairs = []
        for hashed_state in self.mdp.qTable:
            if hashed_state is None:
                continue
            state = dict(hashed_state)
            pairs.append((hashed_state, [state[f] for f in self.q_table_features]))

        rows = []
        for hashed_state, state in sorted(pairs, key=lambda x: str(tuple(x[1]))):
            # Features
            row = state
            qRow = self.mdp.get_q_row(hashed_state, actions)
            nRow = self.mdp.get_n_row(hashed_state, actions)
            # Q-values
            row += ["%+.2f (%d)" % (qRow[a], nRow[a]) for a in actions]
            # Total visits
            row.append(sum(nRow.values()))
            # Best action
            maxQ = max(qRow[a] for a in qRow if nRow[a] > 0 or all(n == 0 for n in nRow.values()))
            bestActions = [a for a, q in qRow.items() if q == maxQ]
            row.append(", ".join(bestActions))
            # Append
            rows.append(row)

        # Generate actual strings
        featuresLegend = "\n".join([r + " -> " + f for f, r in featuresReprPairs if f in self.q_table_features and r is not None])
        if len(featuresLegend) > 0:
            featuresLegend += "\n\n"
        headers = (features_list + actions + ['Visits', 'Greedy', 'Collective decision'])
        statesStr = tabulate(rows, headers)
        return featuresLegend + statesStr