from util.vector import Vector
from typing import List, Optional
from abc import abstractmethod


class Predictor:

    @abstractmethod
    def predict(self, x: Vector) -> float:
        pass

    @abstractmethod
    def sample_action(self, x: Vector, la: List[int]) -> int:
        pass


class LearnerPredictor(Predictor):

    @abstractmethod
    def learn(
        self,
        x_t: Vector,
        la_t: List[int],
        a_t: int,
        x_tp1: Vector,
        la_tp1: List[int],
        bpi_t: float,
        r_t: Optional[float],
    ):
        pass

    @abstractmethod
    def end_of_episode (self):
        pass
