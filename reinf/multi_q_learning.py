# -*- coding: utf-8 -*-
"""
Q-Learning classes that deal with factored states (i.e., states that are expressed as dictionaries).

@author: Marco Tamassia
"""


from typing import List, Optional, Iterable, Tuple, Union, Dict, Set
from collections import defaultdict

from util.typing2 import Action, State, HashedState

from util.translator import Aggregator
from math import sqrt
from util.util2 import normalized_dict, confidenceInterval, combineStatistics
from scipy.stats import ttest_ind_from_stats
from reinf.exploration_strategies import ExplorationParams, sample1
from reinf.options.option import Option
from reinf.common import Explorator, Learner
from reinf.mdp_agent import MDPAgent
from tabulate import tabulate
from util.util2 import project, hash_dictionary


class MultiQLearningAgent(MDPAgent, Learner, Explorator):

    def __init__(
        self,
        features: List[str],
        features_sets: List[List[str]],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Optional[Iterable[Option]]=None,
        early_termination: bool=True,
        disable_primitive_actions: bool=True,
        q_table_features: Optional[List[str]]=None
    ):

        if q_table_features is None:
            q_table_features = features

        self.features = sorted(features)
        self.features_sets = {",".join(features_set): features_set for partition_id, features_set in enumerate(features_sets)} # type: Dict[str, List[str]]
        self.q_table_features = sorted(q_table_features)
        self.matches_cache = {features_set: defaultdict(set) for features_set in self.features_sets}
        super().__init__(
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        Learner.__init__(self)
        Explorator.__init__(self, exploration_params)
        self.abstractions_usage = {}
        self.significance = exploration_params.significance


    def hash(self, state: State) -> HashedState:
        if state is None:
            return None
        else:
            state = {k: v for k, v in state.items() if k in self.q_table_features}
            return frozenset(state.items())

    def get_action (
            self,
            state: HashedState,
            legal_actions: Optional[Iterable[Action]] = None
    ) -> Action:
        abstractions, qStatsRow = self.choose_abstractions(state, legal_actions)
        for i in abstractions:
            self.abstractions_usage.setdefault(i, 0)
            self.abstractions_usage[i] += 1
        qRow, nRow = self.get_decision_q_and_n_row(abstractions, state, legal_actions)
        es = self.get_expl_strategy()
        a = self.mdp.choose_by_q_row(es, qRow, nRow, qStatsRow)
        return a

    def learn(
        self,
        state: State,
        legal_actions: List[Action],
        action: Action,
        reward: float,
        new_state: Optional[State],
        new_legal_actions: List[Action],
        own_decision: bool=True
    ):
        """
        Updates internal knowledge.
        :param state: state code
        :param legal_actions: legal actions code (for state s)
        :param action: action code
        :param reward: reward
        :param new_state: next state code
        :param new_legal_actions: legal actions code (for state ns)
        :param own_decision: whether the decision was made by this agent (has implications when using options)
        """
        if self.isLearning():
            hs, hns = self.hash(state), self.hash(new_state)
            if hs not in self.mdp.qTable:
                self.update_caches(state, hs)
            if hns not in self.mdp.qTable and hns is not None:
                self.update_caches(new_state, hns)
            self.mdp.learn(hs, legal_actions, action, reward, hns, new_legal_actions, own_decision)

    def episode_is_over (self) -> None:
        """
        Updates internal information concerning the current episode.
        """
        self.mdp.episode_is_over(self)
        self.abstractions_usage = {}

    def get_abstractions_usage(self):
        return self.abstractions_usage

    def __str__(self) -> str:
        output = self.str_q_table(self.features, None, 'main-table')
#        output += "\n\nDecision table\n"
#        output += self.strQTable(self.features, lambda x: [x], 'decision-table')
        for partitioning_id in range(len(self.features_sets)):
            features = self.features_sets[partitioning_id]
            output += "\n\nAbstraction %s\n" % partitioning_id
            output += self.str_q_table(features, t='abstraction-table')
        return output + "\n\n" # + str(self.mdp)

    def str_q_table(
        self,
        features: Iterable[str],
        aggregator: Aggregator=None,
        t: str='main-table'
    ) -> str:
        featuresRepr = {f: "F%d" % i if len(f) >= 8 else None
                        for i, f in enumerate(self.features)}
        featuresReprPairs = [(f, featuresRepr[f]) for f in features if featuresRepr[f] is not None]
        featuresList = [f if featuresRepr[f] is None else featuresRepr[f] for f in self.features]
        actionsRepr = sorted(self.action_translator.codeToDict.values())
        actions = [self.encode_action(action) for action in actionsRepr]
#
#
        pairs = []  # type: List[Tuple[Union[int,List[int]], List[str]]]
        if t == 'abstraction-table':
            added = set()
            for s, d in self.state_translator.codeToDict.items():
                if s in added or d is None:
                    continue
                ss = aggregator.findSiblings(s)
                featuresValues = [d[f] if f in features else '*' for f in self.features] # Abstraction dict
                pairs.append((list(ss), featuresValues))
                added |= ss
        elif t == 'main-table' or t == 'decision-table':
            for s, d in self.state_translator.codeToDict.items():
                if d is None:
                    continue
                pairs.append((s, [d[f] for f in self.features]))
#
#
        rows = []
        for pair in sorted(pairs, key=lambda x: str(tuple(x[1]))):
            ss, featuresValues = pair
            if t == 'abstraction-table':
                # State + Features
                row = [",".join([str(s) for s in ss])] + featuresValues
                qRow, nRow = self.qAgent.get_aggregated_q_and_n_row(ss, actions)
            elif t == 'decision-table':
                # State + Features
                row = [ss] + featuresValues
                qRow, nRow = self.qAgent.get_decision_q_and_n_row(self.aggregators, ss, actions)
            elif t == 'main-table':
                # State + Features
                row = [ss] + featuresValues
                qRow = self.mdp.get_q_row(ss, actions)
                nRow = self.mdp.get_n_row(ss, actions)
            else:
                raise ValueError()
            # Q-values
            row += ["%.2f (%d)" % (qRow[a], nRow[a]) for a in actions]
            # Total visits
            row.append(sum(nRow.values()))
            # Best action
            maxQ = max(qRow.values())
            bestActions = [str(self.decode_action(a)) for a, q in qRow.items() if q == maxQ]
            row.append(", ".join(bestActions))
            # Policy action
            if t == 'main-table':
                # Is this the best abstraction?
                qRow, _ = self.qAgent.get_decision_q_and_n_row(self.aggregators, ss, actions)
                maxQ = max(qRow.values())
                bestActions = [str(self.decode_action(a)) for a, q in qRow.items() if q == maxQ]
                row.append(", ".join(bestActions))
            # Append
            rows.append(row)
#
        # Generate actual strings
        featuresLegend = "\n".join([r + " -> " + f for f, r in featuresReprPairs if f in features and r is not None])
        if len(featuresLegend) > 0:
            featuresLegend += "\n\n"
        headers = (['State'] + featuresList + actionsRepr + ['Visits', 'Greedy'])
        if t == 'main-table':
            headers += ['Collective decision']
        statesStr = tabulate(rows, headers)
        return featuresLegend + statesStr


    def update_caches(
            self,
            state: State,
            s: HashedState
    ):
        for partitioning_id in self.features_sets:
            projected_state = project(state, self.features_sets[partitioning_id])
            hashed_projected_state = hash_dictionary(projected_state)
            self.matches_cache[partitioning_id][hashed_projected_state].add(s)


    def find_siblings(
            self,
            partitioning_id: str,
            state: State
    ) -> Set[HashedState]:
        """
        Finds matches of the given state. Calls to this function are cached after projecting the parameter on the
        relevant features.
        :param partitioning_id: id of the partitioning to use (a.k.a.: abstraction, features set)
        :param state: state whose matches are to be found
        :return: matching states code
        """
        projected_state = project(state, self.features_sets[partitioning_id])
        hashed_projected_state = hash_dictionary(projected_state)
        if hashed_projected_state in self.matches_cache:
            return self.matches_cache[partitioning_id][hashed_projected_state]
        else:
            all_states = [dict(s) for s in self.mdp.qTable]
            matching_states = (state for state in all_states
                               if state is not None
                               and projected_state == project(state, self.features_sets[partitioning_id])
            )
            matching_codes = {hash_dictionary(state) for state in matching_states}
            self.matches_cache[partitioning_id][hashed_projected_state] = matching_codes
            return matching_codes



    def choose_abstractions (
            self,
            s: HashedState,
            la: Iterable[Action]
    ) -> Tuple[List[str], Dict[Action, Tuple[int, float, float]]]:
        partitioning_id = None
        q_stats_row = None
        for partitioning_id in sorted(self.features_sets, key=lambda a: len(a), reverse=True):
            ss = self.find_siblings(partitioning_id, s)
            reliable, q_stats_row, __ = self.check_abstraction_via_t_test(ss, la)
            if reliable:
                break
        if partitioning_id is None:
            raise ValueError("No abstractions provided")
        return [partitioning_id], q_stats_row

    def check_abstraction_via_t_test (
            self,
            ss: Iterable[HashedState],
            la: Iterable[Action]
    ) -> Tuple[bool, Dict[Action, Tuple[int, float, float]], Dict[Action, float]]:
        """
        :param ss: list of states code
        :param la: legal actions code
        :return: whether the abstraction is reliable and second-order statistics
        """
        q_stats = {}
        best_actions, highest_mean_q = [], float('-inf')
        for a in la:
            stats = [self.mdp.qStats[(y, a)] for y in ss if (y, a) in self.mdp.qStats]
            q_stats[a] = combineStatistics(stats)
            mean_q = q_stats[a][1]
            if mean_q is not None:
                if mean_q > highest_mean_q:
                    highest_mean_q = mean_q
                    best_actions = [a]
                elif mean_q == highest_mean_q:
                    best_actions.append(a)
        if len(best_actions) == 0:
            return False, q_stats, {a: 1.0 for a in la}
        ba = sample1(best_actions)
        n_ba, m_ba, v_ba = q_stats[ba]
        s_ba = None if v_ba is None else sqrt(v_ba)
        p_values = {}
        for a in la:
            if a in best_actions:
                continue
            n_a, m_a, v_a = q_stats[a]
            if n_ba >= 2 and n_a >= 2:
                if v_ba != 0 and v_a != 0:
                    # noinspection PyTypeChecker
                    t, p = ttest_ind_from_stats(m_ba, s_ba, n_ba, m_a, sqrt(v_a), n_a, equal_var=False)
                    p_values[a] = p
                else:
                    p_values[a] = 1.0
            else:
                p_values[a] = 1.0
        if len(p_values) == 0:
            return True, q_stats, {a: 0.0 for a in la}
        max_p_value = max(p_values.values())
        return max_p_value / 2 <= self.significance, q_stats, p_values

    def check_abstraction_via_conf_int (
            self,
            ss: List[HashedState],
            la: Iterable[Action]
    ) -> Tuple[bool, Dict[Action, Tuple[int, float, float]]]:
        qStatsRows = {}
        lbs, means, ubs = {}, {}, {}
        for a in la:
            stats = [self.mdp.qStats[(y, a)] for y in ss if (y, a) in self.mdp.qStats]
            n, m, v = combineStatistics(stats)
            qStatsRows[a] = (n, m, v)
            means[a] = m
            lbs[a], ubs[a] = confidenceInterval(n, m, v, self.significance)
        if all([means[a] is None for a in la]):
            return True, qStatsRows
        baMean = max([m_ for m_ in means.values() if m_ is not None])
        ba = [a for a in la if means[a] == baMean]
        baLB = max(lbs[a] for a in la)  # Best action lower bound
        if all((baLB > ubs[a] or a in ba for a in la)):
            return True, qStatsRows
        else:
            return False, qStatsRows

    def get_aggregated_q_and_n_row (
            self,
            ss: Iterable[HashedState],
            la: Iterable[Action]
    ):
        # Get Q- and N-rows of "brother" states (just caching)
        b_q_rows, b_n_rows = {}, {}
        for y in ss:
            b_q_rows[y] = self.mdp.get_q_row(y, la)
            b_n_rows[y] = self.mdp.get_n_row(y, la)
        # Compute aggregated Q- and N-rows
        q_row, n_row = {}, {}
        visits = {y: sum(b_n_rows[y].values()) for y in ss}
        w = normalized_dict(visits)
        for a in la:
            q_row[a] = sum([b_q_rows[y][a] * w[y] for y in ss])
            n_row[a] = sum([b_n_rows[y][a] for y in ss])
        return q_row, n_row

    def get_decision_q_and_n_row (
            self,
            abstractions: Iterable[str],
            s: HashedState,
            la: Iterable[Action]
    ):
        q_row_abstractions = {a: [] for a in la}
        n_row_abstractions = {a: [] for a in la}
        for partitioning_id in abstractions:
            ss = self.find_siblings(partitioning_id, s)
            q_row_abstraction, n_row_abstraction = self.get_aggregated_q_and_n_row(ss, la)
            for a in la:
                q_row_abstractions[a].append(q_row_abstraction[a])
                n_row_abstractions[a].append(n_row_abstraction[a])
        agg_q_row, agg_n_row = {}, {}
        for a in la:
            agg_q_row[a] = sum(q_row_abstractions[a])
            #            agg_q_row[a] = sum([sign(q) * q * q for q in q_row_abstractions[a]])
            agg_n_row[a] = min(n_row_abstractions[a])
        return agg_q_row, agg_n_row
