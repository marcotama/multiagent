# -*- coding: utf-8 -*-
"""
Pacman agent using the Q-learning algorithm.

@author: Marco Tamassia
"""

from typing import Iterable, List, Optional
from reinf.common import Learner
from reinf.exploration_strategies import ExplorationParams, FixedRank, TargetQValue
from reinf.options.option import Option
from reinf.q_learning import QLearningAgent as QL
from util.typing2 import MyTypes as T
from util.typing2 import State, Action
from util.util2 import hash_dictionary



class ChallengeSensitiveActionSelectionAgent(Learner):


    def __init__ (
            self,
            index: int,
            evaluation_cycle: int,
            threshold: float,
            features: List[T.Feature],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            options: Iterable[Option],
            early_termination: bool,
            disable_primitive_actions: bool
    ):
        Learner.__init__(self)
        self.predictor = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        # Special exploration strategy that always chooses the action ranked in a given percentile
        self.action_selection = FixedRank(exploration_params)
        self.evaluation_cycle = evaluation_cycle
        self.threshold = threshold
        self.level = 0.5
        self.steps = 0

    def __str__ (self):
        return str(self.predictor)

    def learn (
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            reward: float,
            new_state: Optional[State],
            new_legal_actions: List[Action],
            own_decision: bool=True
    ):
        return self.predictor.learn(state, legal_actions, action, reward, new_state, new_legal_actions, own_decision)


    def get_action(
        self,
        state: State,
        legal_actions: List[Action],
        expected_outcome: float
    ):
        self.update_level(expected_outcome)
        self.action_selection.level = self.level
        hashed_state = hash_dictionary(state)
        action = self.predictor.get_mdp().choose_action(hashed_state, self.action_selection, legal_actions)
        return action

    def get_stakes_mdp (self):
        return self.predictor.mdp

    def end_of_episode (self, *args, **kwargs):
        self.predictor.end_of_episode()

    def update_level(self, expected_outcome: float):
        if self.steps % self.evaluation_cycle == 0:
            # outcome=+1 -> adaptive agent wins, outcome=-1 -> opponent wins
            if expected_outcome > +self.threshold:
                self.level -= 0.1
            elif expected_outcome < -self.threshold:
                self.level += 0.1
        self.level = min(1.0, max(0.0, self.level))

        self.steps += 1





class QValueSensitiveActionSelectionAgent(Learner):

    def __init__ (
            self,
            index: int,
            features: List[T.Feature],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            options: Iterable[Option],
            early_termination: bool,
            disable_primitive_actions: bool
    ):
        Learner.__init__(self)
        self.predictor = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        self.action_selection = TargetQValue(exploration_params)
        self.action_selection.target = 0.0

    def __str__ (self):
        return str(self.predictor)

    def learn (
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            reward: float,
            new_state: Optional[State],
            new_legal_actions: List[Action],
            own_decision: bool=True
    ):
        return self.predictor.learn(state, legal_actions, action, reward, new_state, new_legal_actions, own_decision)


    def get_action(
        self,
        state: State,
        legal_actions: List[Action]
    ):
        hashed_state = hash_dictionary(state)
        action = self.predictor.get_mdp().choose_action(hashed_state, self.action_selection, legal_actions)
        return action

    def update_target(self, target):
        self.action_selection.target = target

    def get_mdp (self):
        return self.predictor.mdp

    def end_of_episode (self, *args, **kwargs):
        self.predictor.end_of_episode()







class StakesSensitiveActionSelectionAgent(Learner):

    def __init__ (
            self,
            index: int,
            features: List[T.Feature],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            options: Iterable[Option],
            early_termination: bool,
            disable_primitive_actions: bool
    ):
        Learner.__init__(self)
        # Normal Q-learning
        self.reward_predictor = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        # Also Q-learning, but instead of rewards, it is fed stakes (difference between best and worst action)
        self.stakes_predictor = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions
        )
        self.action_selection = TargetQValue(exploration_params)
        self.action_selection.target = 0.0


    def get_stakes(
            self,
            state: State,
            legal_actions: List[Action]
    ):
        hashed_state = hash_dictionary(state)
        q_row = self.reward_predictor.get_mdp().get_q_row(hashed_state, legal_actions)
        stakes = max(q_row.values()) - min(q_row.values())
        return stakes


    def __str__ (self):
        return str(self.reward_predictor) + '\n\n' + str(self.stakes_predictor)


    def learn (
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            reward: float,
            new_state: Optional[State],
            new_legal_actions: List[Action],
            own_decision: bool=True
    ):
        self.reward_predictor.learn(state, legal_actions, action, reward, new_state, new_legal_actions, own_decision)
        return self.stakes_predictor.learn(state, legal_actions, action, self.get_stakes(state, legal_actions), new_state, new_legal_actions, own_decision)


    def get_action (
            self,
            state: State,
            legal_actions: List[Action]
    ):
        self.stakes_predictor.get_mdp().choose_action(state, self.action_selection, legal_actions)


    def get_reward_mdp (self):
        return self.reward_predictor.mdp

    def get_stakes_mdp (self):
        return self.stakes_predictor.mdp

    def end_of_episode (self, *args, **kwargs):
        self.reward_predictor.end_of_episode()
        self.stakes_predictor.end_of_episode()