# -*- coding: utf-8 -*-
"""
Implementation of various exploration strategies.

@author: Marco Tamassia
"""
import random
from abc import abstractmethod, ABCMeta
from functools import partial
from math import exp, sqrt, log
from typing import Dict, Tuple, Iterable, NamedTuple, Optional, List, Union

import numpy as np
from util.typing2 import Action
from util.sorting import arg_sort, arg_max, arg_min

from util.util2 import weightedChoice, sigmoid, confidenceInterval

ExplorationParams = NamedTuple('ExplorationParams', [
    ('exploration_strategy', str),
    ('epsilon', float),
    ('epsilon_ci', float),
    ('cp', float),
    ('significance', float),
    ('annealing_variable', str),
    ('init_temperature', float),
    ('start_descent', int),
    ('end_descent', int)
])

def sample1(
    choices: Iterable[Action]
) -> Action:
    if not (isinstance(choices, list) or isinstance(choices, list)):
        choices = list(choices)
    return random.sample(choices, 1)[0]


def sampleMax(
    qRow: Dict[Action, float]
) -> Action:
    maxQ = max(qRow.values())
    actionsWithMaxQ = [a for a, q in qRow.items() if q == maxQ]
    return sample1(actionsWithMaxQ)


class ExplorationStrategy(metaclass=ABCMeta):
    @abstractmethod
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: q-value for each state-action pair
        """
        self.explParams = explParams

    @abstractmethod
    def decide(
        self,
        **kwargs  # Useless args
    ) -> Action:
        pass


class Random(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; none are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)

    def decide(
        self,
        qRow: Dict[Action, float],
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns a random action.
        :param qRow: q-value for each state-action pair
        :param kwargs: every other parameter will be ignored
        """
        a = sample1(qRow)
        return a


class Greedy(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams=None
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; none are is used by this method
        """
        ExplorationStrategy.__init__(self, explParams)

    def decide(
        self,
        qRow: Dict[Action, float],
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns the action with the highest Q-value (if more than one action
        has maximum Q-value, one is chosen at random).
        :param qRow: q-value for each state-action pair
        :param kwargs: every other parameter will be ignored
        """
        return sampleMax(qRow)


class EpsilonGreedy(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only epsilon is used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.epsilon = explParams.epsilon

    def decide(
        self,
        qRow: Dict[Action, float],
        epsilon: float=None,
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the epsilon-greedy strategy.
        :param qRow: q-value for each state-action pair
        :param kwargs: every other parameter will be ignored
        :param epsilon: probability of performing a random action (this is annealed over time)
        """
        if epsilon is None:
            epsilon = self.epsilon
        if np.random.rand(1,1) > epsilon:
            a = sampleMax(qRow)
        else:
            a = sample1(qRow)
        return a


class AnnealingEpsilonGreedy(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only epsilon, start_descent, end_descent and annealing_variable are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.epsilonGreedyStrategy = EpsilonGreedy(explParams)
        self.epsilonSchedule = generateSchedule(explParams.start_descent, explParams.end_descent, 0, explParams.epsilon)
        self.annealing_variable = explParams.annealing_variable

    def decide(
        self,
        qRow: Dict[Action, float],
        nRow: Dict[Action, float],
        eps: int,
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the epsilon-greedy strategy where the
        value of epsilon is annealed over time.
        :param qRow: q-value for each state-action pair
        :param nRow: number of times each state-action pair has been experienced
        :param eps: number of episodes
        :param kwargs: every other parameter will be ignored
        """
        if self.annealing_variable == 'visits':
            epsilon = self.epsilonSchedule(min(nRow.values()))
        elif self.annealing_variable == 'episodes':
            epsilon = self.epsilonSchedule(eps)
        else:
            raise ValueError("Unknown annealing variable: %s" % self.annealing_variable)
        return self.epsilonGreedyStrategy.decide(qRow, epsilon)


class EpsilonCIGreedy(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only epsilon, epsilonCI and significance are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.epsilon = explParams.epsilon
        self.epsilonCI = explParams.epsilonCI
        self.significance = explParams.significance

    def decide(
        self,
        qRow: Dict[Action, float],
        qStatsRow: Dict[Action, Tuple[int, float, float]],
        epsilon: float=None,
        epsilonCI: float=None,
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the epsilonCI-greedy strategy.
        :param qRow: q-value for each state-action pair
        :param qStatsRow: number of samples, mean and variance for the q-value of each state-action pair
        :param epsilon: probability of performing a random action (this is annealed over time)
        :param epsilonCI: probability of performing an action to strategically reduce confidence intervals (this is annealed over time)
        :param kwargs: every other parameter will be ignored
        """
        if epsilon is None:
            epsilon = self.epsilon
        if epsilonCI is None:
            epsilonCI = self.epsilonCI
        r = np.random.rand(1,1)
        if r > epsilon + epsilonCI:
            a = sampleMax(qRow)
        elif r < epsilon:
            a = sample1(qRow)
        else:
            actionsNeedingMoreSamples = []
            lbs, means, ubs = {}, {}, {}
            for a in qStatsRow:
                n, m, v = qStatsRow[a]
                means[a] = m
                lbs[a], ubs[a] = confidenceInterval(n, m, v, self.significance)
                if m is None or v is None:
                    actionsNeedingMoreSamples.append(a)
            if actionsNeedingMoreSamples:
                a = sample1(actionsNeedingMoreSamples)
            else:
                baMean = max([means[a] for a in qStatsRow])
                ba = [a for a in qStatsRow if means[a] == baMean]
                baLB = max(lbs[a] for a in qStatsRow) # Best action lower bound
                K1 = [a for a in qStatsRow if a not in ba and ubs[a] > baMean]
                K2 = [a for a in qStatsRow if a not in ba and means[a] > baLB]
                if K1 and K2:
                    a = sample1(K1 + ba)
                elif K1:
                    a = sample1(K1)
                else:
                    a = sample1(ba)
        return a


class AnnealingEpsilonCIGreedy(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only epsilon, start_descent, end_descent and annealing_variable are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.epsilonCIGreedyStrategy = EpsilonCIGreedy(explParams)
        self.epsilonSchedule = generateSchedule(explParams.start_descent, explParams.end_descent, 0, explParams.epsilon)
        self.epsilonCISchedule = generateSchedule(explParams.start_descent, explParams.end_descent, 0, explParams.epsilonCI)
        self.annealing_variable = explParams.annealing_variable

    def decide(
        self,
        qRow: Dict[Action, float],
        nRow: Dict[Action, float],
        qStatsRow: Dict[Action, Tuple[int, float, float]],
        eps: int,
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the epsilon-greedy strategy where the
        value of epsilon is annealed over time.
        :param qRow: q-value for each state-action pair
        :param nRow: number of times each state-action pair has been experienced
        :param qStatsRow: number of samples, mean and variance for the q-value of each state-action pair
        :param eps: number of episodes
        :param kwargs: every other parameter will be ignored
        """
        if self.annealing_variable == 'visits':
            epsilonR = self.epsilonSchedule(min(nRow.values()))
            epsilonCI = self.epsilonCISchedule(min(nRow.values()))
        elif self.annealing_variable == 'episodes':
            epsilonR = self.epsilonSchedule(eps)
            epsilonCI = self.epsilonCISchedule(eps)
        else:
            raise ValueError("Unknown annealing variable: %s" % self.annealing_variable)
        return self.epsilonCIGreedyStrategy.decide(qRow, qStatsRow, epsilonR, epsilonCI)


class UCT(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only Cp are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.Cp = explParams.Cp

    def decide(
        self,
        qRow: Dict[Action, float],
        nRow: Dict[Action, int],
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the UCT strategy.
        :param qRow: q-value for each state-action pair
        :param nRow: number of times each state-action pair has been experienced
        :param kwargs: every other parameter will be ignored
        """
        n = sum(nRow.values())
        if n == 0:
            a = sample1(qRow)
        else:
            # For readability
            c = self.Cp
            qs, ns = qRow, nRow
            uct = {s: float("inf") if ns[s] == 0 else qs[s] + c * sqrt(2 * log(n + 1) / ns[s]) for s in qs}
            a = sampleMax(uct)
        return a


class Softmax(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; only Cp are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.temperatureSchedule = generateSchedule(explParams.start_descent, explParams.end_descent, 0, explParams.init_temperature)
        self.annealing_variable = explParams.annealing_variable

    def decide(
        self,
        qRow: Dict[Action, float],
        nRow: Dict[Action, int],
        eps: int,
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns an action according to the Softmax strategy.
        :param qRow: q-value for each state-action pair
        :param nRow: number of times each state-action pair has been experienced
        :param eps: number of episodes
        :param kwargs: every other parameter will be ignored
        """
        if self.annealing_variable == 'visits':
            temperature = self.temperatureSchedule(sum(nRow.values())/float(len(nRow)))
        elif self.annealing_variable == 'episodes':
            temperature = self.temperatureSchedule(eps)
        else:
            raise ValueError("Unknown annealing variable: %s" % self.annealing_variable)
        try:
            weights = {a: exp(q / temperature) for a, q in qRow.items()}
            a = weightedChoice(weights)
        except (ValueError, ZeroDivisionError, OverflowError):
            a = sampleMax(qRow)
        return a


class FixedRank(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams,
        whitelisted_choices: Optional[List[Union[int, str]]]=None
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; none are used by this method
        :param whitelisted_choices: only possible choices to be returned
        """
        ExplorationStrategy.__init__(self, explParams)
        self.level = 0.5
        self.whitelisted_choices = whitelisted_choices

    def decide(
        self,
        qRow: Dict[Action, float],
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns a random action.
        :param qRow: q-value for each state-action pair
        :param kwargs: every other parameter will be ignored
        """
        if self.whitelisted_choices is not None:
            qRow = {a: q for a, q in qRow.items() if a in self.whitelisted_choices}
        sorted_actions = arg_sort(qRow) # ascending
        i = min(int(self.level * len(qRow)), len(qRow) - 1)
        return sorted_actions[i]


class TargetQValue(ExplorationStrategy):
    def __init__(
        self,
        explParams: ExplorationParams
    ):
        """
        Returns a random action.
        :param explParams: exploration parameters; none are used by this method
        """
        ExplorationStrategy.__init__(self, explParams)
        self.target = 0.0

    def decide(
        self,
        qRow: Dict[Action, float],
        **kwargs  # Useless args
    ) -> Action:
        """
        Returns a random action.
        :param qRow: q-value for each state-action pair
        :param kwargs: every other parameter will be ignored
        """
        # if possible, choose an action right above target
        if any(q > self.target for q in qRow.values()):
            diffs = {a: q - self.target for a, q in qRow.items() if q > self.target}
            return arg_min(diffs)
        # otherwise, if all are inferior, choose the best (i.e., the closest)
        else:
            return arg_max(qRow)


def generateSchedule(start_descent, end_descent, minV, maxV):
    mid = (start_descent + end_descent) / 2
    width = end_descent - start_descent
    schedule = partial(sigmoid, mid, width, minV, maxV, True)
    return schedule


def getExplorationStrategy(
    explParams: ExplorationParams
) -> ExplorationStrategy:
    """
    Returns an action according to the exploration-exploitation strategy.
    :param explParams: Exploration strategy parameters
    """
    module = __import__('reinf')
    submodule = getattr(module, 'exploration_strategies')
    cls = getattr(submodule, explParams.exploration_strategy)
    return cls(explParams)


explorationStrategies = [
    'EpsilonGreedy',
    'EpsilonCIGreedy',
    'UCT',
    'AnnealingEpsilonGreedy',
    'AnnealingEpsilonCIGreedy',
    'Softmax',
    'Greedy',
    'Random'
]