from reinf.exploration_strategies import getExplorationStrategy, ExplorationParams


class Explorator:
    def __init__(
        self,
        exploration_params: ExplorationParams
    ):
        self.explStrategy = getExplorationStrategy(exploration_params)
        greedyParams = ExplorationParams(
            exploration_strategy='Greedy',
            epsilon=0.0,
            epsilon_ci=0.0,
            cp=0.0,
            significance=0.0,
            annealing_variable='',
            init_temperature=0.0,
            start_descent=0,
            end_descent=0
        )
        self.greedyStrategy = getExplorationStrategy(greedyParams)
        self.beGreedy = False

    def get_expl_strategy(self):
        if self.beGreedy:
            return self.greedyStrategy
        else:
            return self.explStrategy

    def setExplStrategy(
        self,
        explParams: ExplorationParams
    ):
        self.explStrategy = getExplorationStrategy(explParams)

    def explore(self):
        self.beGreedy = False

    def exploit(self):
        self.beGreedy = True


class Learner:
    def __init__(self):
        self.learning = True

    def doLearn(self):
        self.learning = True

    def dontLearn(self):
        self.learning = False

    def isLearning(self):
        return self.learning