from util.vector import Vector, SparseVector
from typing import List, Callable, Tuple, Optional, Dict
from collections import OrderedDict
from util.state_action_encoder import StateActionEncoder
from reinf.predictor import Predictor, LearnerPredictor
from reinf.gq import GQ, GreedyGQ
from reinf.controller import LearnerController
from util.trace import Trace
from util.functions import StateActionNewStateFn
from reinf.policy.discrete import Policy
import multiprocessing
from joblib import Parallel, delayed
import pickle
from abc import abstractmethod


class Horde:

    def __init__(self, behavior_factory: Callable[[Predictor], Policy]):
        self.daemons = OrderedDict()  # type: Dict[str, LearnerPredictor]
        self.behavior = behavior_factory(self.daemons)

    @abstractmethod
    def learn (self, x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t=None) -> Vector: pass

    @abstractmethod
    def propose_action (self, x: Vector, la: List[int], label: str) -> int: pass

    @abstractmethod
    def compute_value_function (self, x: Vector, la: List[int], label: str) -> float: pass

    def end_of_episode (self):
        for learner in self.daemons.values():
            learner.end_of_episode()
        self.behavior.tick()

    def save (self, file_path: str, label: Optional[str]):
        if label is None:
            with open(file_path, 'rb'):
                pickle.dump(self.daemons)
        else:
            gq, target, reward_fn, gamma_fn = self.daemons[label]
            with open(file_path, 'rb'):
                pickle.dump(gq, target, reward_fn, gamma_fn)


class HordeOfGQ(Horde):
    """
    A set of GQ agents, each learning off-policy from a different reward function and lambda
    """

    def __init__ (self, behavior_factory: Callable[[Predictor], Policy], actions: List[int],
                  state_action_encoder: StateActionEncoder,
                  daemons_params: List[Tuple[
                      str, # identifier
                      float, # alpha
                      float, # eta
                      Callable[[], Trace], # trace factory
                      Callable[[Predictor], Policy], # target factory
                      StateActionNewStateFn, # lambda function
                      StateActionNewStateFn, # gamma function
                      StateActionNewStateFn, # interest function
                      StateActionNewStateFn, # reward function
                  ]],
                  parallel_updates: Optional[bool]=True):
        super().__init__(behavior_factory)
        self.parallel_updates = parallel_updates
        if parallel_updates:
            self.parallel = None

        for label, alpha, eta, trace_factory, target_factory, lambda_fn, gamma_fn, interest_fn, reward_fn in daemons_params:
            gq = GQ(state_action_encoder.vector_size(), alpha, eta, trace_factory, target_factory,
                     state_action_encoder, gamma_fn, lambda_fn, interest_fn, reward_fn)
            self.daemons[label] = gq

        self.rho_t = 0.0
        self.actions = actions
        self.state_action_encoder = state_action_encoder
        self.phi_bar_tp1 = SparseVector(state_action_encoder.out_vec_size)

    def end_of_episode (self):
        super().end_of_episode()
        self.parallel = None  # We do this so this object is picklable at the end of an episode

    # noinspection PyTypeChecker
    def learn (self, x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t=None, phis=None) -> Vector:
        if phis is None:
            phis = self.state_action_encoder.state_actions(x_t, la_t)
        if self.parallel_updates:
            if self.parallel is None:
                num_cores = multiprocessing.cpu_count()
                self.parallel = Parallel(n_jobs=num_cores, backend='threading')

            self.parallel(delayed(gq.learn)(x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t, phis=phis)
                          for gq in self.daemons.values())
        else:
            for gq in self.daemons.values():
                gq.learn(x_t, la_t, a_t, x_tp1, la_tp1, bpi_t, r_t, phis=phis)

    def propose_action (self, x: Vector, la: List[int], label: str) -> int:
        return self.daemons[label].sample_action(x, la)

    def compute_value_function (self, x: Vector, la: List[int], label: str) -> float:
        gq, target, _, _ = self.daemons[label]
        phis = self.state_action_encoder.state_actions(x, la)
        target._update(phis)
        v_s = 0.0
        # V(s) = \sum_{a \in A} \pi(s,a) * Q(s,a)
        for a in self.actions:
            v_s += target._pi(a) * gq.predict(phis[a])
        return v_s


class HordeOfGQControlledByGreedyGQ(LearnerController):

    def __init__ (
        self,
        target_factory: Callable[[Predictor], Policy],
        behavior_factory: Callable[[Predictor], Policy],
        actions: List[int],
        state_action_encoder: StateActionEncoder,
        daemons_params: List[Tuple[
            str, # identifier
            float, # alpha
            float, # eta
            Callable[[], Trace], # trace factory
            Callable[[Predictor], Policy], # target factory
            StateActionNewStateFn, # lambda function
            StateActionNewStateFn, # gamma function
            StateActionNewStateFn, # interest function
            StateActionNewStateFn, # reward function
        ]],
        alpha: float,
        eta: float,
        trace_factory: Callable[[], Trace],
        gamma_fn: StateActionNewStateFn,
        lambda_fn: StateActionNewStateFn,
        interest_fn: StateActionNewStateFn,
        reward_fn: Optional[StateActionNewStateFn]=None
        ):
        self.state_action_encoder = state_action_encoder
        self.horde = HordeOfGQ(behavior_factory, actions, state_action_encoder, daemons_params)
        self.ggq = GreedyGQ(alpha, eta, trace_factory, target_factory, behavior_factory, actions, state_action_encoder,
                            gamma_fn, lambda_fn, interest_fn, reward_fn)

    def learn (self, x_t, la_t, a_t, x_tp1, la_tp1, r_t=None) -> Vector:
        phis = self.state_action_encoder.state_actions(x_t, la_t)
        bpi_t = self.ggq.behavior.pi(a_t, phis)
        self.horde.learn(x_t, la_t, a_t, x_tp1, la_tp1, bpi_t)
        self.ggq.learn(x_t, la_t, a_t, x_tp1, la_tp1, r_t)

    def propose_greedy_action (self, x: Vector, la: List[int]) -> int:
        return self.ggq.propose_greedy_action(x, la)

    def propose_exploratory_action (self, x: Vector, la: List[int]) -> int:
        return self.ggq.propose_exploratory_action(x, la)

    def end_of_episode (self):
        self.horde.end_of_episode()
        self.ggq.end_of_episode()
