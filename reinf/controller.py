from util.vector import Vector
from typing import List, Optional
from abc import abstractmethod


class Controller:

    @abstractmethod
    def propose_greedy_action(self, x: Vector, la: List[int]) -> int:
        pass

    @abstractmethod
    def propose_exploratory_action(self, x: Vector, la: List[int]) -> int:
        pass


class LearnerController(Controller):

    @abstractmethod
    def learn(
        self,
        x_t: Vector,
        la_t: List[int],
        a_t: int,
        x_tp1: Vector,
        la_tp1: List[int],
        r_t: Optional[float],
    ):
        pass

    @abstractmethod
    def end_of_episode (self):
        pass

    def step(
        self,
        x_t: Vector,
        la_t: List[int],
        a_t: int,
        x_tp1: Vector,
        la_tp1: List[int],
        r_t: Optional[float]=None
    ) -> int:
        self.learn(x_t, la_t, a_t, x_tp1, la_tp1, r_t)
        return self.propose_exploratory_action(x_tp1, la_tp1)
