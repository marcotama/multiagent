# -*- coding: utf-8 -*-
"""
Runs multiple episodes of Pacman with a learning agent.

@author: Marco Tamassia
"""
import sys
import time
import timeit
import traceback

from typing import List, Tuple

from pacman import graphics_display
from pacman import text_display
from pacman.data_structures import EpisodeData
from pacman.game import Agent
from pacman.layout import Layout
from pacman.pacman import ClassicGameRules


class GamesRunner:

    def __init__(
        self,
        pacman_agent: Agent,
        ghost_agents: List[Agent],
        layout: Layout,
        episodes: int,
        test_greedily: bool=True,
        quiet: bool=False,
        graphics: str='null',
        graphics_after: int=0,
        frame_time: float=0.05,
        save_moves_histories: bool=True,
        show_countdown: bool=False
    ):
        self.episodes = episodes
        self.layout = layout
        self.initialized = False
        self.graphics = graphics
        self.graphicsAfter = graphics_after
        self.pacmanAgent = pacman_agent  # type: Agent
        self.ghostAgents = ghost_agents # type: List[Agent]
        self.frameTime = frame_time
        self.saveMovesHistories = save_moves_histories
        self.testGreedily = test_greedily
        self.quiet = quiet
        self.currentEpisode = 0
        self.display = None
        self.showCountdown = show_countdown

    def signalAgentsToLearn(self):
        agent = self.pacmanAgent
        if hasattr(agent, 'explore') and callable(agent.explore):
            agent.explore()
        if hasattr(agent, 'doLearn') and callable(agent.doLearn):
            agent.doLearn()

    def signalAgentsToBeGreedy(self):
        agent = self.pacmanAgent
        if hasattr(agent, 'exploit') and callable(agent.exploit):
            agent.exploit()
        if hasattr(agent, 'dontLearn') and callable(agent.dontLearn):
            agent.dontLearn()

    def signalAgentsEpisodeIsOver(self):
        agent = self.pacmanAgent
        if hasattr(agent, 'episodeIsOver') and callable(agent.episodeIsOver):
            agent.episodeIsOver()

    def test(self):
        epsData = []
        records = []
        agent = self.pacmanAgent
        if hasattr(agent, 'featuresExtractor'):
            agent.featuresExtractor.set_layout(self.layout)

        self.signalAgentsToLearn()
        for e in range(self.episodes):

            self.currentEpisode = e

            if self.testGreedily:
                self.signalAgentsToLearn()
                self.runEpisode()
                self.signalAgentsToBeGreedy()

            epData, layout, moveHistory = self.runEpisode()
            self.signalAgentsEpisodeIsOver()

            epsData.append(epData)
            if self.saveMovesHistories:  # This 'if' saves memory
                records.append((layout, moveHistory))

        if not self.quiet:
            # noinspection PyBroadException
            try:
                print()
                print()
                print(self.pacmanAgent)
                for ghostAgent in self.ghostAgents:
                    print()
                    print()
                    print(ghostAgent)
            except:
                pass
        return epsData, records

    def runEpisode(
        self
    ) -> Tuple[EpisodeData, Layout, List[Tuple[int, int]]]:
        self.initialized = True
        rules = ClassicGameRules()

        if self.graphics == 'null' or self.currentEpisode < self.graphicsAfter:
            self.display = text_display.NullGraphics()
        elif self.graphics == 'full':
            self.display = graphics_display.PacmanGraphics(1, frameTime=self.frameTime)
        elif self.graphics == 'text':
            text_display.SLEEP_TIME = self.frameTime
            self.display = text_display.PacmanGraphics()


        retryCount = 3
        total_time = None
        game = None
        while retryCount > 0:
            game = rules.newGame(self.layout, self.pacmanAgent, self.ghostAgents, self.display, quiet=True)

            # noinspection PyBroadException
            try:
                if self.graphics != 'null' and self.currentEpisode >= self.graphicsAfter and self.showCountdown:
                    for i in range(3, 0, -1):
                        print("Next game in %d..." % i, end='\r')
                        time.sleep(1)
                    print("Start!" + " " * 20, end='\r')
                start = timeit.default_timer()
                game.run()
                stop = timeit.default_timer()
                total_time = stop - start
                game.state.getAndResetExplored()
                break
            except KeyboardInterrupt:
                sys.exit(0)
            except:
                traceback.print_exc()
                retryCount -= 1
                if retryCount > 0:
                    sys.stdout.write("Game failed. Trying again.\n")
                else:
                    sys.stdout.write("Game failed too many times. Quitting.\n")
                

        data = EpisodeData(
            episode=self.currentEpisode,
            outcomeStr='WON' if game.state.isWin() else 'LOST',
            outcome=1 if game.state.isWin() else 0,
            score=game.state.getScore(),
            movesCount=game.numMoves,
            statesCount=self.pacmanAgent.getStatesCount() if hasattr(self.pacmanAgent, 'getStatesCount') else None,
            stateActionPairsCount=self.pacmanAgent.getStateActionPairsCount() if hasattr(self.pacmanAgent, 'getStateActionPairsCount') else None,
            abstractionsUsage=self.pacmanAgent.getAbstractionsUsage() if hasattr(self.pacmanAgent, 'getAbstractionsUsage') else None,
            executionTime=total_time,
            eatenGhostsCount=game.state.data.eatenGhostsCount,
            eatenFoodCount=game.state.data.eatenFoodCount,
            eatenCapsulesCount=game.state.data.eatenCapsulesCount
        )

        if not self.quiet:
            print("Episode #{episode}, {outcomeStr}, score: {score}, moves: {movesCount}".format(**data._asdict()))

        if retryCount == 0:
            sys.exit(1)
        else:
            return data, game.state.data.layout, game.moveHistory

