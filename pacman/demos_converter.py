# -*- coding: utf-8 -*-
"""
Utility class to transform a Pacman demo to a MDP trace.
Demo format: (layout, [(agent_id, action) x N_agents x N_demo_steps])
Trace format: (state, legalActions, action, reward)

@author: Marco Tamassia
"""
from typing import Tuple, List

from pacman.game import ClassicGameRules, Direction
from pacman import text_display
from pacman.agents import simple_ghost_agents
from pacman.layout import Layout
from pacman.agents.q_learning_agents import QLearningAgent



class PacmanDemosConverter:
    def __init__(
        self,
        agent: QLearningAgent,
    ):
        self.agent = agent


    def iterDemo (
        self,
        demo: Tuple[Layout, List[Tuple[int, Direction]]]
    ):
        """
        Takes a demo as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demos are formatted as follows: (layout, [(agent_id, action) x N_demo_steps])
        """
        rules = ClassicGameRules()

        # Load game info
        layout, movesHistory = demo
        if 'setLayout' in dir(self.agent):
            self.agent.set_layout(layout)

        # Setup agents
        agents = [self.agent]
        agents += [simple_ghost_agents.RandomGhost(i + 1) for i in range(layout.getNumGhosts())]

        # Setup new game
        game = rules.newGame(layout, agents[0], agents[1:], text_display.NullGraphics(), quiet=True)
        gameState = game.state

        # Iterate through moves
        score = 0
        for agentIndex, newAction in movesHistory:
            if agentIndex == 0:
                if 'observationFunction' in dir(self.agent):
                    newState, newScore, newLegalActions = self.agent.observation_fn(gameState)
                else:
                    newState = gameState
                    newScore = gameState.getScore()
                    newLegalActions = gameState.getLegalActions(agentIndex)
                yield newState, newLegalActions, newAction, newScore - score
                score = newScore

            # Update game state
            gameState = gameState.generateSuccessor(agentIndex, newAction)
            rules.process(gameState, game)

        # Pass info about the final step to the agent
        if 'observationFunction' in dir(self.agent):
            _, newScore, _ = self.agent.observation_fn(gameState)
        else:
            newScore = gameState.getScore()
        yield None, [], None, newScore - score


    def iterDemoWithNextState(
        self,
        demo: Tuple[Layout, List[Tuple[int, Direction]]]
    ):
        prev = None
        for ns, nla, na, nr in self.iterDemo(demo):
            if prev is not None:
                s, la, a, r = prev
                yield s, la, a, r, ns, nla
            prev = ns, nla, na, nr


    def iterDemos(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Direction]]]]
    ):
        """
        Takes a demo file as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demo file must be formatted as follows: [(layout, [(agent_id, action) x N_demo_steps]) x N_demos]
        """
        for i, demo in enumerate(demos):
            for s, la, a, r in self.iterDemo(demo):
                yield s, la, a, r


    def iterDemosWithNextState(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Direction]]]]
    ):
        """
        Takes a demo file as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demo file must be formatted as follows: [(layout, [(agent_id, action) x N_demo_steps]) x N_demos]
        """
        for demo in demos:
            for s, la, a, r, ns, nla in self.iterDemoWithNextState(demo):
                yield s, la, a, r, ns, nla


    def demoToTrace(
        self,
        demo: Tuple[Layout, List[Tuple[int, Direction]]]
    ):
        return list(self.iterDemo(demo))


    def demosToTraces(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Direction]]]]
    ):
        return [self.demoToTrace(demo) for demo in demos]


    def demoToMDPTrace(
        self,
        demo: Tuple[Layout, List[Tuple[int, Direction]]]
    ):
        if 'qAgent' in dir(self.agent):
            enc_s = self.agent.qAgent.stateTranslator.getCode
            enc_a = self.agent.qAgent.actionTranslator.getCode
        else:
            enc_s = lambda x: x
            enc_a = lambda x: x
        return [(enc_s(s), [enc_a(a_) for a_ in la], enc_a(a), r) for s, la, a, r in self.iterDemo(demo)]


    def demosToMDPTraces(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Direction]]]]
    ):
        return [self.demoToMDPTrace(demo) for demo in demos]