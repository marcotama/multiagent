# -*- coding: utf-8 -*-
"""
Features extractor for Pac-Man

@author: Marco Tamassia
"""
from functools import partial
import re
import math
from typing import List, Tuple, Optional, Union, Dict

from pacman.game import Direction, AgentState, Actions, SCARED_TIME, GameState
from pacman.layout import Layout
from util.maze_utils import Maze
from util.sorting import arg_min


class PacmanFeaturesExtractor:

    _reps = [
        ('..', 'nearest[2]-'),
        ('.', 'nearest[1]-'),
        ('~', 'agent'),
        ('Pm', 'agent[0]'),
        ('NScGh', 'non_scared_ghost'),
        ('ScGh', 'scared_ghost'),
        ('Gh', 'ghost'),
        ('Fd', 'food'),
        ('Cp', 'capsule'),
        ('SfJn', 'safe_junction'),
        ('SpDs', '-spawn_point_sp_distance'),
        ('Dr', '-shortest_path_direction'),
        ('Ds', '-shortest_path_distance'),
        ('Tm', '-scared_timer'),
        ('#', '-counter'),
        ('Dv', 'shortest_path_distance_to_nearest-'),
        ('@', '-via-'),
        ('Nt', 'north'),
        ('St', 'south'),
        ('Wt', 'west'),
        ('Et', 'east'),
    ]

    @staticmethod
    def get_long_name (name: str):
        for short, long in PacmanFeaturesExtractor._reps:
            name = name.replace(short, long)
        return name

    @staticmethod
    def get_short_name(name: str):
        for short, long in PacmanFeaturesExtractor._reps:
            name = name.replace(long, short)
        return name

    def __init__(self, features: List[str], agent_index: int):

        self.entities = {
            'pacman'          : self.get_pacmans,
            'ghost'           : self.get_ghosts,
            'scared_ghost'    : self.get_scared_ghosts,
            'non_scared_ghost': self.get_nonscared_ghosts,
            'capsule'         : self.get_capsules,
            'food'            : self.get_food,
            'safe_junction'   : self.get_safe_junctions
        }

        self.attributes = {
            'coordinates'             : self.get_coordinates,
            'x'                       : self.get_x,
            'y'                       : self.get_y,
            'shortest_path_distance'  : self.dijkstra_distance_to_self,
            'shortest_path_direction' : self.dijkstra_direction_to_self,
            'scared_timer'            : self.get_scared_timer,
            'spawn_point_sp_distance' : self.get_spawn_point_shortest_path_distance
        }

        self.directions = ['north','east','south','west']

        self.admissiblePatterns = {
            'self_coordinates'         : re.compile(r'self_(x|y)'),
            'self_score'               : re.compile(r'self_score'),
            'legal_moves'              : re.compile(r'legal-(%s)' % '|'.join(self.directions)),
            'objects'                  : re.compile(r'(%s)-(%s|counter)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))),
            'nearest_object'           : re.compile(r'nearest\[([0-9]+)\]-(%s)-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))),
            'agent'                    : re.compile(r'agent\[([0-9]+)\]-(%s)(:?)' % '|'.join(self.attributes)),
            'self'                     : re.compile(r'agent-(%s)(:?)' % '|'.join(self.attributes)),
            'dist_to_obj_via_direction': re.compile(r'shortest_path_distance_to_nearest-(%s)-via-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.directions)))
        }

        self.desired_features = features

        self.mazes = {}
        self.maze = None  # type: Maze
        self.layout = None  # type: Layout
        self.gameState = None  # type: GameState
        self.agent_index = agent_index
        self.reference_index = agent_index

    def admissible(self, feature):
        return any(re.match(pattern, feature) for pattern in self.admissiblePatterns.values())

    def clear_cache(self):
        self.maze.clear_cache()

    @staticmethod
    def identity(*args):
        return args

    @staticmethod
    def get_agent_position(agent: AgentState) -> Tuple[int, int]:
        x, y = agent.getPosition()
        d = agent.getDirection()
        if d == Direction.north:
            x, y = int(x), int(math.floor(y))
        elif d == Direction.south:
            x, y = int(x), int(math.ceil(y))
        elif d == Direction.east:
            x, y = int(math.floor(x)), int(y)
        elif d == Direction.west:
            x, y = int(math.ceil(x)), int(y)
        return x, y

    def arg_euclidean_nearest_to_self(self, entities, rank=1):
        return self.arg_nearest_to(entities, self.euclidean_distance_to_self, k=rank, zero_allowed=False)

    def arg_dijkstra_nearest_to_self(self, entities, rank=1):
        return self.arg_nearest_to(entities, self.dijkstra_distance_to_self, k=rank, zero_allowed=False)

    def arg_dijkstra_nearest_to_self_p1(self, entities, rank=1):
        return self.arg_nearest_to(entities, self.dijkstra_distance_to_self, k=rank, zero_allowed=True)

    @staticmethod
    def arg_nearest_to(entities, distance_measure, k=1, zero_allowed=False) -> Optional[float]:
        # handle trivial cases manually, save computation
        if len(entities) == 0:
            return None
        if len(entities) == 1:
            return 0 # first element

        distances = [distance_measure(e) for e in entities]
        distances = [float('+inf') if d is None else d for d in distances]
        distances = [float('+inf') if not zero_allowed and d == 0 else d for d in distances]
        if all(math.isinf(d) for d in distances):
            return None
        else:
            for _ in range(k-1):
                distances[arg_min(distances)] = float('+inf')
            return arg_min(distances)

    def get_self(self) -> AgentState:
        return self.gameState.data.agentStates[self.reference_index]

    def get_pacmans(self) -> List[AgentState]:
        return [self.gameState.getPacmanState()]

    def get_ghosts(self) -> List[AgentState]:
        return self.gameState.getGhostStates()

    def get_agent(self, index) -> AgentState:
        return self.gameState.data.agentStates[index]

    def get_safe_junctions(self) -> List[Tuple[int, int]]:
        px, py = self.get_self_coordinates()
        ghost_states = self.get_nonscared_ghosts()
        ghost_coords = [self.get_agent_position(gh) for gh in ghost_states]
        safe_junctions = self.maze.get_safe_junctions(int(px), int(py), ghost_coords)
        return safe_junctions

    def get_scared_ghosts(self) -> List[AgentState]:
        return [g for g in self.gameState.getGhostStates() if g.scaredTimer > 0]

    def get_nonscared_ghosts(self) -> List[AgentState]:
        return [g for g in self.gameState.getGhostStates() if g.scaredTimer == 0]

    def get_coordinates(self, entity: Union[AgentState, Tuple[float, float]]) -> Tuple[float, float]:
        if isinstance(entity, AgentState):
            return self.get_agent_position(entity)
        else:
            return entity

    def get_scared_timer(self, entity: AgentState):
        if hasattr(entity, 'scared_timer'):
            return entity.scaredTimer
        else:
            return 0

    def get_spawn_point_shortest_path_distance(self, entity: AgentState) -> List[AgentState]:
        x0, y0 = self.get_self_coordinates()
        x1, y1 = entity.start.pos
        return self.maze.shortest_path_distance(int(x0), int(y0), int(x1), int(y1))

    def get_self_coordinates(self) -> Tuple[float, float]:
        return self.get_agent_position(self.get_self())

    def get_self_score(self) -> Tuple[float, float]:
        return self.get_score(self.get_self())

    def get_score(self, entity: AgentState):
        if hasattr(entity, 'isPacman') and entity.isPacman:
            return self.gameState.data.score
        else:
            return 0

    def get_x(self, entity: AgentState) -> float:
        return self.get_coordinates(entity)[0]

    def get_y(self, entity: AgentState) -> float:
        return self.get_coordinates(entity)[1]

    def get_capsules(self) -> List[Tuple[int, int]]:
        return self.gameState.getCapsules()

    def get_food(self) -> List[Tuple[int, int]]:
        grid = self.gameState.getFood()
        food = []
        for x in range(grid.width):
            for y in range(grid.height):
                if grid[x][y]:
                    food.append((x, y))
        return food

    def get_legal_actions(self, reference_index:Optional[int]=None):
        if reference_index is None:
            return set(self.gameState.getLegalActions(self.reference_index))
        else:
            return set(self.gameState.getLegalActions(reference_index))

    def dijkstra_distance_to_self(self, entity: AgentState):
        x0, y0 = self.get_self_coordinates()
        x1, y1 = self.get_coordinates(entity)
        return self.maze.shortest_path_distance(int(x0), int(y0), int(x1), int(y1))

    def dijkstra_distance_via_direction(self, direction: Direction, entity: AgentState):
        xp, yp = self.get_self_coordinates()
        xp, yp = int(xp), int(yp)
        dx, dy = Actions.directionToVector(direction)
        xo, yo = int(xp+dx), int(yp+dy)
        w, h = self.maze.geom
        if xp < 0 or xp > w or yp < 0 or yp > h or not self.maze.walkable(xo, yo):
            return None
        xe, ye = self.get_coordinates(entity)
        return self.maze.shortest_path_distance_force_2nd_node(xp, yp, xo, yo, int(xe), int(ye))

    def dijkstra_direction_to_self(self, entity: AgentState):
        x0, y0 = self.get_self_coordinates()
        x1, y1 = self.get_coordinates(entity)
        sp_direction = self.maze.shortest_path_direction(int(x0), int(y0), int(x1), int(y1))
        if sp_direction is None:
            return None
        else:
            return Actions.vectorToDirection(sp_direction)

    def euclidean_distance_to_self(self, entity: AgentState):
        x0, y0 = self.get_self_coordinates()
        x1, y1 = self.get_coordinates(entity)
        return math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)

    def euclidean_direction_to_self(self, entity: AgentState):
        x0, y0 = self.get_self_coordinates()
        x1, y1 = self.get_coordinates(entity)
        return math.degrees(math.atan2(y1 - y0, x1 - x0))

    def set_layout(self, layout: Layout):
        """
        Sets the layout for the features extractor.
        :param layout: level layout
        :type layout: Layout
        """
        w, h = layout.width, layout.height
        from itertools import product
        layout_hash = hash(layout.walls)
        if layout_hash not in self.mazes:
            self.mazes[layout_hash] = Maze(
                blocks=[(x,y) for x, y in product(range(w), range(h)) if layout.walls[x][y]],
                walls=[],
                height=layout.height,
                width=layout.width
            )
        self.maze = self.mazes[layout_hash]
        self.layout = layout

    def extract_feature(self, feature: str, reference_index: Optional[int]=None):
        if reference_index is not None:
            self.reference_index = reference_index

        discretize = feature[-1] == ':'
        if discretize:
            feature = feature[:-1]
        try:
            value = self.extract_raw_feature(feature)
        except ValueError:
            value = self.extract_raw_feature(self.get_long_name(feature))
        if discretize:
            return self.discretize_value(value)
        else:
            return value

    @staticmethod
    def discretize_value(value):
        if value is None:
            return None
        else:
            if value == 0:
                return 0 #'zero'
            elif value == 1:
                return 1 #'very-close'
            elif value < 5:
                return 2 #'close'
            else:
                return 3 #'far'
            # elif value < 15:
            #     return 3 #'far'
            # else:
            #     return 4 #'very-far'

    def extract_raw_feature(self, feature: str):

        match = re.match(self.admissiblePatterns['self_coordinates'], feature)
        if match:
            coordinate = match.group(1)
            coordinates = self.get_self_coordinates()
            if coordinate == 'x':
                return coordinates[0]
            elif coordinate == 'y':
                return coordinates[1]

        match = re.match(self.admissiblePatterns['self_score'], feature)
        if match:
            return self.get_self_score()

        match = re.match(self.admissiblePatterns['legal_moves'], feature)
        if match:
            direction = match.group(1)
            return getattr(Direction, direction) in self.get_legal_actions()

        match = re.match(self.admissiblePatterns['objects'], feature)
        if match:
            entity = match.group(1)
            attribute = match.group(2)
            discretize = match.group(3)
            entities = self.entities[entity]()
            if attribute == 'counter':
                return len(entities)
            else:
                values = tuple(self.attributes[attribute](e) for e in entities)
                if discretize == ':':
                    values = tuple(None if v is None else self.discretize_value(v) for v in values)
                return values

        match = re.match(self.admissiblePatterns['nearest_object'], feature)
        if match:
            rank = int(match.group(1))
            entity = match.group(2)
            attribute = match.group(3)
            discretize = match.group(4)
            entities = self.entities[entity]()
            nearest = self.arg_dijkstra_nearest_to_self(entities, rank)
            if nearest is None:
                return None
            value = self.attributes[attribute](entities[nearest])
            if value is None:
                return None
            else:
                if discretize == ':':
                    value = self.discretize_value(value)
                return value

        match = re.match(self.admissiblePatterns['agent'], feature)
        if match:
            agent_index = int(match.group(1))
            attribute = match.group(2)
            discretize = match.group(3)
            agent = self.get_agent(agent_index)
            value = self.attributes[attribute](agent)
            if discretize == ':':
                value = self.discretize_value(value)
            return value

        match = re.match(self.admissiblePatterns['self'], feature)
        if match:
            attribute = match.group(1)
            discretize = match.group(2)
            agent = self.get_agent(self.agent_index)
            value = self.attributes[attribute](agent)
            if discretize == ':':
                value = self.discretize_value(value)
            return value

        match = re.match(self.admissiblePatterns['dist_to_obj_via_direction'], feature)
        if match:
            entity = match.group(1)
            direction = getattr(Direction, match.group(2))
            discretize = match.group(3)
            entities = self.entities[entity]()
            distance_measure = partial(getattr(self, 'dijkstra_distance_via_direction'), direction)
            nearest = self.arg_nearest_to(entities, distance_measure, k=1)
            if nearest is None:
                return None
            value = distance_measure(entities[nearest])
            if discretize == ':':
                value = self.discretize_value(value)
            return value


        raise ValueError("Unknown feature %s" % feature)


    def get_feature_info(self, feature):

        # 'pacman_(x|y)'
        match = re.match(self.admissiblePatterns['self_coordinates'], feature)
        if match:
            coord = match.group(1)
            if coord == 'x':
                return {'iterable': False, 'type': int, 'min': 0, 'max': self.maze.geom[0] - 1}
            elif coord == 'y':
                return {'iterable': False, 'type': int, 'min': 0, 'max': self.maze.geom[1] - 1}

        # 'legal-(%s)' % '|'.join(self.directions)
        match = re.match(self.admissiblePatterns['legal_moves'], feature)
        if match:
            return {'iterable': False, 'nullable': False, 'type': bool}

        # '(%s)-(%s|counter)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))
        match = re.match(self.admissiblePatterns['objects'], feature)
        if match:
            entity = match.group(1)
            attribute = match.group(2)
            discretize = match.group(3)
            if attribute == 'counter':
                if entity == 'ghost':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': self.layout.numGhosts, 'max': self.layout.numGhosts}
                elif entity == 'scared_ghost':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.layout.numGhosts}
                elif entity == 'non_scared_ghost':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.layout.numGhosts}
                elif entity == 'capsule':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': len(self.layout.capsules)}
                elif entity == 'food':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': len(self.layout.food)}
                elif entity == 'safe_junction':
                    return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': 4}
            else:
                there_may_be_no_such_entity = entity != 'ghost'
                if discretize == ':':
                    return {'iterable': True, 'type': 'enum', 'nullable': there_may_be_no_such_entity, 'possible_values': [None, 0, 1, 2, 3] if there_may_be_no_such_entity else [0, 1, 2, 3]}
                elif attribute == 'x':
                    return {'iterable': True, 'type': int, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': self.maze.geom[0] - 1}
                elif attribute == 'y':
                    return {'iterable': True, 'type': int, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': self.maze.geom[1] - 1}
                elif attribute == 'euclidean_distance':
                    return {'iterable': True, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': math.sqrt((self.maze.geom[0] - 1) ** 2 + (self.maze.geom[1] - 1) ** 2)}
                elif attribute == 'angular_direction':
                    return {'iterable': True, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': 360}
                elif attribute == 'shortest_path_distance':
                    return {'iterable': True, 'type': float, 'nullable': True, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
                elif attribute == 'spawn_point_sp_distance':
                    return {'iterable': True, 'type': float, 'nullable': True, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
                elif attribute == 'shortest_path_direction':
                    return {'iterable': True, 'type': 'enum', 'nullable': True, 'possible_values': [Direction.east, Direction.north, Direction.south, Direction.west]}
                elif attribute == 'scared_timer':
                    return {'iterable': True, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': SCARED_TIME}

        # 'nearest\[([0-9]+)\]-(%s)-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))
        match = re.match(self.admissiblePatterns['nearest_object'], feature)
        if match:
            entity = match.group(2)
            attribute = match.group(3)
            discretize = match.group(4)
            there_may_be_no_such_entity = entity != 'ghost'
            if discretize == ':':
                return {'iterable': False, 'type': 'enum', 'nullable': there_may_be_no_such_entity, 'possible_values': [None, 0, 1, 2, 3] if there_may_be_no_such_entity else [0, 1, 2, 3]}
            elif attribute == 'x':
                return {'iterable': False, 'type': int, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': self.maze.geom[0] - 1}
            elif attribute == 'y':
                return {'iterable': False, 'type': int, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': self.maze.geom[1] - 1}
            elif attribute == 'euclidean_distance':
                return {'iterable': False, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': math.sqrt((self.maze.geom[0] - 1) ** 2 + (self.maze.geom[1] - 1) ** 2)}
            elif attribute == 'angular_direction':
                return {'iterable': False, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': 360}
            elif attribute == 'shortest_path_distance':
                return {'iterable': False, 'type': float, 'nullable': True, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'spawn_point_sp_distance':
                return {'iterable': False, 'type': float, 'nullable': True, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'shortest_path_direction':
                return {'iterable': False, 'type': 'enum', 'nullable': True, 'possible_values': [Direction.east, Direction.north, Direction.south, Direction.west]}
            elif attribute == 'scared_timer':
                return {'iterable': False, 'type': float, 'nullable': there_may_be_no_such_entity, 'min': 0, 'max': SCARED_TIME}


        # 'agent-(%s)-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))
        match = re.match(self.admissiblePatterns['agent'], feature)
        if match:
            attribute = match.group(2)
            discretize = match.group(3)
            if discretize == ':':
                return {'iterable': False, 'type': 'enum', 'nullable': False, 'possible_values': [0, 1, 2, 3]}
            elif attribute == 'x':
                return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] - 1}
            elif attribute == 'y':
                return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.maze.geom[1] - 1}
            elif attribute == 'euclidean_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': math.sqrt((self.maze.geom[0] - 1) ** 2 + (self.maze.geom[1] - 1) ** 2)}
            elif attribute == 'angular_direction':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': 360}
            elif attribute == 'shortest_path_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'spawn_point_sp_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'shortest_path_direction':
                return {'iterable': False, 'type': 'enum', 'nullable': False, 'possible_values': [Direction.east, Direction.north, Direction.south, Direction.west]}
            elif attribute == 'scared_timer':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': SCARED_TIME}


        # 'agent-(%s)-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.attributes))
        match = re.match(self.admissiblePatterns['self'], feature)
        if match:
            attribute = match.group(1)
            discretize = match.group(2)
            if discretize == ':':
                return {'iterable': False, 'type': 'enum', 'nullable': False, 'possible_values': [0, 1, 2, 3]}
            elif attribute == 'x':
                return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] - 1}
            elif attribute == 'y':
                return {'iterable': False, 'type': int, 'nullable': False, 'min': 0, 'max': self.maze.geom[1] - 1}
            elif attribute == 'euclidean_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': math.sqrt((self.maze.geom[0] - 1) ** 2 + (self.maze.geom[1] - 1) ** 2)}
            elif attribute == 'angular_direction':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': 360}
            elif attribute == 'shortest_path_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'spawn_point_sp_distance':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}  # TODO this is not accurate if there are convex blocks
            elif attribute == 'shortest_path_direction':
                return {'iterable': False, 'type': 'enum', 'nullable': False, 'possible_values': [Direction.east, Direction.north, Direction.south, Direction.west]}
            elif attribute == 'scared_timer':
                return {'iterable': False, 'type': float, 'nullable': False, 'min': 0, 'max': SCARED_TIME}

        # 'shortest_path_distance_to_nearest-(%s)-via-(%s)(:?)' % ('|'.join(self.entities), '|'.join(self.directions)))
        match = re.match(self.admissiblePatterns['dist_to_obj_via_direction'], feature)
        if match:
            return {'iterable': False, 'type': int, 'nullable': True, 'min': 0, 'max': self.maze.geom[0] + self.maze.geom[1] - 2}


        raise ValueError("Unknown feature %s" % feature)


    def get_features_info(self, features: List[str]):
        """
        Extracts all the wanted features from the game state. Features list is
        set in the constructor.
        :param features: desired features
        """
        try:
            return {f: self.get_feature_info(f) for f in features}
        except ValueError:
            return {f: self.get_feature_info(self.get_long_name(f)) for f in features}


    def extract_features(
        self,
        gameState=None,
        desired_features: Optional[List[str]]=None,
        reference_index: Optional[int]=None
    ) -> Dict[str, Union[int,float,None,Direction]]:
        """
        Extracts all the wanted features from the game state. Features list is
        set in the constructor.
        :param gameState: game state
        :param reference_index: index of the agent to be used as reference (default, agent index, passed to __init__)
        :param desired_features: desired features
        """
        if reference_index is None:
            self.reference_index = self.agent_index
        else:
            self.reference_index = reference_index
        if gameState is not None:
            self.gameState = gameState
        if desired_features is None:
            features = self.desired_features
        else:
            features = desired_features
        features_values = {f: self.extract_feature(f) for f in features}
        self.reference_index = self.agent_index
        return features_values

    def set_game_state(self, gameState):
        self.gameState = gameState
