from reinf.options.option import Option
from util.translator import Translator
from util.typing2 import MyTypes as T
from typing import Iterable, Optional
import random
from pacman.game import Direction


class GoToClosestFood(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.FdDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('go_to_closest_food', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        food_dir = state['.FdDr']
        if food_dir is None:
            return random.choice([a for a in la if isinstance(a, T.ActionCode)])
        else:
            return self.actionTranslator.getCode(food_dir)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class GoToClosestCapsule(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.CpDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('go_to_closest_capsule', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        capsule_dir = state['.CpDr']
        if capsule_dir is None:
            return random.choice([a for a in la if isinstance(a, T.ActionCode)])
        else:
            return self.actionTranslator.getCode(capsule_dir)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class GoToClosestScaredGhost(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.ScGhDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('go_to_closest_scared_ghost', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        scared_ghost_dir = state['.ScGhDr']
        if scared_ghost_dir is None:
            return random.choice([a for a in la if isinstance(a, T.ActionCode)])
        else:
            return self.actionTranslator.getCode(scared_ghost_dir)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class AvoidGhost(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.NScGhDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('avoid_closest_non_scared_ghost', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        non_scared_ghost_dir = state['.NScGhDr']
        viable = [a for a in la if isinstance(a,T.ActionCode) and
                  a != self.actionTranslator.getCode(non_scared_ghost_dir) and
                  a != self.actionTranslator.getCode(Direction.stop)]
        if len(viable) == 0:
            return self.actionTranslator.getCode(Direction.stop)
        else:
            return random.choice(viable)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb