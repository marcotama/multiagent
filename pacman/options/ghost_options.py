from reinf.options.option import Option
from util.translator import Translator
from util.typing2 import MyTypes as T
from typing import Iterable, Optional
import random
from pacman.game import Direction


class GoToPacman(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature 'PmDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('go_to_pacman', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        food_dir = state['PmDr']
        if food_dir is None or self.actionTranslator.getCode(food_dir) not in la:
            return random.choice([a for a in la if isinstance(a, T.ActionCode) and
                                  a != self.actionTranslator.getCode(Direction.stop)])
        else:
            return self.actionTranslator.getCode(food_dir)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class GoToClosestCapsule(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.CpDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('go_to_closest_capsule', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        capsule_dir = state['.CpDr']
        if capsule_dir is None or self.actionTranslator.getCode(capsule_dir) not in la:
            return random.choice([a for a in la if isinstance(a, T.ActionCode) and
                                  a != self.actionTranslator.getCode(Direction.stop)])
        else:
            return self.actionTranslator.getCode(capsule_dir)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class AvoidPacman(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature 'PmDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('avoid_pacman', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        pacman_dir = state['PmDr']
        viable = [a for a in la if isinstance(a,T.ActionCode) and
                  a != self.actionTranslator.getCode(pacman_dir) and
                  a != self.actionTranslator.getCode(Direction.stop)]
        if len(viable) == 0:
            return random.choice([a for a in la if isinstance(a,T.ActionCode)])
        else:
            return random.choice(viable)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb


class AvoidClosestCapsule(Option):
    """
    An option whose policy is to return a preselected primitive action if legal, a random one otherwise.

    This option assumes that feature '.CpDr' is part of the state.
    """

    def __init__(
        self,
        stateTranslator: Optional[Translator],
        actionTranslator: Optional[Translator],
        tProb: float,
    ) -> None:
        self.tProb = tProb
        self.stateTranslator = stateTranslator
        self.actionTranslator = actionTranslator
        super().__init__('avoid_closest_capsule', None)

    def _policy(
        self,
        s: T.StateCode,
        la: Iterable[T.ActionCode]
    ) -> T.ActionCode:
        state = self.stateTranslator.getDict(s)
        pacman_dir = state['.CpDr']
        viable = [a for a in la if isinstance(a,T.ActionCode) and
                  a != self.actionTranslator.getCode(pacman_dir) and
                  a != self.actionTranslator.getCode(Direction.stop)]
        if len(viable) == 0:
            return random.choice([a for a in la if isinstance(a,T.ActionCode)])
        else:
            return random.choice(viable)

    def _termProbs(
        self,
        s: T.StateCode
    ) -> float:
        return self.tProb