# -*- coding: utf-8 -*-
"""
Trains an MDP given game records.

@author: Marco Tamassia
"""
from typing import List, Tuple
from pacman.game import Direction
from pacman.demos_converter import PacmanDemosConverter
from pacman.layout import Layout
from pacman.agents.q_learning_agents import QLearningAgent
from reinf.exploration_strategies import ExplorationParams
from reinf.mdp import MDPParams, MDPOptionsParams
from util.util2 import pickle_load

class MDPTrainer:
    def __init__(
        self,
        features: List[str],
        gamma: float,
        alpha: float,
        initQ: float,
        learn_q: bool=True,
        learn_n: bool=True,
        learn_q_stats: bool=True,
        learn_p: bool=True,
        learn_r: bool=True,
        learn_t: bool=True,
        save_history: bool=True
    ):
        # Dummy settings
        # noinspection PyProtectedMember
        explParams = ExplorationParams(**{key: 'Greedy' for key in ExplorationParams._fields})
        # noinspection PyProtectedMember
        mdpOptionsParams = MDPOptionsParams(**{key: None for key in MDPOptionsParams._fields})
        # MDP settings
        mdpParams = MDPParams(gamma=gamma, alpha=alpha, initQ=initQ)
        # Agent creation = this agent will contain the MDP
        self.agent = QLearningAgent(0, features, explParams, mdpParams, mdpOptionsParams)
        # Set the MDP to only learn what is desired
        self.agent.get_mdp().learn_q = learn_q
        self.agent.get_mdp().learn_n = learn_n
        self.agent.get_mdp().learn_q_stats = learn_q_stats
        self.agent.get_mdp().learn_p = learn_p
        self.agent.get_mdp().learn_r = learn_r
        self.agent.get_mdp().learn_t = learn_t
        self.agent.get_mdp().save_history = save_history

        self.demosConverter = PacmanDemosConverter(self.agent)

    def getMDP(self):
        return self.agent.get_mdp()

    def getAgent(self):
        return self.agent

    def getTranslators(self):
        return self.agent.qAgent.stateTranslator, self.agent.qAgent.actionTranslator

    def feedDemoFile(
        self,
        demo_file: str
    ):
        demos = pickle_load(demo_file)  # type: List[Tuple[Layout, List[Tuple[int, Direction]]]]
        for s, la, a, r, ns, nla in self.demosConverter.iterDemosWithNextState(demos):
            self.agent.learningFunc(s, la, a, r, ns, nla)

