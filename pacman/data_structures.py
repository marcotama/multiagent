from collections import namedtuple

EpisodeData = namedtuple('EpisodeData', [
    'episode',
    'outcomeStr',
    'outcome',
    'score',
    'movesCount',
    'statesCount',
    'stateActionPairsCount',
    'executionTime',
    'abstractionsUsage',
    'eatenGhostsCount',
    'eatenFoodCount',
    'eatenCapsulesCount',
])