# -*- coding: utf-8 -*-
"""
Pacman agent using the Q-learning algorithm.

@author: Marco Tamassia
"""

from typing import List, Optional
from abc import abstractmethod

from pacman.game import Direction, GameState
from pacman.layout import Layout
from pacman.agents.common import LearningAgent, get_pacman_score, get_ghost_score
from reinf.policy.discrete import GreedyPolicy, AnnealingEpsilonGreedyPolicy
from reinf.gq import GreedyGQ
from util.projector import TileCoderProjector
from util.trace import AccumulatingTrace, AccumulatingMaxTrace, MaxLengthTrace
from util.functions import ConstantFn
from util.vector import DenseVector, SparseVector
from util.state_action_encoder import StateActionTabularEncoder, StateActionAppendingEncoder
from util.typing2 import MyTypes as T
from util.hashing import UNH
from functools import partial

import numpy as np



class GQAgent(LearningAgent):

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass


    def __init__ (
        self,
        index: int,
        features: List[T.Feature],
        layout: Layout,
        alpha: float,
        eta: float,
        trace: str,
        gamma: float,
        lmbda: float,
        n_tiles: int,
        n_tilings: int,
        hashing_algorithm: str,
        hashing_memory: int,
        start_descent: int,
        end_descent: int,
        projector: str,
        state_action_encoder: str,
        scales: str
    ):
        self.lmbda = lmbda
        self.gamma = gamma
        directions = [Direction.north, Direction.south, Direction.east, Direction.west, Direction.stop]
        self.a2d = {i: a for i, a in enumerate(directions)}
        self.d2a = {a: i for i, a in self.a2d.items()}
        actions = list(self.d2a.values())


        self.be_greedy = False
        self.learning = True

        super().__init__(index, features, layout)
        features_info = self.featuresExtractor.get_features_info(features)
        self.features_info = features_info
        self.bool_features = []
        self.enum_features = []
        self.ranged_features = []
        self.n_binary_features, ranges = self.analyze_features()

        # hashing
        if hashing_algorithm is None or hashing_memory == 0:
            hashing = None
        elif hashing_algorithm == 'UNH':
            hashing = UNH(memory=hashing_memory)
        else:
            raise ValueError("Unknown hashing algorithm: %s" % hashing_algorithm)

        # projector
        if projector == 'tile_coder':
            self.projector = TileCoderProjector(n_tiles,n_tilings,ranges,hashing,self.n_binary_features,include_bias=False,scales=scales)
        else:
            raise ValueError("Unknown projector: %s" % projector)

        # state-action encoder
        if state_action_encoder == 'tabular':
            state_action_encoder = StateActionTabularEncoder(self.projector.vector_size(), self.projector.vector_norm(), actions, include_bias=True)
        elif state_action_encoder == 'appending':
            state_action_encoder = StateActionAppendingEncoder(self.projector.vector_size(), self.projector.vector_norm(), actions, include_bias=True)
        else:
            raise ValueError("Unknown state-action encoder: %s" % state_action_encoder)

        # trace factory
        if trace == 'accumulating':
            trace_factory = AccumulatingTrace
        elif trace == 'accumulating-max':
            trace_factory = AccumulatingMaxTrace
        elif trace == 'max-length':
            trace_factory = MaxLengthTrace
        else:
            raise ValueError("Unknown trace: %s" % trace)

        # Note:
        # Greedy-GQ [49] solves this problem and can find an approximation of the optimal value function
        # as long as the behavior policy is fixed. Unfortunately, in complex domains, a random behavior
        # policy does not visit the interesting parts of the state space often enough for effective learning,
        # and the algorithm is no longer stable with a varying behavior policy.
        # Cited by Louis C. Cobos PhD thesis:
        # Leveraging attention focus for effective reinforcement learning in complex domains
        #behavior = partial(EpsilonGreedyPolicy, epsilon=0.1)
        behavior_factory = partial(
            AnnealingEpsilonGreedyPolicy,
            epsilon=0.1,
            start_descent=start_descent,
            end_descent=end_descent
        )
        target_factory = GreedyPolicy
        gamma_fn = ConstantFn(gamma)
        lambda_fn = ConstantFn(lmbda)
        interest_fn = ConstantFn(1.0)
        self.control = GreedyGQ(
            alpha=alpha,
            eta=eta,
            trace_factory=trace_factory,
            target_factory=target_factory,
            behavior_factory=behavior_factory,
            actions=actions,
            state_action_encoder=state_action_encoder,
            gamma_fn=gamma_fn,
            lambda_fn=lambda_fn,
            interest_fn=interest_fn
        )

        # preallocate
        self.ranged_features_vec = DenseVector(len(self.ranged_features))
        self.binary_features_vec = None
        self.enc_state_t_vec = SparseVector(self.projector.vector_size())
        self.enc_state_tp1_vec = None
        self.phi_temp = DenseVector(state_action_encoder.vector_size())

        self.initialized = False
        # flag saying if learningFunc is called on sequential steps - saves computation time
        self.trained_sequentially = True


    def analyze_features(self):
        """
        Analyze features properties (type, number, order, min/max)
        """
        n_binary_features = 0
        ranges = ([], [])
        for name in sorted(self.features_info.keys()):
            info = self.features_info[name]
            if info['type'] in [int,float]:
                self.ranged_features.append(name)
                ranges[0].append(info['min'])
                ranges[1].append(info['max'])
            elif info['type'] == 'enum':
                self.enum_features.append(name)
                if info['nullable']:
                    n_binary_features += len(info['possible_values']) + 1
                    info['possible_values'] = [None] + info['possible_values']
                else:
                    n_binary_features += len(info['possible_values'])
            elif info['type'] == bool:
                self.bool_features.append(name)
                n_binary_features += 1

        for feature, feature_info in self.features_info.items():
            if feature_info['type'] in [int, float] and ('min' not in feature_info or 'max' not in feature_info):
                raise Exception("Invalid feature %s: %s" % (feature, str(feature_info)))
        ranges = (DenseVector(data=ranges[0]), DenseVector(data=ranges[1]))
        return n_binary_features, ranges


    def process_features(self, state: T.FactoredState):
        # ranged features
        idx = []
        val = []
        if state is None:
            for i in range(len(self.ranged_features)):
                self.ranged_features_vec[i] = np.nan
        else:
            for i, f in enumerate(self.ranged_features):
                self.ranged_features_vec[i] = np.nan if state[f] is None else state[f]

            # enum and bool features
            i = 0
            for f in self.enum_features:
                pv = self.features_info[f]['possible_values']
                idx.append(i + pv.index(state[f]))
                val.append(1.0)
                i += len(pv)
            for f in self.bool_features:
                self.binary_features_vec[i] = float(state[f])
                val.append(float(state[f]))
                i += 1

        self.binary_features_vec = SparseVector(self.n_binary_features, data=val,idx=idx)

    def learning_fn(
        self,
        state: T.FactoredState,
        legalActions: List[T.FactoredAction],
        action: T.FactoredAction,
        reward: float,
        newState: Optional[T.FactoredState],
        newLegalActions: List[T.FactoredAction]
    ):
        action = self.d2a[action]
        legalActions = [self.d2a[a] for a in legalActions]
        newLegalActions = [self.d2a[a] for a in newLegalActions]

        if self.trained_sequentially:
            self.enc_state_t_vec, self.enc_state_tp1_vec = self.enc_state_tp1_vec, self.enc_state_t_vec
        else:
            # encode state
            self.process_features(state)
            self.enc_state_t_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        # encode newState
        self.process_features(newState)
        self.enc_state_tp1_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        self.control.learn(x_t=self.enc_state_t_vec, la_t=legalActions, a_t=action, r_t=reward,
                           x_tp1=self.enc_state_tp1_vec, la_tp1=newLegalActions)


    def decision_fn(
        self,
        state: T.FactoredState,
        legalActions: List[T.FactoredAction]
    ):
        legalActions = [self.d2a[a] for a in legalActions]

        if not self.trained_sequentially:
            self.process_features(state)
            self.enc_state_tp1_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        if self.be_greedy:
            choice = self.control.propose_greedy_action(self.enc_state_tp1_vec, legalActions)
        else:
            choice = self.control.propose_exploratory_action(self.enc_state_tp1_vec, legalActions)

        return self.a2d[choice]


    def __str__ (self):
        return str(self.control)


    # TODO uniform this with Explorator and Learner classes in reinf.common
    def explore (self):
        self.be_greedy = False

    def exploit (self):
        self.be_greedy = True

    def is_acting_greedily (self):
        return self.be_greedy

    def start_learning (self):
        self.learning = True

    def stop_learning (self):
        self.learning = False

    def is_learning (self):
        return self.learning

    def end_of_episode(self):
        self.control.end_of_episode()


class GQPacman(GQAgent):
    get_score = get_pacman_score

class GQGhost(GQAgent):
    get_score = get_ghost_score
