# -*- coding: utf-8 -*-
"""
Pacman agent using the Q-learning algorithm.

@author: Marco Tamassia
"""

import itertools
from typing import Iterable, List
from abc import abstractmethod

from pacman.game import GameState
from pacman.agents.common import LearningAgent, get_pacman_score, get_ghost_score
from pacman.layout import Layout
from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from reinf.q_learning import QLearningAgent as QL
from reinf.multi_q_learning import MultiQLearningAgent as MQL
from util.typing2 import MyTypes as T
from util.delegation import delegate_attrs


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
])
class QLearningAgent(LearningAgent):

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass

    def __init__(
        self,
        index: int,
        features: List[T.Feature],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.qAgent = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )

    def __str__(self):
        return str(self.qAgent)
        
    def learning_fn(self, *args, **kwargs):
        return self.qAgent.learn(*args, **kwargs)
        
    def decision_fn(self, *args, **kwargs):
        return self.qAgent.get_action(*args, **kwargs)

    def get_mdp(self):
        return self.qAgent.mdp

    def end_of_episode (self, *args, **kwargs):
        self.qAgent.end_of_episode()


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
])
class MultiQLearningAgent(LearningAgent):

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass

    def __init__(
        self,
        index: int,
        features_sets: List[List[T.Feature]],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
        layout: Layout
    ):
        features = list(set(itertools.chain(*features_sets)))
        super().__init__(index, features, layout)
        self.qAgent = MQL(
            features=features,
            features_sets=features_sets,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )

    def get_abstractions_usage(self):
        return self.qAgent.get_abstractions_usage()

    def __str__(self):
        return str(self.qAgent)
        
    def learning_fn(self, *args, **kwargs):
        return self.qAgent.learn(*args, **kwargs)
        
    def decision_fn(self, *args, **kwargs):
        return self.qAgent.get_action(*args, **kwargs)

    def get_mdp(self):
        return self.qAgent.mdp

    def end_of_episode (self, *args, **kwargs):
        self.qAgent.end_of_episode()




class QLearningPacman(QLearningAgent):
    get_score = get_pacman_score

class QLearningGhost(QLearningAgent):
    get_score = get_ghost_score


class MultiQLearningPacman(MultiQLearningAgent):
    get_score = get_pacman_score

class MultiQLearningGhost(MultiQLearningAgent):
    get_score = get_ghost_score