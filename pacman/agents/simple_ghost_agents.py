# ghostAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from abc import abstractmethod
from typing import Dict

from pacman.game import Actions, Agent, Direction, GameState
from util.util import manhattanDistance, chooseFromDistribution, raiseNotDefined, Counter


class GhostAgent(Agent):

    def __init__(self, index):
        Agent.__init__(self, index)
        self.index = index

    def get_action(self, gameState):
        dist = self.getDistribution(gameState)
        if len(dist) == 0:
            return Direction.stop
        else:
            return chooseFromDistribution(dist)

    @abstractmethod
    def getDistribution(
        self,
        state
    ) -> Dict[Direction, float]:
        """
        Returns a Counter encoding a distribution over actions from the provided state.
        :param state: state of the environment
        :return: distribution of actions
        """
        raiseNotDefined()
        return {}


class RandomGhost(GhostAgent):
    """
    A ghost that chooses a legal action uniformly at random.
    """

    def getDistribution(self, state: GameState) -> Dict[Direction, float]:
        dist = Counter()
        for a in state.getLegalActions(self.index):
            dist[a] = 1.0
        dist.normalize()
        return dist


class DirectionalGhost(GhostAgent):
    """
    A ghost that prefers to rush Pacman, or flee when scared.
    """

    def __init__(self, index, prob_attack=0.8, prob_scared_flee=0.8):
        GhostAgent.__init__(self, index)
        self.index = index
        self.prob_attack = prob_attack
        self.prob_scared_flee = prob_scared_flee

    def getDistribution(self, state: GameState):
        # Read variables from state
        ghostState = state.getGhostState(self.index)
        legalActions = state.getLegalActions(self.index)
        pos = state.getGhostPosition(self.index)
        isScared = ghostState.scaredTimer > 0

        speed = 1
        if isScared:
            speed = 0.5

        actionVectors = [Actions.directionToVector(a, speed)
                         for a in legalActions]
        newPositions = [(pos[0] + a[0], pos[1] + a[1]) for a in actionVectors]
        pacmanPosition = state.getPacmanPosition()

        # Select best actions given the state
        distancesToPacman = [manhattanDistance(p, pacmanPosition)
                             for p in newPositions]
        if isScared:
            bestScore = max(distancesToPacman)
            bestProb = self.prob_scared_flee
        else:
            bestScore = min(distancesToPacman)
            bestProb = self.prob_attack
        bestActions = [action for action, distance in zip(
            legalActions, distancesToPacman) if distance == bestScore]

        # Construct distribution
        dist = Counter()
        for a in bestActions:
            dist[a] = bestProb / len(bestActions)
        for a in legalActions:
            dist[a] += (1 - bestProb) / len(legalActions)
        dist.normalize()
        return dist
