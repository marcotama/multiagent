import random
from abc import abstractmethod

from pacman.agents.common import get_pacman_heuristic
from pacman.game import Agent, GameState, Direction
from pacman.layout import Layout
from pacman.features_extractor import PacmanFeaturesExtractor


class MultiAgentSearchAgent(Agent):

    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated. It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, index, layout: Layout, depth=2):
        super().__init__(index)
        self.index = index  # Pacman is always agent index 0
        self.depth = depth
        self.previous_action = None
        self.previous_legal_actions = ()
        self.features_extractor = PacmanFeaturesExtractor([], index)
        self.features_extractor.set_layout(layout)

    def get_action(self, game_state: GameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.eval_state.

        Here are some method calls that might be useful when implementing
        minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        :param game_state: game state
        :return: the total number of agents in the game
        """
        legal_actions = set(game_state.getLegalActions(self.index))
        scores = {}
        next_agent = (self.index + 1) % game_state.getNumAgents()
        for action in legal_actions:
            next_state = game_state.generateSuccessor(self.index, action)
            scores[action] = self.deep_eval_state(game_state=next_state, agent_index=next_agent)
        best_score = max(scores.values())
        best_actions = [action for action in legal_actions if scores[action] == best_score]

        # Keep action choice consistent
        if (self.previous_action != Direction.stop and # always prefer moving
            self.previous_action in best_actions and # action choice must be optimal
            legal_actions - self.previous_legal_actions):  # at an intersection, allow change of direction
            action = self.previous_action
        elif len(best_actions) > 1:
            action = random.choice([a for a in best_actions if a != Direction.stop])
        else:
            action = random.choice(best_actions)

        self.previous_action = action
        self.previous_legal_actions = legal_actions
        return action

    @abstractmethod
    def deep_eval_state(self, game_state: GameState, agent_index):
        pass

    @abstractmethod
    def eval_state(self, game_state: GameState):
        pass



class MinimaxAgent(MultiAgentSearchAgent):
    """
    Minimax agent
    """

    @abstractmethod
    def eval_state(self, game_state: GameState):
        pass

    def deep_eval_state(self, game_state: GameState, agent_index):
        return self.minimax_score(game_state=game_state, agent_index=agent_index, current_depth=1)


    def minimax_score(self, game_state: GameState, agent_index, current_depth: int=1):
        if current_depth == self.depth:
            return self.eval_state(game_state)
            # return gameState.getScore()

        minimax_scores = {}
        legal_actions = game_state.getLegalActions(agent_index)
        if not legal_actions:
            return game_state.getScore()

        next_agent = (agent_index + 1) % game_state.getNumAgents()
        next_depth = current_depth + 1 if next_agent == 0 else current_depth

        for action in legal_actions:
            next_state = game_state.generateSuccessor(agent_index, action)
            minimax_scores[action] = self.minimax_score(next_state, next_agent, next_depth)

        if (agent_index == 0) == (self.index == 0): # Maximize
            best_score = max(minimax_scores.values())
        else: # Minimize
            best_score = min(minimax_scores.values())

        return best_score


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning
    """

    @abstractmethod
    def eval_state(self, game_state: GameState):
        pass

    def deep_eval_state(self, game_state: GameState, agent_index):
        return self.alpha_beta_score(game_state=game_state, agent_index=agent_index,
                                     alpha=float('-inf'), beta=float('+inf'), current_depth=1)


    def alpha_beta_score(self, game_state: GameState, agent_index, alpha, beta, current_depth: int=1):
        if current_depth == self.depth:
            return self.eval_state(game_state)

        legal_actions = game_state.getLegalActions(agent_index)
        if not legal_actions:
            return game_state.getScore()

        next_agent = (agent_index + 1) % game_state.getNumAgents()
        next_depth = current_depth + 1 if next_agent == 0 else current_depth

        if (agent_index == 0) == (self.index == 0):  # Maximize
            v = float('-inf')
            for action in legal_actions:
                next_state = game_state.generateSuccessor(agent_index, action)
                v = max(v, self.alpha_beta_score(next_state, next_agent, alpha, beta, next_depth))
                if v > beta:
                    return v
                alpha = max(alpha, v)
            return v
        else: # Minimize
            v = float('+inf')
            for action in legal_actions:
                next_state = game_state.generateSuccessor(agent_index, action)
                v = min(v, self.alpha_beta_score(next_state, next_agent, alpha, beta, next_depth))
                if v < alpha:
                    return v
                beta = min(beta, v)
            return v


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
    Your expectimax agent
    """

    @abstractmethod
    def eval_state(self, game_state: GameState):
        pass

    def deep_eval_state(self, game_state: GameState, agent_index):
        return self.expectimax_score(game_state=game_state, agent_index=agent_index, current_depth=1)


    def expectimax_score(self, game_state: GameState, agent_index, current_depth: int=1):
        if current_depth == self.depth:
            return self.eval_state(game_state)

        expectimax_scores = {}
        legal_actions = game_state.getLegalActions(agent_index)
        if not legal_actions:
            return game_state.getScore()

        next_agent = (agent_index + 1) % game_state.getNumAgents()
        next_depth = current_depth + 1 if next_agent == 0 else current_depth

        for action in legal_actions:
            next_state = game_state.generateSuccessor(agent_index, action)
            expectimax_scores[action] = self.expectimax_score(next_state, next_agent, next_depth)

        if (agent_index == 0) == (self.index == 0): # Maximize
            best_score = max(expectimax_scores.values())
        else: # Minimize
            best_score = sum(expectimax_scores.values()) / len(expectimax_scores)

        return best_score



class MinimaxPacman(MinimaxAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)

class MinimaxGhost(MinimaxAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)

class AlphaBetaPacman(AlphaBetaAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)

class AlphaBetaGhost(AlphaBetaAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)

class ExpectimaxPacman(ExpectimaxAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)

class ExpectimaxGhost(ExpectimaxAgent):
    def eval_state(self, game_state: GameState):
        return get_pacman_heuristic(self, self.features_extractor, game_state)
