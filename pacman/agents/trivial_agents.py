# pacmanAgents.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import random
from pacman.game import Agent, Direction, GameState
from pacman.agents.common import get_pacman_heuristic
from pacman.features_extractor import PacmanFeaturesExtractor


class LeftTurnAgent(Agent):

    """An agent that turns left at every opportunity"""

    def __init__ (self, index):
        super().__init__(index)

    def get_action(self, gameState):
        legal = gameState.getLegalPacmanActions()
        current = gameState.getPacmanState().configuration.direction
        if current == Direction.stop:
            current = Direction.north
        left = Direction.left[current]
        if left in legal:
            return left
        if current in legal:
            return current
        if Direction.right[current] in legal:
            return Direction.right[current]
        if Direction.left[left] in legal:
            return Direction.left[left]
        return Direction.stop


class GreedyAgent(Agent):
    def __init__(self, index):
        super().__init__(index)
        self.features_extractor = PacmanFeaturesExtractor([], index)

    def get_action(self, gameState):
        # Generate candidate actions
        legal = gameState.getLegalPacmanActions()
        if Direction.stop in legal:
            legal.remove(Direction.stop)

        successors = [(gameState.generateSuccessor(0, action), action)
                      for action in legal]
        scored = [(self.eval_state(state), action)
                  for state, action in successors]
        bestScore = max(scored)[0]
        bestActions = [pair[1] for pair in scored if pair[0] == bestScore]
        return random.choice(bestActions)

    def eval_state(self, gameState: GameState):
        return get_pacman_heuristic(self, self.features_extractor, gameState)
