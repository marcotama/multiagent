"""
Classes implementing some Challenge-Sensitive Action Selection
"""

from typing import List, Optional, Union, Tuple, Iterable
from abc import abstractmethod

from reinf.exploration_strategies import ExplorationParams
from util.typing2 import State, Action
from pacman.agents.common import LearningAgent, get_pacman_outcome, get_ghost_outcome, get_pacman_heuristic, get_ghost_heuristic
from pacman.layout import Layout
from pacman.game import GameState
from reinf.adaptive import ChallengeSensitiveActionSelectionAgent as CSAS
from reinf.adaptive import QValueSensitiveActionSelectionAgent as QVSAS
from reinf.adaptive import StakesSensitiveActionSelectionAgent as SSAS


class AdaptiveAgent(LearningAgent):

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass

    def __init__(self,
            index,
            features: List[str],
            layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = None # type: Union[CSAS, QVSAS, SSAS]

    def learning_fn(
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            reward: float,
            new_state: Optional[State],
            new_legal_actions: List[Action]
    ):
        self.intelligence.learn(
            state=state,
            legal_actions=legal_actions,
            action=action,
            reward=reward,
            new_state=new_state,
            new_legal_actions=new_legal_actions
        )

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions
        )


    def end_of_episode (self, *args, **kwargs):
        self.intelligence.end_of_episode()



class ChallengeSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,

        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = CSAS(
            index,
            evaluation_cycle=evaluation_cycle,
            threshold=threshold,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )
        self.last_heuristic = None


    def observation_fn (
        self,
        gameState: GameState
    ) -> Tuple[State, float, Iterable[Action]]:
        """
        Function that maps the game state in some features.
        :param gameState: state of the game
        """
        self.last_heuristic = self.get_expected_outcome(self.featuresExtractor, gameState)
        state = self.featuresExtractor.extract_features(gameState)
        score = self.get_score(gameState)
        legalActions = sorted(gameState.getLegalActions(self.index))
        return state, score, legalActions

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions,
            expected_outcome=self.last_heuristic
        )

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass

    @abstractmethod
    def get_expected_outcome (self, gameState: GameState) -> float:
        pass



class QValueSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,

        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = QVSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass



class StakesSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,
        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = SSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass





class ChallengeSensitiveActionSelectionPacman(ChallengeSensitiveActionSelectionAgent):
    get_score = get_pacman_outcome
    get_expected_outcome = get_pacman_heuristic

class ChallengeSensitiveActionSelectionGhost(ChallengeSensitiveActionSelectionAgent):
    get_score = get_ghost_outcome
    get_expected_outcome = get_ghost_heuristic


class QValueSensitiveActionSelectionPacman(QValueSensitiveActionSelectionAgent):
    get_score = get_pacman_outcome

class QValueSensitiveActionSelectionGhost(QValueSensitiveActionSelectionAgent):
    get_score = get_ghost_outcome


class StakesSensitiveActionSelectionPacman(QValueSensitiveActionSelectionAgent):
    get_score = get_pacman_outcome

class StakesSensitiveActionSelectionGhost(QValueSensitiveActionSelectionAgent):
    get_score = get_ghost_outcome

