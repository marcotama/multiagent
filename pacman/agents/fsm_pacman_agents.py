import random
from pacman.game import Agent, GameState, Direction
from pacman.features_extractor import PacmanFeaturesExtractor


class FSMPacman(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide. You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    GHOST_IN_RANGE = 1
    GHOST_OUT_OF_RANGE = 2
    CAPSULE_EATEN = 3
    CAPSULE_OVER = 4

    nextStates = {
        'ghostsAway-nonEnergized': {
            GHOST_IN_RANGE: 'ghostsClose-nonEnergized',
            CAPSULE_EATEN: 'ghostsAway-energized',
        },
        'ghostsAway-energized': {
            GHOST_IN_RANGE: 'ghostsClose-energized',
            CAPSULE_OVER: 'ghostsAway-nonEnergized',
        },
        'ghostsClose-nonEnergized': {
            GHOST_OUT_OF_RANGE: 'ghostsAway-nonEnergized',
            CAPSULE_EATEN: 'ghostsClose-energized',
        },
        'ghostsClose-energized': {
            GHOST_OUT_OF_RANGE: 'ghostsAway-energized',
            CAPSULE_OVER: 'ghostsClose-nonEnergized',
        }
    }

    def __init__(self, index):
        super().__init__(index)
        self.index = index
        self.featuresExtractor = PacmanFeaturesExtractor([], index)
        self.fsmState = 'ghostsAway-nonEnergized'

    def nextFSMState(self, action):
        if action in self.nextStates[self.fsmState]:
            self.fsmState = self.nextStates[self.fsmState][action]

    def get_action(self, gameState: GameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation
        function.

        Just like in the previous project, getAction takes a GameState and
        returns some Directions.X for some X in the set
        {North, South, West, East, Stop}
        :param gameState: game state
        """
        get = self.featuresExtractor.extract_feature
        self.featuresExtractor.set_game_state(gameState)
        safeDistance = 7
        distClosestGhost = get('.GhDs')
        if distClosestGhost < safeDistance:
            self.nextFSMState(self.GHOST_IN_RANGE)
        else:
            self.nextFSMState(self.GHOST_OUT_OF_RANGE)

        distClosestNonscaredGhost = get('.NScGhDs')
        if distClosestNonscaredGhost is None:
            self.nextFSMState(self.CAPSULE_EATEN)
        else:
            self.nextFSMState(self.CAPSULE_OVER)

        if self.fsmState == 'ghostsAway-nonEnergized':
            dirClosestFood = get('.FdDr')
            action = dirClosestFood

        elif self.fsmState == 'ghostsAway-energized':
            distClosestGhost = get('.ScGhDs')
            distClosestFood = get('.FdDs')
            if distClosestGhost < distClosestFood:
                action = get('.ScGhDr')
            else:
                action = get('.FdDr')

        elif self.fsmState == 'ghostsClose-nonEnergized':
            dirClosestEscapeJunction = get('.SfJnDr')
#            print(dirClosestEscapeJunction)
            action = dirClosestEscapeJunction
            if action is None:
                legalMoves = gameState.getLegalActions()
                dirClosestNonscaredGhost = get(
                    '.NScGhDr')
                action = random.choice(legalMoves)
                # Choose a random move away from the ghost, if any
                if dirClosestNonscaredGhost in legalMoves:
                    legalMoves.remove(dirClosestNonscaredGhost)
                legalMoves.remove(Direction.stop)
#                print(legalMoves)
#                print('dirClosestNonscaredGhost', dirClosestNonscaredGhost)
                if len(legalMoves) > 0:
                    action = random.choice(legalMoves)
#                print(action)

        elif self.fsmState == 'ghostsClose-energized':
            dirClosestGhost = get('.ScGhDr')
            action = dirClosestGhost
        else:
            raise Exception("Unknown FSM state ", self.fsmState)
        if action is None:
            return 'Stop'
        else:
            return action
