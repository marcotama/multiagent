# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import random
from pacman.game import Agent, GameState
from pacman.features_extractor import PacmanFeaturesExtractor


class ConservativePacman(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide. You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    def __init__(self, index=0):
        super().__init__(index)
        self.index = index
        self.featuresExtractor = PacmanFeaturesExtractor([], index)

    def get_action(self, gameState: GameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation
        function.

        Just like in the previous project, getAction takes a GameState and
        returns some Directions.X for some X in the set
        {North, South, West, East, Stop}
        :param gameState: game state
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action)
                  for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index
                       for index, score in enumerate(scores)
                       if score == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, gameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are
        better.

        The code below extracts some useful information from the state, like
        the remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        :param gameState: game state
        :param action: action
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = gameState.generatePacmanSuccessor(action)
        self.featuresExtractor.set_game_state(successorGameState)
        get = self.featuresExtractor.extract_feature  # Alias for readability

        distClosestNonScaredGhost = get('.NScGhDs')
        score = 0.0
        safeDistance = 7
        if distClosestNonScaredGhost is not None:
            score -= max(0, safeDistance - distClosestNonScaredGhost) ** 2

        distClosestFood = get('.FdDs')
        if distClosestFood is not None:
            score -= distClosestFood / 100
        else:
            score += 1000

        remainingFood = get('Fd#')
        score -= remainingFood

        return score


class AggressivePacman(ConservativePacman):
    def evaluationFunction (self, gameState, action):
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = gameState.generatePacmanSuccessor(action)
        self.featuresExtractor.set_game_state(successorGameState)
        get = self.featuresExtractor.extract_feature  # Alias for readability

        score = super().evaluationFunction(gameState, action)

        nCapsules = len(get('capsule-coordinates'))
        nScaredGhosts = len(get('scared_ghost-coordinates'))
        distClosestScaredGhost = get('.ScGhDs')
        lowestPursuableDistance = 10
        score -= nCapsules * 1000
        score -= nScaredGhosts * 100
        if distClosestScaredGhost is not None:
            score -= min(10, distClosestScaredGhost) * lowestPursuableDistance

        return score

