# -*- coding: utf-8 -*-
# game.py
# -------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC
# Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# game.py
# -------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import traceback
from abc import abstractmethod
from util.util import manhattanDistance, nearest_point, TimeoutFunction, TimeoutFunctionException
import sys
import time
import io
#
# Parts worth reading #
#

from typing import Optional, Tuple, List, Union
from enum import Enum


# blueprint for agents
class Agent:
    """
    An agent must define a getAction method:
        def getAction(self, gameState): # returns an action given a game state, optionally updating internal knowledge

    but may also define the following methods which will be called if they exist:
        def registerInitialState(self, state): # inspects the starting state
        def observationFunction(self, state): # maps the game state in some features
        def final(self, state): # updates internal knowledge, but does not need to return anything
    """

    def __init__(self, index=0):
        self.index = index
        self.experience = []

    @abstractmethod
    def get_action(
            self,
            gameState: 'GameState'
    ):
        """
        The Agent will receive a GameState
        (from either {pacman, capture, sonar}.py) and must return an action
        from Directions.{North, South, East, West, Stop}
        :param gameState: state of the environment
        """
        pass


# enum of cardinal directions
class Direction(Enum):
    north = 'North'
    south = 'South'
    east = 'East'
    west = 'West'
    stop = 'Stop'

    def __eq__(self, other):
        return str(self) == str(other)

    def __lt__(self, other):
        return str(self) < str(other)

    def __hash__(self):
        return hash(self.value)

    def __str__(self):
        return self.value

    left = {
        north: west,
        south: east,
        east : north,
        west : south,
        stop : stop
    }

    right = dict([(y, x) for x, y in list(left.items())])

    reverse = {
        north: south,
        south: north,
        east: west,
        west: east,
        stop: stop
    }


# position and direction
class Configuration:
    """
    A Configuration holds the (x,y) coordinate of a character, along with its
    traveling direction.

    The convention for positions, like a graph, is that (0,0) is the lower left
    corner, x increases horizontally and y increases vertically. Therefore,
    north is the direction of increasing y, or (0,1).
    """

    def __init__(
            self,
            pos: Tuple[int, int],
            direction: Direction
    ):
        self.pos = pos
        self.direction = direction  # used to draw sprites

    def getPosition(self):
        return self.pos

    def getDirection(self):
        return self.direction

    def isInteger(self):
        x, y = self.pos
        return x == int(x) and y == int(y)

    def __eq__(self, other: 'Configuration'):
        if other is None:
            return False
        return self.pos == other.pos and self.direction == other.direction

    def __hash__(self):
        x = hash(self.pos)
        y = hash(self.direction)
        return hash(x + 13 * y)

    def __str__(self):
        return "(x,y)=" + str(self.pos) + ", " + str(self.direction)

    def generateSuccessor(self, vector: Tuple[int, int]):
        """
        Generates a new configuration reached by translating the current
        configuration by the action vector.  This is a low-level call and does
        not attempt to respect the legality of the movement.

        Actions are movement vectors.
        :param vector: action vector
        """
        x, y = self.pos
        dx, dy = vector
        direction = Actions.vectorToDirection(vector)
        if direction == Direction.stop:
            direction = self.direction  # There is no stop direction
        return Configuration((x + dx, y + dy), direction)


# position, direction, speed, scared, ...
class AgentState:
    """
    AgentStates hold the state of an agent (configuration, speed, scared, etc).
    """

    def __init__(self, startConfiguration: Configuration, isPacman: bool):
        self.start = startConfiguration
        self.configuration = startConfiguration
        self.isPacman = isPacman
        self.scaredTimer = 0
        self.numCarrying = 0
        self.numReturned = 0

    def __str__(self):
        if self.isPacman:
            return "Pacman: " + str(self.configuration)
        else:
            return "Ghost: " + str(self.configuration)

    def __eq__(self, other: 'AgentState'):
        if other is None:
            return False
        return (self.configuration == other.configuration and
                self.scaredTimer == other.scaredTimer)

    def __hash__(self):
        return hash(hash(self.configuration) + 13 * hash(self.scaredTimer))

    def copy(self):
        state = AgentState(self.start, self.isPacman)
        state.configuration = self.configuration
        state.scaredTimer = self.scaredTimer
        state.numCarrying = self.numCarrying
        state.numReturned = self.numReturned
        return state

    def getPosition(self):
        if self.configuration is None:
            return None
        return self.configuration.getPosition()

    def getDirection(self):
        return self.configuration.getDirection()


# 2d bool array
class Grid:
    """
    A 2-dimensional array of objects backed by a list of lists. Data is
    accessed via grid[x][y] where (x,y) are positions on a Pacman map with x
    horizontal, y vertical and the origin (0,0) in the bottom left corner.

    The __str__ method constructs an output that is oriented like a pacman
    board.
    """

    def __init__(self, width, height, initialValue=False,
                 bitRepresentation=None):
        if initialValue not in [False, True]:
            raise Exception('Grids can only contain booleans')
        self.CELLS_PER_INT = 30

        self.width = width
        self.height = height
        self.data = [[initialValue] * height for _ in range(width)]
        if bitRepresentation:
            self._unpackBits(bitRepresentation)

    def __getitem__(self, i):
        return self.data[i]

    def __setitem__(self, key, item):
        self.data[key] = item

    def __str__(self):
        out = [[str(self.data[x][y])[0] for x in range(self.width)]
               for y in range(self.height)]
        out.reverse()
        return '\n'.join([''.join(x) for x in out])

    def __eq__(self, other):
        if other is None:
            return False
        return self.data == other.data

    def __hash__(self):
        # return hash(str(self))
        base = 1
        h = 0
        for l in self.data:
            for i in l:
                if i:
                    h += base
                base *= 2
        return hash(h)

    def copy(self):
        g = Grid(self.width, self.height)
        g.data = [x[:] for x in self.data]
        return g

    def deepCopy(self):
        return self.copy()

    def shallowCopy(self):
        g = Grid(self.width, self.height)
        g.data = self.data
        return g

    def count(self, item=True):
        return sum([x.count(item) for x in self.data])

    def asList(self, key=True):
        l = []
        for x in range(self.width):
            for y in range(self.height):
                if self[x][y] == key:
                    l.append((x, y))
        return l

    def packBits(self):
        """
        Returns an efficient int list representation

        (width, height, bitPackedInts...)
        """
        bits = [self.width, self.height]
        currentInt = 0
        for i in range(self.height * self.width):
            bit = self.CELLS_PER_INT - (i % self.CELLS_PER_INT) - 1
            x, y = self._cellIndexToPosition(i)
            if self[x][y]:
                currentInt += 2 ** bit
            if (i + 1) % self.CELLS_PER_INT == 0:
                bits.append(currentInt)
                currentInt = 0
        bits.append(currentInt)
        return tuple(bits)

    def _cellIndexToPosition(self, index):
        x = index / self.height
        y = index % self.height
        return x, y

    def _unpackBits(self, bits):
        """
        Fills in data from a bit-level representation
        """
        cell = 0
        for packed in bits:
            for bit in self._unpackInt(packed, self.CELLS_PER_INT):
                if cell == self.width * self.height:
                    break
                x, y = self._cellIndexToPosition(cell)
                self[x][y] = bit
                cell += 1

    def _unpackInt(self, packed, size):
        booleans = []
        if packed < 0:
            raise ValueError("must be a positive integer")
        for i in range(size):
            n = 2 ** (self.CELLS_PER_INT - i - 1)
            if packed >= n:
                booleans.append(True)
                packed -= n
            else:
                booleans.append(False)
        return booleans


def reconstituteGrid(bitRep):
    if not isinstance(bitRep, tuple):
        return bitRep
    width, height = bitRep[:2]
    return Grid(width, height, bitRepresentation=bitRep[2:])


#
# Parts you shouldn't have to read #
#


class Actions:
    """
    A collection of static methods for manipulating move actions.
    """
    # Directions
    _directions = {
        Direction.north: (0, 1),
        Direction.south: (0, -1),
        Direction.east: (1, 0),
        Direction.west: (-1, 0),
        Direction.stop: (0, 0)
    }

    TOLERANCE = .001

    @staticmethod
    def reverseDirection(action):
        if action == Direction.north:
            return Direction.south
        if action == Direction.south:
            return Direction.north
        if action == Direction.east:
            return Direction.west
        if action == Direction.west:
            return Direction.east
        return action

    @staticmethod
    def vectorToDirection(vector: Tuple[int, int]): # vectors should be horizontal or vertical, or behavior is undefined
        dx, dy = vector
        if dy > 0:
            return Direction.north
        if dy < 0:
            return Direction.south
        if dx < 0:
            return Direction.west
        if dx > 0:
            return Direction.east
        return Direction.stop

    @staticmethod
    def directionToVector(direction, speed=1.0):
        dx, dy = Actions._directions[direction]
        return dx * speed, dy * speed

    @staticmethod
    def getPossibleActions(config, walls) -> List[Direction]:
        possible = []
        x, y = config.pos
        x_int, y_int = int(x + 0.5), int(y + 0.5)

        # In between grid points, all agents must continue straight
        if abs(x - x_int) + abs(y - y_int) > Actions.TOLERANCE:
            return [config.getDirection()]

        for d, vec in Actions._directions.items():
            dx, dy = vec
            next_y = y_int + dy
            next_x = x_int + dx
            if not walls[next_x][next_y]:
                possible.append(d)

        return possible

    @staticmethod
    def getLegalNeighbors(position, walls):
        x, y = position
        x_int, y_int = int(x + 0.5), int(y + 0.5)
        neighbors = []
        for vec in Actions._directions.values():
            dx, dy = vec
            next_x = x_int + dx
            if next_x < 0 or next_x == walls.width:
                continue
            next_y = y_int + dy
            if next_y < 0 or next_y == walls.height:
                continue
            if not walls[next_x][next_y]:
                neighbors.append((next_x, next_y))
        return neighbors

    @staticmethod
    def getSuccessor(position, action):
        dx, dy = Actions.directionToVector(action)
        x, y = position
        return x + dx, y + dy


# game state information
class GameStateData:
    """
    Game state information, such as layout of the level, food pellets position, agent states, win/lose, score and some
    stats.
    """

    def __init__(self, prevState: 'GameStateData' = None):
        """
        Generates a new data packet by copying information from its
        predecessor.
        """
        if prevState is not None:
            self.food = prevState.food.copy()
            self.numFood = prevState.numFood
            self.capsules = prevState.capsules[:]
            self.agentStates = self.copyAgentStates(prevState.agentStates)
            self.layout = prevState.layout
            self.eaten = prevState.eaten
            self.score = prevState.score
            self.eatenGhostsCount = prevState.eatenGhostsCount
            self.eatenFoodCount = prevState.eatenFoodCount
            self.eatenCapsulesCount = prevState.eatenCapsulesCount

        self.foodEaten = None
        self.foodAdded = None
        self.capsuleEaten = None
        self.agentMoved = None
        self.lose = False
        self.win = False
        self.scoreChange = 0

    def deepCopy(self):
        state = GameStateData(self)
        state.food = self.food.deepCopy()
        state.numFood = self.numFood
        state.layout = self.layout.deepCopy()
        state.agentMoved = self.agentMoved
        state.foodEaten = self.foodEaten
        state.foodAdded = self.foodAdded
        state.capsuleEaten = self.capsuleEaten
        return state

    @staticmethod
    def copyAgentStates(agentStates):
        copiedStates = []
        for agentState in agentStates:
            copiedStates.append(agentState.copy())
        return copiedStates

    def __eq__(self, other):
        """
        Allows two states to be compared.
        """
        if other is None:
            return False
        if not isinstance(other, GameStateData):
            return False
        if self.agentStates != other.agentStates:
            return False
        if self.food != other.food:
            return False
        if self.capsules != other.capsules:
            return False
        if self.score != other.score:
            return False
        return True

    def __hash__(self):
        """
        Allows states to be keys of dictionaries.
        """
        for i, state in enumerate(self.agentStates):
            try:
                int(hash(state))
            except TypeError as e:
                print(e)
                # hash(state)
        return int((
            hash(tuple(self.agentStates)) +
            13 * hash(self.food) +
            113 * hash(tuple(self.capsules)) +
            7 * hash(self.score)) % 1048575)

    def __str__(self):
        width, height = self.layout.width, self.layout.height
        grid = Grid(width, height)
        if isinstance(self.food, tuple):
            self.food = reconstituteGrid(self.food)
        for x in range(width):
            for y in range(height):
                food, walls = self.food, self.layout.walls
                grid[x][y] = self._foodWallStr(food[x][y], walls[x][y])

        for agentState in self.agentStates:
            if agentState is None:
                continue
            if agentState.configuration is None:
                continue
            x, y = [int(i)
                    for i in nearest_point(agentState.configuration.pos)]
            agent_dir = agentState.configuration.direction
            if agentState.isPacman:
                grid[x][y] = self._pacStr(agent_dir)
            else:
                grid[x][y] = self._ghostStr(agent_dir)

        for x, y in self.capsules:
            grid[x][y] = 'o'

        return str(grid) + ("\nScore: %d\n" % self.score)

    @staticmethod
    def _foodWallStr(hasFood, hasWall):
        if hasFood:
            return '.'
        elif hasWall:
            return '%'
        else:
            return ' '

    _pStr = {
        Direction.north: 'v',
        Direction.south: '^',
        Direction.east: '<',
        Direction.west: '>',
        Direction.stop: '*',
    }
    @staticmethod
    def _pacStr(d):
        return GameStateData._pStr[d]

    _gStr = {
        Direction.north: 'G',
        Direction.south: 'G',
        Direction.east: 'G',
        Direction.west: 'G',
        Direction.stop: 'G',
    }
    @staticmethod
    def _ghostStr(d):
        return GameStateData._gStr[d]

    def initialize(self, layout, numGhostAgents):
        """
        Creates an initial game state from a layout array (see layout.py).
        :param layout: level layout
        :param numGhostAgents: number of ghost agents
        """
        self.food = layout.food.copy()
        self.numFood = self.food.count()
        self.capsules = layout.capsules.copy()
        self.layout = layout
        self.score = 0
        self.agentStates = []
        numGhosts = 0
        for isPacman, pos in layout.agentPositions:
            if not isPacman:
                if numGhosts == numGhostAgents:
                    continue  # Max ghosts reached already
                else:
                    numGhosts += 1
            self.agentStates.append(AgentState(Configuration(pos, Direction.stop), isPacman))
        self.eaten = [False] * len(self.agentStates)

        self.eatenGhostsCount = 0
        self.eatenFoodCount = 0
        self.eatenCapsulesCount = 0


try:
    # noinspection PyUnresolvedReferences
    import boinc

    _BOINC_ENABLED = True
except ImportError:
    _BOINC_ENABLED = False


class Game:
    """
    The Game manages the control flow, soliciting actions from agents.
    """

    def __init__(
            self,
            agents,
            display: 'Union['
                         'pacman.text_display.NullGraphics,'
                         'pacman.text_display.PacmanGraphics, '
                         'pacman.graphics_display.PacmanGraphics'
                     ']',
            rules,
            startingIndex=0,
            muteAgents=False,
            catchExceptions=False):
        self.agentCrashed = False
        self.agents = agents
        self.display = display
        self.rules = rules
        self.startingIndex = startingIndex
        self.gameOver = False
        self.muteAgents = muteAgents
        self.catchExceptions = catchExceptions
        self.moveHistory = [] # type: List[Tuple[int, int]]
        self.totalAgentTimes = [0] * len(agents)
        self.totalAgentTimeWarnings = [0] * len(agents)
        self.agentTimeout = False
        self.numMoves = 0
        self.state = None  # type: GameState
        self.agentOutput = [io.StringIO()] * len(agents)

    def getProgress(self):
        if self.gameOver:
            return 1.0
        else:
            return self.rules.getProgress(self)

    def _agentCrash(self, agentIndex, quiet=False):
        """
        Helper method for handling agent crashes
        """
        if not quiet:
            traceback.print_exc()
        self.gameOver = True
        self.agentCrashed = True
        self.rules.agentCrash(self, agentIndex)

    OLD_STDOUT = None
    OLD_STDERR = None

    def mute(self, agentIndex):
        if not self.muteAgents:
            return
        global OLD_STDOUT, OLD_STDERR
        OLD_STDOUT = sys.stdout
        OLD_STDERR = sys.stderr
        sys.stdout = self.agentOutput[agentIndex]
        sys.stderr = self.agentOutput[agentIndex]

    def unmute(self):
        if not self.muteAgents:
            return
        global OLD_STDOUT, OLD_STDERR
        # Revert stdout/stderr to originals
        sys.stdout = OLD_STDOUT
        sys.stderr = OLD_STDERR

    def agentExecute(self,
                     agentIndex: int,
                     functionName: str,
                     functionParam: object,
                     timeout: Optional[float],
                     onTimeoutMsg: str,
                     onTimeoutCrash: bool,
                     raiseIfNotImplemented: bool):

        agent = self.agents[agentIndex]
        startTime = time.time()
        if functionName in dir(agent):
            agentFunction = getattr(agent, functionName)
            self.mute(agentIndex)
            if self.catchExceptions and timeout is not None:
                # noinspection PyBroadException
                try:
                    timedFunc = TimeoutFunction(agentFunction, timeout)
                    try:
                        retValue = timedFunc(functionParam)
                    except TimeoutFunctionException:
                        print(onTimeoutMsg % agentIndex, file=sys.stderr)
                        self.unmute()
                        if onTimeoutCrash:
                            self._agentCrash(agentIndex, quiet=True)
                        retValue = None
                except Exception:
                    self._agentCrash(agentIndex, quiet=False)
                    self.unmute()
                    retValue = None
            else:
                retValue = agentFunction(functionParam)
            self.unmute()
        elif raiseIfNotImplemented:
            raise NotImplementedError("Agent does not implement function %s" %
                                      functionName)
        else:
            retValue = None
        timeTaken = time.time() - startTime
        return retValue, timeTaken

    def registerAgentTime(self, agentIndex, timeTaken, remainingTime):
        if self.catchExceptions:
            if remainingTime < 0:
                self.totalAgentTimeWarnings += 1
                print(("Agent %d took too long to make a move! " +
                       "This is warning %d") % (agentIndex, self.totalAgentTimeWarnings),
                      file=sys.stderr)
                if self.totalAgentTimeWarnings > self.rules.getMaxTimeWarnings(agentIndex):
                    print(("Agent %d exceeded the maximum number of " +
                           "warnings: %d") % (agentIndex, self.totalAgentTimeWarnings),
                          file=sys.stderr)
                    self.agentTimeout = True
                    self._agentCrash(agentIndex, quiet=True)
                    self.unmute()
                    return True

                self.totalAgentTimes[agentIndex] += timeTaken
                tat = self.totalAgentTimes[agentIndex]

                if tat > self.rules.getMaxTotalTime(agentIndex):
                    print("Agent %d ran out of time! (time: %1.2f)" %
                          (agentIndex, tat), file=sys.stderr)
                    self.agentTimeout = True
                    self._agentCrash(agentIndex, quiet=True)
                    self.unmute()
                    return True
                self.unmute()
        return False

    def agentRegisterInitialState(self, agentIndex, gameState):
        self.agentExecute(
            agentIndex=agentIndex,
            functionName="register_initial_state",
            functionParam=gameState,
            timeout=int(self.rules.getMaxStartupTime(agentIndex)),
            onTimeoutMsg="Agent %d ran out of time on startup!",
            onTimeoutCrash=True,
            raiseIfNotImplemented=False)

    def agentObservationFunction(self, agentIndex, gameState):
        observation, timeTakenObserving = self.agentExecute(
            agentIndex=agentIndex,
            functionName="observation_fn",
            functionParam=gameState,
            timeout=int(self.rules.getMoveTimeout(agentIndex)),
            onTimeoutMsg="Agent %d timed out while observing state!",
            onTimeoutCrash=False,
            raiseIfNotImplemented=False)
        return observation, timeTakenObserving

    def agentGetAction(self, agentIndex, observation, timeout):
        action, timeTakenDeciding = self.agentExecute(
            agentIndex=agentIndex,
            functionName="get_action",
            functionParam=observation,
            timeout=timeout,
            onTimeoutMsg="Agent %d timed out while choosing action!",
            onTimeoutCrash=False,
            raiseIfNotImplemented=True)
        return action, timeTakenDeciding

    def agentFinal(self, agentIndex, gameState):
        self.agentExecute(
            agentIndex=agentIndex,
            functionName="final",
            functionParam=gameState,
            timeout=None,
            onTimeoutMsg="Agent %d ran out of time on final observation!",
            onTimeoutCrash=False,
            raiseIfNotImplemented=False)

    def run(self, passDeepCopies=False):
        """
        Main control loop for game play.
        :param passDeepCopies: if True, deep copies of the state are passed to the agents; if False, references to the
            actual state are passed; True should be used if agents cannot be trusted (i.e. written by students), but
            performance-wise, False is a better choice.
        """
        self.display.initialize(self.state.data)
        self.numMoves = 0

        # self.display.initialize(self.state.makeObservation(1).data)
        # inform learning agents of the game start
        for agentIndex, agent in enumerate(self.agents):
            sys.stderr.flush()
            if not agent:
                self.mute(agentIndex)
                # this is a null agent, meaning it failed to load
                # the other team wins
                print("Agent %d failed to load" % agentIndex, file=sys.stderr)
                self.unmute()
                self._agentCrash(agentIndex, quiet=True)
                return
            if passDeepCopies:
                self.agentRegisterInitialState(agentIndex, self.state.deepCopy())
            else:
                self.agentRegisterInitialState(agentIndex, self.state)

        agentIndex = self.startingIndex
        numAgents = len(self.agents)

        # one iteration == one turn of one agent
        while not self.gameOver:
            sys.stderr.flush()
            remainingTime = int(self.rules.getMoveTimeout(agentIndex))

            # Generate an observation of the state
            if passDeepCopies:
                observation, timeTakenObserving = self.agentObservationFunction(agentIndex, self.state.deepCopy())
            else:
                observation, timeTakenObserving = self.agentObservationFunction(agentIndex, self.state)
            remainingTime -= timeTakenObserving

            # Solicit an action
            if remainingTime >= 0:
                if observation is None:
                    if passDeepCopies:
                        observation = self.state.deepCopy()
                    else:
                        observation = self.state
                action, timeTakenDeciding = self.agentGetAction(agentIndex, observation, remainingTime)
                remainingTime -= timeTakenDeciding
            else:
                timeTakenDeciding = 0

            # Check time limits
            timeTaken = timeTakenObserving + timeTakenDeciding
            self.registerAgentTime(agentIndex, timeTaken, remainingTime)

            # Execute the action
            self.moveHistory.append((agentIndex, action))
            if self.catchExceptions:
                # noinspection PyBroadException
                try:
                    self.state = self.state.generateSuccessor(agentIndex, action)
                except Exception:
                    self.mute(agentIndex)
                    self._agentCrash(agentIndex)
                    self.unmute()
                    return
            else:
                self.state = self.state.generateSuccessor(agentIndex, action)

            # Change the display
            self.display.update(self.state.data)
            # idx = agentIndex - agentIndex % 2 + 1
            # self.display.update( self.state.makeObservation(idx).data )

            # Allow for game specific conditions (winning, losing, etc.)
            self.rules.process(self.state, self)
            # Track progress
            if agentIndex + 1 == numAgents:
                self.numMoves += 1

            # Next agent
            agentIndex = (agentIndex + 1) % numAgents

            if _BOINC_ENABLED:
                boinc.set_fraction_done(self.getProgress())

        # Inform a learning agent of the game result
        for agentIndex, agent in enumerate(self.agents):
            if passDeepCopies:
                self.agentFinal(agentIndex, self.state.deepCopy())
            else:
                self.agentFinal(agentIndex, self.state)
        self.display.finish()


class GameState:
    """
    A GameState specifies the full game state, including the food, capsules,
    agent configurations and score changes.

    GameStates are used by the Game object to capture the actual state of the
    game and can be used by agents to reason about the game.

    Much of the information in a GameState is stored in a GameStateData object.
    We strongly suggest that you access that data via the accessor methods
    below rather than referring to the GameStateData object directly.

    Note that in classic Pacman, Pacman is always agent 0.
    """

    #
    # Accessor methods: use these to access state data #
    #

    # static variable keeps track of which states have had getLegalActions
    # called
    explored = set()
    keepTrackOfExploredStates = False

    def getAndResetExplored():
        tmp = GameState.explored.copy()
        GameState.explored = set()
        return tmp

    getAndResetExplored = staticmethod(getAndResetExplored)

    def getLegalActions(self, agentIndex=0) -> List[Direction]:
        """
        Returns the legal actions for the agent specified.
        :param agentIndex: agent index
        """
        if GameState.keepTrackOfExploredStates:
            GameState.explored.add(self)
        if self.isWin() or self.isLose():
            return []

        if agentIndex == 0:  # Pacman is moving
            return PacmanRules.getLegalActions(self)
        else:
            return GhostRules.getLegalActions(self, agentIndex)

    def generateSuccessor(self, agentIndex, action):
        """
        Returns the successor state after the specified agent takes the action.
        :param agentIndex: agent index
        :param action: action of the agent
        """
        # Check that successors exist
        if self.isWin() or self.isLose():
            raise Exception('Can\'t generate a successor of a terminal state.')

        # Copy current state
        state = GameState(self)

        # Let agent's logic deal with its action's effects on the board
        if agentIndex == 0:  # Pacman is moving
            state.data.eaten = [False] * state.getNumAgents()
            PacmanRules.applyAction(state, action)
        else:  # A ghost is moving
            GhostRules.applyAction(state, action, agentIndex)

        # Time passes
        if agentIndex == 0:
            state.data.scoreChange += - \
                TIME_PENALTY  # Penalty for waiting around
        else:
            GhostRules.decrementTimer(state.data.agentStates[agentIndex])

        # Resolve multi-agent effects
        GhostRules.checkDeath(state, agentIndex)

        # Book keeping
        state.data.agentMoved = agentIndex
        state.data.score += state.data.scoreChange
        if GameState.keepTrackOfExploredStates:
            GameState.explored.add(self)
            GameState.explored.add(state)
        return state

    def getLegalPacmanActions(self):
        return self.getLegalActions(0)

    def generatePacmanSuccessor(self, action):
        """
        Generates the successor state after the specified pacman move
        :param action: action of pacman
        """
        return self.generateSuccessor(0, action)

    def getPacmanState(self):
        """
        Returns an AgentState object for pacman (in game.py)

        state.pos gives the current position
        state.direction gives the travel vector
        """
        return self.data.agentStates[0].copy()

    def getPacmanPosition(self):
        return self.data.agentStates[0].getPosition()

    def getGhostStates(self):
        return self.data.agentStates[1:]

    def getGhostState(self, agentIndex):
        if agentIndex == 0 or agentIndex >= self.getNumAgents():
            raise Exception("Invalid index passed to getGhostState")
        return self.data.agentStates[agentIndex]

    def getGhostPosition(self, agentIndex):
        if agentIndex == 0:
            raise Exception("Pacman's index passed to getGhostPosition")
        return self.data.agentStates[agentIndex].getPosition()

    def getGhostPositions(self):
        return [s.getPosition() for s in self.getGhostStates()]

    def getNumAgents(self):
        return len(self.data.agentStates)

    def getScore(self):
        return float(self.data.score)

    def getCapsules(self):
        """
        Returns a list of positions (x,y) of the remaining capsules.
        """
        return self.data.capsules

    def getNumFood(self):
        return self.data.numFood

    def getFood(self):
        """
        Returns a Grid of boolean food indicator variables.

        Grids can be accessed via list notation, so to check
        if there is food at (x,y), just call

        currentFood = state.getFood()
        if currentFood[x][y] == True: ...
        """
        return self.data.food

    def getWalls(self):
        """
        Returns a Grid of boolean wall indicator variables.

        Grids can be accessed via list notation, so to check
        if there is a wall at (x,y), just call

        walls = state.getWalls()
        if walls[x][y] == True: ...
        """
        return self.data.layout.walls

    def hasFood(self, x, y):
        return self.data.food[x][y]

    def hasWall(self, x, y):
        return self.data.layout.walls[x][y]

    def isLose(self):
        return self.data.lose

    def isWin(self):
        return self.data.win

    #
    # Helper methods:               #
    # You shouldn't need to call these directly #
    #

    def __init__(self, prevState=None):
        """
        Generates a new state by copying information from its predecessor.
        """
        if prevState is None:  # Initial state
            self.data = GameStateData()
        else:
            self.data = GameStateData(prevState.data)

    def deepCopy(self):
        state = GameState(self)
        state.data = self.data.deepCopy()
        return state

    def __eq__(self, other):
        """
        Allows two states to be compared.
        """
        return hasattr(other, 'data') and self.data == other.data

    def __hash__(self):
        """
        Allows states to be keys of dictionaries.
        """
        return hash(self.data)

    def __str__(self):

        return str(self.data)

    def initialize(self, layout, numGhostAgents=1000):
        """
        Creates an initial game state from a layout array (see layout.py).
        :param layout: level layout
        :type layout: Layout
        :param numGhostAgents: number of ghosts
        :type numGhostAgents: int
        """
        self.data.initialize(layout, numGhostAgents)


#
# THE HIDDEN SECRETS OF PACMAN                         #
#
# You shouldn't need to look through the code in this section of the file. #
#

SCARED_TIME = 40  # Moves ghosts are scared
COLLISION_TOLERANCE = 0.7  # How close ghosts must be to Pacman to kill
TIME_PENALTY = 1  # Number of points lost each round


class ClassicGameRules:
    """
    These game rules manage the control flow of a game, deciding when
    and how the game starts and ends.
    """

    def __init__(self, timeout=30):
        self.timeout = timeout
        self.initialState = None
        self.quiet = False

    def newGame(self, layout, pacmanAgent, ghostAgents, display, quiet=False,
                catchExceptions=False) -> Game:
        agents = [pacmanAgent] + ghostAgents[:layout.getNumGhosts()]
        initState = GameState()
        initState.initialize(layout, len(ghostAgents))
        game = Game(agents, display, self, catchExceptions=catchExceptions)
        game.state = initState  # type: GameState
        self.initialState = initState.deepCopy()
        self.quiet = quiet
        return game

    def process(self, gameState, game):
        """
        Checks to see whether it is time to end the game.
        :param gameState: game state
        :param game: game
        """
        if gameState.isWin():
            self.win(gameState, game)
        if gameState.isLose():
            self.lose(gameState, game)

    def win(self, state, game):
        if not self.quiet:
            print("Pacman emerges victorious! Score: %d" % state.data.score)
        game.gameOver = True

    def lose(self, state, game):
        if not self.quiet:
            print("Pacman died! Score: %d" % state.data.score)
        game.gameOver = True

    def getProgress(self, game):
        return float(game.state.getNumFood()) / self.initialState.getNumFood()

    def agentCrash(self, game, agentIndex):
        if agentIndex == 0:
            print("Pacman crashed")
        else:
            print("A ghost crashed")

    def getMaxTotalTime(self, agentIndex):
        return self.timeout

    def getMaxStartupTime(self, agentIndex):
        return self.timeout

    def getMoveWarningTime(self, agentIndex):
        return self.timeout

    def getMoveTimeout(self, agentIndex):
        return self.timeout

    def getMaxTimeWarnings(self, agentIndex):
        return 0


class PacmanRules:
    """
    These functions govern how pacman interacts with his environment under
    the classic game rules.
    """
    PACMAN_SPEED = 1

    @staticmethod
    def getLegalActions(gameState: GameState):
        """
        Returns a list of possible actions.
        :param gameState: game state
        """
        return Actions.getPossibleActions(gameState.getPacmanState().configuration,
                                          gameState.data.layout.walls)


    @staticmethod
    def applyAction(gameState, action):
        """
        Edits the state to reflect the results of the action.
        :param gameState: game state
        :param action: action to be performed
        """
        legal = PacmanRules.getLegalActions(gameState)
        if action not in legal:
            raise Exception("Illegal action " + str(action))

        pacmanState = gameState.data.agentStates[0]

        # Update Configuration
        vector = Actions.directionToVector(action, PacmanRules.PACMAN_SPEED)
        currConfiguration = pacmanState.configuration
        pacmanState.configuration = currConfiguration.generateSuccessor(vector)

        # Eat
        nxt = pacmanState.configuration.getPosition()
        nearest = nearest_point(nxt)
        if manhattanDistance(nearest, nxt) <= 0.5:
            # Remove food
            PacmanRules.consume(nearest, gameState)


    @staticmethod
    def consume(
            position: Tuple[int, int],
            state: GameState
    ):
        x, y = position
        # Eat food
        if state.data.food[x][y]:
            state.data.scoreChange += 10
            state.data.eatenFoodCount += 1
            state.data.food = state.data.food
            state.data.food[x][y] = False
            state.data.foodEaten = position
            state.data.numFood -= 1
            if state.data.numFood == 0 and not state.data.lose:
                state.data.scoreChange += 500
                state.data.win = True
        # Eat capsule
        if position in state.getCapsules():
            state.data.eatenCapsulesCount += 1
            state.data.capsules.remove(position)
            state.data.capsuleEaten = position
            # Reset all ghosts' scared timers
            for index in range(1, len(state.data.agentStates)):
                state.data.agentStates[index].scaredTimer = SCARED_TIME


class GhostRules:
    """
    These functions dictate how ghosts interact with their environment.
    """
    GHOST_SPEED = 1.0

    @staticmethod
    def getLegalActions(
            gameState: GameState,
            ghostIndex: int
    ):
        """
        Ghosts cannot stop, and cannot turn around unless they
        reach a dead end, but can turn 90 degrees at intersections.
        :param gameState: game state
        :param ghostIndex: ghost index
        """
        conf = gameState.getGhostState(ghostIndex).configuration
        possibleActions = Actions.getPossibleActions(
            conf, gameState.data.layout.walls)
        reverse = Actions.reverseDirection(conf.direction)
        if Direction.stop in possibleActions:
            possibleActions.remove(Direction.stop)
        if reverse in possibleActions and len(possibleActions) > 1:
            possibleActions.remove(reverse)
        return possibleActions


    @staticmethod
    def applyAction(state, action, ghostIndex):

        legal = GhostRules.getLegalActions(state, ghostIndex)
        if action not in legal:
            raise Exception("Illegal ghost action " + str(action))

        ghostState = state.data.agentStates[ghostIndex]
        speed = GhostRules.GHOST_SPEED
        if ghostState.scaredTimer > 0:
            speed /= 2.0
        vector = Actions.directionToVector(action, speed)
        ghostState.configuration = ghostState.configuration.generateSuccessor(vector)


    @staticmethod
    def decrementTimer(ghostState):
        timer = ghostState.scaredTimer
        if timer == 1:
            ghostState.configuration.pos = nearest_point(
                ghostState.configuration.pos)
        ghostState.scaredTimer = max(0, timer - 1)


    @staticmethod
    def checkDeath(state, agentIndex):
        pacmanPosition = state.getPacmanPosition()
        if agentIndex == 0:  # Pacman just moved; Anyone can kill him
            for index in range(1, len(state.data.agentStates)):
                ghostState = state.data.agentStates[index]
                ghostPosition = ghostState.configuration.getPosition()
                if GhostRules.canKill(pacmanPosition, ghostPosition):
                    GhostRules.collide(state, ghostState, index)
        else:
            ghostState = state.data.agentStates[agentIndex]
            ghostPosition = ghostState.configuration.getPosition()
            if GhostRules.canKill(pacmanPosition, ghostPosition):
                GhostRules.collide(state, ghostState, agentIndex)


    @staticmethod
    def collide(state, ghostState, agentIndex):
        if ghostState.scaredTimer > 0:
            state.data.scoreChange += 200
            state.data.eatenGhostsCount += 1
            GhostRules.placeGhost(state, ghostState)
            ghostState.scaredTimer = 0
            # Added for first-person
            state.data.eaten[agentIndex] = True
        else:
            if not state.data.win:
                state.data.scoreChange -= 500
                state.data.lose = True


    @staticmethod
    def canKill(pacmanPosition, ghostPosition):
        manDist = manhattanDistance(ghostPosition, pacmanPosition)
        return manDist <= COLLISION_TOLERANCE


    @staticmethod
    def placeGhost(state, ghostState):
        ghostState.configuration = ghostState.start

