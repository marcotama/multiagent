from fighting_ice.agents.common import LearningFightingController


class Trainer:
    def __init__(self, agent: LearningFightingController):
        self.agent = agent
        if hasattr(self.agent, 'qAgent'):
            self.agent.qAgent.mdp.save_history = False

    def train(self, data):
        """
        Runs the data against the given agent. The data should have the same format output by the engine.
        :param data: Fight data. Should have the same format output by the engine. This is a list of frames. Each
            frame should have fields remaining_frames, action, action_id, state, state_id, hp, energy, x, y, speed_x,
            speed_y, attack (optional) and projectiles. The field attack should have fields speed_x, speed_y,
            hit_damage, guard_damage, start_add_energy, hit_add_energy, guard_add_energy, give_energy, give_guard_recov,
            attack_type, attack_type_id, impact_x, impact_y and hit_area. The field hit_area should have fields bottom,
            top, left and right. The field projectiles should have the same structure as attack.
        """
        self.agent.initialize(data, True) # player does not really matter as such info is only necessary when deciding an action
        for round_data in data['rounds']:
            for frame_data in round_data:
                self.agent.getInformation(frame_data)
            self.agent.end_of_episode()
        self.agent.close()

    def train_from_json(self, file: str):
        import json

        try:
            with open(file, 'r') as f:
                data = json.load(f)
            self.train(data)
        except json.decoder.JSONDecodeError:
            pass

    def get_agent(self):
        return self.agent

