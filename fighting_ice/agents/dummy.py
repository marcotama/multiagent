from fighting_ice.agents.common import FightingController

class Dummy(FightingController):

    def getCharacter (self):
        return 'ZEN'

    def get_action (self):
        return None