from py4j.java_gateway import get_field

class MacheteOrig:
    def __init__(self):
        # Good practice to declare instance variables in __init__
        self.gateway = None
        self.frame_data = None # Updated after every call to getInformation
        self.input_key = None
        self.cc = None
        self.player = None
        self.opponent = None
        self.player_str = None
        self.opponent_str = None
        self.game_data = None
        self.simulator = None

        self.is_game_just_started = False

    def set_gateway(self, gateway):
        self.gateway = gateway

    def getCharacter(self):
        return "ZEN"

    def close(self):
        self.input_key = None
        self.player = None
        self.frame_data = None
        self.cc = None
        self.simulator = None
        self.game_data = None

    def getInformation(self, frameData):
        self.frame_data = frameData

    def initialize(self, gameData, player):
        self.input_key = self.gateway.jvm.structs.Key()
        self.player = player
        self.frame_data = self.gateway.jvm.structs.FrameData()
        self.cc = self.gateway.jvm.commandcenter.CommandCenter()
        self.game_data = gameData
        self.simulator = self.game_data.getSimulator()
        self.is_game_just_started = True



        self.player = player
        self.opponent = not player
        self.player_str = 'P1' if player else 'P2'
        self.opponent_str = 'P2' if player else 'P1'

        return 0

    def input(self):
        return self.input_key

    def processing(self):
        if self.frame_data.getEmptyFlag() or self.frame_data.getRemainingTime() <= 0:
            self.is_game_just_started = True
            return

        if not self.is_game_just_started:
            self.frame_data = self.simulator.simulate(self.frame_data, self.player, None, None, 17)
        else:
            self.is_game_just_started = False

        self.cc.setFrameData(self.frame_data, self.player)

        distance = self.cc.getDistanceX()

        my = self.cc.getMyCharacter()
        energy = my.getEnergy()
        my_x = get_field(my, 'x')
        my_state = get_field(my, "state")

        opp = self.cc.getEnemyCharacter()
        opp_x = get_field(opp, 'x')
        opp_state = get_field(opp, "state")

        xDifference = my_x - opp_x

        if self.cc.getskillFlag():
            self.input_key = self.cc.getSkillKey()
            return

        self.input_key.empty()
        self.cc.skillCancel()

        if (get_field(opp, "energy") >= 300) and ((get_field(my, "hp") - get_field(opp, "hp")) <= 300):
            self.cc.commandCall("FOR_JUMP _B B B")
        elif not my_state.equals(self.gateway.jvm.enumerate.State.AIR) and not my_state.equals(self.gateway.jvm.enumerate.State.DOWN):
            if distance > 150:
                self.cc.commandCall("FOR_JUMP")
            elif energy >= 300:
                self.cc.commandCall("STAND_D_DF_FC")
            elif (distance > 100) and (energy >= 50):
                self.cc.commandCall("STAND_D_DB_BB")
            elif opp_state.equals(self.gateway.jvm.enumerate.State.AIR):
                self.cc.commandCall("STAND_F_D_DFA")
            elif distance > 100:
                self.cc.commandCall("6 6 6")
            else:
                self.cc.commandCall("B")
        elif ((distance <= 150) and (my_state.equals(self.gateway.jvm.enumerate.State.AIR) or my_state.equals(self.gateway.jvm.enumerate.State.DOWN))
            and (((self.game_data.getStageXMax() - my_x) >= 200) or (xDifference > 0))
            and ((my_x >= 200) or xDifference < 0)):
            if energy >= 5:
                self.cc.commandCall("AIR_DB")
            else:
                self.cc.commandCall("B")
        else:
            self.cc.commandCall("B")

    class Java:
        implements = ["gameInterface.AIInterface"]