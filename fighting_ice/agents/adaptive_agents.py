"""
Classes implementing some Challenge-Sensitive Action Selection
"""

from typing import List, Optional, Union

from reinf.exploration_strategies import ExplorationParams
from util.typing2 import State, Action
from fighting_ice.agents.common import LearningFightingController
from reinf.adaptive import ChallengeSensitiveActionSelectionAgent as CSAS
from reinf.adaptive import QValueSensitiveActionSelectionAgent as QVSAS
from reinf.adaptive import StakesSensitiveActionSelectionAgent as SSAS


class AdaptiveAgent(LearningFightingController):

    def getCharacter (self):
        return 'ZEN'

    def __init__(self, features: List[str]):
        super().__init__(features)
        self.intelligence = None # type: Union[CSAS, QVSAS, SSAS]

    def learning_fn(
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            new_state: Optional[State],
            new_legal_actions: List[Action],
            reward: float
    ):
        self.intelligence.learn(
            state=state,
            legal_actions=legal_actions,
            action=action,
            reward=reward,
            new_state=new_state,
            new_legal_actions=new_legal_actions
        )

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions
        )


    def end_of_episode (self, *args, **kwargs):
        self.intelligence.end_of_episode()





class ChallengeSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
            self,
            index,
            evaluation_cycle: int,
            threshold: float,

            features: List[str],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            disable_primitive_actions: bool
    ):
        super().__init__(features)
        self.intelligence = CSAS(
            index,
            evaluation_cycle=evaluation_cycle,
            threshold=threshold,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            disable_primitive_actions=disable_primitive_actions
        )

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions,
            expected_outcome=1 # TODO
        )



class QValueSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
            self,
            index,

            features: List[str],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            disable_primitive_actions: bool
    ):
        super().__init__(features)
        self.intelligence = QVSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            disable_primitive_actions=disable_primitive_actions
        )



class StakesSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
            self,
            index,
            features: List[str],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            disable_primitive_actions: bool
    ):
        super().__init__(features)
        self.intelligence = SSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            disable_primitive_actions=disable_primitive_actions
        )
