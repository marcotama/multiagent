from fighting_ice.action import Action
from fighting_ice.agents.common import LearningFightingController
from typing import List, Iterable, Dict, Hashable
from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from reinf.q_learning import QLearningAgent as QL
from util.delegation import delegate_attrs


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
])
class QLearningAgent(LearningFightingController):

    def getCharacter (self):
        return 'ZEN'

    def __init__ (
            self,
            index: int,
            features: List[str],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            options: Iterable[Option],
            early_termination: bool,
            disable_primitive_actions: bool,
            q_table_features: List[str]
    ):
        super().__init__(features)
        self.qAgent = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions,
            q_table_features=q_table_features
        )

    def __str__ (self):
        return str(self.qAgent)

    def learning_fn (
            self,
            prev_state: Dict[str, Hashable],
            prev_legal_actions: List[Action],
            prev_action: str,
            cur_state: Dict[str, Hashable],
            cur_legal_actions: List[Action],
            reward: float
    ):
        return self.qAgent.learn(prev_state, prev_legal_actions, prev_action, reward, cur_state, cur_legal_actions)

    def decision_fn (
            self,
            state: Dict[str, Hashable],
            legal_actions: List[Action],
    ):
        return self.qAgent.get_action(state, legal_actions)

    def get_mdp (self):
        return self.qAgent.mdp

    def end_of_episode (self):
        self.qAgent.end_of_episode()
