from typing import List, Dict, Hashable
from fighting_ice.action import Action, ALL_USEFUL_ACTIONS
from reinf.policy.discrete import GreedyPolicy, AnnealingEpsilonGreedyPolicy
from reinf.gq import GreedyGQ
from util.projector import TileCoderProjector
from util.trace import AccumulatingTrace, AccumulatingMaxTrace, MaxLengthTrace
from util.functions import ConstantFn
from util.vector import DenseVector, SparseVector
from util.state_action_encoder import StateActionTabularEncoder, StateActionAppendingEncoder
from util.typing2 import MyTypes as T
from util.hashing import UNH
from functools import partial
from util.delegation import delegate_attrs
from fighting_ice.agents.common import LearningFightingController
import numpy as np


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
])
class LinearQLearningAgent(LearningFightingController):

    def getCharacter (self):
        return 'ZEN'

    def initialize(self, game_data, player):
        super().initialize(game_data, player)
        features_info = self.features_extractor.get_features_info(self.features)
        self.features_info = features_info
        self.n_binary_features, ranges = self.analyze_features()

        # hashing
        if self.hashing_algorithm is None or self.hashing_memory == 0:
            hashing = None
        elif self.hashing_algorithm == 'UNH':
            hashing = UNH(memory=self.hashing_memory)
        else:
            raise ValueError("Unknown hashing algorithm: %s" % self.hashing_algorithm)

        # projector
        if self.projector_name == 'tile_coder':
            self.projector = TileCoderProjector(self.n_tiles,self.n_tilings,ranges,hashing,self.n_binary_features,include_bias=False,scales=self.scales)
        else:
            raise ValueError("Unknown projector: %s" % self.projector)

        # state-action encoder
        if self.state_action_encoder_name == 'tabular':
            state_action_encoder = StateActionTabularEncoder(self.projector.vector_size(), self.projector.vector_norm(), ALL_USEFUL_ACTIONS, include_bias=True)
        elif self.state_action_encoder_name == 'appending':
            state_action_encoder = StateActionAppendingEncoder(self.projector.vector_size(), self.projector.vector_norm(), ALL_USEFUL_ACTIONS, include_bias=True)
        else:
            raise ValueError("Unknown state-action encoder: %s" % self.state_action_encoder)

        # trace factory
        if self.trace == 'accumulating':
            trace_factory = AccumulatingTrace
        elif self.trace == 'accumulating-max':
            trace_factory = AccumulatingMaxTrace
        elif self.trace == 'max-length':
            trace_factory = MaxLengthTrace
        else:
            raise ValueError("Unknown trace: %s" % self.trace)

        # Note:
        # Greedy-GQ [49] solves this problem and can find an approximation of the optimal value function
        # as long as the behavior policy is fixed. Unfortunately, in complex domains, a random behavior
        # policy does not visit the interesting parts of the state space often enough for effective learning,
        # and the algorithm is no longer stable with a varying behavior policy.
        # Cited by Louis C. Cobos PhD thesis:
        # Leveraging attention focus for effective reinforcement learning in complex domains
        #behavior = partial(EpsilonGreedyPolicy, epsilon=0.1)
        behavior_factory = partial(
            AnnealingEpsilonGreedyPolicy,
            epsilon=0.1,
            start_descent=self.start_descent,
            end_descent=self.end_descent
        )
        target_factory = GreedyPolicy
        gamma_fn = ConstantFn(self.gamma)
        lambda_fn = ConstantFn(self.lmbda)
        interest_fn = ConstantFn(1.0)
        self.control = GreedyGQ(
            alpha=self.alpha,
            eta=self.eta,
            trace_factory=trace_factory,
            target_factory=target_factory,
            behavior_factory=behavior_factory,
            actions=ALL_USEFUL_ACTIONS,
            state_action_encoder=state_action_encoder,
            gamma_fn=gamma_fn,
            lambda_fn=lambda_fn,
            interest_fn=interest_fn
        )

        # preallocate
        self.ranged_features_vec = DenseVector(len(self.ranged_features))
        self.binary_features_vec = None
        self.enc_state_t_vec = SparseVector(self.projector.vector_size())
        self.enc_state_tp1_vec = None
        self.phi_temp = DenseVector(state_action_encoder.vector_size())

        self.initialized = True


    def __init__ (
        self,
        features: List[T.Feature],
        alpha: float,
        eta: float,
        trace: str,
        gamma: float,
        lmbda: float,
        n_tiles: int,
        n_tilings: int,
        hashing_algorithm: str,
        hashing_memory: int,
        start_descent: int,
        end_descent: int,
        projector: str,
        state_action_encoder: str,
        scales: str
    ):
        super().__init__(features)
        self.features_extractor.discretize = False
        self.alpha = alpha
        self.lmbda = lmbda
        self.gamma = gamma
        self.eta = eta
        self.trace = trace
        self.start_descent = start_descent
        self.end_descent = end_descent
        self.projector_name = projector
        self.state_action_encoder_name = state_action_encoder
        self.scales = scales
        self.n_tiles = n_tiles
        self.n_tilings = n_tilings
        self.hashing_algorithm = hashing_algorithm
        self.hashing_memory = hashing_memory


        self.be_greedy = False
        self.learning = True

        self.features_info = None
        self.bool_features = []
        self.enum_features = []
        self.ranged_features = []
        self.n_binary_features = None

        # preallocate
        self.ranged_features_vec = None
        self.binary_features_vec = None
        self.enc_state_t_vec = None
        self.enc_state_tp1_vec = None
        self.phi_temp = None

        self.initialized = False
        self.projector = None
        self.control = None
        # flag saying if learningFunc is called on sequential steps - saves computation time
        self.trained_sequentially = False


    def analyze_features(self):
        """
        Analyze features properties (type, number, order, min/max)
        """
        n_binary_features = 0
        ranges = ([], [])
        for name in sorted(self.features_info.keys()):
            info = self.features_info[name]
            if info['type'] in [int,float]:
                self.ranged_features.append(name)
                ranges[0].append(info['min'])
                ranges[1].append(info['max'])
            elif info['type'] == 'enum':
                self.enum_features.append(name)
                if info['nullable']:
                    n_binary_features += len(info['possible_values']) + 1
                    info['possible_values'] = [None] + info['possible_values']
                else:
                    n_binary_features += len(info['possible_values'])
            elif info['type'] == bool:
                self.bool_features.append(name)
                n_binary_features += 1

        for feature, feature_info in self.features_info.items():
            if feature_info['type'] in [int, float] and ('min' not in feature_info or 'max' not in feature_info):
                raise Exception("Invalid feature %s: %s" % (feature, str(feature_info)))
        ranges = (DenseVector(data=ranges[0]), DenseVector(data=ranges[1]))
        return n_binary_features, ranges


    def process_features(self, state: T.FactoredState):
        # ranged features
        idx = []
        val = []
        if state is None:
            for i in range(len(self.ranged_features)):
                self.ranged_features_vec[i] = np.nan
        else:
            for i, f in enumerate(self.ranged_features):
                self.ranged_features_vec[i] = np.nan if state[f] is None else state[f]

            # enum and bool features
            i = 0
            for f in self.enum_features:
                pv = self.features_info[f]['possible_values']
                idx.append(i + pv.index(state[f]))
                val.append(1.0)
                i += len(pv)
            for f in self.bool_features:
                self.binary_features_vec[i] = float(state[f])
                val.append(float(state[f]))
                i += 1

        self.binary_features_vec = SparseVector(self.n_binary_features, data=val,idx=idx)

    def __str__ (self):
        return str(self.qAgent)

    def learning_fn (
            self,
            prev_state: Dict[str, Hashable],
            prev_legal_actions: List[Action],
            prev_action: str,
            cur_state: Dict[str, Hashable],
            cur_legal_actions: List[Action],
            reward: float
    ):

        if self.trained_sequentially:
            self.enc_state_t_vec, self.enc_state_tp1_vec = self.enc_state_tp1_vec, self.enc_state_t_vec
        else:
            # encode state
            self.process_features(prev_state)
            self.enc_state_t_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        # encode newState
        self.process_features(cur_state)
        self.enc_state_tp1_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        self.control.learn(x_t=self.enc_state_t_vec, la_t=prev_legal_actions, a_t=prev_action, r_t=reward,
                           x_tp1=self.enc_state_tp1_vec, la_tp1=cur_legal_actions)

    def decision_fn (
            self,
            state: Dict[str, Hashable],
            legal_actions: List[Action],
    ):
        self.do_not_learn() # disable learning online - for performance (TODO remove)

        legalActions = [self.d2a[a] for a in legal_actions]

        if not self.trained_sequentially:
            self.process_features(state)
            self.enc_state_tp1_vec = self.projector.project(self.ranged_features_vec, self.binary_features_vec)

        if self.be_greedy:
            choice = self.control.propose_greedy_action(self.enc_state_tp1_vec, legalActions)
        else:
            choice = self.control.propose_exploratory_action(self.enc_state_tp1_vec, legalActions)

        return choice

    def get_mdp (self):
        return self.qAgent.mdp

    def end_of_episode (self):
        self.qAgent.end_of_episode()