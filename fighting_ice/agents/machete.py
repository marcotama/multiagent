from fighting_ice.agents.common import FightingController
from py4j.java_gateway import get_field


class Machete(FightingController):
    def getCharacter (self):
        return 'ZEN'

    def get_action (self):

        distance = self.cc.getDistanceX()

        my = self.cc.getMyCharacter()
        energy = my.getEnergy()
        my_x = get_field(my, 'x')
        my_state = get_field(my, "state")

        opp = self.cc.getEnemyCharacter()
        opp_x = get_field(opp, 'x')
        opp_state = get_field(opp, "state")

        xDifference = my_x - opp_x

        if (get_field(opp, "energy") >= 300) and ((get_field(my, "hp") - get_field(opp, "hp")) <= 300):
            return "FOR_JUMP _B B B"
        elif not my_state.equals(self.gateway.jvm.enumerate.State.AIR) and not my_state.equals(self.gateway.jvm.enumerate.State.DOWN):
            if distance > 150:
                return "FOR_JUMP"
            elif energy >= 300:
                return "STAND_D_DF_FC"
            elif (distance > 100) and (energy >= 50):
                return "STAND_D_DB_BB"
            elif opp_state.equals(self.gateway.jvm.enumerate.State.AIR):
                return "STAND_F_D_DFA"
            elif distance > 100:
                return "6 6 6"
            else:
                return "B"
        elif ((distance <= 150) and (my_state.equals(self.gateway.jvm.enumerate.State.AIR) or my_state.equals(
                self.gateway.jvm.enumerate.State.DOWN))
              and (((self.game_data.getStageXMax() - my_x) >= 200) or (xDifference > 0))
              and ((my_x >= 200) or xDifference < 0)):
            if energy >= 5:
                return "AIR_DB"
            else:
                return "B"
        else:
            return "B"
