from py4j.java_gateway import JavaGateway
from abc import abstractmethod
from typing import Hashable, List, Dict, Tuple, Optional
from fighting_ice.action import Action, ALL_USEFUL_ACTIONS, AIR_ACTIONS, GROUND_ACTIONS
from fighting_ice.features_extractor import FightingFeaturesExtractor

class FightingController:

    # ---- Do NOT override these methods ----

    class Java:
        implements = ["gameInterface.AIInterface"]

    def __init__(self):
        # Good practice to declare instance variables in __init__
        self.gateway = None
        self.frame_data = None # Updated after every call to getInformation
        self.input_key = None
        self.cc = None
        self.player = None
        self.opponent = None
        self.player_str = None
        self.opponent_str = None
        self.game_data = None
        self.simulator = None

        self.p1_motions = None
        self.p2_motions = None
        self.player_motions = None
        self.opponent_motions = None
        self.p1_energy_req = None
        self.p2_energy_req = None

    def getInformation(self, frameData):
        """
        Receives and stores frame data for use by other methods.
        :param frameData: data of the current frame.
        """
        self.frame_data = frameData
        self.observe()


    def set_gateway(self, gateway: JavaGateway):
        """
        Sets the gateway to communicate with Java.
        :param gateway: the Java gateway
        """
        self.gateway = gateway
        self.input_key = self.gateway.jvm.structs.Key()
        self.frame_data = self.gateway.jvm.structs.FrameData()
        self.cc = self.gateway.jvm.commandcenter.CommandCenter()


    def initialize(self, game_data, player):
        """
        Prepares this AI for operation.
        :param game_data: information about the game the AI is about to enter
        :param player: boolean flag indicating whether this AI controls P1 (true) or P2 (false)
        :return: 0 if the initialization is successful, the error code otherwise
        """
        self.player = player
        self.opponent = not player
        self.player_str = 'P1' if player else 'P2'
        self.opponent_str = 'P2' if player else 'P1'
        self.game_data = game_data
        if hasattr(self.game_data, 'getSimulator'):
            self.simulator = self.game_data.getSimulator()
        if hasattr(self.game_data, 'getPlayerOneMotion') and hasattr(self.game_data, 'getPlayerTwoMotion'):
            self.p1_motions = game_data.getPlayerOneMotion()
            self.p2_motions = game_data.getPlayerTwoMotion()
            if player:
                self.player_motions = self.p1_motions if player else self.p2_motions
                self.opponent_motions = self.p2_motions if player else self.p1_motions
            self.p1_energy_req = {m.getMotionName(): abs(m.getAttackStartAddEnergy()) for m in self.p1_motions}
            self.p2_energy_req = {m.getMotionName(): abs(m.getAttackStartAddEnergy()) for m in self.p2_motions}

        self.init()

        return 0

    def input(self):
        """
        Returns the command decided by the AI for the current frame.
        :return: the command decided by the AI for the current frame.
        """
        return self.input_key


    def processing(self):
        """
        Sets the input for the current frame.
        """
        if self.gateway is None:
            print("Gateway not set (yet)")
            return

        if not self.frame_data.getEmptyFlag() and self.frame_data.getRemainingTime() > 0:

            # Let CommandCenter check if this is a new round - if so, the current skill should be canceled.
            self.cc.setFrameData(self.frame_data, self.player)

            if not self.cc.getskillFlag():
                self.input_key.empty()
                self.cc.skillCancel()
                command = self.get_action()
                if command is not None:
                    self.cc.commandCall(command)

            self.input_key = self.cc.getSkillKey()


    # ---- DO override these methods ----

    def init (self):
        """
        Prepares this AI for operation.
        """
        pass

    @abstractmethod
    def getCharacter(self):
        """
        Returns the character to be controlled by this AI.
        Legal values are 'ZEN', 'GARNET', 'LUD', and 'KFM'.
        :return: the character to be controlled by this AI.
        """
        pass

    def observe(self):
        """
        Uses latest frameData to update internal knowledge.
        """
        pass

    @abstractmethod
    def get_action(self):
        """
        Computes the next command to execute. This method is only called after the previous command is done executing.
        :return: the next command to execute.
        """
        pass

    def close (self):
        """
        Executes operations necessary before the AI is shut down (e.g., saving data etc.).
        """
        pass




class LearningFightingController(FightingController):

    def __init__ (self, features):
        """
        Expects self.index, self.learningFunc and self.decisionFunc to be setup
        """
        super().__init__()
        self.features = features
        self.features_extractor = None
        self._previous_step_p1 = None # type: Optional[Tuple[Dict[str,Hashable],float]]
        self._previous_step_p2 = None # type: Optional[Tuple[Dict[str,Hashable],float]]
        self.features_extractor = FightingFeaturesExtractor(self.features)


    def init(self):
        self.features_extractor.set_game_data(self.game_data)
        self._previous_step_p1 = None
        self._previous_step_p2 = None


    def observe(self):

        if hasattr(self.frame_data, 'getEmptyFlag') and self.frame_data.getEmptyFlag():
            return # ignore empty frames

        fd = self.frame_data
        p1_cur_hp = self.features_extractor.get_hp(fd, 'P1')
        p2_cur_hp = self.features_extractor.get_hp(fd, 'P2')
        p1_cur_state = self.features_extractor.get_features(fd, 'P1')
        p2_cur_state = self.features_extractor.get_features(fd, 'P2')
        p1_prev_action = self.features_extractor.get_action(fd, 'P1')
        p2_prev_action = self.features_extractor.get_action(fd, 'P2')
        p1_character_state = self.features_extractor.get_state(fd, 'P1')
        p2_character_state = self.features_extractor.get_state(fd, 'P2')
        p1_cur_available_actions = AIR_ACTIONS if p1_character_state == 'AIR' else GROUND_ACTIONS
        p2_cur_available_actions = AIR_ACTIONS if p2_character_state == 'AIR' else GROUND_ACTIONS
        p1_cur_score = p1_cur_hp - p2_cur_hp
        p2_cur_score = p2_cur_hp - p1_cur_hp

        # Map all no-buttons-pressed actions to one, so as to make more use of the data available
        if p1_prev_action not in ALL_USEFUL_ACTIONS:
            p1_prev_action = Action.NEUTRAL
        if p2_prev_action not in ALL_USEFUL_ACTIONS:
            p2_prev_action = Action.NEUTRAL


        # Update from P1 point of view
        if self._previous_step_p1 is not None:
            p1_prev_state, p1_prev_score, p1_prev_available_actions = self._previous_step_p1
            p1_reward = p1_cur_score - p1_prev_score

            self.learning_fn(p1_prev_state, p1_prev_available_actions, p1_prev_action, p1_cur_state, p1_cur_available_actions, p1_reward)
        self._previous_step_p1 = (p1_cur_state, p1_cur_score, p1_cur_available_actions)

        # Update from P2 point of view
        if self._previous_step_p2 is not None:
            p2_prev_state, p2_prev_score, p2_prev_available_actions = self._previous_step_p2
            p2_reward = p2_cur_score - p2_prev_score

            self.learning_fn(p2_prev_state, p2_prev_available_actions, p2_prev_action, p2_cur_state, p2_cur_available_actions, p2_reward)
        self._previous_step_p2 = (p2_cur_state, p2_cur_score, p2_cur_available_actions)


    def get_action (self) -> Action:
        fd = self.simulator.simulate(self.frame_data, self.player, None, None, 0)
        if self.player:
            state = self.features_extractor.get_features(fd, 'P1')
        else:
            state = self.features_extractor.get_features(fd, 'P2')

        character_state = self.features_extractor.get_state(fd, self.player_str)
        available_actions = AIR_ACTIONS if character_state == 'AIR' else GROUND_ACTIONS
        energy_req = self.p1_energy_req if self.player else self.p2_energy_req
        energy = abs(self.features_extractor.get_energy(fd, self.player_str))
        available_actions = [a for a in available_actions if energy_req[a] <= energy]
        action = self.decision_fn(state, available_actions)
        return action


    def close (self):
        # TODO: how can we get the last frameData to call learning_fn()?
        self._previous_step_p1 = None
        self._previous_step_p2 = None
        self.end_of_episode()


    @abstractmethod
    def learning_fn (
            self,
            prev_state: Dict[str, Hashable],
            prev_legal_actions: List[Action],
            prev_action: Action,
            cur_state: Dict[str, Hashable],
            cur_legal_actions: List[Action],
            reward: float
    ):
        """
        Updates internal knowledge given information on this and the previous time-step.
        :param prev_state: State at the previous time-step as a shallow dict state-representation
        :param prev_legal_actions: Legal actions at the previous time-step
        :param prev_action: Action taken at the previous time-step
        :param cur_state: State at the current time-step as a shallow dict state-representation
        :param cur_legal_actions: Legal actions at the current time-step
        :param reward: Reward received at this transition between time-steps
        """
        pass


    @abstractmethod
    def decision_fn (
            self,
            state: Dict[str, Hashable],
            legal_actions: List[Action],
    ) -> Action:
        """
        Decides an action given information on the current state.
        :param state: State at the current time-step as a shallow dict state-representation
        :param legal_actions: Legal actions at the current time-step
        :returns: Action to be executed next
        """
        pass


    @abstractmethod
    def end_of_episode (self):
        """
        Performs operations at the end of an episode (save data, clear cache, ...).
        """
        pass
