#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
exit_code_message () {
    if [ $? == 0 ]; then
        printf " ${GREEN}Succeeded${NC}\n"
    else
        printf " ${RED}Failed${NC}\n"
    fi
}

mkdir -p tests_output/
rm tests_output/* 2> /dev/null





# Run all unit tests
python -m unittest tests/test_*



# Test software as a whole with different settings
echo -n "Testing KeyboardAgent..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'KeyboardAgent'}" \
  --graphics full \
> tests_output/keyboard.out \
2> tests_output/keyboard.err
exit_code_message

echo -n "Testing Conservative Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
> tests_output/conservative.out \
2> tests_output/conservative.err
exit_code_message

echo -n "Testing Aggressive Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'AggressivePacman'}" \
> tests_output/aggressive.out \
2> tests_output/aggressive.err
exit_code_message

echo -n "Testing FSM Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'FSMPacman'}" \
> tests_output/fsm.out \
2> tests_output/fsm.err
exit_code_message

echo -n "Testing Minimax Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'MinimaxPacman', 'depth': 1}" \
> tests_output/minimax.out \
2> tests_output/minimax.err
exit_code_message

echo -n "Testing Minimax Ghosts..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --ghosts "{'controller': 'MinimaxGhost', 'depth': 1}" \
           "{'controller': 'DirectionalGhost', 'depth': 1}" \
> tests_output/minimax.out \
2> tests_output/minimax.err
exit_code_message

echo -n "Testing AlphaBeta Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'AlphaBetaPacman', 'depth': 1}" \
> tests_output/alphabeta.out \
2> tests_output/alphabeta.err
exit_code_message

echo -n "Testing AlphaBeta Ghosts..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --ghosts "{'controller': 'AlphaBetaGhost', 'depth': 1}" \
           "{'controller': 'DirectionalGhost', 'depth': 1}" \
> tests_output/alphabeta.out \
2> tests_output/alphabeta.err
exit_code_message

echo -n "Testing Expectimax Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ExpectimaxPacman', 'depth': 1}" \
> tests_output/expectimax.out \
2> tests_output/expectimax.err
exit_code_message

echo -n "Testing Expectimax Ghosts..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --ghosts "{'controller': 'ExpectimaxGhost', 'depth': 1}" \
           "{'controller': 'DirectionalGhost', 'depth': 1}" \
> tests_output/expectimax.out \
2> tests_output/expectimax.err
exit_code_message

echo -n "Testing Q-Learning Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing Q-Learning Pacman with options..."
echo -e "
import pickle
from reinf.options.option import RandomActionOption
from util.translator import Translator
with open('temp.options','wb') as f:
    options = [RandomActionOption('_random~%d' % i, 0.2) for i in range(5)]
    pickle.dump((options, Translator(), Translator()),f)
" | python3 \
|| exit 1
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'addOptionsFromFile': 'temp.options', 'earlyTerm': True,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
> tests_output/qlearning_with_options.out \
2> tests_output/qlearning_with_options.err
exit_code_message
rm temp.options

echo -n "Testing Q-Learning Pacman with Consistent Bellman operator..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm'],
  'operator': 'consistent-bellman'}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing Q-Learning Pacman with Advantage-Learning operator..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm'],
  'operator': 'advantage-learning'}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing Q-Learning Pacman with Persistent Advantage-Learning operator..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm'],
  'operator': 'persistent-advantage-learning'}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing Q-Learning Pacman with Lazy operator..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0,
  'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm'],
  'operator': 'lazy'}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing Multi-QLearning Pacman..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'MultiQLearningPacman', 'features_sets': [
  ['.FdDr','.FdDs','.NScGhDr','.NScGhDs','.ScGhDr','.ScGhDs','.CpDr','.CpDs'],
  ['.FdDr','.FdDs','.NScGhDr','.NScGhDs','.ScGhDr','.ScGhDs'],
  ['.FdDr','.FdDs','.NScGhDr','.NScGhDs']]}" \
> tests_output/multi_qlearning.out \
2> tests_output/multi_qlearning.err
exit_code_message

echo -n "Testing Q-Learning Pacman with Subramanian options..."
echo -e "
import pickle
from pacman.options.pacman_options import GoToClosestCapsule, GoToClosestFood, GoToClosestScaredGhost, AvoidGhost
from util.translator import Translator
termProb = 0.2
stateTranslator = Translator()
actionTranslator = Translator()
with open('temp.options','wb') as f:
    options = [
        GoToClosestCapsule(stateTranslator, actionTranslator, termProb),
        GoToClosestFood(stateTranslator, actionTranslator, termProb),
        GoToClosestScaredGhost(stateTranslator, actionTranslator, termProb),
        AvoidGhost(stateTranslator, actionTranslator, termProb)
    ]
    pickle.dump((options, stateTranslator, actionTranslator),f)
" | python3 \
|| exit 1
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'addOptionsFromFile': 'temp.options', 'earlyTerm': True, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
> tests_output/qlearning_with_subramanian_options.out \
2> tests_output/qlearning_with_subramanian_options.err
exit_code_message
rm temp.options

echo -n "Testing GQ agents..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'GQPacman', 'gamma': 0.97, 'alpha': 0.05, 'eta': 5, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
  --ghosts "{'controller': 'GQGhost', 'gamma': 0.97, 'alpha': 0.05, 'eta': 5, 'features': ['~[0]Dr','~[1]Tm']}" \
           "{'controller': 'GQGhost', 'gamma': 0.97, 'alpha': 0.05, 'eta': 5, 'features': ['~[0]Dr','~[2]Tm']}" \
> tests_output/gq.out \
2> tests_output/gq.err
exit_code_message

echo -n "Testing Conservative Pacman and Random ghosts..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --ghosts "{'controller': 'RandomGhost'}" \
           "{'controller': 'RandomGhost'}" \
> tests_output/random_ghost.out \
2> tests_output/random_ghost.err
exit_code_message

echo -n "Testing Aggressive Pacman and Directional ghosts..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'AggressivePacman'}" \
  --ghosts "{'controller': 'DirectionalGhost'}" \
           "{'controller': 'DirectionalGhost'}" \
> tests_output/directional_ghost.out \
2> tests_output/directional_ghost.err
exit_code_message

echo -n "Testing Q-Learning agents..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QLearningPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
  --ghosts "{'controller': 'QLearningGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[1]Tm']}" \
           "{'controller': 'DirectionalGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[2]Tm']}" \
> tests_output/qlearning.out \
2> tests_output/qlearning.err
exit_code_message

echo -n "Testing CSAS agents..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ChallengeSensitiveActionSelectionPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
  --ghosts "{'controller': 'ChallengeSensitiveActionSelectionGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[1]Tm']}" \
           "{'controller': 'DirectionalGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[2]Tm']}" \
> tests_output/csas.out \
2> tests_output/csas.err
exit_code_message

echo -n "Testing QVSAS agents..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'QValueSensitiveActionSelectionPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
  --ghosts "{'controller': 'QValueSensitiveActionSelectionGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[1]Tm']}" \
           "{'controller': 'DirectionalGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[2]Tm']}" \
> tests_output/qvsas.out \
2> tests_output/qvsas.err
exit_code_message

echo -n "Testing SSAS agents..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'StakesSensitiveActionSelectionPacman', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['.FdDr','.NScGhDr','.NScGhDs','.CpDr','.CpDs','.ScGhDr','.ScGhTm']}" \
  --ghosts "{'controller': 'StakesSensitiveActionSelectionGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[1]Tm']}" \
           "{'controller': 'DirectionalGhost', 'gamma': 0.97, 'alpha': 0.1, 'init_q': 0.0, 'features': ['~[0]Dr','~[0]Ds:','Fd#:','.CpDs:','.CpDr','~[2]Tm']}" \
> tests_output/ssas.out \
2> tests_output/ssas.err
exit_code_message

echo -n "Testing text output..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --frame-time 0.05 \
  --graphics text \
  --episodes 1 \
> tests_output/text_graphics.out \
2> tests_output/text_graphics.err
exit_code_message

echo -n "Testing full graphics..."
python3 run_pacman_game.py \
  --layout 'smallClassic' \
  --pacman "{'controller': 'ConservativePacman'}" \
  --graphics full \
  --episodes 1 \
> tests_output/full_graphics.out \
2> tests_output/full_graphics.err
exit_code_message