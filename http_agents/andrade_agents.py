"""
Classes implementing some Challenge-Sensitive Action Selection
"""

from typing import List, Optional
from abc import abstractmethod

from http_agents.common import LearningAgent, Features
from reinf.exploration_strategies import ExplorationParams, FixedRank, TargetQValue
from reinf.options.option import Option
from reinf.options.mdp_with_options import MDPWithOptions
from reinf.q_learning import QLearningAgent
from util.typing2 import MyTypes as T



class PredictorBasedAgent(LearningAgent):
    def __init__(
        self,
        port: Optional[int]=None
    ):
        if port is None:
            super().__init__()
        else:
            super().__init__(port)
        self.predictor = None

    def learning_fn(
        self,
        state: Features,
        legal_actions: List[Features],
        action: T.FactoredAction,
        reward: float,
        new_state: Optional[Features],
        new_legal_actions: List[Features]
    ):
        self.predictor.learning_fn(
            state=state,
            legal_actions=legal_actions,
            action=action,
            reward=reward,
            new_state=new_state,
            new_legal_actions=new_legal_actions
        )

    @abstractmethod
    def decision_fn(
        self,
        state: T.FactoredState,
        legal_actions: List[T.FactoredAction]
    ):
        pass


    def end_of_episode (self, *args, **kwargs):
        self.predictor.end_of_episode()



class CSASAgent(PredictorBasedAgent):
    """
    Superclass implementing general Challenge-Sensitive Action Selection strategy.
    Current level is stored in 0 <= self.level <= 1, where higher level implies stronger action to be selected.

    Ref: Andrade, Gustavo, et al. "Challenge-sensitive action selection: an application to game balancing." IEEE/WIC/ACM International Conference on Intelligent Agent Technology. IEEE, 2005.
    Link: https://www.researchgate.net/profile/Emmanuelle_Corruble/publication/4207671_Challenge-sensitive_action_selection_an_application_to_game_balancing/links/0912f5058c21f68713000000.pdf
    """

    def __init__(
        self,
        evaluation_cycle: int,
        threshold: float,
        port: Optional[int]=None
    ):
        super().__init__(port)

        self.evaluation_cycle = evaluation_cycle
        self.threshold = threshold
        self.level = 0.5
        self.steps = 0

    @abstractmethod
    def decision_fn (self, state: T.FactoredState, legal_actions: List[T.FactoredAction]):
        pass


    def update_level(
        self,
        self_score: float,
        opponent_score: float
    ):
        if self.steps % self.evaluation_cycle == 0:
            if self_score > opponent_score + self.threshold:
                self.level -= 0.1
            elif self_score < opponent_score - self.threshold:
                self.level += 0.1
        self.level = min(1.0, max(0.0, self.level))

        self.steps += 1


class ChallengeSensitiveActionSelectionAgent(CSASAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """

    def __init__(
        self,
        evaluation_cycle: int,
        threshold: float,

        features: List[T.Feature],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        port: Optional[int]=None
    ):

        super().__init__(evaluation_cycle, threshold, port)
        self.qAgent = QLearningAgent(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha
        )
        # Special exploration strategy that always chooses the action ranked in a given percentile
        self.action_selection = FixedRank(exploration_params)


    def decision_fn(
        self,
        state: T.FactoredState,
        legal_actions: List[T.FactoredAction]
    ):
        """
        This function expects '_self_score' and '_opponent_score' to be features present in the state.
        Such features are then removed before passing the state information to Q-learning.
        :param state: state information in the form of a dict mapping strings to  (hashable) features
        :param legal_actions: list of legal actions in the form of a list of dicts mapping strings to (hashable) features
        :return: an entry chosen from legal_action
        """
        self.update_level(state['_self_score'], state['_opponent_score'])
        self.action_selection.level = self.level
        s = self.predictor.encode_state(state)
        la = [self.predictor.encode_action(action) for action in legal_actions]
        a = self.predictor.qAgent.mdp.choose_action(s, self.action_selection, la)
        return self.predictor.decode_action(a)




class ChallengeSensitiveActionSelectionWithOptionsAgent(CSASAgent):
    """
    A Challenge Sensitive Action Selection with options agent using Tabular Q-learning as a predictor.
    """

    def __init__(
        self,
        evaluation_cycle: int,
        threshold: float,

        features: List[T.Feature],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: List[Option],
        early_termination: bool,

        port: Optional[int]=None
    ):

        super().__init__(evaluation_cycle, threshold, port)
        self.qAgent = QLearningAgent(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )

        mdp = self.predictor.get_mdp() # type: MDPWithOptions
        for option in mdp.options.values():
            option.stateTranslator = self.predictor.qAgent.state_translator
            option.actionTranslator = self.predictor.qAgent.action_translator

        # Special exploration strategy that always chooses the action ranked in a given percentile
        self.action_selection = FixedRank(exploration_params, [o.name for o in options])


    def decision_fn(
        self,
        state: T.FactoredState,
        legal_actions: List[T.FactoredAction]
    ):
        """
        This function expects '_self_score' and '_opponent_score' to be features present in the state.
        Such features are then removed before passing the state information to Q-learning.
        :param state: state information in the form of a dict mapping strings to  (hashable) features
        :param legal_actions: list of legal actions in the form of a list of dicts mapping strings to (hashable) features
        :return: an entry chosen from legal_action
        """
        self.update_level(state['_self_score'], state['_opponent_score'])
        self.action_selection.level = self.level
        s = self.predictor.encode_state(state)
        la = [self.predictor.encode_action(action) for action in legal_actions]
        a = self.predictor.qAgent.mdp.choose_action(s, self.action_selection, la)
        return self.predictor.decode_action(a)








class QValueSensitiveActionSelectionAgent(PredictorBasedAgent):
    """
    A Q-Value Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """

    def __init__(
        self,
        opponent_index: int,

        features: List[T.Feature],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        port: Optional[int]=None
    ):
        super().__init__(port)
        self.opponent_index = opponent_index
        self.qAgent = QLearningAgent(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha
        )
        # Special exploration strategy that always chooses the action ranked in a given percentile
        self.action_selection = TargetQValue(exploration_params)


    def learning_fn(
        self,
        state: T.PacmanState,
        legal_actions: List[T.FactoredAction],
        action: T.FactoredAction,
        reward: float,
        new_state: Optional[T.PacmanState],
        new_legal_actions: List[T.FactoredAction]
    ):
        self.predictor.learning_fn(
            state=state,
            legal_actions=legal_actions,
            action=action,
            reward=reward,
            new_state=new_state,
            new_legal_actions=new_legal_actions
        )


    def decision_fn(
        self,
        state: Features,
        legal_actions: List[Features]
    ):
        """
        This function expects all features used to describe the state to be in the state variable twice, both for the
        adaptive agent (self) and for the opponent. Each feature should start with either '_self_' or '_opponent_'. For
        example, if the state is described by features 'speed' and 'color', the state parameter should contain four
        entries: '_self_speed', '_self_color', '_opponent_speed' and '_opponent_color'.

        In addition to the state features for adaptive agent and opponent, there should be a special feature reporting
        the legal actions for the opponent. Each legal action must be a dict of hashable features. This special feature
        should be mapped by the key '_opponent_legal_actions'.

        Such features are then separated before passing them to Q-learning.

        :param state: state information in the form of a dict mapping strings to  (hashable) features
        :param legal_actions: list of legal actions in the form of a list of dicts mapping strings to (hashable) features
        :return: an entry chosen from legal_action
        """
        self_state = {}
        opponent_state = {}
        for k, v in state.items():
            if k.startswith('_self_'):
                f = k[len('_self_'):]
                self_state[f] = v
            elif k.startswith('_opponent_'):
                f = k[len('_opponent_'):]
                opponent_state[f] = v
        opponent_legal_actions = state['_opponent_legal_actions']
        opp_q_values = self.predictor.qAgent.predict(opponent_state, opponent_legal_actions)
        sorted_opp_q_values = sorted(opp_q_values.values()) # ascending
        median_i = min(int(0.5 * len(opp_q_values)), len(opp_q_values) - 1)
        opp_median_q_value = sorted_opp_q_values[median_i]
        self.action_selection.target = opp_median_q_value

        s = self.predictor.encode_state(self_state)
        la = [self.predictor.encode_action(action) for action in legal_actions]
        print(self.predictor.qAgent.mdp.get_q_row(s, la))
        a = self.predictor.qAgent.mdp.choose_action(s, self.action_selection, la)
        return self.predictor.decode_action(a)
