from itertools import product
from tabulate import tabulate
import json
import argparse


# Example JSON
# {
#     "repetitions": 100,
#     "setup": {
#         "layout": "smallClassic",
#         "episodes": 3000
#     },
#     "configurations": [
#         {
#             "_set_name": "GQ",
#             "controller": ["GQPacman"],
#             "alpha": [0.003],
#             "eta": [0.1],
#             "hashing_memory": [100],
#             "gamma": [0.97],
#             "lmbda": [0.4],
#             "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs",".ScGhTm"]],
#             "start_descent": [500],
#             "end_descent": [2500],
#             "n_tiles": [5],
#             "n_tilings": [5],
#             "scales": ["log"]
#         },
#         {
#             "_set_name": "Linear Q-learning",
#             "controller": ["LinearQLAgent"],
#             "alpha": [0.003],
#             "hashing_memory": [100],
#             "gamma": [0.97],
#             "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs",".ScGhTm"]],
#             "start_descent": [500],
#             "end_descent": [2500],
#             "n_tiles": [5],
#             "n_tilings": [5],
#             "scales": ["log"]
#         },
#         {
#             "_set_name": "Tabular Q-learning",
#             "controller": ["QLearningAgent"],
#             "alpha": [0.1],
#             "gamma": [0.97],
#             "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs",".ScGhTm"]],
#             "start_descent": [500],
#             "end_descent": [2500]
#         }
#     ]
# }


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(description='Generate configurations')
    parser.add_argument(
        dest='json_file',
        type=str,
        help="json file to load settings from; the file should contain a field 'repetitions' with an integer value and "
             "a field 'configurations' with an object (i.e., a dictionary, in Python terms) mapping strings to arrays "
             "(i.e., lists, in Python terms) of ints/floats/strings; every possible combination of such values will be "
             "added to a tsv file; in particular, every possible configuration is repeated `repetitions` number of "
             "times in the tsv file; `configuration` should not contain any of the following keys: 'repetition',"
             "'config_id'.")
    args = parser.parse_args()

    # load settings
    with open(args.json_file, 'r') as f:
        settings = json.load(f)
    repetitions = settings['repetitions']
    setup = settings['setup']
    sets_of_configurations = settings['configurations']
    assert all(
        '_set_name' in configurations for configurations in sets_of_configurations)
    assert all(
        '_repetition' not in configurations for configurations in sets_of_configurations)
    assert all(
        '_config_id' not in configurations for configurations in sets_of_configurations)

    # create table
    table = []
    config_ids = set()

    setup_parameters = sorted(setup.keys())

    # repetitions in the outermost loop
    for repetition in range(1, repetitions + 1):
        for configurations in sets_of_configurations:  # configurations is a dictionary, with a list for every value
            all_parameters = sorted(configurations.keys())
            del all_parameters[all_parameters.index('_set_name')]
            varying_parameters = list(
                par for par in all_parameters if len(configurations[par]) > 1)
            # configuration is a tuple, values ordered based on all_parameters
            for configuration in product(*(configurations[par] for par in all_parameters)):
                if varying_parameters:
                    config_id = "%s - %s" % (configurations['_set_name'],
                                             '  '.join('%s(%s)' % (par, configuration[all_parameters.index(par)]) for par in varying_parameters))
                else:
                    config_id = configurations['_set_name']
                config_ids.add(config_id)
                table.append([config_id, repetition] + [str({par: arg for par, arg in zip(
                    all_parameters, configuration)})] + [setup[par] for par in setup_parameters])

    # print configurations with repetitions
    with open('runs.tsv', 'w') as f:
        headers = ['_config_id', '_repetition',
                   '_controller'] + setup_parameters
        f.write(tabulate(table, headers, tablefmt='tsv',
                         stralign=None, numalign=None) + '\n')

    # print unique configurations (no repetitions)
    with open('configurations.txt', 'w') as f:
        f.write('\n'.join(sorted(config_ids)) + '\n')
