# -*- coding: utf-8 -*-
"""
Code to abstract Pacman game state to cut it to a manageable state space.

@author: Marco Tamassia
"""
from taxi.game import Action, GameState, Layout
from util.maze_utils import Maze


class TaxiFeaturesExtractor:
    longNames = {
        'ClDs':     'client-shortestPathDistance:',
        'ClDr':     'client-shortestPathDirection',
        'ClBd':     'client-onBoard',
        'DsDs':     'destination-shortestPathDistance:',
        'DsDr':     'destination-shortestPathDirection'
    }
    shortNames = {l: s for s, l in longNames.items()}
    admissibleFeatures = [
        'client-shortestPathDistance',
        'client-shortestPathDirection',
        'client-onBoard',
        'destination-shortestPathDistance',
        'destination-shortestPathDirection',
        'legal-north',
        'legal-east',
        'legal-south',
        'legal-west',
        'legal-pickup',
        'legal-putdown',
    ]

    def __init__(self, features):
        self.features = [f if f in self.admissibleFeatures else self.longNames[f] for f in features]

        self.mazes = {}
        self.maze = None
        self.gameState = None # type: GameState

    def empty_cache(self):
        self.maze.clear_cache()

    @staticmethod
    def identity(*args):
        return args

    def get_legal_actions(self):
        return set(self.gameState.getLegalActions())

    def dijkstra_distance_to_client(self):
        x0, y0 = self.gameState.data.taxi_pos
        x1, y1 = self.gameState.data.client_pos
        return self.maze.shortestPathDistance(x0, y0, x1, y1)

    def dijkstra_direction_to_client(self):
        x0, y0 = self.gameState.data.taxi_pos
        x1, y1 = self.gameState.data.client_pos
        return Action.vectorToDirection(self.maze.shortestPathDirection(x0, y0, x1, y1))

    def dijkstra_distance_to_destination(self):
        x0, y0 = self.gameState.data.taxi_pos
        x1, y1 = self.gameState.data.client_dest
        return self.maze.shortestPathDistance(x0, y0, x1, y1)

    def dijkstra_direction_to_destination(self):
        x0, y0 = self.gameState.data.taxi_pos
        x1, y1 = self.gameState.data.client_dest
        return Action.vectorToDirection(self.maze.shortestPathDirection(x0, y0, x1, y1))

    def set_layout(self, layout: Layout):
        """
        Sets the layout for the features extractor.
        :param layout: level layout
        :type layout: Layout
        """
        layout_hash = hash(tuple(layout.walls))
        if layout_hash not in self.mazes:
            self.mazes[layout_hash] = Maze(
                blocks=[],
                walls=layout.walls,
                height=layout.size[1],
                width=layout.size[0]
            )
        self.maze = self.mazes[layout_hash]

    def extract_feature(self, feature: str):
        discretize = feature[-1] == ':'
        if discretize:
            feature = feature[:-1]
        if feature == 'client-shortestPathDistance':
            retValue = self.dijkstra_distance_to_client()
        elif feature == 'client-shortestPathDirection':
            retValue = self.dijkstra_direction_to_client()
        elif feature == 'destination-shortestPathDistance':
            retValue = self.dijkstra_distance_to_destination()
        elif feature == 'destination-shortestPathDirection':
            retValue = self.dijkstra_direction_to_destination()
        elif feature == 'client-onBoard':
            retValue = self.gameState.data.client_onboard
        elif feature == 'legal-north':
            return Action.north in self.get_legal_actions()
        elif feature == 'legal-east':
            return Action.east in self.get_legal_actions()
        elif feature == 'legal-south':
            return Action.south in self.get_legal_actions()
        elif feature == 'legal-west':
            return Action.west in self.get_legal_actions()
        else:
            raise ValueError("Unknown feature %s" % feature)

        if discretize and retValue is not None:
            if retValue == 0:
                retValue = 0 #'zero'
            elif retValue == 1:
                retValue = 1 #'very-close'
            elif retValue < 5:
                retValue = 2 #'close'
            elif retValue < 15:
                retValue = 3 #'far'
            else:
                retValue = 4 #'very-far'
        return retValue

    def extract_features(self, gameState):
        """
        Extracts all the wanted features from the game state. Features list is
        set in the constructor.
        :param gameState: game state
        """
        self.gameState = gameState
        return {self.shortNames[f] if f in self.shortNames else f:
                self.extract_feature(f)
                for f in self.features}

    def set_game_state(self, gameState):
        self.gameState = gameState
