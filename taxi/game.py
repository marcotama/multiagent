# -*- coding: utf-8 -*-

from abc import abstractmethod
import sys
from copy import copy, deepcopy
import random
import time

from tkinter import Tk, Canvas
from typing import Tuple, List

class Layout:

    """
    A Layout manages the static information about the game board.
    """

    def __init__(self, size, walls, poi):
        self.size = size # type: Tuple[int,int]
        self.walls = walls# type: List[Tuple[int,int]]
        self.poi = poi # type: List[Tuple[int,int]]

    def deepCopy(self):
        return Layout(copy(self.size), copy(self.walls), copy(self.poi))

    @staticmethod
    def get_default():
        default_size = (5, 5)
        # default_walls = [(2,0,2,1),(2,1,2,2),(1,3,1,4),(3,3,3,4),(1,4,1,5),(3,4,3,5),(3,3,4,3)]
        default_walls = [(1,0,2,0),(1,1,2,1),(0,3,1,3),(2,3,3,3),(0,4,1,4),(2,4,3,4)]
        default_poi = [(4,0),(0,0),(0,4),(3,4)]
        return Layout(default_size, default_walls, default_poi)

    def __str__(self):
        return (
            "size=(%d,%d)\n" % self.size +
            "walls=%s\n" % str(self.walls) +
            "poi=%s" % str(self.poi)
        )


class Agent:
    """
    An agent must define a getAction method, but may also define the
    following methods which will be called if they exist:

    def registerInitialState(self, state): # inspects the starting state
    """

    def __init__(self):
        self.experience = []

    @abstractmethod
    def get_action(
            self,
            gameState: 'GameState'
    ):
        """
        The Agent will receive a GameState and must return an action
        from Action.{North, South, East, West, Pickup, Putdown}
        :param gameState: state of the environment
        """
        pass


class Action:
    """
    A collection of static methods for manipulating move actions.
    """
    north = 'North'
    south = 'South'
    east = 'East'
    west = 'West'
    pickup = 'Pickup'
    putdown = 'Putdown'

    @staticmethod
    def vectorToDirection(vector):
        dx, dy = vector
        if dy > 0:
            return Action.north
        if dy < 0:
            return Action.south
        if dx < 0:
            return Action.west
        if dx > 0:
            return Action.east
        return None


class GameStateData:
    """

    """

    def __init__(self, layout):
        """
        Generates a new data packet by copying information from its
        predecessor.
        """
        self.layout = layout
        self.taxi_pos = [random.randint(0,layout.size[0]-1), random.randint(0,layout.size[1]-1)]
        self.client_pos, self.client_dest = random.sample(layout.poi, 2)
        self.client_pos = list(self.client_pos)
        self.client_onboard = False
        self.score = 0

    def deepCopy(self):
        return deepcopy(self)

    def __eq__(self, other):
        """
        Allows two states to be compared.
        """
        return (
            other is not None and
            isinstance(other, GameStateData) and
            hash(self) == hash(other)
        )

    def __hash__(self):
        """
        Allows states to be keys of dictionaries.
        """
        return self.taxi_pos, self.client_pos, self.client_dest, self.client_onboard, self.score

    # noinspection PyStringFormat
    def __str__(self):
        return (
            "taxi(x,y)=(%d,%d)\n" % tuple(self.taxi_pos) +
            "client(x,y)=(%d,%d)\n" % tuple(self.client_pos) +
            "dest(x,y)=(%d,%d)\n" % tuple(self.client_dest) +
            "client_onboard=%s\n" % str(self.client_onboard) +
            "score=%d\n" % self.score # +
            # "layout:\n     %s" % str(self.layout).replace("\n","\n    ")
        )

    def isOver(self):
        return self.client_pos == list(self.client_dest) and not self.client_onboard



class Game:
    """
    The Game manages the control flow, soliciting actions from agents.
    """

    def __init__(self, agent, display, rules):
        self.agentCrashed = False
        self.agent = agent
        self.display = display
        self.rules = rules
        self.gameOver = False
        self.moveHistory = []
        self.numMoves = 0
        self.state = None  # type: GameState

    def getProgress(self):
        if self.gameOver:
            return 1.0
        else:
            return self.rules.getProgress(self)

    def agExec(
        self,
        fName: str,
        required: bool=False,
        *args,
        **kwargs
    ):
        if fName in dir(self.agent):
            agentFunction = getattr(self.agent, fName)
            retValue = agentFunction(*args, **kwargs)
        elif required:
            raise NotImplementedError("Agent does not implement function %s" % fName)
        else:
            retValue = None
        return retValue

    def run(self, passDeepCopies=False, frameTime=0.05):
        """
        Main control loop for game play.
        :param passDeepCopies: if True, deep copies of the state are passed to the agents; if False, references to the
            actual state are passed; True should be used if agents cannot be trusted (i.e. written by students), but
            performance-wise, False is a better choice.
        :param frameTime: time for each frame (seconds)
        """
        if passDeepCopies:
            getState = lambda game: deepcopy(game.state)
        else:
            getState = lambda game: game.state
        self.display.initialize(self.state.data)
        self.numMoves = 0

        # self.display.initialize(self.state.makeObservation(1).data)
        # inform learning agents of the game start
        sys.stderr.flush()
        if not self.agent:
            print("Agent failed to load", file=sys.stderr)
            return
        self.agExec("registerInitialState", observation=getState(self))


        while not self.gameOver:
            # Generate an observation of the state
            observation = self.agExec("observationFunction", gameState=getState(self))
            # Solicit an action
            if observation is None:
                observation = getState(self)
            while True:
                if isinstance(self.display, TaxiGraphics):
                    self.display.master.update()
                    self.display.master.update_idletasks()
                action = self.agExec("getAction", required=True, gameState=observation)
                if action is not None:
                    break
                time.sleep(0.05)

            # Execute the action
            self.moveHistory.append(action)
            self.state = self.state.generateSuccessor(action)

            # Change the display
            self.display.update(self.state.data)

            # Allow for game specific conditions
            self.rules.process(self.state, self)
            # Track progress
            self.numMoves += 1
            time.sleep(frameTime)
        self.state.data.score += WIN_REWARD

        # Inform a learning agent of the game result
        self.agExec("final", gameState=getState(self))
        self.display.finish()


class GameState:
    """
    A GameState specifies the full game state.

    GameStates are used by the Game object to capture the actual state of the
    game and can be used by agents to reason about the game.

    Much of the information in a GameState is stored in a GameStateData object.
    """

    def __init__(self, layout):
        """
        Generates a new state by copying information from its predecessor.
        """
        self.data = GameStateData(layout)


    def getLegalActions(self):
        layout = self.data.layout
        possible = []
        if self.data.taxi_pos == self.data.client_pos and not self.data.client_onboard:
            possible.append(Action.pickup)
        if self.data.client_onboard:
            possible.append(Action.putdown)

        tx, ty = self.data.taxi_pos
        w, h = layout.size
        walls = layout.walls

        if tx > 0 and (tx-1,ty,tx,ty) not in walls:
            possible.append(Action.west)
        if tx < h - 1 and (tx,ty,tx+1,ty) not in walls:
            possible.append(Action.east)
        if ty > 0 and (tx,ty-1,tx,ty) not in walls:
            possible.append(Action.south)
        if ty < h - 1 and (tx,ty,tx,ty+1) not in walls:
            possible.append(Action.north)
        return possible

    def generateSuccessor(self, action):
        """
        Returns the successor state after the specified agent takes the action.
        :param action: action of the agent
        """
        # Check that successors exist
        if self.isOver():
            raise Exception('Can\'t generate a successor of a terminal state.')

        # Copy current state
        state = deepcopy(self)

        # Let agent's logic deal with its action's effects on the board
        self.applyAction(state, action)

        # Time passes
        if action != Action.pickup:
            state.data.score -= TIME_PENALTY
        return state

    def getScore(self):
        return self.data.score

    def isOver(self):
        return self.data.isOver()

    def __eq__(self, other):
        """
        Allows two states to be compared.
        """
        return hasattr(other, 'data') and self.data == other.data

    def __hash__(self):
        """
        Allows states to be keys of dictionaries.
        """
        return hash(self.data)

    def __str__(self):

        return str(self.data)

    def applyAction(self, state, action):
        legal = self.getLegalActions()
        if action not in legal:
            raise Exception("Illegal action " + str(action))
        if action == Action.east:
            state.data.taxi_pos[0] += 1
        if action == Action.west:
            state.data.taxi_pos[0] -= 1
        if action == Action.north:
            state.data.taxi_pos[1] += 1
        if action == Action.south:
            state.data.taxi_pos[1] -= 1
        if state.data.client_onboard:
            state.data.client_pos[:] = state.data.taxi_pos[:]
        if (
            action == Action.pickup and
            not state.data.client_onboard and
            state.data.client_pos == self.data.taxi_pos
        ):
            state.data.client_onboard = True
        if (
            action == Action.putdown and
            state.data.client_onboard
        ):
            state.data.client_onboard = False


TIME_PENALTY = 1  # Number of points lost each round
WIN_REWARD = 1
class ClassicGameRules:
    """
    These game rules manage the control flow of a game, deciding when
    and how the game starts and ends.
    """

    def __init__(self):
        self.initialState = None
        self.quiet = False

    def newGame(self, layout, agent, display, quiet=False) -> Game:
        initState = GameState(layout)
        game = Game(agent, display, self)
        game.state = initState  # type: GameState
        self.initialState = deepcopy(initState)
        self.quiet = quiet
        return game

    def process(self, gameState, game):
        """
        Checks to see whether it is time to end the game.
        :param gameState: game state
        :param game: game
        """
        if gameState.isOver():
            self.win(gameState, game)

    def win(self, state, game):
        if not self.quiet:
            print("You win! Score: %d" % state.data.score)
        game.gameOver = True

class TaxiGraphics:
    colors = ['green','red','yellow','blue','purple','aquamarine2','chocolate3']
    def __init__(self, unit=100):
        self.master = Tk()
        self.unit = unit
        self.color_client = None
        self.taxi = None
        self.client = None
        self.board = None
        self.nextAction = None

    def initialize(self, state: GameStateData):
        # Graphic elements
        self.board = Canvas(
            self.master,
            width= state.layout.size[0] * self.unit,
            height=state.layout.size[1] * self.unit
        )
        # Render grid
        for i in range(state.layout.size[0]):
            for j in range(state.layout.size[1]):
                self.board.create_rectangle(
                    i * self.unit,
                    j * self.unit,
                    (i + 1) * self.unit,
                    (j + 1) * self.unit,
                    fill="white",
                    width=1
                )
        for poi, color in zip(state.layout.poi, self.colors):
            i, j = poi
            self.board.create_rectangle(
                i * self.unit,
                j * self.unit,
                (i + 1) * self.unit,
                (j + 1) * self.unit,
                fill=color,
                width=1
            )
        # board.create_rectangle(0*self.unit, 0*self.unit, (x+1)*self.unit, (y+1)*self.unit, width=10)
        for i1, j1, i2, j2 in state.layout.walls:
            if i1 == i2 and j2 - j1 == 1:
                self.board.create_line(
                    i2 * self.unit,
                    j2 * self.unit,
                    (i2+1) * self.unit,
                    j2 * self.unit,
                    width=10
                )
            if j1 == j2 and i2 - i1 == 1:
                self.board.create_line(
                    i2 * self.unit,
                    j2 * self.unit,
                    i2 * self.unit,
                    (j2+1) * self.unit,
                    width=10
                )


        self.taxi = self.board.create_rectangle(
            state.taxi_pos[0] * self.unit+self.unit * 2/10,
            state.taxi_pos[1] * self.unit+self.unit * 2/10,
            state.taxi_pos[0] * self.unit+self.unit * 8/10,
            state.taxi_pos[1] * self.unit+self.unit * 8/10,
            fill="orange",
            width=1,
            tag="taxi"
        )
        self.client = self.board.create_oval(
            state.client_pos[0] * self.unit+self.unit * 3/10,
            state.client_pos[1] * self.unit+self.unit * 3/10,
            state.client_pos[0] * self.unit+self.unit * 7/10,
            state.client_pos[1] * self.unit+self.unit * 7/10,
            fill=self.colors[state.layout.poi.index(state.client_dest)],
            width=1,
            tag="client"
        )
        self.board.grid(row=0, column=0)

    def update(self, state: GameStateData):
        self.board.coords(
            self.taxi,
            state.taxi_pos[0] * self.unit + self.unit * 2 / 10,
            state.taxi_pos[1] * self.unit + self.unit * 2 / 10,
            state.taxi_pos[0] * self.unit + self.unit * 8 / 10,
            state.taxi_pos[1] * self.unit + self.unit * 8 / 10
        )
        if state.client_onboard:
            self.board.coords(
                self.client,
                state.client_pos[0] * self.unit + self.unit * 4 / 10,
                state.client_pos[1] * self.unit + self.unit * 4 / 10,
                state.client_pos[0] * self.unit + self.unit * 6 / 10,
                state.client_pos[1] * self.unit + self.unit * 6 / 10
            )
        else:
            self.board.coords(
                self.client,
                state.client_pos[0] * self.unit + self.unit * 3 / 10,
                state.client_pos[1] * self.unit + self.unit * 3 / 10,
                state.client_pos[0] * self.unit + self.unit * 7 / 10,
                state.client_pos[1] * self.unit + self.unit * 7 / 10
            )

    def finish(self):
        self.master.destroy()

    def saveAction(self, action):
        self.nextAction = action

class NullGraphics:
    def __init__(self):
        pass
    def initialize(self, state: GameStateData):
        pass
    def update(self, state: GameStateData):
        pass
    def finish(self):
        pass