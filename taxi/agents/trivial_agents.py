from taxi.features_extractor import TaxiFeaturesExtractor
from taxi.game import Agent, Action



class GreedyAgent(Agent):

    def __init__(self):
        super().__init__()
        self.featuresExtractor = TaxiFeaturesExtractor([])

    def get_action(self, gameState):
        self.featuresExtractor.set_game_state(gameState)
        get = self.featuresExtractor.extract_feature
        if get('client-onBoard'):
            if get('destination-shortestPathDistance') == 0:
                return Action.putdown
            else:
                return get('destination-shortestPathDirection')
        else:
            if get('client-shortestPathDistance') == 0:
                return Action.pickup
            else:
                return get('client-shortestPathDirection')
