from taxi.game import Agent, Action

class KeyboardAgent(Agent):

    """
    An agent controlled by the keyboard.
    """

    def __init__(self):
        super().__init__()
        self.nextAction = None

    def get_action(self, gameState):
        legal = gameState.get_legal_actions()
        if self.nextAction not in legal:
            self.nextAction = None
        action = self.nextAction
        self.nextAction = None
        return action

    def goWest(self, _):
        self.nextAction = Action.west
    def goEast(self, _):
        self.nextAction = Action.east
    def goNorth (self, _):
        self.nextAction = Action.north
    def goSouth (self, _):
        self.nextAction = Action.south
    def pickup (self, _):
        self.nextAction = Action.pickup
    def putdown (self, _):
        self.nextAction = Action.putdown