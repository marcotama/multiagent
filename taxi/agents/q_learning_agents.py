# -*- coding: utf-8 -*-
"""
Pacman agent using the Q-learning algorithm.

@author: Marco Tamassia
"""

import itertools
from typing import Iterable, List

from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from reinf.q_learning import QLearningAgent as QL
from reinf.multi_q_learning import MultiQLearningAgent as MQL
from util.delegation import delegate_attrs
from taxi.agents.common import LearningAgent


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
    '__str__',
])
class QLearningAgent(LearningAgent):

    def __init__(
        self,
        features: List[str],
        explParams: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
    ):
        super().__init__(features)
        self.qAgent = QL(
            features=features,
            exploration_params=explParams,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )
        
    def learning_fn(self, *args, **kwargs):
        return self.qAgent.learn(*args, **kwargs)
        
    def decision_fn(self, *args, **kwargs):
        return self.qAgent.get_action(*args, **kwargs)

    def end_of_episode(self):
        self.qAgent.end_of_episode()

    def get_mdp(self):
        return self.qAgent.mdp


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
    '__str__',
])
class MultiQLearningAgent(LearningAgent):

    def __init__(
        self,
        featuresSets: List[List[str]],
        explParams: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        options: Iterable[Option],
        early_termination: bool,
    ):
        features = list(set(itertools.chain(*featuresSets)))
        super().__init__(features)
        features = list(set(itertools.chain(*featuresSets)))
        self.qAgent = MQL(
            features=features,
            featuresSets=featuresSets,
            exploration_params=explParams,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination
        )

    def get_abstractions_usage(self):
        return self.qAgent.get_abstractions_usage()
        
    def learning_fn(self, *args, **kwargs):
        return self.qAgent.learn(*args, **kwargs)
        
    def decision_fn(self, *args, **kwargs):
        return self.qAgent.get_action(*args, **kwargs)

    def end_of_episode(self):
        self.qAgent.end_of_episode()

    def get_mdp(self):
        return self.qAgent.mdp