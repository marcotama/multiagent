# -*- coding: utf-8 -*-
"""
Utility class to transform a Pacman demo to a MDP trace.
Demo format: (layout, [(agent_id, action) x N_agents x N_demo_steps])
Trace format: (state, legalActions, action, reward)

@author: Marco Tamassia
"""
from typing import Tuple, List

from taxi.game import ClassicGameRules, Layout, NullGraphics, Action
from taxi.agents.q_learning_agents import QLearningAgent



class TaxiDemosConverter:
    def __init__(
        self,
        agent: QLearningAgent,
    ):
        self.agent = agent


    def iterDemo (
        self,
        demo: Tuple[Layout, List[Tuple[int, Action]]]
    ):
        """
        Takes a demo as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demos are formatted as follows: (layout, [(agent_id, action) x N_demo_steps])
        """
        rules = ClassicGameRules()

        # Load game info
        layout, movesHistory = demo
        self.agent.setLayout(layout)

        # Setup agents
        agent = self.agent
        # Setup new game
        game = rules.newGame(layout, agent, NullGraphics(), quiet=True)
        gameState = game.state

        # Iterate through moves
        score = 0
        for agentIndex, newAction in movesHistory:
            if agentIndex == 0:
                newState, newScore, newLegalActions = self.agent.observationFunction(gameState)
                yield newState, newLegalActions, newAction, newScore - score
                score = newScore

            # Update game state
            gameState = gameState.generateSuccessor(agentIndex, newAction)
            rules.process(gameState, game)

        # Pass info about the final step to the agent
        _, newScore, _ = self.agent.observationFunction(gameState)
        yield None, [], None, newScore - score


    def iterDemoWithNextState(
        self,
        demo: Tuple[Layout, List[Tuple[int, Action]]]
    ):
        prev = None
        for ns, nla, na, nr in self.iterDemo(demo):
            if prev is not None:
                s, la, a, r = prev
                yield s, la, a, r, ns, nla
            prev = ns, nla, na, nr


    def iterDemos(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Action]]]]
    ):
        """
        Takes a demo file as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demo file must be formatted as follows: [(layout, [(agent_id, action) x N_demo_steps]) x N_demos]
        """
        for i, demo in enumerate(demos):
            for s, la, a, r in self.iterDemo(demo):
                yield s, la, a, r


    def iterDemosWithNextState(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Action]]]]
    ):
        """
        Takes a demo file as input and generates a sequence of (state, legalActions, action, reward) tuples.
        Demo file must be formatted as follows: [(layout, [(agent_id, action) x N_demo_steps]) x N_demos]
        """
        for demo in demos:
            for s, la, a, r, ns, nla in self.iterDemoWithNextState(demo):
                yield s, la, a, r, ns, nla


    def demoToTrace(
        self,
        demo: Tuple[Layout, List[Tuple[int, Action]]]
    ):
        return list(self.iterDemo(demo))


    def demosToTraces(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Action]]]]
    ):
        return [self.demoToTrace(demo) for demo in demos]


    def demoToMDPTrace(
        self,
        demo: Tuple[Layout, List[Tuple[int, Action]]]
    ):
        enc_s = self.agent.qAgent.stateTranslator.getCode
        enc_a = self.agent.qAgent.actionTranslator.getCode
        return [(enc_s(s), [enc_a(a_) for a_ in la], enc_a(a), r) for s, la, a, r in self.iterDemo(demo)]


    def demosToMDPTraces(
        self,
        demos: List[Tuple[Layout, List[Tuple[int, Action]]]]
    ):
        return [self.demoToMDPTrace(demo) for demo in demos]