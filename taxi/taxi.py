"""
Pacman.py holds the logic for the classic pacman game along with the main
code to run a game.  This file is divided into three sections:

  (i)  Your interface to the pacman world:
          Pacman is a complex environment.  You probably don't want to
          read through all of the code we wrote to make the game runs
          correctly.  This section contains the parts of the code
          that you will need to understand in order to complete the
          project.  There is also some code in game.py that you should
          understand.

  (ii)  The hidden secrets of pacman:
          This section contains all of the logic code that the pacman
          environment uses to decide who can move where, who dies when
          things collide, etc.  You shouldn't need to read this section
          of code, but you can if you want.

  (iii) Framework to start a game:
          The final section contains the code for reading the command
          you use to set up the game, then starting up a new game, along with
          linking in all the external parts (agent functions, graphics).
          Check this section out to see all the options available to you.

To play your first game, type 'python taxi.py' from the command line.
The keys are 'a', 's', 'd', and 'w' to move (or arrow keys).  Have fun!
"""

import random
import sys
import pkgutil
from importlib import import_module

from taxi.game import ClassicGameRules


#
# YOUR INTERFACE TO THE PACMAN WORLD: A GameState #
#

#
# FRAMEWORK TO START A GAME #
#


def default(string):
    return string + ' [Default: %default]'


def parseAgentArgs(string):
    if string is None:
        return {}
    pieces = string.split(',')
    opts = {}
    for p in pieces:
        if '=' in p:
            key, val = p.split('=')
        else:
            key, val = p, 1
        opts[key] = val
    return opts


def readCommand(argv):
    """
    Processes the command used to run pacman from the command line.
    :param argv: param list
    """
    from optparse import OptionParser
    usageStr = """
    USAGE:      python taxi.py <options>
    EXAMPLES:   (1) python taxi.py
                    - starts an interactive game
                (2) python taxi.py --layout smallClassic --zoom 2
                OR  python taxi.py -l smallClassic -z 2
                    - starts an interactive game on a smaller board, zoomed in
    """
    parser = OptionParser(usageStr)

    parser.add_option('-n',
                      '--numGames',
                      dest='numGames',
                      type='int',
                      help=default('the number of GAMES to play'),
                      metavar='GAMES',
                      default=1)
    parser.add_option('-l',
                      '--layout',
                      dest='layout',
                      help=default('the LAYOUT_FILE from which to load the ' +
                                   'map layout'),
                      metavar='LAYOUT_FILE',
                      default='mediumClassic')
    parser.add_option('-p',
                      '--pacman',
                      dest='pacman',
                      help=default('the agent TYPE in the pacmanAgents ' +
                                   'module to use'),
                      metavar='TYPE',
                      default='KeyboardAgent')
    parser.add_option('-t',
                      '--textGraphics',
                      action='store_true',
                      dest='textGraphics',
                      help='Display output as text only',
                      default=False)
    parser.add_option('-q',
                      '--quietTextGraphics',
                      action='store_true',
                      dest='quietGraphics',
                      help='Generate minimal output and no graphics',
                      default=False)
    parser.add_option('-g',
                      '--ghosts',
                      dest='ghost',
                      help=default('the ghost agent TYPE in the ghostAgents ' +
                                   'module to use'),
                      metavar='TYPE',
                      default='RandomGhost')
    parser.add_option('-k',
                      '--numghosts',
                      type='int',
                      dest='numGhosts',
                      help=default('The maximum number of ghosts to use'),
                      default=4)
    parser.add_option('-z',
                      '--zoom',
                      type='float',
                      dest='zoom',
                      help=default('Zoom the size of the graphics window'),
                      default=1.0)
    parser.add_option('-f',
                      '--fixRandomSeed',
                      action='store_true',
                      dest='fixRandomSeed',
                      help='Fixes the random seed to always play the same ' +
                           'game',
                      default=False)
    parser.add_option('-r',
                      '--recordActions',
                      type=str,
                      dest='record',
                      help='Writes game histories to the given file',
                      default=None)
    parser.add_option('--replay',
                      dest='gameToReplay',
                      help='A recorded game file (pickle) to replay',
                      default=None)
    parser.add_option('-a',
                      '--agentArgs',
                      dest='agentArgs',
                      help='Comma separated values sent to agent. e.g. ' +
                           '"opt1=val1,opt2,opt3=val3"')
    parser.add_option('-x',
                      '--numTraining',
                      dest='numTraining',
                      type='int',
                      help=default('How many episodes are training ' +
                                   '(suppresses output)'),
                      default=0)
    parser.add_option('--frameTime',
                      dest='frameTime',
                      type='float',
                      help=default('Time to delay between frames; <0 means ' +
                                   'keyboard'),
                      default=0.1)
    parser.add_option('-c',
                      '--catchExceptions',
                      action='store_true',
                      dest='catchExceptions',
                      help='Turns on exception handling and timeouts during ' +
                           'games',
                      default=False)
    parser.add_option('--timeout',
                      dest='timeout',
                      type='int',
                      help=default('Maximum length of time an agent can ' +
                                   'spend computing in a single game'),
                      default=30)

    options, otherjunk = parser.parse_args(argv)
    if len(otherjunk) != 0:
        raise Exception('Command line input not understood: ' + str(otherjunk))
    args = {}

    # Fix the random seed
    if options.fixRandomSeed:
        random.seed('games')

    # Choose a layout
    args['layout'] = getLayout(options.layout)
    if args['layout'] is None:
        raise Exception("The layout " + options.layout + " cannot be found")

    # Choose a Pacman agent
    noKeyboard = options.gameToReplay is None and (
        options.textGraphics or options.quietGraphics)
    pacmanType = loadAgent(options.pacman, noKeyboard)
    agentOpts = parseAgentArgs(options.agentArgs)
    if options.numTraining > 0:
        args['numTraining'] = options.numTraining
        if 'numTraining' not in agentOpts:
            agentOpts['numTraining'] = options.numTraining
    pacman = pacmanType(**agentOpts)  # Instantiate Pacman with agentArgs
    args['pacman'] = pacman

    # Don't display training games
    if 'numTrain' in agentOpts:
        options.numQuiet = int(agentOpts['numTrain'])
        options.numIgnore = int(agentOpts['numTrain'])

    # Choose a ghost agent
    ghostType = loadAgent(options.ghost, noKeyboard)
    args['ghosts'] = [ghostType(i + 1) for i in range(options.numGhosts)]

    # Choose a display format
    if options.quietGraphics:
        args['display'] = text_display.NullGraphics()
    elif options.textGraphics:
        text_display.SLEEP_TIME = options.frameTime
        args['display'] = text_display.PacmanGraphics()
    else:
        args['display'] = graphics_display.PacmanGraphics(
            options.zoom, frameTime=options.frameTime)
    args['numGames'] = options.numGames
    args['record'] = options.record
    args['catchExceptions'] = options.catchExceptions
    args['timeout'] = options.timeout

    # Special case: recorded games don't use the runGames method or args
    # structure
    if options.gameToReplay is not None:
        print('Replaying recorded game %s.' % options.gameToReplay)
        import pickle
        f = open(options.gameToReplay, 'rb')
        try:
            recorded = pickle.load(f)
        finally:
            f.close()
        recorded['display'] = args['display']
        replayGame(**recorded)
        sys.exit(0)

    return args


def replayGame(layout, actions, display):
    rules = ClassicGameRules()
    agents = [trivial_agents.GreedyAgent()]
    agents += [ghostAgents.RandomGhost(i + 1)
               for i in range(layout.getNumGhosts())]
    game = rules.newGame(layout, agents[0], agents[1:], display)
    state = game.state
    display.end_of_episode(state.data)

    for action in actions:
        # Execute the action
        state = state.generateSuccessor(*action)
        # Change the display
        display.update(state.data)
        # Allow for game specific conditions (winning, losing, etc.)
        rules.process(state, game)

    display.finish()


def loadAgent(pacman, nographics):
    agentsModuleNames = [name for _, name, _ in pkgutil.iter_modules(['pacman/agents'])]
    for agentsModuleName in agentsModuleNames:
        if not agentsModuleName.endswith('gents'):
            continue
        agentsModule = import_module("taxi.agents." + agentsModuleName)
        if pacman in dir(agentsModule):
            if nographics and agentsModule == 'keyboard_agents.py':
                raise Exception('Using the keyboard requires graphics (not text display)')
            return getattr(agentsModule, pacman)
    raise Exception('The agent %s is not specified in any *Agents.py.' % pacman)


def runGames(layout, pacman, ghosts, display, numGames, record, numTraining=0,
             catchExceptions=False, timeout=30):
    # import __main__
    # __main__.__dict__['_display'] = display

    rules = ClassicGameRules(timeout)
    games = []

    for i in range(numGames):
        beQuiet = i < numTraining
        if beQuiet:
                # Suppress output and graphics
            gameDisplay = text_display.NullGraphics()
            rules.quiet = True
        else:
            gameDisplay = display
            rules.quiet = False
        game = rules.newGame(
            layout, pacman, ghosts, gameDisplay, beQuiet, catchExceptions)
        game.run()
        if not beQuiet:
            games.append(game)

        if record is not None:
            import pickle
            fname = record
            f = open(fname, 'wb')
            components = {'layout': layout, 'actions': game.moveHistory}
            pickle.dump(components, f)
            f.close()

    if (numGames - numTraining) > 0:
        scores = [g.state.getScore() for g in games]
        wins = [g.state.isWin() for g in games]
        winRate = wins.count(True) / float(len(wins))
        print('Average Score:', sum(scores) / float(len(scores)))
        print('Scores:       ', ', '.join([str(score) for score in scores]))
        print('Win Rate:      %d/%d (%.2f)' %
              (wins.count(True), len(wins), winRate))
        print('Record:       ',
              ', '.join([['Loss', 'Win'][int(w)] for w in wins]))

    return games

if __name__ == '__main__':
    """
    The main function called when taxi.py is run
    from the command line:

    > python taxi.py

    See the usage string for more details.

    > python taxi.py --help
    """
    def do_stuff():
        args = readCommand(sys.argv[1:])  # Get game components based on input
        runGames(**args)

    do_stuff()

    # import cProfile
    # cProfile.run("runGames( **args )")
