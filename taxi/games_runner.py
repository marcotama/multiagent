# -*- coding: utf-8 -*-
"""
Runs multiple episodes of Taxi with a learning agent.

@author: Marco Tamassia
"""
import sys
import timeit
import time
import traceback
from typing import Union, List, Tuple

from taxi.agents.q_learning_agents import QLearningAgent, MultiQLearningAgent
from taxi.agents.keyboard_agents import KeyboardAgent
from taxi.data_structures import EpisodeData
from taxi.game import NullGraphics, TaxiGraphics, Layout, ClassicGameRules


class GamesRunner:
    def __init__(
            self,
            taxi: Union[KeyboardAgent, QLearningAgent, MultiQLearningAgent],
            layout: Layout,
            episodes: int,
            testGreedily: bool=True,
            quiet: bool=False,
            graphics: str='null',
            graphicsAfter: int=0,
            frameTime: float=0.05,
            saveMovesHistories: bool=True
    ):
        self.episodes = episodes
        self.layout = layout
        self.initialized = False
        self.graphics = graphics
        self.graphicsAfter = graphicsAfter
        self.taxi = taxi
        self.frameTime = frameTime
        self.saveMovesHistories = saveMovesHistories
        self.testGreedily = testGreedily
        self.quiet = quiet
        self.currentEpisode = 0
        self.display = None

    def getAgentFunctions(self):
        if hasattr(self.taxi, 'explore') and callable(self.taxi.explore):
            explore = lambda: self.taxi.explore()
        else:
            explore = lambda: None
        if hasattr(self.taxi, 'exploit') and callable(self.taxi.exploit):
            exploit = lambda: self.taxi.exploit()
        else:
            exploit = lambda: None
        if hasattr(self.taxi, 'doLearn') and callable(self.taxi.doLearn):
            doLearn = lambda: self.taxi.doLearn()
        else:
            doLearn = lambda: None
        if hasattr(self.taxi, 'dontLearn') and callable(self.taxi.dontLearn):
            dontLearn = lambda: self.taxi.dontLearn()
        else:
            dontLearn = lambda: None
        if hasattr(self.taxi, 'episodeIsOver') and callable(self.taxi.dontLearn):
            episodeIsOver = lambda: self.taxi.episodeIsOver()
        else:
            episodeIsOver = lambda: None
        return explore, exploit, doLearn, dontLearn, episodeIsOver

    def test(self):
        epsData = []
        records = []
        if 'featuresExtractor' in dir(self.taxi):
            self.taxi.featuresExtractor.set_layout(self.layout)
        startExploration, stopExploration, startLearning, stopLearning, episodeIsOver = self.getAgentFunctions()
        startExploration()
        startLearning()
        for e in range(self.episodes):

            self.currentEpisode = e

            if self.testGreedily:
                startExploration()
                startLearning()
                self.runEpisode()
                stopExploration()
                stopLearning()
            epData, record = self.runEpisode()
            episodeIsOver()

            epsData.append(epData)
            if self.saveMovesHistories:  # This 'if' saves memory
                records.append(record)

        if not self.quiet:
            print()
            print()
            print(self.taxi)
        return epsData, records

    def runEpisode(
        self
    ) -> Tuple[EpisodeData, List[Tuple[int, int]]]:
        self.initialized = True
        rules = ClassicGameRules()

        if self.graphics == 'null' or self.currentEpisode < self.graphicsAfter:
            self.display = NullGraphics()
        elif self.graphics == 'full':
            self.display = TaxiGraphics()
            if isinstance(self.taxi, KeyboardAgent):
                self.display.master.bind("<Down>", self.taxi.goNorth)
                self.display.master.bind("<Up>", self.taxi.goSouth)
                self.display.master.bind("<Right>", self.taxi.goEast)
                self.display.master.bind("<Left>", self.taxi.goWest)
                self.display.master.bind("u", self.taxi.pickup)
                self.display.master.bind("d", self.taxi.putdown)

        game = rules.newGame(self.layout, self.taxi, self.display, quiet=True)


        retryCount = 3
        total_time = None
        while retryCount > 0:
            # noinspection PyBroadException
            try:
                if self.graphics != 'null' and self.currentEpisode >= self.graphicsAfter:
                    for i in range(3, 0, -1):
                        print("Next game in %d..." % i, end='\r')
                        time.sleep(1)
                    print("Start!" + " " * 20, end='\r')
                start = timeit.default_timer()
                game.run(frameTime=self.frameTime)
                stop = timeit.default_timer()
                total_time = stop - start
                break
            except KeyboardInterrupt:
                sys.exit(0)
            except:
                traceback.print_exc()
                retryCount -= 1
                if retryCount > 0:
                    sys.stdout.write("Game failed. Trying again.\n")
                else:
                    sys.stdout.write("Game failed too many times. Quitting.\n")
                

        data = EpisodeData(
            episode=self.currentEpisode,
            outcomeStr='WON' if game.state.isOver() else 'CRASHED',
            outcome=1 if game.state.isOver() else 0,
            score=game.state.data.score,
            movesCount=game.numMoves,
            statesCount=self.taxi.getStatesCount() if 'getStatesCount' in dir(self.taxi) else None,
            stateActionPairsCount=self.taxi.getStateActionPairsCount() if 'getStateActionPairsCount' in dir(self.taxi) else None,
            abstractionsUsage=self.taxi.getAbstractionsUsage() if 'getAbstractionsUsage' in dir(self.taxi) else None,
            executionTime=total_time
        )
        record = game.state.data.layout, game.moveHistory

        if not self.quiet:
            print("Episode #{episode}, {outcomeStr}, score: {score}, moves: {movesCount}".format(**data._asdict()))
        return data, record
