#!/usr/bin/env bash

WORKER="${1}"
INSTANCE_ID=`date +%Y-%m-%d__%H.%M.%S`
WORKING_DIR=/home/ubuntu/http-server-${INSTANCE_ID}
TAR_PATH=/home/ubuntu/code.tar.gz

ssh ${WORKER} mkdir -p ${WORKING_DIR}/uploads
scp http_server/server.py ${WORKER}:${WORKING_DIR}
ssh -t ${WORKER} "killall screen"
ssh -t ${WORKER} "screen -dRR http_server -dm bash -c \"echo Server opened. Press Ctrl-A and then Ctrl-D to detach from this screen. ; cd ${WORKING_DIR} && python3 ${WORKING_DIR}/server.py | tee log.txt\""
