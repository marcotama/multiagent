#!/usr/bin/env bash
pkill -f py4j
cd /home/marco/Desktop/fightingice/FightingICE
java -Djava.library.path=lib/native/linux -cp "bin/:lib/*" Main --py4j --fastmode &
sleep 3
