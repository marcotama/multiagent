#!/usr/bin/env bash

# Do not forget to accept the public keys of all the machines before running this script.
# To do so just SSH one machine at a time and enter yes when prompted for key acceptance.
# This command erases known hosts
#   ssh-keygen -R {}
# This command adds the given IP to the known hosts known hosts
#   ssh-keyscan -H {} >> ~/.ssh/known_hosts

hosts='118.138.244.8 118.138.244.81 118.138.244.80 118.138.244.78 118.138.244.73 118.138.244.79 118.138.244.77 118.138.244.75 118.138.244.72'

for ip in $hosts; do
    ssh ubuntu@$ip -oStrictHostKeyChecking=no pwd
    scp configure_machine.sh ubuntu@$ip:/tmp/configure_machine.sh
    ssh ubuntu@$ip sudo bash /tmp/configure_machine.sh &> configuration_$ip.log &
done