#!/usr/bin/env bash

WORKERS_PARALLEL="16/ubuntu@131.170.250.69,4/ubuntu@131.170.250.70,4/ubuntu@131.170.250.94,2/ubuntu@131.170.250.95"
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`
parallel --nonall -S ${WORKERS} killall python -9
