#!/usr/bin/env bash

WORKERS=`echo $1 | sed 's/[0-9]*\///g'` # Remove number of CPUs, if present
parallel --nonall -S ${WORKERS} ${@:2}