#!/usr/bin/env bash

WORKERS=`echo $1 | sed 's/[0-9]*\///g'` # Remove number of CPUs, if present
parallel scp -r {}:"$2" "$3"  ::: `echo ${WORKERS} | sed 's/,/ /g'`
