#!/usr/bin/env bash

echo "" > /etc/apt/sources.list
echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt xenial main restricted universe multiverse'           >> /etc/apt/sources.list
echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt xenial-updates main restricted universe multiverse'   >> /etc/apt/sources.list
echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt xenial-backports main restricted universe multiverse' >> /etc/apt/sources.list
echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt xenial-security main restricted universe multiverse'  >> /etc/apt/sources.list
apt-get update
apt-get --assume-yes install make python3 python3-pip gfortran libblas-dev liblapack-dev python3-tk python3-matplotlib rsync
rm -R /tmp/pip_build_root/numpy
rm -R /tmp/pip_build_root/scipy
rm -R /tmp/pip_build_root/tabulate
rm -R /tmp/pip_build_root/mypy-lang
pip3 install numpy scipy tabulate mypy-lang
(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - http://pi.dk/3) | bash
echo "will cite" | parallel --bibtex
