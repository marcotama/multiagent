from reinf.options.option import UniformTerminationProbabilityOption
from util.typing2 import HashedState, Action
from typing import Iterable
import random
from pacman_ctf.game import Direction


class GoToOption(UniformTerminationProbabilityOption):
    """
    An option whose policy is to go to the closest instance of an entity of given type (e.g., food, capsule, ...).
    Assumes that 'best_way_to_<entity_type>' is part of the state.
    """
    def __init__(
            self,
            goToWhat: str,
            tProb: float
    ):
        self.get_all_state_features = None
        super().__init__('go_to_%s' % goToWhat, None, tProb)
        self.targetFeature = 'best_way_to_%s' % goToWhat

    def _policy(
        self,
        s: HashedState,
        la: Iterable[Action]
    ) -> Action:
        state = self.get_all_state_features()
        directionToFollow = state[self.targetFeature]
        if directionToFollow is None:
            return random.choice(list(set(la) & Direction.all))
        else:
            return directionToFollow



class AvoidOption(UniformTerminationProbabilityOption):
    """
    An option whose policy is to avoid the closest instance of an entity of given type (e.g., food, capsule, ...).
    Assumes that 'best_way_to_<entity_type>' is part of the state.
    """
    def __init__(
        self,
        avoidWhat: str,
        tProb: float,
    ) -> None:
        self.get_all_state_features = None
        super().__init__('avoid_%s' % avoidWhat, None, tProb)
        self.targetFeature = 'best_way_to_%s' % avoidWhat

    def _policy(
        self,
        s: HashedState,
        la: Iterable[Action]
    ) -> Action:
        state = self.get_all_state_features()
        directionNotToFollow = state[self.targetFeature]
        viable = {a for a in la if isinstance(a, Action) and
                  a != directionNotToFollow and
                  a != 'Stop'}
        selectable = list(viable & Direction.all)
        if selectable:
            return random.choice(selectable)
        else:
            return Direction.STOP





class GoToFood(GoToOption):
    """
    An option whose policy is to go to the closest edible food. Assumes that 'best_way_to_food' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('food', tProb)

class GoToCapsule(GoToOption):
    """
    An option whose policy is to go to the closest edible capsule. Assumes that 'best_way_to_capsule' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('capsule', tProb)

class GoToInvader(GoToOption):
    """
    An option whose policy is to go to the closest invader. Assumes that 'best_way_to_invader' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('invader', tProb)

class GoToScaredDefender(GoToOption):
    """
    An option whose policy is to go to the closest edible capsule. Assumes that 'best_way_to_scared_defender' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('scared_defender', tProb)

class GoToHome(GoToOption):
    """
    An option whose policy is to go home. Assumes that 'best_way_to_home' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('home', tProb)


class AvoidNonScaredDefender(AvoidOption):
    """
    An option whose policy is avoid the closest non-scared defender. Assumes that feature 'best_way_to_non_scared_defender' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('non_scared_defender', tProb)

class AvoidInvader(GoToOption):
    """
    An option whose policy is to avoid the closest invader. Assumes that 'best_way_to_invader' is part of the state.
    """
    def __init__(self, tProb) -> None:
        super().__init__('invader', tProb)


