# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from pacman_ctf.distance_calculator import Distancer
from pacman_ctf.capture import GameState, halfGrid
from pacman_ctf.game import Direction
from util.util import nearest_point
from util.sorting import arg_min
from typing import List, Dict, Tuple, Callable, Union, Hashable, Optional


class CaptureFeaturesExtractor:
    """
    A Dummy agent to serve as an example of the necessary agent structure.
    You should look at baseline_team.py for more details about how to
    create an agent as this is the bare minimum.
    """

    def __init__(
            self,
            index: int,
            game_state: GameState,
            default_features: List[str]=None,
            maze_distances: bool=False
    ):
        if default_features is None:
            default_features = []
        self.index = index
        self.default_features = default_features
        self.isRed = game_state.isOnRedTeam(self.index)
        self.start = [game_state.getAgentPosition(index) for index in range(game_state.getNumAgents())]
        self.distancer = Distancer(game_state.data.layout)
        if maze_distances:
            self.distancer.getMazeDistances()

    def get_successor(self, game_state, action, index: Optional[int]=None):
        """
        Finds the next successor which is a grid position (location tuple).
        """
        if index is None:
            index = self.index

        successor = game_state.generateSuccessor(index, action)
        pos = successor.getAgentState(index).getPosition()
        if pos != nearest_point(pos):
            # Only half a grid position was covered
            return successor.generateSuccessor(index, action)
        else:
            return successor

    def get_invaders(self, game_state: GameState) -> List[Tuple[int, int]]:
        enemies = [game_state.getAgentState(i) for i in self.get_opponents(game_state)]
        invaders_pos = [e.getPosition() for e in enemies if e.isPacman]
        return [p for p in invaders_pos if p is not None]

    def get_defenders(self, game_state: GameState) -> List[Tuple[int, int]]:
        enemies = [game_state.getAgentState(i) for i in self.get_opponents(game_state)]
        defenders_pos = [e.getPosition() for e in enemies if not e.isPacman]
        return [p for p in defenders_pos if p is not None]

    def get_scared_defenders(self, game_state: GameState) -> List[Tuple[int, int]]:
        enemies = [game_state.getAgentState(i) for i in self.get_opponents(game_state)]
        defenders_pos = [e.getPosition() for e in enemies if not e.isPacman and e.scaredTimer > 0]
        return [p for p in defenders_pos if p is not None]

    def get_non_scared_defenders(self, game_state: GameState) -> List[Tuple[int, int]]:
        enemies = [game_state.getAgentState(i) for i in self.get_opponents(game_state)]
        defenders_pos = [e.getPosition() for e in enemies if not e.isPacman and e.scaredTimer <= 0]
        return [p for p in defenders_pos if p is not None]

    def best_way_to(
            self,
            successors: Dict[Direction, GameState],
            entities_getter: Callable[[GameState], List[Tuple[int,int]]],
            index: Optional[int]=None
    ):
        if index is None:
            index = self.index

        distance_by_direction = {}
        for direction, successor in successors.items():
            my_position = successor.getAgentState(index).getPosition()
            entities_position = entities_getter(successor)
            if len(entities_position) > 0:
                dists = [self.get_maze_distance(my_position, entity_position) for entity_position in entities_position]
                distance_by_direction[direction] = min(dists)
        if distance_by_direction:
            return arg_min(distance_by_direction)
        else:
            return None

    def distance_to_closest(
            self,
            successors: Dict[Direction, GameState],
            entities_getter: Callable[[GameState], List[Tuple[int,int]]],
            index: Optional[int]=None
    ):
        if index is None:
            index = self.index

        distance_by_direction = {}
        for direction, successor in successors.items():
            my_position = successor.getAgentState(index).getPosition()
            entities_position = entities_getter(successor)
            if len(entities_position) > 0:
                dists = [self.get_maze_distance(my_position, entity_position) for entity_position in entities_position]
                distance_by_direction[direction] = min(dists)
        if distance_by_direction:
            return min(distance_by_direction.values())
        else:
            return None


    def get_home(self, game_state: GameState, index: Optional[int]=None):
        if index is None:
            index = self.index

        return [self.start[index]]


    def get_food_for_your_team_to_eat(self, game_state: GameState, index: Optional[int]=None) -> List[Tuple[int, int]]:
        """
        Returns the food you're meant to eat. This is in the form of a matrix
        where m[x][y]=true if there is food you can eat (based on your team) in that square.
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getBlueFood().asList()
        else:
            return game_state.getRedFood().asList()

    def get_food_for_opponent_team_to_eat(self, game_state: GameState, index: Optional[int]=None) -> List[Tuple[int, int]]:
        """
        Returns the food you're meant to protect (i.e., that your opponent is
        supposed to eat). This is in the form of a matrix where m[x][y]=true if
        there is food at (x,y) that your opponent can eat.
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getRedFood().asList()
        else:
            return game_state.getBlueFood().asList()

    def get_layout_food_for_your_team_to_eat(self, game_state: GameState, index: Optional[int]=None):
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        return halfGrid(game_state.data.layout.food, red=isRed).asList()

    def get_layout_food_for_opponent_team_to_eat(self, game_state: GameState, index: Optional[int]=None):
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        return halfGrid(game_state.data.layout.food, red=not isRed).asList()

    def get_food_your_team_is_carrying(self, game_state: GameState, index: Optional[int]=None):
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        your_team = self.get_team(game_state)
        return sum(game_state.getAgentState(i).numCarrying for i in your_team)

    def get_food_opponent_team_is_carrying(self, game_state: GameState, index: Optional[int]=None):
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        opponent_team = self.get_opponents(game_state)
        return sum(game_state.getAgentState(i).numCarrying for i in opponent_team)

    def get_capsules(self, game_state: GameState, index: Optional[int]=None) -> List[Tuple[int, int]]:
        """
        Returns the food you're meant to eat. This is in the form of a list of
        coordinates pairs.
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getBlueCapsules()
        else:
            return game_state.getRedCapsules()

    def get_capsules_you_are_defending(self, game_state: GameState, index: Optional[int]=None) -> List[Tuple[int, int]]:
        """
        Returns the food you're meant to protect (i.e., that your opponent is
        supposed to eat). This is in the form of a list of coordinates pairs.
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getRedCapsules()
        else:
            return game_state.getBlueCapsules()

    def get_opponents(self, game_state: GameState, index: Optional[int]=None) -> List[int]:
        """
        Returns agent indices of your opponents. This is the list of the numbers
        of the agents (e.g., red might be "1,3,5")
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getBlueTeamIndices()
        else:
            return game_state.getRedTeamIndices()

    def get_team(self, game_state: GameState, index: Optional[int]=None) -> List[int]:
        """
        Returns agent indices of your team. This is the list of the numbers
        of the agents (e.g., red might be the list of 1,3,5)
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return game_state.getRedTeamIndices()
        else:
            return game_state.getBlueTeamIndices()

    def get_score(self, game_state: GameState, index: Optional[int]=None) -> float:
        """
        Returns how much you are beating the other team by in the form of a number
        that is the difference between your score and the opponents score. If this number
        is negative if you're losing.
        """
        if index is None:
            isRed = self.isRed
        else:
            isRed = game_state.isOnRedTeam(index)

        if isRed:
            return +game_state.getScore()
        else:
            return -game_state.getScore()

    def get_maze_distance(
            self,
            pos1: Tuple[Union[float, int], Union[float, int]],
            pos2: Tuple[Union[float, int], Union[float, int]]
    ) -> Union[float, int]:
        """
        Returns the distance between two points; These are calculated using the provided
        distancer object.

        If distancer.getMazeDistances() has been called, then maze distances are available.
        Otherwise, this just returns Manhattan distance.
        """
        d = self.distancer.getDistance(pos1, pos2)
        return d

    @staticmethod
    def discretize(n):
        if n is None:
            return None
        n = int(n)
        if 0 <= n <= 1:
            return 0
        if 2 <= n <= 5:
            return 1
        return 2

    def extract_features(self, game_state: GameState, index: Optional[int]=None) -> Dict[str, Hashable]:
        if index is None:
            index = self.index

        directions = [Direction.EAST, Direction.NORTH, Direction.SOUTH, Direction.WEST]
        successors = {direction: self.get_successor(game_state, direction, index) for direction in directions
                      if direction in game_state.getLegalActions(index)}

        features = {}

        if 'best_way_to_invader' in self.default_features:
            features['best_way_to_invader'] = self.best_way_to(successors, getattr(self, 'get_invaders'))
        if 'best_way_to_defender' in self.default_features:
            features['best_way_to_defender'] = self.best_way_to(successors, getattr(self, 'get_defenders'))
        if 'best_way_to_scared_defender' in self.default_features:
            features['best_way_to_scared_defender'] = self.best_way_to(successors, getattr(self, 'get_scared_defenders'))
        if 'best_way_to_non_scared_defender' in self.default_features:
            features['best_way_to_non_scared_defender'] = self.best_way_to(successors, getattr(self, 'get_non_scared_defenders'))
        if 'best_way_to_capsule' in self.default_features:
            features['best_way_to_capsule'] = self.best_way_to(successors, getattr(self, 'get_capsules'))
        if 'best_way_to_food' in self.default_features:
            features['best_way_to_food'] = self.best_way_to(successors, getattr(self, 'get_food_for_your_team_to_eat'))

        if 'distance_to_invader' in self.default_features:
            features['distance_to_invader'] = self.distance_to_closest(successors, getattr(self, 'get_invaders'))
            features['distance_to_invader'] = self.discretize(features['distance_to_invader'])
        if 'distance_to_defender' in self.default_features:
            features['distance_to_defender'] = self.distance_to_closest(successors, getattr(self, 'get_defenders'))
            features['distance_to_defender'] = self.discretize(features['distance_to_defender'])
        if 'distance_to_scared_defender' in self.default_features:
            features['distance_to_scared_defender'] = self.distance_to_closest(successors, getattr(self, 'get_scared_defenders'))
            features['distance_to_scared_defender'] = self.discretize(features['distance_to_scared_defender'])
        if 'distance_to_non_scared_defender' in self.default_features:
            features['distance_to_non_scared_defender'] = self.distance_to_closest(successors, getattr(self, 'get_non_scared_defenders'))
            features['distance_to_non_scared_defender'] = self.discretize(features['distance_to_non_scared_defender'])
        if 'distance_to_capsule' in self.default_features:
            features['distance_to_capsule'] = self.distance_to_closest(successors, getattr(self, 'get_capsules'))
            features['distance_to_capsule'] = self.discretize(features['distance_to_capsule'])
        if 'distance_to_food' in self.default_features:
            features['distance_to_food'] = self.distance_to_closest(successors, getattr(self, 'get_food_for_your_team_to_eat'))
            features['distance_to_food'] = self.discretize(features['distance_to_food'])

        if 'best_way_to_home' in self.default_features:
            features['best_way_to_home'] = self.best_way_to(successors, getattr(self, 'get_home'))
        if 'am_I_attacking' in self.default_features:
            features['am_I_attacking'] = game_state.getAgentState(index).isPacman
        if 'am_I_scared' in self.default_features:
            features['am_I_scared'] = game_state.getAgentState(index).scaredTimer > 0
        if 'is_teammate_attacking' in self.default_features:
            team = self.get_team(game_state)
            teammate = next(iter(set(team) - {index}))
            features['is_teammate_attacking'] = game_state.getAgentState(teammate).isPacman
        if 'am_I_carrying_enough_food_to_win' in self.default_features:
            features['am_I_carrying_enough_food_to_win'] = len(self.get_food_for_your_team_to_eat(game_state, index)) <= 2
        if 'closest_entity' in self.default_features:
            entities = {
                'invader': self.distance_to_closest(successors, getattr(self, 'get_invaders')),
                'cared_defender': self.distance_to_closest(successors, getattr(self, 'get_scared_defenders')),
                'non_scared_defender': self.distance_to_closest(successors, getattr(self, 'get_non_scared_defenders')),
                'food': self.distance_to_closest(successors, getattr(self, 'get_food_for_your_team_to_eat')),
                'capsule': self.distance_to_closest(successors, getattr(self, 'get_capsules')),
            }
            entities = {k: v for k, v in entities.items() if v is not None}
            features['closest_entity'] = arg_min(entities)

        return features
