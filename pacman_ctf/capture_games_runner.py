# -*- coding: utf-8 -*-
"""
Runs multiple episodes of Pacman with a learning agent.

@author: Marco Tamassia
"""
import sys
import time
import timeit
import traceback

from typing import List, Tuple

from pacman_ctf.displays import graphics_display
from pacman_ctf.displays import text_display
from pacman_ctf.game import Agent
from pacman_ctf.layout import Layout
from pacman_ctf.capture import CaptureRules

from collections import namedtuple
from contextlib import redirect_stdout
import os

EpisodeData = namedtuple('EpisodeData', [
    'episode',
    'outcome',
    'score',
    'movesCount',
    'executionTime',
    'defendersEatenByRedTeam',
    'invadersEatenByRedTeam',
    'foodEatenByRedTeam',
    'capsulesEatenByRedTeam',
    'defendersEatenByBlueTeam',
    'invadersEatenByBlueTeam',
    'foodEatenByBlueTeam',
    'capsulesEatenByBlueTeam',
])


class GamesRunner:

    def __init__(
        self,
        blue_agents: List[Agent],
        red_agents: List[Agent],
        layout: Layout,
        episodes: int,
        test_greedily: bool=True,
        quiet: bool=False,
        graphics: str='null',
        graphics_after: int=0,
        frame_time: float=0.05,
        save_moves_histories: bool=True,
        show_countdown: bool=False
    ):
        self.episodes = episodes
        self.layout = layout
        self.initialized = False
        self.graphics = graphics
        self.graphicsAfter = graphics_after
        self.blue_agents = blue_agents
        self.red_agents = red_agents
        self.agents = [red_agents[0], blue_agents[0], red_agents[1], blue_agents[1]]
        self.frameTime = frame_time
        self.saveMovesHistories = save_moves_histories
        self.testGreedily = test_greedily
        self.quiet = quiet
        self.currentEpisode = 0
        self.display = None
        self.showCountdown = show_countdown

    def signalAgentsToLearn(self):
        for agent in self.agents:
            if hasattr(agent, 'explore') and callable(agent.explore):
                agent.explore()
            if hasattr(agent, 'doLearn') and callable(agent.doLearn):
                agent.doLearn()

    def signalAgentsToBeGreedy(self):
        for agent in self.agents:
            if hasattr(agent, 'exploit') and callable(agent.exploit):
                agent.exploit()
            if hasattr(agent, 'dontLearn') and callable(agent.dontLearn):
                agent.dontLearn()

    def signalAgentsEpisodeIsOver(self):
        for agent in self.agents:
            if hasattr(agent, 'episodeIsOver') and callable(agent.episodeIsOver):
                agent.episodeIsOver()

    def test(self):
        epsData = []
        records = []

        self.signalAgentsToLearn()
        for e in range(self.episodes):

            self.currentEpisode = e

            if self.testGreedily:
                self.signalAgentsToLearn()
                self.runEpisode()
                self.signalAgentsToBeGreedy()

            epData, layout, moveHistory = self.runEpisode()
            self.signalAgentsEpisodeIsOver()

            epsData.append(epData)
            if self.saveMovesHistories:  # This 'if' saves memory
                records.append((layout, moveHistory))

        for agent in self.agents:
            # noinspection PyBroadException
            try:
                print()
                print()
                print(type(agent))
                print(agent)
            except:
                import traceback
                traceback.print_exc()
        return epsData, records

    def runEpisode(
        self
    ) -> Tuple[EpisodeData, Layout, List[Tuple[int, int]]]:
        self.initialized = True
        rules = CaptureRules()

        if self.graphics == 'null' or self.currentEpisode < self.graphicsAfter:
            self.display = text_display.NullGraphics()
        elif self.graphics == 'full':
            self.display = graphics_display.PacmanGraphics(1, frameTime=self.frameTime)
        elif self.graphics == 'text':
            text_display.SLEEP_TIME = self.frameTime
            self.display = text_display.PacmanGraphics()


        retryCount = 3
        total_time = None
        game = None
        with open(os.devnull, 'w') as f:#, redirect_stdout(f):
            while retryCount > 0:
                game = rules.newGame(self.layout, self.agents, self.display, length=1000, muteAgents=False, catchExceptions=False)

                # noinspection PyBroadException
                try:
                    if self.graphics != 'null' and self.currentEpisode >= self.graphicsAfter and self.showCountdown:
                        for i in range(3, 0, -1):
                            print("Next game in %d..." % i, end='\r')
                            time.sleep(1)
                        print("Start!" + " " * 20, end='\r')
                    start = timeit.default_timer()
                    game.run()
                    stop = timeit.default_timer()
                    total_time = stop - start
                    game.state.getAndResetExplored()
                    break
                except KeyboardInterrupt:
                    sys.exit(0)
                except:
                    traceback.print_exc()
                    retryCount -= 1
                    if retryCount > 0:
                        sys.stdout.write("Game failed. Trying again.\n")
                    else:
                        sys.stdout.write("Game failed too many times. Quitting.\n")
                

        data = EpisodeData(
            episode=self.currentEpisode,
            outcome=rules.outcome(game.state),
            score=game.state.getScore(),
            movesCount=game.numMoves,
            executionTime=total_time,
            defendersEatenByRedTeam=game.state.data.eatenDefenders[0],
            invadersEatenByRedTeam=game.state.data.eatenInvaders[0],
            foodEatenByRedTeam=game.state.data.eatenFood[0],
            capsulesEatenByRedTeam=game.state.data.eatenCapsules[0],
            defendersEatenByBlueTeam=game.state.data.eatenDefenders[1],
            invadersEatenByBlueTeam=game.state.data.eatenInvaders[1],
            foodEatenByBlueTeam=game.state.data.eatenFood[1],
            capsulesEatenByBlueTeam=game.state.data.eatenCapsules[1],
        )

        if not self.quiet:
            print("Episode #{episode}, {outcome}, score: {score}, moves: {movesCount}".format(**data._asdict()))

        if retryCount == 0:
            sys.exit(1)
        else:
            return data, game.state.data.layout, game.moveHistory

