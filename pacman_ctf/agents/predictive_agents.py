from pacman_ctf.agents.common import LearningAgent
from typing import List, Iterable
from reinf.exploration_strategies import ExplorationParams
from reinf.options.option import Option
from pacman_ctf.layout import Layout
from reinf.q_learning import QLearningAgent as QL
from util.delegation import delegate_attrs


@delegate_attrs('qAgent', [
    'get_exploration_strategy',
    'set_exploration_strategy',
    'explore',
    'exploit',
    'is_learning',
    'do_learn',
    'do_not_learn',
    'get_states_count',
    'get_state_action_pairs_count',
    'end_of_episode',
    'get_states_count',
    'get_states_count',
])
class QLearningAgent(LearningAgent):

    def __init__ (
            self,
            index: int,
            features: List[str],
            exploration_params: ExplorationParams,
            alpha: float,
            gamma: float,
            init_q: float,
            operator: str,
            operator_alpha: float,
            options: Iterable[Option],
            early_termination: bool,
            disable_primitive_actions: bool,
            q_table_features: List[str],
            layout: Layout
    ):
        super().__init__(index, features, layout)
        self.qAgent = QL(
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=options,
            early_termination=early_termination,
            disable_primitive_actions=disable_primitive_actions,
            q_table_features=q_table_features
        )

    def __str__ (self):
        return str(self.qAgent)

    def learning_fn (self, *args, **kwargs):
        return self.qAgent.learn(*args, **kwargs)

    def decision_fn (self, *args, **kwargs):
        return self.qAgent.get_action(*args, **kwargs)

    def get_mdp (self):
        return self.qAgent.mdp

    def end_of_episode (self, *args, **kwargs):
        self.qAgent.end_of_episode()
