# pacmanAgents.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from pacman_ctf.pacman import Direction
from pacman_ctf.game import Agent
import random
from util.util import lookup


class LeftTurnAgent(Agent):
    """An agent that turns left at every opportunity"""

    def get_action(self, state):
        legal = state.getLegalPacmanActions()
        current = state.getPacmanState().configuration.direction
        if current == Direction.STOP:
            current = Direction.NORTH
        left = Direction.left[current]
        if left in legal:
            return left
        if current in legal:
            return current
        if Direction.right[current] in legal:
            return Direction.right[current]
        if Direction.left[left] in legal:
            return Direction.left[left]
        return Direction.STOP


class GreedyAgent(Agent):

    def __init__(self, index, evalFn="scoreEvaluation"):
        super().__init__(index)
        self.evaluationFunction = lookup(evalFn, globals())
        assert self.evaluationFunction is not None

    def get_action(self, state):
        # Generate candidate actions
        legal = state.getLegalPacmanActions()
        if Direction.STOP in legal:
            legal.remove(Direction.STOP)

        successors = [(state.generateSuccessor(0, action), action) for action in legal]
        scored = [(self.evaluationFunction(state), action) for state, action in successors]
        bestScore = max(scored)[0]
        bestActions = [pair[1] for pair in scored if pair[0] == bestScore]
        return random.choice(bestActions)


def scoreEvaluation(state):
    return state.getScore()
