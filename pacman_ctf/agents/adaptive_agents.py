"""
Classes implementing some Challenge-Sensitive Action Selection
"""

from typing import List, Optional, Union, Tuple, Iterable
from abc import abstractmethod

from reinf.exploration_strategies import ExplorationParams
from util.typing2 import State, Action
from pacman_ctf.agents.common import LearningAgent
from pacman_ctf.layout import Layout
from pacman_ctf.capture import GameState
from reinf.adaptive import ChallengeSensitiveActionSelectionAgent as CSAS
from reinf.adaptive import QValueSensitiveActionSelectionAgent as QVSAS
from reinf.adaptive import StakesSensitiveActionSelectionAgent as SSAS


class AdaptiveAgent(LearningAgent):

    def __init__(self,
            index,
            features: List[str],
            layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = None # type: Union[CSAS, QVSAS, SSAS]

    def learning_fn(
            self,
            state: State,
            legal_actions: List[Action],
            action: Action,
            reward: float,
            new_state: Optional[State],
            new_legal_actions: List[Action]
    ):
        self.intelligence.learn(
            state=state,
            legal_actions=legal_actions,
            action=action,
            reward=reward,
            new_state=new_state,
            new_legal_actions=new_legal_actions
        )

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions
        )


    def end_of_episode (self, *args, **kwargs):
        self.intelligence.end_of_episode()


    def get_score (self, gameState: GameState) -> float:
        return self.features_extractor.get_score(gameState, self.index)

    def get_expected_outcome (self, gameState: GameState) -> float:
        num_rem_food = len(self.features_extractor.get_food_for_your_team_to_eat(gameState))
        num_tot_food = len(self.features_extractor.get_layout_food_for_your_team_to_eat(gameState))
        num_carr_food = len(self.features_extractor.get_food_your_team_is_carrying(gameState))
        num_eaten_food = num_tot_food - num_rem_food

        num_rem_food_opp = len(self.features_extractor.get_food_for_opponent_team_to_eat(gameState))
        num_tot_food_opp = len(self.features_extractor.get_layout_food_for_opponent_team_to_eat(gameState))
        num_carr_food_opp = len(self.features_extractor.get_food_opponent_team_is_carrying(gameState))
        num_eaten_food_opp = num_tot_food_opp - num_rem_food_opp

        progress = (num_eaten_food + num_carr_food / 2) / num_tot_food
        progress_opp = (num_eaten_food_opp + num_carr_food_opp / 2) / num_tot_food_opp
        return progress - progress_opp




class ChallengeSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,

        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = CSAS(
            index,
            evaluation_cycle=evaluation_cycle,
            threshold=threshold,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )
        self.last_heuristic = None


    def observation_fn (
        self,
        gameState: GameState
    ) -> Tuple[State, float, Iterable[Action]]:
        """
        Function that maps the game state in some features.
        :param gameState: state of the game
        """
        self.last_heuristic = self.get_expected_outcome(gameState)
        state = self.features_extractor.extract_features(gameState)
        score = self.get_score(gameState)
        legalActions = sorted(gameState.getLegalActions(self.index))
        return state, score, legalActions

    def decision_fn(
            self,
            state: State,
            legal_actions: List[Action],
    ):
        return self.intelligence.get_action(
            state=state,
            legal_actions=legal_actions,
            expected_outcome=self.last_heuristic
        )



class QValueSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,

        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,

        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = QVSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass



class StakesSensitiveActionSelectionAgent(AdaptiveAgent):
    """
    A Challenge Sensitive Action Selection agent using Tabular Q-learning as a predictor.
    """
    def __init__(
        self,
        index,
        opponent_index: int,
        evaluation_cycle: int,
        threshold: float,
        features: List[str],
        exploration_params: ExplorationParams,
        alpha: float,
        gamma: float,
        init_q: float,
        operator: str,
        operator_alpha: float,
        layout: Layout
    ):
        super().__init__(index, features, layout)
        self.intelligence = SSAS(
            index,
            features=features,
            exploration_params=exploration_params,
            alpha=alpha,
            gamma=gamma,
            init_q=init_q,
            operator=operator,
            operator_alpha=operator_alpha,
            options=[],
            early_termination=False,
            layout=layout
        )

    @abstractmethod
    def get_score (self, gameState: GameState) -> float:
        pass
