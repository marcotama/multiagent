#!/usr/bin/env bash

mkdir -p "${1}Results/"
python3 -O analyze_logs.py \
  -i "${1}Tests/" \
  -o "${1}Results/" \
  --limit-x "$2" \
  --min-episode 2700 \
  --max-episode 3000 \
  -ws "$3" \
  -pt PNG,PDF \
  -ot plots csv \
  -s \
  --legend-args "{'loc': 'upper left', 'fontsize': 'small', 'ncol': 2}" \
  -dg \
    "CSAS ghost" \
    "CSAS with options ghost" \
    "Directional ghost" \
    "Q-learning ghost" \
    "QVSAS ghost" \
    "Random ghost" \
  -dn \
    "CSAS ghost" \
    "CSAS with options ghost" \
    "Directional ghost" \
    "Q-learning ghost" \
    "QVSAS ghost" \
    "Random ghost"
