from fighting_ice.trainer import Trainer
import argparse
import ast
from util.util2 import pickle_load, pickle_save
import os


def fill_defaults (settings):
    import json
    with open('defaults.json', 'r') as f:
        defaults = json.load(f)

    # common defaults
    for parameter_name, default_value in defaults.items():
        if parameter_name not in settings:
            settings[parameter_name] = default_value

    # game-specific defaults
    if 'features' not in settings:
        settings['features'] = [
            "players_x_diff()",
            "players_y_diff()",
            # "players_x_distance()",
            # "players_y_distance()",
            "closest_projectile_x_distance()",
            "attack_x_distance()",
            # "player_is_falling()",
            # "opponent_is_falling()",
            "opponent.state",
            "opponent_is_approaching()",
            # "self.energy",
            # "opponent_is_busy()"
        ]
    if 'q_table_features' not in settings:
        settings['q_table_features'] = [
            "players_x_diff()",
            "players_y_diff()",
            # "players_x_distance()",
            # "players_y_distance()",
            "closest_projectile_x_distance()",
            "attack_x_distance()",
            # "player_is_falling()",
            # "opponent_is_falling()",
            "opponent.state",
            "opponent_is_approaching()",
            # "self.energy",
            # "opponent_is_busy()"
        ]
    return settings


def load_agent(pacman, no_graphics):
    import pkgutil
    from importlib import import_module

    agentsModuleNames = [name for _, name, _ in pkgutil.iter_modules(['fighting_ice/agents'])]
    for agentsModuleName in agentsModuleNames:
        agentsModule = import_module("fighting_ice.agents." + agentsModuleName)
        if pacman in dir(agentsModule):
            return getattr(agentsModule, pacman)
    raise Exception('The agent %s is not specified in any module.' % pacman)


def instantiate_agent(settings):
    import inspect
    from reinf.exploration_strategies import ExplorationParams

    if 'add_options_from_file' not in settings or settings['add_options_from_file'] == '':
        settings['options'], stateTranslator, actionTranslator = [], None, None
    else:
        settings['options'], stateTranslator, actionTranslator = pickle_load(settings['add_options_from_file'])

    # noinspection PyProtectedMember
    explSettings = {key: settings[key] if key in settings else None for key in ExplorationParams._fields}
    exploration_params = ExplorationParams(**explSettings)
    if 'load_agent' in settings and settings['load_agent']:
        agent = pickle_load(settings['load_agent'])
        if not settings['do_not_override_exploration_strategy']:
            agent.set_exploration_strategy(exploration_params)
    else:
        ag_cls = load_agent(settings['controller'], True)
        params = inspect.signature(ag_cls.__init__).parameters

        # if the desired class wants exploration_params, mdpParams or mdpOptionsParams, create them
        additionalParams = {
            'exploration_params': ExplorationParams,
        }
        for key, params_cls in additionalParams.items():
            if key in params:
                try:
                    settings[key] = params_cls(**{key: settings[key] for key in params_cls._fields})
                except KeyError as e:
                    raise ValueError("Missing parameter: %s" % e.args)

        agent = ag_cls(**{key: settings[key] for key in params if key != 'self'})

        if hasattr(agent, 'qAgent'):
            if stateTranslator is not None:
                agent.qAgent.stateTranslator = stateTranslator
            if actionTranslator is not None:
                agent.qAgent.actionTranslator = actionTranslator

    return agent


def parse_arguments ():

    parser = argparse.ArgumentParser(
        description='Train an agent using JSON fight logs.')
    parser.add_argument(
        '--controller',
        dest='controller',
        type=str,
        help='Parameters for the agent controller. The value should be a string '
             'representing a Python dictionary. A tentative list of values is:\n'
             '- name: an identifier for the agent\n'
             '- load_agent: file from which to load a previously saved agent\n'
             '- do_not_override_exploration_strategy: whether to override the loaded exploration strategy when using load_agent\n'
             '- controller: the controller for the agent (one of Machete, QLearningAgent)\n'
             '- features: features to use for Q-learning agents (as a list)'
             '- for Q-learning:\n'
             '  - alpha: learning parameter for Q-learning\n'
             '  - gamma: discount factor for the MDPs'
             '  - init_q: initial value for Q-table entries\n'
             '  - operator: operator to be used for Q-learning updates; can be bellman, consistent-bellman, '
             'advantage-learning, persistent-advantage-learning or lazy (see Bellemare et al., 2015)\n'
             '  - operator_alpha: alpha parameter for non-standard operators\n'
             '- for MDPs w/ options:\n'
             '  - add_options_from_file: options to be loaded (a file location)\n'
             '  - early_termination: whether or not options should be terminated early if chosen action is not the best one\n'
             '- for exploration strategies:\n'
             '  - explStrategy: exploration strategy to be used (one of EpsilonGreedyPolicy, EpsilonCIGreedy, UCT, '
             'AnnealingEpsilonGreedy, AnnealingEpsilonCIGreedy, Softmax, GreedyPolicy, Random)\n'
             '  - epsilon: exploration probability for epsilon-greedy strategy\n'
             '  - epsilon_ci: CI exploration probability for epsilon-CI-greedy strategy\n'
             '  - cp: UCT exploration strategy coefficient\n'
             '  - init_temperature: initial temperature for Softmax strategy\n'
             '  - annealing_variable: variable to consider for annealing schedules; can be visits or episodes\n'
             '  - start_descent: qualitatively, where to start the descent of the sigmoid schedule for strategies with'
             'annealing (i.e. AnnealingEpsilonGreedy, AnnealingCIEpsilonGreedy and Softmax)\n'
             '  - end_descent: qualitatively, where to end the descent of the sigmoid schedule\n'
             '  - significance: significance value for t-tests (for multi-abstraction Q-Learning\n'
             '- depth: depth for tree search (for relevant agents, such as minimax etc.)')
    parser.add_argument(
        '--save-agent',
        type=str,
        required=True,
        help='file where to save the learning agent')
    parser.add_argument(
        '--limit',
        type=int,
        default=False,
        help='Maximum number of log files to process. This can be useful when debugging.')
    parser.add_argument(
        '--log-paths',
        type=str,
        nargs='+',
        default=None,
        help='Files containing the logs of fights to be used for training. If a directory is passed, all files'
             'within the directory matching *.json will be used.')
    args = parser.parse_args()
    return args


def doStuff ():
    args = parse_arguments()


    settings = ast.literal_eval(args.controller)
    settings = fill_defaults(settings)
    controller = instantiate_agent(settings)

    trainer = Trainer(controller)

    json_files = []
    for logs_path in sorted(args.log_paths):
        if os.path.isdir(logs_path):
            json_files += [os.path.join(logs_path, file_path) for file_path in os.listdir(logs_path) if file_path.endswith('.json')]
        if os.path.isfile(logs_path):
            json_files.append(logs_path)


    if args.limit:
        args.limit = min(args.limit, len(json_files)) # type: int
        from random import shuffle
        shuffle(json_files)
        for i, json_file in enumerate(json_files[:args.limit]):
            print("Processing logs file (%d/%d): %s" % (i+1, args.limit, json_file), end='\r')
            trainer.train_from_json(json_file)
    else:
        for i, json_file in enumerate(sorted(json_files)):
            print("Processing logs file (%d/%d): %s" % (i+1, len(json_files), json_file), end='\r')
            trainer.train_from_json(json_file)

    pickle_save(args.save_agent, trainer.get_agent())


if __name__ == '__main__':
        doStuff()
