#!/usr/bin/env bash

# This script manages the execution of one or more configurations of a script, repeatedly, in a parallel
# fashion. Multiple executions are sent to remote workers to which is possible a non-interactive SSH access.

# Terminology:
#  - experiments class: the set of all the experiments, past and future, that test the same script; when you run this
#      script, you are adding one experiment to its class
#  - experiment: execution of a script, possibly with multiple configurations, possibly repeated multiple times for
#      statistical purposes
#  - configuration: all the parameters used to launch an algorithm; the script can be launched multiple times with
#      different random seeds to obtain different samples from the population of results (seed passed as a parameter)
#  - repetition: a single run of an algorithm with a specific configuration and a specific random seed

EXPERIMENT_CLASS='csas'

if [[ $# -ne 1 ]]
then
	TIMESTAMP=`date +%Y-%m-%d`
	EXPERIMENT_DIR="experiments/exp-${TIMESTAMP}__${EXPERIMENT_CLASS}"
else
	EXPERIMENT_DIR=$1
fi

[ -e "${EXPERIMENT_DIR}" ] || mkdir -p ${EXPERIMENT_DIR}
echo "Experiment folder: $EXPERIMENT_DIR"

EXPERIMENT_ID=`date +%Y-%m-%d__%H.%M.%S`


# ------ parameters ------
cat > configurations.json <<END_OF_JSON
{
    "repetitions": 100,
    "setup": {
        "layout": "smallClassic",
        "episodes": 5000,
        "pacman": "{'controller': 'AlphaBetaPacman', 'depth': 2}"
    },
    "configurations": [
        {
            "_set_name": "Random ghost",
            "controller": ["RandomGhost"]
        },
        {
            "_set_name": "Directional ghost",
            "controller": ["DirectionalGhost"]
        },
        {
            "_set_name": "Q-learning ghost",
            "controller": ["QLearningGhost"],
            "features": [["PmDr","PmDs:","Fd#:",".CpDs:",".CpDr","~Tm"]],
            "alpha": [0.1],
            "gamma": [0.97],
            "exploration_strategy": ["AnnealingEpsilonGreedy"],
            "epsilon": [0.1],
            "annealing_variable": ["episodes"],
            "start_descent": [500],
            "end_descent": [2000]
        },
        {
            "_set_name": "CSAS ghost",
            "controller": ["ChallengeSensitiveActionSelectionGhost"],
            "features": [["PmDr","PmDs:","Fd#:",".CpDs:",".CpDr","~Tm"]],
            "alpha": [0.1],
            "gamma": [0.97],
            "exploration_strategy": ["AnnealingEpsilonGreedy"],
            "epsilon": [0.1],
            "annealing_variable": ["episodes"],
            "start_descent": [500],
            "end_descent": [2000],
            "evaluation_cycle": [10],
            "threshold": [0.1],
            "opponent_index": [0]
        },
        {
            "_set_name": "CSAS with options ghost",
            "controller": ["ChallengeSensitiveActionSelectionWithOptionsGhost"],
            "features": [["PmDr","PmDs:","Fd#:",".CpDs:",".CpDr","~Tm"]],
            "alpha": [0.1],
            "gamma": [0.97],
            "exploration_strategy": ["AnnealingEpsilonGreedy"],
            "epsilon": [0.1],
            "annealing_variable": ["episodes"],
            "start_descent": [500],
            "end_descent": [2000],
            "evaluation_cycle": [10],
            "threshold": [0.1],
            "opponent_index": [0],
            "add_options_from_file": ["ghost.options"]
        },
        {
            "_set_name": "QVSAS ghost",
            "controller": ["QValueSensitiveActionSelectionGhost"],
            "features": [["PmDr","PmDs:","Fd#:",".CpDs:",".CpDr","~Tm"]],
            "alpha": [0.1],
            "gamma": [0.97],
            "exploration_strategy": ["AnnealingEpsilonGreedy"],
            "epsilon": [0.1],
            "annealing_variable": ["episodes"],
            "start_descent": [500],
            "end_descent": [2000],
            "opponent_index": [0]
        }
    ]
}
END_OF_JSON
# ------------------------
# "features": [[".FdDr",".FdDs",".NScGhDr",".NScGhDs","..NScGhDr","..NScGhDs",".CpDr",".CpDs",".ScGhDr",".ScGhDs",".ScGhTm"]]
python gen_runs_parameters.py configurations.json

WORKERS_PARALLEL="4/ubuntu@118.138.244.81,4/ubuntu@118.138.244.80,4/ubuntu@118.138.244.78,4/ubuntu@118.138.244.73,4/ubuntu@118.138.244.79,4/ubuntu@118.138.244.77,4/ubuntu@118.138.244.75,4/ubuntu@118.138.244.72"






# Setup dirs
REMOTE_WORKING_DIR="/tmp/${EXPERIMENT_CLASS}_${EXPERIMENT_ID}"
TESTS_FOLDER="${EXPERIMENT_DIR}/Tests"
TESTS_DIRS=`cat configurations.txt`
mkdir -p "${TESTS_FOLDER}/"


echo -e "
import pickle
from pacman.options.ghost_options import GoToPacman, GoToClosestCapsule, AvoidPacman, AvoidClosestCapsule
from util.translator import Translator
termProb = 0.2
stateTranslator = Translator()
actionTranslator = Translator()
with open('ghost.options','wb') as f:
    options = [
        GoToPacman(stateTranslator, actionTranslator, termProb),
        GoToClosestCapsule(stateTranslator, actionTranslator, termProb),
        AvoidPacman(stateTranslator, actionTranslator, termProb),
        AvoidClosestCapsule(stateTranslator, actionTranslator, termProb)
    ]
    pickle.dump((options, stateTranslator, actionTranslator),f)
" | python3 \
|| exit 1


# Backup code
LOCAL_TAR_PATH="${TESTS_FOLDER}/code.tar.gz"
REMOTE_TAR_PATH="${REMOTE_WORKING_DIR}/code.tar.gz"
tar -zcf "${LOCAL_TAR_PATH}" \
    ghost.options \
    *.py \
    *.sh \
    pacman/ \
    pacman_ctf/ \
    reinf/ \
    util/ \

# Config parallelization
WORKERS=`echo ${WORKERS_PARALLEL} | sed 's/[0-9]*\///g'`

# Remove remote working directory, if already existing
parallel --nonall -S ${WORKERS} rm -rf ${REMOTE_WORKING_DIR} ${REMOTE_TAR_PATH}
# Create remote working directory
parallel --nonall -S ${WORKERS} mkdir -p ${REMOTE_WORKING_DIR}
# Copy code remotely
parallel scp "${LOCAL_TAR_PATH}" {}:"${REMOTE_WORKING_DIR}" ::: `echo ${WORKERS} | sed 's/,/ /g'`
# Extract code on the remote worker
parallel --nonall -S ${WORKERS} tar --warning=no-timestamp -xzf ${REMOTE_TAR_PATH} -C ${REMOTE_WORKING_DIR}
# Generate a list of configuration already processed, based on existing .log files
while read CONFIG_NAME; do
    mkdir -p "${TESTS_FOLDER}/${CONFIG_NAME}"
    (cd "${TESTS_FOLDER}/${CONFIG_NAME}" && ls *.log) > "${TESTS_FOLDER}/${CONFIG_NAME}/skip.list" 2> /dev/null
done < configurations.txt


echo "Running tests"

# Do stuff
parallel \
  --sshlogin ${WORKERS_PARALLEL} \
  --workdir ${REMOTE_WORKING_DIR} \
  --return ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.log \
  --return ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.agents \
  --return ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.out \
  --transferfile ${TESTS_FOLDER}/{_config_id}/skip.list \
  --xapply \
  --header : \
  --colsep '\t' \
  --ungroup \
  cat ${TESTS_FOLDER}/{_config_id}/skip.list '|' grep -Fq exp{_repetition}.log '||' \
  python3 -u -O run_pacman_game.py \
    --random-seed {_repetition} \
    --layout {layout} \
    --episodes {episodes} \
    --save-logs ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.log \
    --save-agents ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.agents \
    --pacman \
      {pacman} \
    --ghosts \
      {_controller} \
      {_controller} \
  '&>' ${TESTS_FOLDER}/{_config_id}/exp{_repetition}.out \
  :::: runs.tsv


echo "All done"
