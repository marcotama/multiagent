# Priority dictionary using binary heaps
# David Eppstein, UC Irvine, 8 Mar 2002
# Marco Tamassia, RMIT University, 30 Nov 2016 (adapt to modern Python)

# Implements a data structure that acts almost like a dictionary, with two modifications:
# (1) D.smallest() returns the value x minimizing D[x]. For this to work correctly,
#        all values D[x] stored in the dictionary must be comparable.
# (2) iterating "for x in D" finds and removes the items from D in sorted order.
#        Each item is not removed until the next item is requested, so D[x] will still
#        return a useful value until the next iteration of the for-loop.
# Each operation takes logarithmic amortized time.

from collections import UserDict
from typing import Iterator, TypeVar



class PriorityDictionary(UserDict):
    K = TypeVar('K')
    V = TypeVar('V')
    def __init__(
        self
    ) -> None:
        """
        Initialize PriorityDictionary by creating binary heap of pairs (value,key).
        Note that changing or removing a dict entry will not remove the old pair from the heap
        until it is found by smallest() or until the heap is rebuilt.
        """
        self._heap = []
        super().__init__(self)

    def smallest(
        self
    ) -> V:
        """
        Find smallest item after removing deleted items from front of heap.
        """
        if len(self) == 0:
            raise IndexError("smallest of empty PriorityDictionary")
        heap = self._heap
        while heap[0][1] not in self or self[heap[0][1]] != heap[0][0]:
            last_item = heap.pop()
            insertion_point = 0
            while 1:
                small_child = 2 * insertion_point + 1
                if small_child + 1 < len(heap) and heap[small_child] > heap[small_child + 1]:
                    small_child += 1
                if small_child >= len(heap) or last_item <= heap[small_child]:
                    heap[insertion_point] = last_item
                    break
                heap[insertion_point] = heap[small_child]
                insertion_point = small_child
        return heap[0][1]

    def __iter__(
        self
    ) -> Iterator[V]:
        """
        Create destructive sorted iterator of PriorityDictionary.
        """
        while len(self) > 0:
            x = self.smallest()
            yield x
            del self[x]

    def __setitem__(
        self,
        key: K,
        val: V
    ) -> None:
        """
        Change value stored in dictionary and add corresponding pair to heap.
        Rebuilds the heap if the number of deleted items gets large, to avoid memory leakage.
        """
        UserDict.__setitem__(self, key, val)
        heap = self._heap
        if len(heap) > 2 * len(self):
            self._heap = [(v, k) for k, v in self.items()]
            self._heap.sort()
                             # builtin sort probably faster than O(n)-time
                             # heapify
        else:
            new_pair = (val, key)
            insertion_point = len(heap)
            heap.append((None,None))
            while insertion_point > 0 and new_pair < heap[(insertion_point - 1) // 2]:
                heap[insertion_point] = heap[(insertion_point - 1) // 2]
                insertion_point = (insertion_point - 1) // 2
            heap[insertion_point] = new_pair

    def setdefault(
        self,
        k: K,
        v: V=None
    ) -> V:
        """
        Re-implement setdefault to pass through our customized __setitem__.
        :param k: key
        :param v: default value
        """
        if k not in self:
            self[k] = v
        return self[k]
