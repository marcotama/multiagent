# -*- coding: utf-8 -*-
"""
Objects of this class provide an interface between PathFinder instances and
coordinates based mazes.

@author: Marco Tamassia
"""
from util.path_finder import PathFinder

from typing import List, Tuple, Iterable, Optional


class Maze:
    def __init__(self, blocks: List[Tuple[int,int]], walls: List[List[bool]], height: int, width: int):
        self.geom = (width, height)
        nodes = self._generate_nodes_and_edges(blocks, walls)
        self.path_finder = PathFinder(nodes)
        self.jn_cache = None

    def _encode_location(self, x: int, y: int) -> int:
        return x * self.geom[1] + y

    def walkable(self, x: int, y: int) -> bool:
        loc = self._encode_location(x, y)
        return loc in self.path_finder.graph

    def node_walkable(self, loc: int):
        return loc in self.path_finder.graph

    def _decode_location(self, i: int) -> Tuple[int,int]:
        return i // self.geom[1], i % self.geom[1]

    def _encode_locations(self, locations: List[Tuple[int,int]]):
        return [self._encode_location(x, y) for x, y in locations]

    def _decode_locations(self, nodes: List[int]) -> List[Tuple[int,int]]:
        return [self._decode_location(n) for n in nodes]

    def clear_cache(self):
        self.path_finder.clear_cache()
        self.jn_cache = None

    def _generate_nodes_and_edges(self, blocks: List[Tuple[int,int]], walls: List[List[bool]]):
        nodes = {}
        for x in range(self.geom[0]):
            for y in range(self.geom[1]):
                if (x,y) in blocks:
                    continue
                c = self._encode_location(x, y)
                nodes[c] = {}
                adj = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
                for _x, _y in adj:
                    if _x < 0 or _x >= self.geom[0]:
                        continue
                    if _y < 0 or _y >= self.geom[1]:
                        continue
                    if (_x,_y) not in blocks and (min(x,_x), min(y,_y), max(x,_x), max(y,_y)) not in walls:
                        n = self._encode_location(_x, _y)
                        nodes[c][n] = 1
        return nodes

    def _shortest_path(self, xo: int, yo: int, xe: int, ye: int):
        start = self._encode_location(xo, yo)
        end = self._encode_location(xe, ye)
        return self.path_finder.shortest_path(start, end)

    def all_paths(self, xo: int, yo: int, xe: int, ye: int, blacklist: Optional[Iterable[Tuple[int,int]]]=None):
        start = self._encode_location(xo, yo)
        end = self._encode_location(xe, ye)
        if blacklist is not None:
            blacklist = [self._encode_location(x, y) for x, y in blacklist]
        return self.path_finder.all_acyclic_paths(start, end, blacklist)

    def shortest_path_includes(self, xo: int, yo: int, xe: int, ye: int, xn: int, yn: int) -> Optional[bool]:
        sp = self._shortest_path(xo, yo, xe, ye)
        if sp is None:
            return None
        node = self._encode_location(xn, yn)
        return node in sp

    def shortest_path_distance(self, xo: int, yo: int, xe: int, ye: int):
        start = self._encode_location(xo, yo)
        end = self._encode_location(xe, ye)
        return self.path_finder.shortest_path_length(start, end)

    def shortest_path_distance_force_2nd_node(self, x1: int, y1: int, x2: int, y2: int, xn: int, yn: int):
        start = self._encode_location(x1, y1)
        second_node = self._encode_location(x2, y2)
        end = self._encode_location(xn, yn)
        return self.path_finder.shortest_path_length(start, end, second_node)

    def shortest_path_direction(self, xo: int, yo: int, xe: int, ye: int):
        sp = self._shortest_path(xo, yo, xe, ye)
        if sp is None:
            return None
        else:
            return self.path_direction(sp)

    def path_direction(self, p):
        if len(p) > 1:
            x0, y0 = self._decode_location(p[0])
            x1, y1 = self._decode_location(p[1])
            return x1 - x0, y1 - y0
        else:
            return 0, 0

    def _get_junctions(self):
        """
        Computes the coordinates of all spots in the maze that give the freedom
        to move in at least 3 directions and that are walkable.
        The output is a list o
        """
        if self.jn_cache is not None:
            junctions = self.jn_cache
        else:
            nodes = self.path_finder.graph
            junctions = [k for k in nodes.keys() if len(nodes[k]) > 2]
            self.jn_cache = junctions
        return list(junctions)  # Return a copy

    def _get_adjacent_junctions(self, rx: int, ry: int) -> List[Tuple[int, List[int]]]:
        """
        Computes the coordinates of all junctions that are walkable and are adjacent to a
        reference position (i.e., it is possible to reach them without passing
        through any other junction). If the reference location is a junction,
        it is automatically removed from the returned list.
        The output is a list of nodes.
        """
        junctions = self._get_junctions()

        # Remove reference location
        rn = self._encode_location(rx, ry)
        if rn in junctions:
            junctions.remove(rn)

        # Filter out junctions that are not adjacent
        adj_jns = []
        for jn in junctions:
            refPaths = self.path_finder.all_acyclic_paths(rn, jn, blacklist=junctions)
            if len(refPaths) > 0:
                adj_jns.append((jn, refPaths))
        return adj_jns

    def _get_escape_junctions(
        self,
        rx: int,
        ry: int,
        threats_coords: List[Tuple[int, int]]
    ) -> List[Tuple[int,List[int],List[List[int]]]]:
        """
        Computes the coordinates of all adjacent junctions to a reference point that
        do not have ghosts coming from all directions.
        The output is a list in the form [(x0, y0), (x1, y1), ...].
        """
        threats = self._encode_locations(threats_coords)
        adj_jns = self._get_adjacent_junctions(rx, ry)
        esc_jns = []
        for junction_node, ref_path in adj_jns:
            jx, jy = self._decode_location(junction_node)
            ref_shortest_path = self._shortest_path(rx, ry, jx, jy)
            if ref_shortest_path is None:
                continue
            adj_nodes = self.path_finder.graph[junction_node]
            adj_nodes = [node for node in adj_nodes if self.node_walkable(node) and node not in ref_shortest_path]
            threats_path = []
            for threat_node in threats:
                threat_path = self.path_finder.shortest_path(threat_node, junction_node)
                if threats_path is None:
                    continue
                threats_path.append(threat_path)
                # Remove nodes adjacent to the junction that are on the shortest path of a threat to
                # the junction.
                adj_nodes = [adj_node for adj_node in adj_nodes if adj_node not in threat_path]
            if junction_node not in threats and len(ref_path) > 0 and len(adj_nodes) > 0:
                esc_jns.append((junction_node, ref_path, threats_path))
        return esc_jns

    def get_safe_junctions(
        self,
        rx: int,
        ry: int,
        threats_coords: List[Tuple[int, int]]
    ) -> List[Tuple[int,int]]:
        """
        Computes the coordinates of all the escape junctions for a reference point that can be
        reached before any threat can.
        """
        esc_jns = self._get_escape_junctions(rx, ry, threats_coords)
        safe_jns = []
        for junction_node, ref_path, threats_path in esc_jns:
            jx, jy = self._decode_location(junction_node)
            ref_to_junction_distance = self.shortest_path_distance(rx, ry, jx, jy)
            if threats_path:
                nearest_threat_to_junction_distance = min([len(tp) for tp in threats_path])
                if ref_to_junction_distance < nearest_threat_to_junction_distance - 1:
                    safe_jns.append(self._decode_location(junction_node))
            else:
                safe_jns.append(self._decode_location(junction_node))
        return safe_jns
