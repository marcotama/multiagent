from typing import Union, Dict, List, Any, TypeVar


K = TypeVar('K')
V = TypeVar('V')


def arg_sort(seq: Union[Dict[K, V], List[V]]) -> List[Union[K, int]]:
    if isinstance(seq, Dict):
        return sorted(seq.keys(), key=seq.__getitem__)
    elif isinstance(seq, List):
        return sorted(range(len(seq)), key=seq.__getitem__)


def arg_max(seq: Union[Dict[K, V], List[V]]) -> Union[K, int]:
    if isinstance(seq, Dict):
        return max(seq.keys(), key=seq.__getitem__)
    elif isinstance(seq, List):
        return max(range(len(seq)), key=seq.__getitem__)


def arg_min(seq: Union[Dict[K, V], List[V]]) -> Union[K, int]:
    if isinstance(seq, Dict):
        return min(seq.keys(), key=seq.__getitem__)
    elif isinstance(seq, List):
        return min(range(len(seq)), key=seq.__getitem__)
