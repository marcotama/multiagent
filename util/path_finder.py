# -*- coding: utf-8 -*-
"""
Support class with methods related to path finding. Each instance is tied to a particular configuration.

@author: Marco Tamassia
"""
from util.priodict import PriorityDictionary
from typing import Dict, Optional, Iterable, List, Tuple
import functools

class PathFinder:
    def __init__(
        self,
        graph: Dict[int, Dict[int, int]]
    ) -> None:
        self.graph = graph

    @functools.lru_cache(maxsize=None)
    def dijkstra(
        self,
        start: int,
        end: Optional[int]=None,
        forced_2nd_node: Optional[int]=None
    ) -> Tuple[Dict[int, float], Dict[int, int]]:
        """
        Find shortest paths from the start vertex to all vertices nearer than or equal to the end.
        Optionally, a node to be forcefully the second in the path can be provided. In this case, the
        path is prevented from going back to the starting node and has to take a longer path.

        Code by: David Eppstein
        :param start: start node
        :param end: end node
        :param forced_2nd_node: node that is forced to be the second in the path
        """
        graph = self.graph
        distance = {} # of final nodes (when a value is inserted, it will not change)
        predecessor = {} # for the shortest path
        est_distance = PriorityDictionary() # of non-final nodes

        if forced_2nd_node is None:
            est_distance[start] = 0
        else:
            distance[start] = 0
            if forced_2nd_node not in graph[start]:
                raise ValueError("Second node not connected to first node")
            est_distance[forced_2nd_node] = graph[start][forced_2nd_node]
            predecessor[forced_2nd_node] = start

        for v in est_distance:
            distance[v] = est_distance[v]
            if v == end:
                break

            for w in graph[v]:
                start_v_w_path_length = distance[v] + graph[v][w]
                if w in distance:
                    if start_v_w_path_length < distance[w]:
                        raise ValueError("Dijkstra: found better path to already-final vertex")
                elif w not in est_distance or start_v_w_path_length < est_distance[w]:
                    est_distance[w] = start_v_w_path_length
                    predecessor[w] = v

        return distance, predecessor


    @functools.lru_cache(maxsize=None)
    def shortest_path(
        self,
        start: int,
        end: Optional[int]=None,
        forced_2nd_node: Optional[int]=None
    ) -> Optional[List[int]]:
        """
        Computes the shortest path between to nodes of the graph (each vertex has cost 1).
        Optionally, a node to be forcefully the second in the path can be provided. In this case, the
        path is prevented from going back to the starting node and has to take a longer path.

        :param start: start node
        :param end: end node
        :param forced_2nd_node: node that is forced to be the second in the path
        """
        _, predecessor = self.dijkstra(start, end=None, forced_2nd_node=forced_2nd_node) # optimize cache
        sp = []
        while end != start:
            sp.append(end)
            if end not in predecessor:
                return None
            end = predecessor[end]
        sp.append(start)
        sp.reverse()
        return sp


    def all_acyclic_paths(
        self,
        start: int,
        end: int,
        blacklist: Optional[Iterable[int]]=None
    ) -> List[List[int]]:
        """
        Computes all the acyclic paths from start to end.
        Optionally, a set of nodes can be blacklisted; blacklisted nodes are considered unreachable and
        cannot therefore be part of any path.

        :param start: start node
        :param end: end node
        :param blacklist: node that is forced to be the second in the path
        """
        if blacklist is None:
            blacklist = []
        blacklist = [n for n in blacklist if n not in [start, end]]
        path = []
        paths = []
        queue = [(start, path)]
        while queue:
            start, path = queue.pop()
            path = path + [start]
            if start == end:
                paths.append(path)
            for node in set(self.graph[start]).difference(path):
                if node not in blacklist:
                    queue.append((node, path))
        return paths


    def shortest_path_length(
        self,
        start: int,
        end: Optional[int]=None,
        forced_2nd_node: Optional[int]=None
    ) -> float:
        """
        Computes the shortest path between to nodes of the graph (each vertex has cost 1).
        Optionally, a node to be forcefully the second in the path can be provided. In this case, the
        path is prevented from going back to the starting node and has to take a longer path.

        :param start: start node
        :param end: end node
        :param forced_2nd_node: node that is forced to be the second in the path
        """
        distance, _ = self.dijkstra(start, end=None, forced_2nd_node=forced_2nd_node) # optimize cache
        if end in distance:
            return distance[end]
        else:
            return float('inf')

    def clear_cache(self):
        self.dijkstra.cache_clear()
        self.shortest_path.cache_clear()