from abc import abstractmethod
from util.vector import Vector


class StateActionNewStateFn:
    @abstractmethod
    def __call__(self, x_t: Vector, a_t: int, x_tp1: Vector):
        pass


class SinglePeakFn(StateActionNewStateFn):
    """
    Function that returns 1 if x_t == x_target, 0 otherwise
    """
    def __init__ (self, x_target: Vector):
        self.x_target = x_target

    def __call__ (self, x_t: Vector, a_t: int, x_tp1: Vector):
        return int(x_t == self.x_target)


class InvertedSinglePeakFn(StateActionNewStateFn):
    """
    Function that returns 0 if x_t == x_target, 1 otherwise
    """
    def __init__ (self, x_target: Vector):
        self.x_target = x_target

    def __call__ (self, x_t: Vector, a_t: int, x_tp1: Vector):
        return int(x_t != self.x_target)


class ConstantFn(StateActionNewStateFn):
    """
    Function that always returns value
    """
    def __init__(self, value: float):
        self.value = value

    def __call__(self, x_t: Vector, a_t: int, x_tp1: Vector):
        return self.value
