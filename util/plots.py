import numpy as np
from collections import OrderedDict, defaultdict
from typing import List, Dict, Union, Iterable, Iterator, Optional
import brewer2mpl
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset
from util.util2 import numify, camel_case_split
from util.data_manipulation import filter_groups
import pandas as pd


def smooth (
    x: np.array,
    window_len: int=11,
    window: str='hanning'
):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.

    input:
    :param x: the input signal
    :param window_len: the dimension of the smoothing window; should be an odd integer
    :param window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.
    :return: the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    w = get_window(window, window_len)
    y = np.convolve(w, x[window_len:])
    z = y[:-window_len + 1]
    return z


def get_window (window, window_len):
    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')
    return w / w.sum()


def smooth_curves (
    curves: Dict[str, List[float]],
    window_size: int
) -> Dict[str, np.ndarray]:
    """
    Takes a dict of iterables of floats and smooths the curves using a moving window.
    Order of curves is preserved, if present.
    :param curves: dict-like of sized iterables of floats, the curves to be smoothed
    :param window_size: the size of the smoothing window
    :return: OrderedDict of numpy arrays, the smoothed curves
    """
    smoothed_curves = OrderedDict()
    for label, curve in curves.items():
        curve = np.asarray(curve)
        if len(curve) < window_size:
            smoothed_curves[label] = curve
        else:
            smoothed_curves[label] = smooth(curve, window_size, "hanning")
    return smoothed_curves



def get_colors (
    n: int=9,
    method: str='handcoded3'
) -> List[str]:
    """
    Produces a list of n visually distinct colors. Parameter method can
    be any of 'handcoded', 'pylab' and 'colorbrewer'. In case of 'handcoded',
    parameter n is ignored.
    :param n: number of desired colors in output
    :param method: method to use for color generation; can be any of
        'handcoded', 'pylab' and 'colorbrewer'
    :return: the list of produced colors
    """
    # Hand-coded
    if method == 'handcoded':
    # http://matplotlib.org/examples/color/named_colors.html
        colors = ['green',         'darkred',       'indigo',
                  'darkgoldenrod', 'black',         'mediumblue',
                  'darkorange',    'lightseagreen', 'deepskyblue',
                  'red',           'magenta',       'dimgrey'
        ]
    elif method == 'handcoded2':
        colors = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']
    elif method == 'handcoded3':
        colors = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

    # PyLab
    elif method == 'pylab':
        color_map = plt.get_cmap('rainbow')
        indices = np.linspace(0, color_map.N, n)
        colors = [color_map(int(i)) for i in indices]

    # Color Brewer
    elif method == 'colorbrewer':
        # brewer2mpl.get_map args: set name  set type  number of colors
        try:
            bmap = brewer2mpl.get_map('Set2', 'qualitative', n)
            colors = bmap.mpl_colors
        except ValueError:
            colors = get_colors(n, method='handcoded')

    else:
        raise ValueError('Unknown method %s for colors selection' % method)

    return colors

def calc_plots_args (
    curves: Dict[str, List[float]],
    colors: Iterator[str],
    line_style: str,
    limit_x: int
) -> Dict[List, Dict]:
    """
    Generates parameters to pass to plt.plot.
    :param curves: dict-like of lists-like of floats, the curves and labels to be plotted
    :param colors: iterable of valid matplotlib colors (i.e., strings with color names, tuples of RGBs, etc)
    :param line_style: matplotlib line style
    :param limit_x: maximum x value
    :return: OrderedDict of tuples like (args, kwargs) to be passed to plt.plot
    """

    plot_args = OrderedDict()

    if not curves:
        return plot_args

    max_x = min(len(c) for c in curves.values())
    if limit_x:
        max_x = min(limit_x, max_x)

    for label, curve in curves.items():
        curve = curve[:max_x]
        kwargs = {
            'color'    : next(colors),
            'label'    : label,
            'linewidth': 1.5,
            'linestyle': line_style
        }
        args = [np.arange(len(curve)), curve]
        plot_args[label] = (args, kwargs)
    return plot_args


def calc_fill_between_args (
    curves: Dict[str, List[float]],
    colors: Iterator[str],
    limit_x: int
) -> Dict[List, Dict]:
    """
    Generates parameters to pass to plt.stackplot.
    :param curves: dict-like of lists-like of floats, the curves and labels to be plotted
    :param colors: iterable of valid matplotlib colors (i.e., strings with color names, tuples of RGBs, etc)
    :param limit_x: maximum x value
    :return: OrderedDict of tuples like (args, kwargs) to be passed to plt.plot
    """
    alpha = 0.3

    max_x = min(len(c) for c in curves.values())
    if limit_x is not None:
        max_x = min(limit_x, max_x)

    base = np.zeros(max_x)
    plot_args = OrderedDict()
    for label, curve in curves.items():
        curve = curve[:max_x]
        kwargs = {
            'color'    : next(colors),
            'label'    : label,
            'alpha'    : alpha,
            'linewidth': 1.5
        }
        args = [np.arange(len(curve)), base.copy(), base + curve]
        plot_args[label] = (args, kwargs)
        base += curve
    return plot_args



def add_insets (
    ax,
    insets: List[Dict[str, Union[Axes, str, float]]],
    inset_call_args: Iterable,
    inset_call_kwargs: Dict,
    call: str
):
    """
    Adds insets to the given matplotlib axis. Insets are rectangular areas inside
    superimposed on a plot that show arbitrary other plots. This method uses the
    same arbitrary plot for all the insets; this is useful for zooming on different
    areas of the plot.
    :param ax: axis instance on which to add the insets
    :param insets: parameters of the insets, as returned by parse_insets_params()
    :param inset_call_args: *args to pass to a call to the plot function
    :param inset_call_kwargs: **kwargs to pass to a call to the plot function
    :param call: method of inset to call; can be 'plot' or 'fill_between'
    """
    for inset in insets:
        inset['ax'] = zoomed_inset_axes(
            parent_axes=ax,
            zoom=inset['zf'],
            loc=inset['loc'],
            bbox_to_anchor=(inset['bbx'], inset['bby'])
        )  # See http://matplotlib.org/api/legend_api.html
        ax.xticks(visible=False)
        ax.yticks(visible=False)

        if call == 'plot':
            inset['ax'].plot(*inset_call_args, **dict(inset_call_kwargs, linewidth=inset['lw']))
        elif call == 'fill_between':
            inset['ax'].fill_between(*inset_call_args, **inset_call_kwargs)

    for inset in insets:
        inset['ax'].set_xlim(inset['x1'], inset['x2'])  # apply the x-limits
        inset['ax'].set_ylim(inset['y1'], inset['y2'])  # apply the y-limits
        mark_inset(ax, inset['ax'], loc1=inset['loc1'], loc2=inset['loc2'], fc="none", ec="0.5")
    plt.sca(ax)


def parse_insets_params(plot_insets_str):
    """
    Takes an iterable of strings formatted as specified in for parameters insets and insets2
    and parses it to an iterable of dictionaries.
    :param plot_insets_str: iterable of strings to parse
    :return: list of dicts, containing the parsed values
    """
    insets = defaultdict(list)
    for plot_insets in plot_insets_str:
        for inset in plot_insets.split(';'):
            params = [param.split('=') for param in inset.split(',')]
            params = {k: numify(v) for k, v in params}
            insets[params['plot']].append(params)
    return insets


def apply_figure_params (fig, ax, figure_specs):
    plt.xscale(figure_specs['xscale'])
    plt.yscale(figure_specs['yscale'])
    plt.xlabel(figure_specs['xlabel'])
    plt.ylabel(figure_specs['ylabel'])
    plt.grid(True)
    plt.title(figure_specs['title'])
    plt.tight_layout()
    plt.draw()
    fig.set_size_inches(figure_specs['width_in'], figure_specs['height_in'])
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height])


def save_plot (plot_args, figure_specs, show=False, file_path: Optional[str]=None, file_formats=('PNG', 'PDF'), legend_args=None, insets=()):
    """
    Generates plots calling matplotlib's plot function and saves and/or shows them.
    :param legend_args: args for the call to the legend method
    :param plot_args: specifications for the plot
    :param figure_specs: params for the figure object containing the plot
    :param show: whether to show the plot on screen or not
    :param file_path: if a string, the plot is saved at the path indicated
    :param file_formats: iterable of file formats for the output files
    :param insets: params for the insets to be generated, if any (see add_insets())
    """
    if not plot_args:
        return
    if legend_args is None:
        legend_args = {'loc': 'lower right', 'fontsize': 'small', 'ncol': 2}

    fig = plt.figure('save_plot')
    ax = plt.gca()

    # Populate
    for args, kwargs in plot_args.values():
        ax.plot(*args, **kwargs)
        add_insets(ax, insets, args, kwargs, call='plot')

    # Refine
    lgd =ax.legend(plot_args.keys(), **legend_args)
    apply_figure_params(fig, ax, figure_specs)

    # Output
    if file_path is not None:
        for file_format in file_formats:
            plt.savefig("%s.%s" % (file_path, file_format.lower()), format=file_format, bbox_extra_artists=(lgd,),
                        bbox_inches='tight')
    if show:
        plt.show()

    plt.close('save_plot')


def save_stackplot (fill_between_args, figure_specs, show=False, file_path=True, file_formats=('PNG','PDF'), insets=()):
    """
    Generates stacked plots calling matplotlib's fill_between function and saves and/or shows them.
    :param fill_between_args: specifications for the plot
    :param figure_specs: params for the figure object containing the plot
    :param show: whether to show the plot on screen or not
    :param file_path: if a string, the plot is saved at the path indicated
    :param file_formats: iterable of file formats for the output files
    :param insets: params for the insets to be generated, if any (see add_insets())
    """
    if not fill_between_args:
        return

    fig = plt.figure('save_stacked_area_plot')
    ax = plt.gca()

    # Populate
    legend_proxies = []
    for args, kwargs in fill_between_args:
        plt.fill_between(*args, **kwargs)
        add_insets(ax, insets, args, kwargs, call='fill_between')
        legend_proxies.append(plt.Rectangle((0, 0), 1, 1, fc=kwargs['color'], alpha=kwargs['alpha']))

    # Refine
    plt.legend(legend_proxies, fill_between_args.keys(), loc="upper right", fontsize='small')
    apply_figure_params(fig, ax, figure_specs)

    # Output
    if file_path:
        for file_format in file_formats:
            plt.savefig("%s.%s" % (file_path, file_format.lower()), format=file_format)
    if show:
        plt.show()

    plt.close('save_stacked_area_plot')


def save_boxplot (learning_agents_data, desired_groups, desired_names, output_file, file_formats, field, title):
    learning_agents_data = filter_groups(learning_agents_data, desired_groups, desired_names)

    df = pd.DataFrame(OrderedDict(
        ("\n".join(camel_case_split(name)), pd.Series(game[field]))
        for name, game in learning_agents_data.items()))

    fig = plt.figure('save_plot', figsize=(6,5))
    df.boxplot(return_type='axes')
    fig.autofmt_xdate()
    plt.title(title)
    plt.ylabel(title)
    for file_format in file_formats:
        plt.savefig("%s.%s" % (output_file, file_format.lower()), format=file_format)
    plt.close('save_plot')
