import numpy as np
from collections import OrderedDict, defaultdict
from typing import List, Dict, Union, Iterable, TypeVar, Optional
import os
import pickle
from util.util2 import pickle_load
import pandas as pd
from tabulate import tabulate
from scipy.stats import ttest_ind
from itertools import combinations, chain



# from http://stackoverflow.com/a/33002123
def cohen_d(x,y):
    nx = len(x)
    ny = len(y)
    dof = nx + ny - 2
    return (np.mean(x) - np.mean(y)) / np.sqrt(((nx-1)*np.std(x, ddof=1) ** 2 + (ny-1)*np.std(y, ddof=1) ** 2) / dof)


def normalize (
    data: Dict[str, np.ndarray]
) -> Dict[str, np.ndarray]:
    """
    Takes a dict of numpy arrays of equal length and returns it normalized across values.
    That is, the returned dict has the property that, for all i, the sum of the i-th element of all
    dict entries is 1 while retaining proportions between i-th elements.
    Order of curves is preserved, if present.

    Example
    Input: {
        'x': [+1,+2,+6,+60],
        'y': [+1,+4,+1,+10],
        'z': [+2,+2,+1,+10],
    }
    Output: {
        'x': [+0.25,+0.25,+0.750,+0.750],
        'y': [+0.25,+0.50,+0.125,+0.125],
        'z': [+0.50,+0.25,+0.125,+0.125],
    },
    :param data: dict-like of numpy arrays of equal length
    :return: OrderedDict structured as input, but normalized
    """
    for key in data:
        data[key] = np.asarray(data[key])

    element_by_element_sum = sum(data.values())
    nonzero_elements = element_by_element_sum > 0

    normalized_curves = OrderedDict()
    for label in data:
        normalized_curves[label] = np.asarray(data[label], dtype=float)
        normalized_curves[label][nonzero_elements] /= element_by_element_sum[nonzero_elements]

    return normalized_curves






def get_averages (
    data: Dict[str, Dict[str, Union[Iterable[float], Dict[str, Iterable[float]]]]],
    deeper_keys: Optional[List[str]]=None,
    copy_keys: Optional[List[str]]=None
) -> Dict[str, Dict[str, Union[float, Dict[str, float]]]]:
    """
    Takes a dict of dicts of iterables and returns an OrderedDict of dicts of floats;
    the floats are the arithmetic averages of the corresponding iterables.
    Keys in the inner dictionary that are in deeper_keys are assumed to contain a dict of iterables as opposed to an iterable.
    Keys in the inner dictionary that are in copy_keys have their values just copied.
    Order of groups is preserved, if present.

    Example
    Input (with copy_keys=['c'] and deeper_keys=['d']: {
        'a': {
            'x': [+1,+2,+3,+4],
            'y': [+3,+4,+5,+6],
            'z': [+5,+6,+7,+8],
            'c': 4,
            'd': {
                '1': [+1,+2,+3,+4],
                '2': [-3,+4,+5,-6],
                '3': [-5,-6,-7,-8],
            },
        },
        'b': {
            'x': [-1,-2,-3,-4],
            'y': [-3,-4,-5,-6],
            'z': [-5,-6,-7,-8],
            'c': 4,
            'd': {
                '1': [-1,-2,-3,-4],
                '2': [+3,-4,-5,+6],
                '3': [+5,+6,+7,+8],
            },
        },
    }
    Output: {
        'a': {
            'x': +2.5,
            'y': +4.5,
            'z': +6.5,
            'c': 4,
            'd': {
                '1': +2.5,
                '2': 0.0,
                '3': -6.5,
            },
        },
        'b': {
            'x': -2.5,
            'y': -4.5,
            'z': -6.5,
            'c': 4,
            'd': {
                '1': -2.5,
                '2': 0.0,
                '3': +6.5,
            },
        },
    }

    :param data: dict-like of dicts-like of iterables to be averaged
    :param deeper_keys: keys in the inner dictionary that contain a dict of iterables as opposed to an iterable
    :param copy_keys: keys in the inner dictionary that should just be copied
    :return: OrderedDict of dict of floats, containing the averages
    """
    averages = OrderedDict()
    for first_level_key, group_data in data.items():

        averages[first_level_key] = {}
        for second_level_key in group_data:
            if deeper_keys is not None and second_level_key in deeper_keys:
                # data[first_level_key][second_level_key] is a dict of iterables, all of which will be separately averaged
                if second_level_key not in averages[first_level_key]:
                    averages[first_level_key][second_level_key] = {}
                for third_level_key in group_data[second_level_key]:
                    sequence = np.asarray(group_data[second_level_key][third_level_key])
                    averages[first_level_key][second_level_key][third_level_key] = sequence.mean()
            elif copy_keys is not None and second_level_key in copy_keys:
                # data[first_level_key][second_level_key] will just be copied
                averages[first_level_key][second_level_key] = group_data[second_level_key]
            else:
                # data[first_level_key][second_level_key] is an iterable and will be averaged
                sequence = np.asarray(group_data[second_level_key])
                averages[first_level_key][second_level_key] = sequence.mean()

    return averages


T = TypeVar('T')
def filter_groups(
    data: Dict[str, T],
    desired_groups: List[str],
    desired_names: List[str]
) -> Dict[str, T]:
    """
    Filters out groups with label not in desired_groups and changes label
    according to desired_names. Notice that len(desired_groups) must equal
    len(desired_names).
    Order of the groups is that of desired_groups.
    :param data: dict-like to be filtered
    :param desired_groups: desired labels
    :param desired_names: renames for labels
    :return: OrderedDict with filtered data and renamed labels
    """
    filtered_data = OrderedDict()
    for orig_name, desired_name in zip(desired_groups, desired_names):
        if orig_name not in data:
            continue
        filtered_data[desired_name] = data[orig_name]
    return filtered_data




T = TypeVar('T')
def transpose(
    data: Dict[str, Dict[str, T]],
    fields: List[str]
) -> Dict[str, Dict[str, T]]:
    """
    Takes in a dict with some keys (a,b,c,...), whose values are dicts each indexed by some keys (1,2,3,...) and returns
    a dict indexed by keys 1,2,3,... whose values are dicts indexed by keys a,b,c,...
    Order of groups is preserved, if present.
    :param data: dict-like of dicts-like (gd['alg_name']['feature'])
    :param fields: list of fields, must be consistent with keys of the inner dictionaries
    :return: OrderedDict of dicts with the same data but indexed differently
        (gd['feature']['alg_name'])
    """
    items = OrderedDict((field, OrderedDict()) for field in fields)
    for group_name, group_data in data.items():
        for field in fields:
            if field not in group_data:
                continue
            items[field][group_name] = group_data[field]
    return items





def load_eps_data (
    folder: str,
    desired_folders: Iterable[str],
    min_eps: int,
    max_eps: int,
    game_fields: Optional[List[str]]=None,
    players_fields: Optional[List[str]]=None,
):
    """
    Loads data and aggregates it. Data is expected to be organized in folders (folder name is used as identifier).
    Data in each file is expected to be an iterable of instances of named tuples.

    Data is returned as a dictionary indexed first by folder name and then by variable.
    The output is a tuple with three elements: they all are aggregations of the input data, in two different forms:
     - average across repetitions (that is, across files in the same folder);
     - sum across episodes (that is, across instances of named tuples in the same iterable).
    :param folder: string indicating the path where data is located
    :param desired_folders: folders to be included in the output
    :param min_eps: first episode to use for limiting
    :param max_eps: last episode to use for limiting
    :param game_fields: fields in the named tuple that are scalars
    :param players_fields: fields in the named tuple that contain one value per player
    :return: a tuple of three OrderedDicts, both indexed by folder name and then by variable; they contain, respectively:
        - the data averaged across repetitions (useful for plotting), further indexed by episode number
         i.e. data[groupLabel][variable][episode]
         e.g. data['AggressiveOptions']['score'][3000];
        - the data summed across all episodes (useful for statistical tests), further indexed by repetition
         i.e. sumd[groupLabel][variable][rep]
         e.g. sumd['AggressiveOptions']['score'][100].
        - the data summed across episodes in the range [min_eps,max_eps], further indexed by repetition
         i.e. sumd[groupLabel][variable][rep]
         e.g. sumd['AggressiveOptions']['score'][100].
    """
    if game_fields is None:
        game_fields = []
    assert game_fields is not None
    if players_fields is None:
        players_fields = []
    assert players_fields is not None
    all_fields = list(chain(game_fields, players_fields))
    data_avg_by_ep = defaultdict(dict)
    data_avg_by_rep = defaultdict(dict)
    data_avg_by_rep_lim = defaultdict(dict)
    subFolders = os.listdir(folder)
    for i, subFolder in enumerate(subFolders):
        if not os.path.isdir(folder + subFolder):
            continue
        if subFolder not in desired_folders:
            continue
        if not subFolder.endswith("/"):
            subFolder += "/"
        label = subFolder[:-1]

        # folder_data[groupLabel][variable][rep][episode]
        # e.g. folder_data['AggressiveOptions']['score'][100][3000]
        folder_data = {field: [] for field in all_fields}
        folder_data['count'] = 0

        files = os.listdir(folder + subFolder)
        for j, file in enumerate(files):
            print("Loading games in %s; current subfolder: %d/%d; current game: %d/%d     \r" % (folder, i+1, len(subFolders), j+1, len(files)), end="")
            # print(folder + subFolder + file)

            # file_data[variable][rep][episode]
            # e.g. file_data['score'][100][3000]
            file_data = {field: [] for field in all_fields}
            with open(folder + subFolder + file, 'rb') as f:
                if not file.endswith(".log"):
                    continue
                eps = pickle.load(f)
                for ep in eps:
                    for field in all_fields:
                        x = getattr(ep, field)
                        file_data[field].append(x)
            for field in all_fields:
                folder_data[field].append(file_data[field])
            folder_data['count'] += 1
        data_avg_by_ep[label]['count'] = folder_data['count']
        for field in game_fields:
            data = np.asarray(folder_data[field], dtype=float)
            data_avg_by_ep[label][field] = data.mean(axis=0)
            data_avg_by_rep[label][field] = data.mean(axis=1)
            data_avg_by_rep_lim[label][field] = data[:,min_eps:max_eps].mean(axis=1)
        for field in players_fields:
            data = np.asarray(folder_data[field], dtype=float)
            data_avg_by_ep[label][field] = data.mean(axis=0)
            data_avg_by_rep[label][field] = data.mean(axis=1)
            data_avg_by_rep_lim[label][field] = data[:,min_eps:max_eps,:].mean(axis=1)

    data_avg_by_ep = OrderedDict((name, data_avg_by_ep[name]) for name in desired_folders if name in data_avg_by_ep)
    data_avg_by_rep = OrderedDict((name, data_avg_by_rep[name]) for name in desired_folders if name in data_avg_by_rep)
    data_avg_by_rep_lim = OrderedDict((name, data_avg_by_rep_lim[name]) for name in desired_folders if name in data_avg_by_rep_lim)

    return data_avg_by_ep, data_avg_by_rep, data_avg_by_rep_lim




def load_games_data (
    folder: str,
    fields: List[str]
):
    grouped = OrderedDict()
    if folder:
        subFolders = os.listdir(folder)
        for i, subFolder in enumerate(subFolders):
            if not subFolder.endswith("/"):
                subFolder += "/"
            if not os.path.isdir(folder + subFolder):
                continue

            label = subFolder[:-1]
            grouped[label] = {field: [] for field in fields}
            grouped[label]['count'] = 0

            files = os.listdir(folder + subFolder)
            for j, file in enumerate(files):
                print("Loading demos in %s; current subfolder folder: %d/%d; current demo: %d/%d     \r" % (folder, i+1, len(subFolders), j+1, len(files)), end="")
                if not os.path.isfile(folder + subFolder + file):
                    continue
                if not file.endswith(".log"):
                    continue
                eps_data = pickle_load(folder + subFolder + file)
                for ep in eps_data:
                    grouped[label]['count'] += 1
                    for field in fields:
                        x = getattr(ep, field)
                        if x is not None:
                            grouped[label][field].append(x)
    print()
    return grouped




def pairwise_stat_analyses (learningAgentsData, outputFileTemplate, limitEps, fields):
    for field in fields:
        pairwise_stat_analysis(learningAgentsData, outputFileTemplate % field, limitEps, field)


def pairwise_stat_analysis (learningAgentsData, outputFile, limitEps, field):
    df = pd.DataFrame(OrderedDict(
                      (name, pd.Series(game[field][:limitEps]))
                      for name, game in learningAgentsData.items()))

    with open(outputFile, 'w') as f:
        rows = []
        hds = ["Name", "Count", "Mean", "Std. dev", "Median", "25% quant.", "75% quant.", "Skewness", "Kurtosis"]
        for g in df.columns.values:
            rows.append([
                g,
                df[g].count(),
                df[g].mean(),
                df[g].std(),
                df[g].median(),
                df[g].quantile(0.25),
                df[g].quantile(0.75),
                df[g].skew(),
                df[g].kurt()
            ])
        print(tabulate(rows, hds,floatfmt='.3f'), file=f)
        print(file=f)
        rows = []
        hds = ["Higher value", "Lower value", "Effect size", "p-value"]
        for g1, g2 in sorted(combinations(df.columns.values, 2)):
            d = cohen_d(df[g1], df[g2])
            _, p = ttest_ind(df[g1], df[g2], nan_policy='omit')
            if d < 0:
                d, g1, g2 = -d, g2, g1
            rows.append([g1, g2, d, p])
        rows = sorted(rows, key=lambda row: row[2],reverse=True)
        print(tabulate(rows, hds,floatfmt='.3f'), file=f)

def generate_csv (data, output_file, limit_eps, field):
    df = pd.DataFrame(OrderedDict(
        (name, pd.Series(game[field][:limit_eps]))
        for name, game in data.items()))
    df.to_csv(output_file, na_rep='', index=False)