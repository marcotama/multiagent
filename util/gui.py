import tkinter

class InputDialog(object):

    def __init__(self, requestMessage, title, copyText=None, copyButtonText=None, font=None):
        self.root = tkinter.Tk()
        self.root.wm_title(title)
        self.string = None
        self.frame = tkinter.Frame(self.root)
        self.frame.pack()
        self.e = None
        self.copyText = copyText
        self.copyButton = copyButtonText
        self.textVariable = tkinter.StringVar()
        self.font = font
        self.acceptInput(requestMessage)

    def acceptInput(self, requestMessage):
        r = self.frame

        if self.font is None:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT)
        else:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT, font=self.font)
        self.textVariable.set(requestMessage)
        k.pack()
        self.e = tkinter.Entry(r)
        self.e.pack()
        self.e.focus_set()
        if self.copyText is not None and self.copyButton is not None:
            b = tkinter.Button(r, text=self.copyButton, command=self.copy)
            b.pack(side=tkinter.LEFT)
        b = tkinter.Button(r, text='OK', command=self.getText)
        b.pack(side=tkinter.LEFT)
        b = tkinter.Button(r, text='Cancel', command=self.close)
        b.pack(side=tkinter.RIGHT)

    def updateLabel(self, requestMessage):
        self.textVariable.set(requestMessage)

    def getText(self):
        self.string = self.e.get()
        self.root.destroy()

    def copy(self):
        self.root.clipboard_clear()
        self.root.clipboard_append(self.copyText)

    def close(self):
        self.string = None
        self.root.destroy()

    def getString(self):
        return self.string

    def waitForInput(self):
        self.root.mainloop()


class YesNoDialog(object):

    def __init__(self, requestMessage, title, copyText=None, copyButtonText=None, font=None):
        self.root = tkinter.Tk()
        self.root.wm_title(title)
        self.value = None
        self.frame = tkinter.Frame(self.root)
        self.frame.pack()
        self.copyText = copyText
        self.copyButton = copyButtonText
        self.textVariable = tkinter.StringVar()
        self.font = font
        self.acceptInput(requestMessage)

    def acceptInput(self, requestMessage):
        r = self.frame

        if self.font is None:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT)
        else:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT, font=self.font)
        self.textVariable.set(requestMessage)
        k.pack()
        if self.copyText is not None and self.copyButton is not None:
            b = tkinter.Button(r, text=self.copyButton, command=self.copy)
            b.pack(side=tkinter.LEFT)
        b = tkinter.Button(r, text='Yes', command=self.yes)
        b.pack(side=tkinter.RIGHT)
        b = tkinter.Button(r, text='No', command=self.no)
        b.pack(side=tkinter.LEFT)

    def updateLabel(self, requestMessage):
        self.textVariable.set(requestMessage)

    def yes(self):
        self.value = True
        self.root.destroy()

    def no(self):
        self.value = False
        self.root.destroy()

    def copy(self):
        self.root.clipboard_clear()
        self.root.clipboard_append(self.copyText)

    def getValue(self):
        return self.value

    def waitForInput(self):
        self.root.mainloop()


class MessageDialog(object):

    def __init__(self, requestMessage, title, copyText=None, copyButtonText=None, showButton=True, font=None):
        self.root = tkinter.Tk()
        self.root.wm_title(title)
        self.frame = tkinter.Frame(self.root)
        self.frame.pack()
        self.copyText = copyText
        self.textVariable = tkinter.StringVar()
        self.copyButton = copyButtonText
        self.showButton = showButton
        self.font = font
        self.acceptInput(requestMessage)

    def acceptInput(self, requestMessage):
        r = self.frame

        if self.font is None:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT)
        else:
            k = tkinter.Label(r, textvariable=self.textVariable, justify=tkinter.LEFT, font=self.font)
        self.textVariable.set(requestMessage)
        k.pack()
        if self.copyText is not None and self.copyButton is not None:
            b = tkinter.Button(r, text=self.copyButton, command=self.copy)
            b.pack(side=tkinter.LEFT)
        if self.showButton:
            b = tkinter.Button(r, text='OK', command=self.done)
            b.pack()

    def updateLabel(self, requestMessage):
        self.textVariable.set(requestMessage)

    def done(self):
        self.root.destroy()

    def copy(self):
        self.root.clipboard_clear()
        self.root.clipboard_append(self.copyText)

    def waitForInput(self):
        self.root.mainloop()

def input_dialog(text, title, copyText=None, copyButtonText=None, font=None):
    msgBox = InputDialog(text, title, copyText, copyButtonText, font=font)
    msgBox.waitForInput()
    return msgBox.getString()

def yes_no_dialog(message, title, copyText=None, copyButtonText=None, font=None):
    dialog = YesNoDialog(message, title, copyText, copyButtonText, font=font)
    dialog.waitForInput()
    return dialog.getValue()

def message_dialog(message, title, copyText=None, copyButtonText=None, font=None):
    dialog = MessageDialog(message, title, copyText, copyButtonText, font=font)
    dialog.waitForInput()

def timed_dialog(message, title, seconds=3, copyText=None, copyButtonText=None, font=None):
    timer = seconds

    def get_message():
        nonlocal message, timer
        try:
            return message % timer
        except TypeError:
            return message

    def tick():
        nonlocal timer
        timer -= 1
        if timer == 0:
            dialog.root.destroy()
        dialog.updateLabel(get_message())

    dialog = MessageDialog(get_message(), title, copyText, copyButtonText, showButton=False, font=font)
    for t in range(seconds):
        dialog.root.after(1000 * (t+1), tick)
    dialog.waitForInput()