import dropbox
import pickle
import os
import sys

from util.gui import input_dialog

def get_dropbox_auth_code(auth_code_file, app_key, app_secret, refresh=False):
    if os.path.isfile(auth_code_file) and not refresh:
        with open(auth_code_file, 'rb') as f:
            _, access_token = pickle.load(f)
    else:
        flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
        # Have the user sign in and authorize this token
        authorize_url = flow.start()
        text = (
"""To upload of the log files, we need your authorization in Dropbox.
(We will store your code so you only need to do this once)

To authorize us, please:
1. Go to:
    """ + authorize_url + """
2. Click "Allow" (you might have to log in first)
3. Copy the authorization code.
Enter the authorization code here:""")

        access_token = None
        while access_token is None:
            # noinspection PyBroadException
            try:
                code = input_dialog(text, 'Dropbox Authorization Code', authorize_url, 'Copy URL')
                if code is None:
                    break
                # Will fail if access_token is invalid
                access_token, user_id = flow.finish(code)
            except:
                pass
        if access_token is None:
            raise InterruptedError
        with open(auth_code_file, 'wb') as f:
            pickle.dump(("Please, don't do this...", access_token), f)

    return access_token


def upload_to_dropbox(auth_code_file, app_key, app_secret, file_path: str, remote_file_path: str):
    if os.path.isfile(file_path):
        # noinspection PyBroadException
        try:
            access_token = get_dropbox_auth_code(auth_code_file, app_key, app_secret)
            client = dropbox.Dropbox(access_token)
        except InterruptedError:
            sys.exit(0)
        except:
            access_token = get_dropbox_auth_code(auth_code_file, app_key, app_secret, refresh=True)
            client = dropbox.Dropbox(access_token)
        with open(file_path, 'rb') as f:
            client.files_upload(f, remote_file_path)