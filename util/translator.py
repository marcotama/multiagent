# -*- coding: utf-8 -*-
"""
Class representing a bidirectional hash-map between dictionaries (of hashable)
and codes.

@author: Marco Tamassia
"""
from typing import Hashable, Dict, Tuple, Union, Optional, Callable, Iterable, Set
from util.typing2 import MyTypes as T
from util.util2 import project, hash_dictionary
from collections import defaultdict


class Translator:
    def __init__(
            self,
            newDictCallback: Optional[Callable[[Dict[int, Dict[str, Hashable]], int], None]]=None
    ):
        self.codeToDict = {}  # type: Dict[int, Dict[str, Hashable]]
        self.hashToCode = {}  # type: Dict[Tuple, int]
        self.nEntries = 0
        self.newDictCallback = newDictCallback

    def __str__(
        self
    ) -> str:
        result = ""
        for c in self.codeToDict:
            result += "%d --> %s\n" % (c, str(self.codeToDict[c]))
        return result

    def getCode(
        self,
        d: Optional[Union[Hashable, Dict[str, Hashable]]]
    ) -> Optional[int]:
        """
        Returns the code for the given dictionary.
        :param d: dictionary to get the code for
        """
        if d is None:
            return None
        else:
            h = self.hash(d)
            if h not in self.hashToCode:
                self._addDict(d)
            return self.hashToCode[h]

    def getDict(
        self,
        c: Optional[int]
    ) -> Optional[Dict[str, Hashable]]:
        """
        Returns the dict corresponding to the given code. None is returned if
        there is no such dict.
        :param c: code to get the dictionary for
        """
        if c is None:
            return None
        return self.codeToDict[c]

    @staticmethod
    def hash(
        d: Union[Hashable, Dict[int, Dict[str, Hashable]]]
    ) -> Tuple[int, Dict[str, Hashable]]:
        """
        Returns a hash for the given dict.
        :param d: dictionary to hash
        """
        if 'items' in dir(d):
            return tuple(sorted(d.items()))
        else:
            return d

    @staticmethod
    def dehash(
        h: Tuple[int, Dict[str, Hashable]]
    ) -> Dict[int, Dict[str, Hashable]]:
        d = {k: v for k, v in h}
        return d

    def hashCode(
        self,
        c: int
    ) -> Hashable:
        """
        Returns a hash for the dict corresponding to the given code.
        :param c: code to de-hash
        """
        d = self.codeToDict[c]
        h = self.hash(d)
        return h

    def _addDict(
        self,
        d: Optional[Dict[Hashable, Dict[str, Hashable]]]
    ) -> None:
        """
        Checks whether the given dict has already been added to the
        internal data structures and, in case it has not, it adds it.
        """
        if d is None:
            return
        h = self.hash(d)
        if h not in self.hashToCode:
            c = self.nEntries
            self.codeToDict[c] = d
            self.hashToCode[h] = c
            self.nEntries += 1
            if self.newDictCallback:
                self.newDictCallback(d, c)


class Aggregator:
    def __init__(
        self,
        relevantFeatures: Iterable[str],
        origTranslator: Translator
    ):
        """
        Aggregates state codes by ignoring some of the original features.
        """
        self.origTranslator = origTranslator
        self.aggrTranslator = Translator()
        self.relevantFeatures = relevantFeatures
        self.matchesCache = defaultdict(set)

    def aggregate(
        self,
        s: T.StateCode
    ) -> T.StateCode:
        """
        Maps the given state code to another code that is the same for all
        states that are identical when compared on a subset of their original
        features.
        :param s: state code
        """
        state = self.origTranslator.getDict(s)
        pd = project(state, self.relevantFeatures)  # Projected dict (only relevant features)
        c = self.aggrTranslator.getCode(pd)
        return c

    def findMatches(
        self,
        state: T.State
    ) -> Set[T.StateCode]:
        """
        Finds matches of the given state. Calls to this function are cached after projecting the parameter on the
        relevant features.
        :param state: state whose matches are to be found
        :return: matching states code
        """
        projectedState = project(state, self.relevantFeatures)
        hashedProjectedState = hash_dictionary(projectedState)
        if hashedProjectedState in self.matchesCache:
            return self.matchesCache[hashedProjectedState]
        else:
            allStates = self.origTranslator.codeToDict.values()
            relF = self.relevantFeatures
            matchingStates = (
                state
                for state in allStates
                if state is not None and projectedState == project(state, relF)
            )
            matchingCodes = [self.origTranslator.getCode(d) for d in matchingStates]
            self.matchesCache[hashedProjectedState] = matchingCodes
            return set(matchingCodes)

    def newState(
        self,
        state: T.State,
        s: T.StateCode
    ):
        """
        Updates internal cache to keep the existence of the given state in account.
        If this is not called when a new state is encountered, the cache could become outdated.
        :param state: the state whose existence needs to be taken into account
        :param s: the code corresponding to parameter state
        """
        projectedState = project(state, self.relevantFeatures)
        hashedProjectedState = hash_dictionary(projectedState)
        self.matchesCache[hashedProjectedState].add(s)

    def reverse(
        self,
        aggS: T.StateCode
    ) -> Set[T.StateCode]:
        """
        Returns a list of all the state codes that would be mapped to the given
        aggregated code by the function aggregate();
        :param aggS: aggregated state code that is to be reversed
        :returns: all states s_ such that aggS == aggregate(s_)
        """
        projectedState = self.aggrTranslator.getDict(aggS)
        return self.findMatches(projectedState)

    def findSiblings(
        self,
        s: T.StateCode
    ) -> Set[T.StateCode]:
        """
        Returns a list of all the state codes that would be mapped to the code given by aggregate(s);
        i.e. all s_ such that aggregate(s) == aggregate(s_).
        :param s: state code whose siblings are to be found
        :returns: all states s_ such that aggregate(s) == aggregate(s_)
        """
        state = self.origTranslator.getDict(s)
        return self.findMatches(state)
