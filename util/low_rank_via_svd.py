import numpy as np
from scipy import linalg

def approximate_via_low_rank(X: np.array, k: int):
    U, d, Vt =  low_rank_factorization(X, k)
    D = linalg.diagsvd(d, X.shape[0], X.shape[1])
    X1 = U @ D @ Vt
    return X1

def low_rank_factorization(X: np.array, k: int):
    U, d, Vt = linalg.svd(X)
    U, d, Vt = decrease_rank(U, d, Vt, k)
    return U, d, Vt

def decrease_rank(U, d, Vt, k):
    U = U[:,:k]
    d = d[:k]
    Vt = Vt[:k,:]
    return U, d, Vt
