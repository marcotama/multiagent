from util.vector import DenseVector, SparseVector, Vector
from abc import abstractmethod
from typing import List, Dict, Hashable
import numpy as np

    
class StateActionEncoder:
    """
    Abstract class for encoders of state-action pairs.
    """
    def __init__(self, vec_size: int):
        self.out_vec_size = vec_size

    @abstractmethod
    def state_action(self, x: Vector, a: Hashable) -> Vector: pass

    @abstractmethod
    def vector_norm(self) -> float:
        """
        Returns the norm of vectors returned by this encoder; that is, the minimum number of cells set to 1.
        If the returned value is n, the number of cells set to 1 can range from n to n + n_additional_features.
        :return: the norm of vectors returned by this projector.
        """
        pass

    @abstractmethod
    def vector_size(self) -> int:
        """
        The number of elements in each encoded vector.
        :return: the number of elements in each encoded vector.
        """
        pass

    def state_actions(self, x: Vector, la: List[Hashable]) -> Dict[int, Vector]:
        if x is None:
            return {a: DenseVector(self.vector_size()) for a in la}
        else:
            return {a: self.state_action(x, a) for a in la}


# Tile coding base projector to state action
class StateActionAppendingEncoder(StateActionEncoder):
    """
    Encodes state-action pairs by adding an extra discrete feature (i.e., the action id) to the state features. The
    whole tuple is then encoded using tile coding.

    Link: http://incompleteideas.net/rlai.cs.ualberta.ca/RLAI/RLtoolkit/tiles.html
    """

    def __init__(self, vec_size: int, vec_norm: int, actions: List[Hashable], include_bias: bool=True,
                 assume_unit_values: bool = True):
        super().__init__(vec_size)
        self.in_vec_size = vec_size
        self.out_vec_size = vec_size + len(actions) + int(include_bias)
        self.in_vec_norm = vec_norm
        self.out_vec_norm = vec_norm + 1 + int(include_bias)
        self.actions = actions
        self.include_bias = include_bias
        self.assume_unit_values = assume_unit_values

        # sequential ids for action
        self.aid_to_i = {}
        self.i_to_aid = {}
        for i, a in enumerate(actions):
            self.aid_to_i[a] = i
            self.i_to_aid[i] = a

    def state_action(self, x: Vector, a: Hashable) -> Vector:

        # trivial case
        if x is None:
            return None

        nz = x.nonzero()
        x_norm = len(nz)
        out_norm = x_norm + 1 + int(self.include_bias)
        idx = np.empty(out_norm)
        val = np.empty(out_norm)

        idx[:x_norm] = nz
        if self.assume_unit_values:
            if isinstance(x, SparseVector):
                val[:x_norm] = x[nz].toarray()[0]
            elif isinstance(x, (DenseVector, np.array)):
                val[:x_norm] = x[nz]
            else:
                raise Exception('Unknown type of x: %s' % type(x))
        else:
            val[:] = 1

        idx[x_norm] = self.in_vec_size + self.aid_to_i[a]
        val[x_norm] = 1.0

        if self.include_bias:
            idx[x_norm + 1] = self.out_vec_size - 1
            val[x_norm + 1] = 1.0

        v = SparseVector(self.out_vec_size, data=val,idx=idx)

        return v

    def vector_norm(self) -> float:
        return self.out_vec_norm

    def vector_size(self) -> int:
        return self.out_vec_size


class StateActionTabularEncoder(StateActionEncoder):
    """
    Encodes state-action pairs using a vector long n_state_features * n_actions.
    The vector for (s, a) is sparse, being zero everywhere, except for cells in range
    n_state_features * a.id() -- n_state_features * (a.id() + 1), which are assigned values `projector(s)`.
    E.g.:
    state_action(s, a_1) => (f_1, f_2, ..., f_n,    0, 0, ...,       0,    0, 0, ...,       0)
    state_action(s, a_2) => (0, 0, ...,       0,    f_1, f_2, ..., f_n,    0, 0, ...,       0)
    state_action(s, a_3) => (0, 0, ...,       0,    0, 0, ...,       0,    f_1, f_2, ..., f_n)

    If `include_bias` is set to True, an extra entry with value `1.0` is added at the end of the state-action vector.
    """

    def __init__(self, vec_size: int, vec_norm: int, actions: List[Hashable], include_bias: bool=True,
                 assume_unit_values: bool=True):
        super().__init__(vec_size)
        self.in_vec_size = vec_size
        self.out_vec_size = vec_size * len(actions) + int(include_bias)
        self.in_vec_norm = vec_norm
        self.out_vec_norm = vec_norm + int(include_bias)
        self.actions = actions
        self.include_bias = include_bias
        self.assume_unit_values = assume_unit_values

        # sequential ids for action
        self.aid_to_i = {}
        self.i_to_aid = {}
        for i, a in enumerate(actions):
            self.aid_to_i[a] = i
            self.i_to_aid[i] = a

    def state_action(self, x: Vector, a: Hashable) -> Vector:

        # trivial case
        if x is None:
            return None

        nz = x.nonzero()
        x_norm = len(nz)
        out_norm = x_norm + int(self.include_bias)
        idx = np.empty(out_norm)
        val = np.empty(out_norm)

        offset = self.in_vec_size * self.aid_to_i[a]
        idx[:x_norm] = nz + offset  # type: np.ndarray
        if self.assume_unit_values:
            val[:] = 1.0
        else:
            # noinspection PyUnresolvedReferences
            val[:x_norm] = x[nz].toarray()[0]

        if self.include_bias:
            idx[x_norm] = self.out_vec_size - 1 # bias
            val[x_norm] = 1.0

        v = SparseVector(self.out_vec_size, data=val,idx=idx)

        return v

    def vector_norm(self) -> float:
        return self.out_vec_norm

    def vector_size(self) -> int:
        return self.out_vec_size
