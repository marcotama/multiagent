import numpy as np
from numpy import exp, ndarray, sqrt, ceil
from numpy.matlib import repmat
from numpy.linalg import solve, norm
from scipy.sparse.linalg import norm as sparse_norm, svds
from scipy.sparse import csr_matrix
from typing import Optional

def OptSpace(
    M_E: ndarray,
    r: Optional[int]=None,
    niter: Optional[int]=50,
    tol: Optional[float]=1e-6
):
    """
    An algorithm for Matrix Reconstruction from a partially revealed set.
    See "Matrix Completion from a Few Entries"(http://arxiv.org/pdf/0901.3150) for details
    Usage :
    X, S, Y, dist = OptSpace(A,r,niter,tol)
    Returns X, S and Y such that M_hat = X*S*Y'

    :param M_E: (matrix) The partially revealed matrix.
    :param r: (int) The rank to be used for reconstruction. Use [] to guess the rank.
    :param niter: (int) The max. no. of iterations. Use [] to use default (50).
    :param tol: Stop iterations if norm( (XSY' - M_E).*E , 'fro' )/sqrt(|E|) < tol, where
       - E_{ij} = 1 if M_{ij} is revealed and zero otherwise,
       - |E| is the size of the revealed set.

    :return X: A size(A,1)xr matrix
    :return S: An rxr matrix
    :return Y: A size(A,2)xr matrix
    :return dist: A vector containing norm( (XSY' - M_E).*E , 'fro' )/sqrt(|E|) at each successive iteration
    Date : 21st April, 2009
    COPYRIGHT 2009 Raghunandan H. Keshavan, Andrea Montanari, Sewoong Oh
    """

    # Process inputs
    M_E = csr_matrix(M_E)
    n, m = M_E.shape
    E = M_E.copy()  # type: csr_matrix
    E.data.fill(1)
    # eps = len(E.nonzero()[0]) / sqrt(m*n)

    if r is None:
        print('Rank not specified. Trying to guess ...')
        r = guessRank(M_E)
        print('Using Rank : %d' % r)

    m0 = 10000
    rho = 0

    rescal_param = sqrt(len(E.nonzero()[0]) * r / sparse_norm(M_E, 'fro') ** 2 )
    M_E *= rescal_param

    print('Trimming ...')
    # Trimming

    M_Et = M_E
    d_= E.sum(0).mean()
    for col in range(m):
        if E[:, col].sum() > 2 * d_:
            pos = E[:, col] > 0  # type: csr_matrix
            lst = pos.find()
            p = np.random.permutation(max(lst.shape))
            M_Et[lst(p[ceil(2 * d_):]), col] = 0

    d_= E.sum(1).mean()
    for row in range(n):
        if E[:, row].sum() > 2 * d_:
            pos = E[row, :] > 0  # type: csr_matrix
            lst = pos.find()
            p = np.random.permutation(max(lst.shape))
            M_Et[row, lst(p[ceil(2 * d_):])] = 0

    print('Sparse SVD ...')
    # Sparse SVD
    # noinspection PyTypeChecker
    X0, _, Y0 = svds(M_Et, r)

    X0 = np.fliplr(X0)
    Y0 = np.flipud(Y0)

    # Initial Guess
    X0 *= sqrt(n)
    Y0 *= sqrt(m)
    # S0 /= eps


    print('Iteration\tFit Error')

    # Gradient Descent
    X = X0
    Y = Y0.T
    S = getoptS(X, Y, M_E, E)

    dist = [norm( (M_E - X @ S @ Y.T) * E, 'fro') / sqrt(len(E.nonzero()[0]))]
    print('0\t\t%e' % dist[0] )

    for i in range(niter):

        # Compute the Gradient
        W, Z = gradF_t(X, Y, S, M_E, E, m0, rho)

        # Line search for the optimum jump length
        t = getoptT(X, W, Y, Z, S, M_E, E, m0, rho)
        X += t*W
        Y += t*Z
        S = getoptS(X, Y, M_E, E)

        # Compute the distortion
        dist.append(sparse_norm(E.multiply(M_E - X @ S @ Y.T), 'fro' ) / sqrt(len(E.nonzero()[0])))
        print('%d\t\t%e' % (i,dist[-1]))
        if dist[-1] < tol:
            break

    S /= rescal_param

    return X, S, Y, dist


# Function to Guess the Rank of the input Matrix
def guessRank(M_E: csr_matrix) -> int:
    n, m = M_E.shape
    epsilon = len(M_E.nonzero()[0])/sqrt(m*n)
    # noinspection PyTypeChecker
    _, S0, _ = svds(M_E, 100)
    S0 = np.fliplr([S0])[0]  # reverse

    S1 = S0[0:-2] - S0[1:-1]
    S1_ = S1 / S1[-10:].mean()
    r1 = 0
    lam = 0.05
    cost = np.empty(len(S1_))
    while r1 <= 0:
        for idx in range(len(S1_)):
            cost[idx] = lam*(S1_[idx:]).max() + idx
        i2 = np.argmin(cost)
        r1 = np.max(i2)
        lam += 0.05

        cost = np.empty(len(S0))
    for idx in range(len(S0) - 1):
        cost[idx] = (S0[idx+1] + sqrt((idx+1)*epsilon) * S0[0]/epsilon) / S0[idx]
    i2 = np.argmin(cost)
    r2 = np.max(i2)
    r = max(r1, r2)
    return r






def F_t(X: ndarray, Y: ndarray, S: ndarray, M_E: csr_matrix, E: csr_matrix, m0: int, rho: int):
    """
    Function to compute the distortion
    """
    n, r = X.shape

    t = E.multiply(X @ S @ Y.T - M_E)
    out1 = t.multiply(t).sum() / 2

    out2 =  rho * G(Y,m0,r)
    out3 =  rho * G(X,m0,r)
    out = out1 + out2 + out3
    return out

def G(X: ndarray, m0: int, r: int):
    z = np.square(X).sum(1) / (2 * m0 * r)
    y = exp(np.square(z-1)) - 1  # type: ndarray
    y[z < 1] = 0
    out = sum(y)
    return out





def gradF_t(X: ndarray, Y: ndarray, S: ndarray, M_E: csr_matrix, E: csr_matrix, m0: int, rho: int):
    """
    Function to compute the gradient
    """
    n, r = X.shape
    m ,r = Y.shape

    XS = X @ S
    YS = Y @ S.T
    XSY = XS @ Y.T

    Qx = X.T * (E.multiply(M_E - XSY)) @ YS / n
    Qy = Y.T * (E.multiply(M_E - XSY)).T @ XS / m

    W = (E.multiply(XSY - M_E)) @ YS + X @ Qx + rho * Gp(X, m0, r)
    Z = (E.multiply(XSY - M_E)).T @ XS + Y @ Qy + rho * Gp(Y,m0,r)
    return W, Z

def Gp(X: ndarray, m0: int, r: int):
    z = np.square(X).sum(1) / (2 * m0 * r)
    z = 2 * exp(np.square(z-1)) * (z-1)  # type: ndarray
    z[z < 0] = 0

    out = X * repmat(z,r,1).T / (m0*r)
    return out






def getoptS(X: ndarray, Y: ndarray, M_E: csr_matrix, E: csr_matrix):
    """
    Function to find Sopt given X, Y
    """

    n, r = X.shape
    C = X.T @ M_E @ Y
    C = C.flatten(order='F')

    A = np.empty((r ** 2, r ** 2))
    for i in range(r):
            for j in range(r):
                    ind = j * r + i
                    x = X[:,i].reshape(-1,1)
                    y = Y[:,j].reshape(1,-1)
                    temp = X.T @ (E.multiply(x @ y)) @ Y
                    A[:,ind] = temp.flatten(order='F')

    S = solve(A, C) # A \ C
    out = S.reshape(r, r, order='F')
    return out



def getoptT(X: ndarray, W: ndarray, Y: ndarray, Z: ndarray, S: ndarray, M_E: csr_matrix, E: csr_matrix, m0: int, rho: int):
    """
    Function to perform line search
    """

    norm2WZ = norm(W,'fro') ** 2 + norm(Z,'fro') ** 2
    f = [F_t(X, Y, S, M_E, E, m0, rho)]

    t = -1e-1
    for i in range(20):
            f.append(F_t(X+t*W, Y+t*Z, S, M_E, E, m0, rho))
            if f[-1] - f[0] <= .5 * t * norm2WZ:
                break
            t /= 2
    out = t
    return out

def do_stuff():
    from math import log10, sqrt
    from numpy import eye
    from numpy.random import randn, rand
    from scipy.io import loadmat, savemat
    from os.path import isfile

    # FILE = 'data_no_noise.mat'
    FILE = 'data_with_noise.mat'
    if isfile(FILE):
        d = loadmat(FILE, appendmat=False)
        m = d['m']
        n = d['n']
        M0 = d['M0']
        M_E = d['M_E']
    else:
        n = 1000
        m = 1000
        r = 3

        eps = 10 * r * log10(n)
        # Generate the matrix with the given parameters
        U = randn(n, r)
        V = randn(m, r)
        Sig = eye(r)
        M0 = U @ Sig @ V.T

        # Select the entries independently with prob. eps/sqrt(mn)
        E = 1 - ceil(rand(n, m) - eps / sqrt(m * n))
        M_E = M0 @ E

        savemat(FILE, {'m': m, 'n': n, 'M0': M0, 'M_E': M_E}, appendmat=False)



    # Call the OptSpace function
    X, S, Y, _ = OptSpace(M_E)

    # Compute the Frobenius norm
    err_nonoise = norm(X @ S @ Y.T - M0, 'fro') / sqrt(m * n)
    print('RMSE (without noise): %e' % err_nonoise)


if __name__ == '__main__':
    do_stuff()