import os
import pycurl
from typing import Optional
from urllib.parse import urlparse

# Same as doing `curl -X POST -F file=@<path-to-file> http://<host-ip>:8000`
def upload_file(
    url: str,
    file_path: str,
    field_name: str='file',
    retries: int=3,
    file_name: Optional[str]=None,
    proxy: Optional[str]=None
):
    """
    Uploads a file using a HTTP POST request simulating a single-field form.
    :param url: (str) The url to send the request to.
    :param file_path: (str) The path to the file to upload
    :param field_name: (str, optional, default='file') The form file field name.
    :param retries: (int) The number of retries in case of failures.
    :param file_name: (str, optional, default=original file name) The name the server will see for the uploaded file.
    :param proxy: (str, optional, default=no proxy is used) A proxy to use, format: [<user>[:<password>]]<host>[:<port>]
    :return:
    """
    for _ in range(retries):
        # noinspection PyBroadException
        try:
            c = pycurl.Curl()

            # Set proxy
            if proxy is not None:
                px = urlparse(proxy)
                c.setopt(pycurl.PROXY, px.hostname)
                if px.port is not None:
                    c.setopt(pycurl.PROXYPORT, px.port)
                if px.username is not None:
                    credentials = px.username
                    if px.password is not None:
                        credentials += ':' + px.password
                    c.setopt(pycurl.PROXYUSERPWD, credentials)

            # Setup file
            if file_name is None:
                file_name = os.path.basename(file_path)
            fields = [(field_name, (pycurl.FORM_FILE, file_path, pycurl.FORM_FILENAME, file_name))]
            c.setopt(c.HTTPPOST, fields)
            c.setopt(pycurl.WRITEFUNCTION, lambda x: None)

            # Perform
            c.setopt(c.URL, url)
            c.perform()
            c.close()
            break
        except:
            pass
