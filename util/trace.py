"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

trace.py

Created on: Jul 18, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https://github.com/samindaa/RLLib
"""

import abc
from collections import UserList
import numpy as np

from util.vector import SparseVector


class Trace(SparseVector):

    def __init__(self, num_features: int):
        super().__init__(num_features)
        self._after_update()

    def __iadd__(self, other):
        super().__iadd__(other)
        self._after_update()

    def __isub__(self, other):
        super().__isub__(other)
        self._after_update()

    def __imul__(self, other):
        super().__imul__(other)
        self._after_update()

    def __itruediv__(self, other):
        super().__itruediv__(other)
        self._after_update()

    @abc.abstractmethod
    def _after_update(self): pass


class AccumulatingTrace(Trace):

    def __init__(self, n_features: int, threshold: float=1e-8):
        self.threshold = threshold
        super().__init__(n_features)

    def _after_update(self):
        nz = self.nonzero()
        nzv = self[nz].toarray()
        bt = nz[np.where(nzv[0] <= 0.1)] # below threshold
        self[bt] = 0.0


class AccumulatingMaxTrace(AccumulatingTrace):

    def __init__(self, capacity: int, threshold: float=1e-8, maximum_value: float=1.0):
        super().__init__(capacity, threshold)
        self.maximum_value = maximum_value

    def _after_update(self):
        nz = self.data.nonzero()
        np.clip(self.data[nz], -self.maximum_value, +self.maximum_value, self.data[nz])


class MaxLengthTrace(Trace):

    def __init__(self, capacity: int, maximum_length: int):
        super().__init__(capacity)
        self.maximum_length = maximum_length

    def _after_update(self):
        v = self.data
        nz = v.nonzero()
        # noinspection PyTypeChecker
        if len(nz >= self.maximum_length):
            n_to_delete = self.maximum_length - len(nz)
            sorted_ii = np.argsort(v[nz])  # indices of indices of sorted entries, with respect to nz
            ii2d = sorted_ii[:n_to_delete]  # indices of the indices of the entries to delete, with respect to nz
            i2d = nz[ii2d]
            v[i2d] = 0.0


class Traces(UserList):

    def __init__(self, that: 'Traces'=None):
        super().__init__()
        if that is None:
            self.data = []
        else:
            self.data = that.data.copy()

    def clear(self):
        self.data = []
