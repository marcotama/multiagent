"""
This code is adapted from the code of Clement Gehring.

Link: https://github.com/gehring/tilecoding
authors: Clement Gehring, Marco Tamassia
contact: gehring@csail.mit.edu, tamassia.marco@gmail.com
date: May 2015
"""

import numpy as np

from util.vector import DenseVector, SparseVector
from util.hashing import Hashing, GoedelHashing
from typing import Tuple, Sized, Optional, Iterable, Union


class TileCoder:
    """
    Represents a series of layer of tile coding. This is equivalent to a single discretization of the input space.
    Link: https://webdocs.cs.ualberta.ca/~sutton/book/ebook/node88.html
    """

    def __init__(
        self,
        state_range: Tuple[Sized,Sized],
        n_tiles: Union[int,Iterable[int]],
        n_tilings: int,
        hashing: Optional[Hashing]=None,
        nullable: Optional[Sized]=None,
        scales: Union[str, Iterable[str]]= 'lin'
    ):
        """
        Constructor for a set of tilings.

        :param state_range: (tuple of two iterables of floats) the range of each dimension of the space. The number of
            dimension is inferred from this item.

        :param n_tiles:  (int or iterable of ints) the number of uniform divisions for each dimension (or all dimensions
            if a single integer is given). Each layer in this set will have the same number of divisions in each
            dimension. If an iterable is passed, it is expected to be consistent in size with the state_range argument.

        :param n_tilings: The number of layers in this set of tilings. Layers are offset by a fraction of a tile equal
            to 1 / n_tilings.

        :param hashing: (Hashing, optional) map from the individual bin index for each dimension to a tile index.
            By default, this is a bijective function, i.e., each combination is mapped to a unique index.
        """
        self.n_dimensions = len(state_range[0])
        if nullable is None:
            self.nullable = np.zeros(self.n_dimensions, dtype=bool)
        else:
            self.nullable = np.array(nullable, dtype=bool)
            assert len(self.nullable) == self.n_dimensions

        if isinstance(n_tiles, int):
            self.n_tiles = np.ones(self.n_dimensions, dtype=int) * n_tiles
        else:
            self.n_tiles = np.array(n_tiles, dtype=int)
            assert len(self.n_tiles) == self.n_dimensions

        if isinstance(scales, str):
            self.lin = np.ones(self.n_dimensions, dtype=bool) * (scales == 'lin')
            self.log = np.ones(self.n_dimensions, dtype=bool) * (scales == 'log')
        else:
            self.lin = np.array([scale == 'lin' for scale in scales], dtype=bool)
            self.log = np.array([scale == 'log' for scale in scales], dtype=bool)
            assert len(self.lin) == self.n_dimensions
            assert len(self.log) == self.n_dimensions

        self.features_min = state_range[0].copy()
        self.features_max = state_range[1].copy()
        self.features_range = self.features_max - self.features_min
        self.bases = np.power(self.features_range + 2, 1 / self.n_tiles)  # for log tiles
        self.ln_bases = np.log(self.bases)
        self.n_tilings = n_tilings

        self.offset = np.empty((self.n_dimensions, n_tilings))
        for i in range(self.n_dimensions):
            self.offset[i, :] = -np.linspace(0.0, 1.0 / self.n_tiles[i], n_tilings, endpoint=False)

        if hashing is None:
            self.hashing = GoedelHashing(self.n_tiles + self.nullable)
        else:
            self.hashing = hashing
        self.index_offset = (self.hashing.memory_size() * np.arange(self.n_tilings)).astype('int')

    def vector_size(self):
        return self.n_tilings * self.hashing.memory_size()

    def get_indices(self, state: np.ndarray):
        if isinstance(state, (DenseVector,SparseVector)):
            state = state.data[0]
        nan = np.isnan(state)
        not_nan = ~nan
        lin = self.lin & not_nan
        log = self.log & not_nan

        # noinspection PyTypeChecker
        if not (all(self.features_min[not_nan] <= state[not_nan]) and all(state[not_nan] <= self.features_max[not_nan])):
            raise ValueError("State is out of range; requirements: %s < %s < %s" % (self.features_min, str(state), self.features_max))
        norm_state = state.copy()
        norm_state[not_nan] -= self.features_min[not_nan]
        norm_state[lin] /= self.features_range[lin]
        norm_state[log] = np.log(norm_state[log] + 1) / self.ln_bases[log]
        norm_state[log] /= self.n_tiles[log]


        indices = np.empty((self.n_dimensions,self.n_tilings), dtype=int)
        indices[nan,:] = 0
        indices[not_nan,:] = (self.offset[not_nan,:] + norm_state[not_nan, np.newaxis]) * self.n_tiles[not_nan, np.newaxis]
        indices[not_nan,:] += self.nullable[not_nan,np.newaxis]
        return self.hashing.hash(indices) + self.index_offset
