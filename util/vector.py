"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

vector.py

Created on: Jul 12, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https:#github.com/samindaa/RLLib
"""

from enum import Enum
import abc
from typing import Union, Iterable
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg
import collections


def int_or_float (n):
    if n == int(n):
        return int(n)
    else:
        return float(n)


class Vector:
    class VectorType(Enum):
        BASE_VECTOR = 1
        DENSE_VECTOR = 2
        SPARSE_VECTOR = 3

    def __init__ (self, vector_type: VectorType) -> None:
        self.vector_type = vector_type
        self.data = None  # type: Union[np.array, sp.csr_matrix]

    @abc.abstractmethod
    def __len__ (self) -> int: pass

    @abc.abstractmethod
    def empty (self) -> bool: pass

    @abc.abstractmethod
    def max (self) -> float: pass

    @abc.abstractmethod
    def l1_norm (self) -> float: pass

    @abc.abstractmethod
    def l2_norm (self) -> float: pass

    @abc.abstractmethod
    def sum (self) -> float: pass

    @abc.abstractmethod
    def get_values (self) -> np.ndarray: pass

    @abc.abstractmethod
    def __neg__ (self) -> 'Vector': pass

    @abc.abstractmethod
    def __iter__ (self) -> Iterable[Union[int, float]]: pass

    @abc.abstractmethod
    def __getitem__ (self, i: int) -> Union[int, float]: pass

    @abc.abstractmethod
    def __matmul__ (self, other: 'Vector') -> Union[int, float]: pass

    @abc.abstractmethod
    def clear (self) -> None: pass

    @abc.abstractmethod
    # TODO the type of i could be a slice?
    def __setitem__ (self, i: int, v: Union[
        int, float, np.ndarray, np.float, np.int]) -> None: pass

    @abc.abstractmethod
    def __delitem__ (self, i: int) -> None: pass

    @abc.abstractmethod
    def __add__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __iadd__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __sub__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __isub__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __mul__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __imul__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __truediv__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __itruediv__ (self, other: Union[int, float, np.ndarray, np.float, np.int, 'Vector']) -> 'Vector': pass

    @abc.abstractmethod
    def __matmul__ (self, other: 'Vector') -> Union[int, float]: pass

    @abc.abstractmethod
    def __abs__ (self) -> 'Vector': pass

    @abc.abstractmethod
    def __str__ (self) -> str: pass

    @abc.abstractmethod
    def astype (self, dtype) -> 'Vector': pass

    @abc.abstractmethod
    def copy (self) -> 'Vector': pass

    @abc.abstractmethod
    def save (self, file_path: str) -> None: pass

    @abc.abstractmethod
    def load (self, file_path: str) -> None: pass

    def get_vector_type (self) -> VectorType:
        return self.vector_type

    @abc.abstractmethod
    def nonzero (self) -> np.array: pass


def _get_data (other: Union[int, float, Vector, np.ndarray]):
    if isinstance(other, (int, float, np.ndarray, np.float, np.int)):
        return other
    elif isinstance(other, (DenseVector, SparseVector)):
        return other.data
    else:
        raise NotImplementedError("Type unknown: %s" % str(type(other)))


class DenseVector(Vector):
    def __init__ (self, capacity: int = 1, data=None, init=None, dtype=float):
        super().__init__(Vector.VectorType.DENSE_VECTOR)
        if data is not None:
            if not isinstance(data, np.ndarray):
                data = np.array([data])
            self.data = data
        elif init is not None:
            self.data = np.ones((1, capacity), dtype=dtype) * init
        else:
            self.data = np.zeros((1, capacity), dtype=dtype)

    def __len__ (self):
        return self.data.size

    def __getitem__ (self, key):
        return self.data[0, key]

    def __setitem__ (self, key, value):
        self.data[0, key] = value

    def __delitem__ (self, key):
        del self.data[key]

    def __neg__ (self):
        return -self.data

    def __add__ (self, other):
        return DenseVector(data=self.data + _get_data(other))

    def __sub__ (self, other):
        return DenseVector(data=self.data - _get_data(other))

    def __mul__ (self, other):
        return DenseVector(data=self.data * _get_data(other))

    def __truediv__ (self, other):
        return DenseVector(data=self.data / _get_data(other))

    def __radd__ (self, other):
        return DenseVector(data=self.data + _get_data(other))

    def __rsub__ (self, other):
        return DenseVector(data=-self.data + _get_data(other))

    def __rmul__ (self, other):
        return DenseVector(data=self.data * _get_data(other))

    def __rtruediv__ (self, other):
        return DenseVector(data=(1.0 / self.data) * _get_data(other))

    def __iadd__ (self, other):
        self.data += _get_data(other)
        return self

    def __isub__ (self, other):
        self.data -= _get_data(other)
        return self

    def __imul__ (self, other):
        self.data *= _get_data(other)
        return self

    def __itruediv__ (self, other):
        self.data /= _get_data(other)
        return self

    def __matmul__ (self, other):
        if isinstance(other, SparseVector):
            return int_or_float(other.data @ self.data.T)
        else:
            return int_or_float(self.data @ other.data.T)  # TODO: check if, when other is sparse, sparse operations are called

    def __rmatmul__ (self, other):
        return int_or_float(other.data @ self.data.T)

    def __iter__ (self):
        return (int_or_float(el) for el in self.data[0])

    def __abs__ (self):
        return DenseVector(data=np.abs(self.data))

    def __str__ (self, n_decimals: int = 3):
        if n_decimals is None:
            return 'DenseVector(%s)' % str(next(iter(self.data.tolist())))
        else:
            return 'DenseVector([%s])' % ', '.join(
                ("%." + str(n_decimals) + "f") % f for f in next(iter(self.data.tolist())))

    def sum (self):
        return float(self.data.sum())

    def max (self, *args, **kwargs):
        return float(self.data.max(*args, **kwargs))

    def l1_norm (self):
        # noinspection PyTypeChecker
        return np.linalg.norm(self, ord=1)

    def l2_norm (self):
        # noinspection PyTypeChecker
        return np.linalg.norm(self, ord=2)

    def pos_bound (self):
        return DenseVector(data=np.clip(self.data, 0.0, np.inf))

    def get_values (self):
        return self.data

    def astype (self, dtype) -> 'Vector':
        return DenseVector(data=self.data.astype(dtype=dtype))

    def copy (self):
        return DenseVector(data=self.data.copy())

    def empty (self):
        return len(self) == 0

    def save (self, file_path: str):
        np.savez(file_path, self.data)

    def load (self, file_path: str):
        self.data = np.load(file_path)

    def clear (self):
        self.data[:] = 0

    def nonzero (self) -> np.array:
        return self.data.nonzero()[1]

    @staticmethod
    def exp (other: 'DenseVector'):
        return DenseVector(data=np.exp(_get_data(other)))


class SparseVector(Vector):
    def __init__ (self, capacity: int = 1, data=None, idx=None, dtype=float):
        super().__init__(Vector.VectorType.SPARSE_VECTOR)
        if data is not None:
            if idx is None:
                self.data = sp.csr_matrix(data, dtype=dtype)
            else:
                self.data = sp.csr_matrix((data, (np.zeros(len(idx)),idx)), shape=(1, capacity), dtype=dtype)
        else:
            self.data = sp.csr_matrix((1, capacity), dtype=dtype)

    def __iter__ (self):
        return iter(self._to_1d_array())  # TODO this creates a dense vector for iterating it - can it be improved?

    def __abs__ (self):
        return SparseVector(data=np.abs(self.data))

    def __neg__ (self):
        return -self.data

    def astype (self, dtype):
        return SparseVector(data=self.data.astype(dtype))

    def copy (self):
        return SparseVector(data=self.data.copy())

    def __len__ (self):
        return self.data.shape[1]

    def __getitem__ (self, key):
        return self.data[0, key]

    def __setitem__ (self, key: Union[int, slice], value):
        if isinstance(value, (int,float)):
            self.data[0, key] = value
        elif isinstance(value, (DenseVector, SparseVector)):
            nz = value.data.nonzero()[1]
            start = 0 if key.start is None else key.start
            stop = len(self) if key.stop is None else key.stop
            key = np.arange(start, stop, key.step)[nz]
            self.data[0, key] = value.data[0, nz]

    def __delitem__ (self, key):
        del self.data[key]

    def __add__ (self, other):
        other_data = _get_data(other)
        if isinstance(other_data, float):
            return DenseVector(data=self.data.todense() + other_data)
        else:
            return SparseVector(data=self.data + other_data)

    def __sub__ (self, other):
        other_data = _get_data(other)
        if isinstance(other_data, float):
            return DenseVector(data=self.data.todense() - other_data)
        else:
            return SparseVector(data=self.data - other_data)

    def __mul__ (self, other):
        return SparseVector(data=self.data.multiply(_get_data(other)))

    def __truediv__ (self, other):
        return SparseVector(data=self.data / _get_data(other))

    def __radd__ (self, other):
        return DenseVector(data=+self.data.todense() + _get_data(other))

    def __rsub__ (self, other):
        return DenseVector(data=-self.data.todense() + _get_data(other))

    def __rmul__ (self, other):
        return SparseVector(data=self.data * _get_data(other))

    # noinspection PyTypeChecker
    def __rtruediv__ (self, other):
        return SparseVector(data=np.asarray(1.0 / self.data.todense()) * _get_data(other))

    def __iadd__ (self, other):
        other_data = _get_data(other)
        if isinstance(other_data, float):
            return DenseVector(data=np.asarray(self.data.todense()) + _get_data(other))
        else:
            self.data += other_data
            return self

    def __isub__ (self, other):
        other_data = _get_data(other)
        if isinstance(other_data, float):
            return DenseVector(data=np.asarray(self.data.todense()) - _get_data(other))
        else:
            self.data -= other_data
            return self

    def __imul__ (self, other):
        self.data = self.data.multiply(_get_data(other))
        return self

    def __itruediv__ (self, other):
        self.data /= _get_data(other)
        return self

    def __matmul__ (self, other):
        return int_or_float(self.data.multiply(other.data).sum())

    def __rmatmul__ (self, other):
        return int_or_float(self.data.multiply(other.data).sum())

    def _to_1d_array (self, indices=None):
        if indices is None:
            return np.squeeze(np.asarray(self.data[0, :].todense()))
        else:
            return np.squeeze(np.asarray(self.data[0, indices].todense()))

    def __str__ (self, n_decimals: int = 3):
        nz = sorted(self.data.nonzero()[1])
        if n_decimals is None:
            return 'SparseVector(%s)' % str({i: self[i] for i in nz})
        else:
            nzf = self._to_1d_array(indices=nz)
            if len(nz) == 1:
                nzf = [nzf]
            return 'SparseVector({%s})' % ', '.join(
                ("%d: %." + str(n_decimals) + "f") % (i, f)
                for i, f in zip(nz, nzf))

    def sum (self):
        self.data.sum()

    def max (self):
        return self.data.todense().max()

    def empty (self) -> bool:
        return len(self) == 0

    def l1_norm (self) -> float:
        return scipy.sparse.linalg.norm(self.data, ord=1)

    # noinspection PyTypeChecker
    def l2_norm (self) -> float:
        return np.linalg.norm(self.data, ord=2)

    def get_values (self) -> np.ndarray:
        return self.data[0, :].todense()

    def save (self, file_path: str) -> None:
        np.savez(file_path, self.data)

    def load (self, file_path: str) -> None:
        self.data = np.load(file_path)

    def clear (self) -> None:
        self.data[self.data.nonzero()] = 0

    def nonzero (self) -> np.array:
        return self.data.nonzero()[1]

    @staticmethod
    def exp (other: 'SparseVector'):
        other_data = _get_data(other)
        if isinstance(other_data, scipy.sparse.csr_matrix):
            other_data = other_data.todense()
        return DenseVector(data=np.exp(other_data))


class Vectors(collections.UserList):
    def __init__ (self):
        super().__init__()

    def save (self, file_path: str) -> None:
        np.savez(file_path, self.data)

    def load (self, file_path: str) -> None:
        self.data = np.load(file_path)
