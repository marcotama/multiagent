"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

projector.py

Created on: Jul 12, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https://github.com/samindaa/RLLib
"""

import abc
import math
import numpy as np
from typing import List, Union, Optional, Tuple, Sized, Iterable

from util.tiles import TileCoder
from util.vector import Vector, DenseVector, SparseVector
from util.hashing import Hashing

MEM_C_TABLE = 2048

class Projector:
    """
    Feature extractor for function approximation.
    """
    @abc.abstractmethod
    def project(self, x: Vector, v: Optional[Vector]=None) -> Vector:
        """
        Projects the given state or state-action pair, where the action is represented by its integer id.
        :param x: (Vector) the state features.
        :param v: (Vector, optional) a pre-allocated vector, where the result will be stored, or None.
        :return: the projected state/state-action pair.
        """
        pass

    @abc.abstractmethod
    def vector_norm(self) -> int:
        """
        The number of non-zero elements in each projection.
        :return: the number of non-zero elements in each projection.
        """
        pass

    @abc.abstractmethod
    def vector_size(self) -> int:
        """
        The number of elements in each projection.
        :return: the number of elements in each projection.
        """
        pass


class TileCoderProjector(Projector):
    """
    Uses tile coding to project a vector.
    Optionally, also encodes an action using one-hot encoding.

    Link: https://webdocs.cs.ualberta.ca/~sutton/book/ebook/node88.html
    Link: https://en.wikipedia.org/wiki/One-hot
    """

    def __init__(self, n_tiles: Union[int,Iterable[int]], n_tilings: int, state_range: Tuple[Sized,Sized],
                 hashing: Optional[Hashing]=None, n_additional_features: int=0, include_bias: bool=True,
                 nullable: Optional[Sized]=None, scales: Union[str, Iterable[str]]='lin'):
        """
        :param n_tiles:
        :param n_tilings:
        :param state_range: A tuple with two iterables containing, respectively, min and max for each feature.
        :param hashing: An instance of Hashing, used to hash
        :param include_bias:
        """
        self.tile_coder = TileCoder(
            state_range=state_range,
            n_tiles=n_tiles,
            n_tilings=n_tilings,
            hashing=hashing,
            nullable=nullable,
            scales=scales
        )
        self.n_additional_features = n_additional_features
        self.include_bias = include_bias
        self.in_vec_size = self.tile_coder.vector_size()
        self.out_vec_size = self.tile_coder.vector_size() + n_additional_features + int(self.include_bias)
        self.in_vec_norm = self.tile_coder.n_tilings
        self.out_vec_norm = self.tile_coder.n_tilings + int(self.include_bias)

    def project(self, x: Vector, xb: Optional[Vector]=None) -> Vector:
        """
        Tile-codes x (and, optionally, a) into a Vector.
        Vector v is filled incrementally from the left; if len(v) > self.vector_size(), unused cells are =0.
        :param x: Values to be tile-coded. Should be ordered consistently with the state_range parameter passed to the constructor.
        :param xb: Additional values that should be appended to the tile-code of x.
        :return: A Vector containing the tile-coded values of state x (or of state-action pair (x,a)).
        """

        if x is None:
            return None

        # tile coded features
        idx = self.tile_coder.get_indices(x)  # type: np.ndarray
        val = np.ones(len(idx))  # type: List

        # additional features
        if xb is not None:
            nz = xb.nonzero()
            idx = np.append(idx, self.in_vec_size + nz)
            if isinstance(xb, SparseVector):
                # noinspection PyUnresolvedReferences,PyTypeChecker
                val = np.append(val, xb[nz].toarray()[0])
            elif isinstance(xb, (DenseVector, np.array)):
                # noinspection PyUnresolvedReferences,PyTypeChecker
                val = np.append(val, xb[nz])
            else:
                raise Exception('Unknown type of x: %s' % type(x))

        # bias
        if self.include_bias:
            idx = np.append(idx, [self.out_vec_size - 1])
            val = np.append(val, [1.0])

        v = SparseVector(self.out_vec_size, data=val, idx=idx)

        return v

    def vector_norm(self):
        """
        Returns the norm of vectors returned by this projector; that is, the number of cells set to 1.
        This is not reliable if a Hashing was passed to the constructor.
        :return: the norm of vectors returned by this projector.
        """
        return self.out_vec_norm

    def vector_size(self):
        """
        Returns the minimum size of vectors returned by this projector.
        :return: the minimum size of vectors returned by this projector.
        """
        return self.out_vec_size


class FourierBasis(Projector):

    def __init__(self, n_inputs: int, order: int, actions: int):
        self.coefficient_vectors = []
        self._compute_fourier_coefficients(n_inputs, order)
        self.feature_vector = DenseVector(len(self.coefficient_vectors) * actions)

    def _compute_fourier_coefficients(self, n_inputs: int, order: int):
        coefficient_vector = DenseVector(n_inputs)
        while coefficient_vector[0] <= order:
            new_coefficient_vector = DenseVector(n_inputs, init=coefficient_vector)
            self.coefficient_vectors.append(new_coefficient_vector)
            self._next_coefficient_vector(coefficient_vector, n_inputs, order)
        assert len(self.coefficient_vectors) == math.pow(order + 1, n_inputs)

    def _next_coefficient_vector(self, coefficient_vector: Vector, n_inputs, order: int):
        coefficient_vector[n_inputs - 1] += 1
        if coefficient_vector[n_inputs - 1] > order:
            if n_inputs > 1:
                coefficient_vector[n_inputs - 1] = 0
                self._next_coefficient_vector(coefficient_vector, n_inputs - 1, order)

    def project(self, x: Vector, h1: int=None) -> Vector:
        # x must be unit normalized [0, 1)
        assert 0.0 <= x.l2_norm() < 1.0
        if h1 is None:
            h1 = 0
        self.feature_vector.clear()
        if x.empty():
            return self.feature_vector
        strip_width = len(self.coefficient_vectors) * h1
        for i in range(len(self.coefficient_vectors)):
            self.feature_vector[i + strip_width] = math.cos(math.pi * x @ self.coefficient_vectors[i])
        return self.feature_vector

    def vector_norm(self):
        return len(self.feature_vector)  # can this be done better?

    def vector_size(self):
        return len(self.feature_vector)

    def get_coefficient_vectors(self) -> List[Vector]:
        return self.coefficient_vectors
