"""
Copyright 2016 Marco Tamassia (tamassia.marco@gmail.com)

Licensed under the Apache License, 2.0 (the "License")
you may not use self file except in compliance with the License.
You may obtain a copy of the License at

http:#www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
See the License for the specific language governing permissions and
limitations under the License.

mathema.py

Created on: Jul 12, 2016
Author: Marco

This library is an adaptation of the RLLib by Saminda Abeyruwan (saminda@cs.miami.edu).
The original code is available at:
https:#github.com/samindaa/RLLib
"""

import collections
import copy
import math
import numpy as np
import random
import sys
from typing import Sequence, Union, Mapping, Hashable, List

from util.vector import Vector


class Boundedness:

    @staticmethod
    def check_value(v: float) -> bool:
        return not math.isnan(v) and not math.isinf(v)

    @staticmethod
    def check_distribution(distribution: Vector) -> bool:
        return math.fabs(1.0 - distribution.sum()) < 1e-6  # for stability


# Helper class for range management for testing environments
class Range:

    def __init__(self, min_value=sys.float_info.min, max_value=sys.float_info.max):
        self.min_value = min_value
        self.max_value = max_value

    def bound(self, v: float) -> float:
        return max(self.min_value, min(self.max_value, v))

    def __contains__(self, v: float) -> bool:
        return self.min_value <= v <= self.max_value

    def length(self) -> float:
        return self.max_value - self.min_value

    def min(self) -> float:
        return self.min_value

    def max(self) -> float:
        return self.max_value

    def center(self) -> float:
        return self.min() + (self.length() / 2.0)

    def choose(self) -> float:
        return random.randrange(self.min(), self.max())

    def to_unit(self, v) -> float:
        return (self.bound(v) - self.min()) / self.length()

    def to_range(self, v, other) -> float:
        return (self.bound(v) - self.min()) / self.length() * other.length() + other.min()


class Ranges(collections.UserList):

    def __init__(self):
        super().__init__()


class Angle:
    """Normalize to [-pi, pi)"""
    @staticmethod
    def normalize(data: float) -> float:
        x = math.fmod(data + math.pi, 2.0 * math.pi)
        if x < 0:
            x += 2.0 * math.pi
        return x - math.pi


def random_weighted(weights: Union[Sequence[float], 'np.ndarray', Mapping[Hashable, float]]):
    keys = None
    if isinstance(weights, List[float]):
        weights = np.array(weights)
    elif isinstance(weights, Mapping[Hashable, float]):
        keys, tmp_w = [], []
        for k, v in weights.items():
            keys.append(k)
            tmp_w.append(v)
        weights = np.array(tmp_w)
    elif isinstance(weights, np.ndarray):
        pass
    else:
        raise ValueError("Unexpected type for weights: %s" % str(weights))

    weights /= weights.sum()

    choice = np.random.choice(len(weights), p=weights)
    if keys is not None:
        return keys[choice]


def random_weighted_no_rep(choices, n):
    if n > len(choices):
        raise Exception("Can not select %d items from %s" % (n, choices))
    choices = copy.deepcopy(choices)
    selected = []
    for i in range(n):
        sel = random_weighted(choices)
        selected.append(sel)
        del choices[sel]
    return selected
