# -*- coding: utf-8 -*-
"""
Types used throughout the codebase.

@author: marco
"""
from typing import Union, Dict, Set, Iterable, Tuple, List, Optional, FrozenSet, Hashable
from util.vector import Vector
import numpy as np


class MyTypes:
    Feature = str
    StateCode = int
    ActionCode = int
    OptionCode = str
    ActionOptionCode = Union[ActionCode, OptionCode]
    StatesSet = Set[StateCode]
    ActionsSet = Set[ActionCode]
    FeatureValues = Dict[Feature, Union[float, int, str, None]]
    FeatureValuesTuple = Tuple[Feature, Union[float, int, str, None]]
    FactoredState = FeatureValues
    FactoredAction = FeatureValues
    PacmanState = FactoredState
    PacmanAction = str
    State = FeatureValues
    Action = FeatureValues
    NumpyArray1D = np.ndarray
    NumpyArray2D = np.ndarray
    NumpyArray3D = np.ndarray
    NumpyTermProbTable = NumpyArray1D
    NumpyPolicy = NumpyArray1D
    NumpyVTable = NumpyArray1D
    NumpyRTable = NumpyArray1D
    NumpyNTable = NumpyArray1D
    NumpyQTable = NumpyArray2D
    NumpyPTable = NumpyArray3D
    QTable = Dict[StateCode, Dict[ActionCode, float]]
    HTable = Dict[StateCode, Dict[ActionCode, List[float]]]
    NTable = Dict[StateCode, Dict[ActionCode, int]]
    PTable = Dict[StateCode, Dict[ActionCode, Dict[StateCode, float]]]
    TTable = Dict[StateCode, Dict[ActionCode, Dict[StateCode, int]]]
    RTable = Dict[StateCode, Dict[ActionCode, float]]
    QOptionsTable = Dict[StateCode, Dict[OptionCode, float]]
    NOptionsTable = Dict[StateCode, Dict[OptionCode, int]]
    POptionsTable = Dict[StateCode, Dict[OptionCode, Dict[StateCode, float]]]
    QMixedTable = Dict[StateCode, Dict[Union[ActionCode, OptionCode], float]]
    NMixedTable = Dict[StateCode, Dict[Union[ActionCode, OptionCode], int]]
    PMixedTable = Dict[StateCode, Dict[Union[ActionCode, OptionCode], Dict[StateCode, float]]]
    PacmanTraceStep = Tuple[PacmanState, PacmanAction]
    PacmanTrace = Iterable[PacmanTraceStep]
    PacmanDemo = Tuple[PacmanTrace, float, float]
    Trace = List[Tuple[StateCode, List[ActionCode], ActionCode, float]]
    TraceWithNextStep = List[Tuple[StateCode, List[ActionCode], ActionCode, float, StateCode, List[ActionCode]]]
    Demo = List[Tuple[StateCode, ActionCode]]

    StateAsVector = Vector
    StateActionAsVector = Vector


State = Optional[Dict[str, Optional[Hashable]]]
HashedState = Optional[FrozenSet[Tuple[str, Optional[Hashable]]]]
Action = Hashable
OptionName = str