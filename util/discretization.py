import math
from typing import Optional, List

def sign(value):
    if value > 0:
        return +1
    elif value < 0:
        return -1
    else:
        return 0

def discretize_log (value: float, max_value: float, n_intervals: int, cut_overflow: bool = True,
                    smallest_interval: float = None):
    """
    Discretizes a value using a logarithmic scale. The given value must be in range (0, max_value]. The scale will
    be divided in n_intervals intervals. Successive intervals increase exponentially in size.

    Example: Let's suppose we are using a log-scale with 5 intervals going from 0 to 243. The intervals are as
    follows:
    0) (0, 3)
    1) [3, 9)
    2) [9, 27)
    3) [27, 81)
    4) [81, 243]

    Let's imagine we are discretizing the value 10. Such value should fall in the third interval (=2, because
    intervals are counted from 0.
    The correct call to this method is: discretize_log(value=10, max_value=243, n_intervals=5). Let's see how this
    is processed:
    - If value == max_value, n_intervals - 1 is returned
    - First, the base for the exponential trend is calculated as 3.0 (because 3.0 ^ 5 = 243)
    - Then the exact position in log-scale of the value is calculated as 2.095903274289385 (because that is the log
    of 10 in base 3.0)
    - Finally, the log-scale value is converted to integer (the decimal part is truncated), i.e., the result is 2.


    WARNING: Make sure value > 0, as log(0) is not defined.

    :param value: The value to be mapped in a log-scale
    :param max_value: The maximum value in the scale, inclusive (i.e., value can be == max_value).
    :param n_intervals: The number of intervals to be used in the scale
    :param cut_overflow: if True, values > max_value will be mapped to the highest interval; if False ValueError
    will be raised
    :param smallest_interval: size of the smallest interval; if None, the smallest interval size will equal the log
    base.
    :return: The index corresponding to the log-scaled interval in which the input value falls.
    :raises ValueError: if value is out of the specified range (that is, if value > max_value) and cut_overflow is
    False.
    """
    if not cut_overflow and not 0 < value <= max_value:
        raise ValueError("Value %f is out of range (0, %f]. Discretization failed." % (value, max_value))

    if value >= max_value:
        return n_intervals - 1

    if smallest_interval is None:
        base = math.pow(max_value, 1 / n_intervals)
        log_position = math.log(value, base)
    else:
        base = math.pow(max_value / smallest_interval, 1 / n_intervals)
        log_position = math.log(value / smallest_interval, base)
    return int(log_position) if log_position >= 0 else 0


def discretize_log_signed(value: float, max_value: float, n_intervals: int, cut_overflow: bool = True,
                           smallest_interval: float = None):
    """
    Discretizes a value using a logarithmic scale. The given value must be in range [-max_value, +max_value],
    excluding 0. The scale will be divided in n_intervals intervals on each side of 0, for a total of 2 * n_intervals
    intervals. Successive intervals increase exponentially in size as the distance from 0 increases.

    Example: Let's suppose we are using a log-scale with 5 intervals going from -243 to +243. The intervals are as
    follows:
    -5) [-243, -81]
    -4) (-81, -27]
    -3) (-27, -9]
    -2) (-9, -3]
    -1) (-3, 0)
    0) (0, +3)
    +1) [+3, +9)
    +2) [+9, +27)
    +3) [+27, +81)
    +4) [+81, +243]

    WARNING: Make sure value != 0, as log(0) is not defined.

    :param value: The value to be mapped in a log-scale
    :param max_value: The maximum value in the scale, inclusive (i.e., value can be == max_value).
    :param n_intervals: The number of intervals to be used in the scale
    :param cut_overflow: if True, value > +max_value will be mapped to the highest interval and value < -max_value
    will be mapped to the lowest interval; if False ValueError will be raised
    :param smallest_interval: size of the smallest interval; if None, the smallest interval size will equal the log
    base.
    :return: The index corresponding to the log-scaled interval in which the input value falls.
    :raises ValueError: if value is out of the specified range (-max_value < value <= +max_value) and cut_overflow
    is False or if value == 0.
    """
    if not cut_overflow and not -max_value <= value <= +max_value:
        raise ValueError("Value %f is out of range [%f, %f]. Discretization failed." % (value, -max_value, +max_value))
    if value == 0:
        raise ValueError("Value %f is zero. Discretization failed." % value)

    if value >= +max_value:
        return n_intervals - 1
    if value <= -max_value:
        return -n_intervals

    if smallest_interval is None:
        base = math.pow(max_value, 1 / n_intervals)
        log_position = math.log(abs(value), base)
    else:
        base = math.pow(max_value / smallest_interval, 1 / n_intervals)
        log_position = math.log(abs(value) / smallest_interval, base)
    if value > 0:
        return int(log_position) if log_position >= 0 else 0
    else:
        return -int(log_position) - 1 if log_position >= 0 else -1


def discretize_bipartite(value: Optional[float], threshold: float):
    """
    Discretizes a value using a threshold. Returns one of 5 values:
    -2 if value < -threshold
    -1 if -threshold <= value < 0
    0 if value == 0
    +1 if 0 < value <= +threshold
    +2 if value > +threshold
    If None is passed, None is returned.

    :param value: The value to discretize
    :param threshold: The threshold to use for discretization
    :return: The bin containing value
    """
    if value is None:
        return None
    elif value < -threshold:
        return -2
    elif -threshold <= value < 0:
        return -1
    elif 0 < value <= +threshold:
        return +1
    elif value > +threshold:
        return +2
    else:
        return 0


def discretize_intervals(value: Optional[float], thresholds: List[float]):
    """
    Discretizes a value using a list of thresholds. The thresholds should be of increasing value.
    This function returns:
    - None if value is None
    - 0 if value == 0
    - +len(thresholds) if value is greater than all thresholds and value > 0
    - -len(thresholds) if abs(value) is greater than all thresholds and value < 0
    - +i+1 where i is the smallest index such that thresholds[i] > value if value > 0
    - -i-1 where i is the smallest index such that thresholds[i] > abs(value) if value < 0


    :param value: The value to discretize
    :param thresholds: The thresholds to use for discretization
    :return: The bin containing value
    """
    if value is None:
        return None
    elif value == 0:
        return 0

    found_i = len(thresholds)
    for i, threshold in enumerate(thresholds):
        if threshold > value:
            found_i = i + 1
            break
    return found_i * sign(value)