"""
Code adapted from: https://programmingideaswithjake.wordpress.com/2015/05/23/python-decorator-for-simplifying-delegate-pattern/
"""

from typing import Set


class DelegatedAttribute:
    def __init__(self, delegate_name, attr_name):
        self.attr_name = attr_name
        self.delegate_name = delegate_name

    def __get__(self, instance, owner):
        if instance is None:
            return self
        else:
            # return instance.delegate.attr
            return getattr(self.delegate(instance),  self.attr_name)

    def __set__(self, instance, value):
        # instance.delegate.attr = value
        setattr(self.delegate(instance), self.attr_name, value)

    def __delete__(self, instance):
        delattr(self.delegate(instance), self.attr_name)

    def delegate(self, instance):
        return getattr(instance, self.delegate_name)

    def __str__(self):
        return ''


def delegate_attrs(to, attributes=frozenset()):
    """
    Function to decorate an object (usually a class) adding attributes that are delegated to another object
    (the "delegate"). The delegate is assumed to be an internal attribute of the decorated class.
    The function takes the name of the delegate and a list of attributes to be delegated. The setup of the delegate is
    not responsibility of this class.

    :param to: the name of the delegate attribute
    :param attributes: the attributes to delegate to the delegate attribute
    :return: the decorated object
    """
    if not isinstance(attributes, Set):
        attributes = set(attributes)

    def inner(cls):
        # don't overwrite attributes that the class already has
        attrs = attributes - set(cls.__dict__.keys())

        for attr in attrs:
            setattr(cls, attr, DelegatedAttribute(to, attr))
        return cls

    return inner