# -*- coding: utf-8 -*-
"""
Some useful functions.

@author: Marco Tamassia
"""
import random
import copy
import numpy as np
import pickle
from re import finditer
from typing import Iterable, List, Tuple, Any, Dict, Hashable, Callable, TypeVar, FrozenSet, Optional
from math import isinf
from scipy.stats import t
import os
from os.path import exists, dirname
from os import makedirs
from collections import defaultdict
from functools import partial

# Black magic to have depth 2 and depth 3 defaultdicts
def recursive_default_dict(factory: Callable[[], Any], depth: int):
    if depth == 1:
        return defaultdict(factory)
    else:
        return defaultdict(partial(recursive_default_dict, factory=factory, depth=depth-1))
defaultdict2 = partial(recursive_default_dict, depth=2)
defaultdict3 = partial(recursive_default_dict, depth=3)

def get_basename_no_ext(path):
    """Returns the basename of a given path without extension; that is, the
    name of the file without extension.
    :type path: str"""
    return get_basename(strip_ext(path))


def strip_ext(path):
    """Returns the given path without file extension.
    :type path: str"""
    return os.path.splitext(path)[0]


def get_basename(path):
    """Returns the basename of a given path; that is, the name of the file.
    :type path: str"""
    return os.path.basename(path)


def aggregateDicts(*dicts):
    aggDict = {}
    for d in dicts:
        for key in d:
            aggDict.setdefault(key, [])
            aggDict[key].append(d[key])
    return aggDict

def sign(x):
    if x > 0:
        return +1
    elif x < 0:
        return -1
    else:
        return 0

def normalized_dict(d):
    tot = sum(d.values())
    if tot > 0:
        n = {k: v / tot for k, v in d.items()}
    else:
        n = {k: 0.0 for k in d}
    return n


def combineStatistics(
    statistics: Iterable[Tuple[int, float, float]]
) -> Tuple[int, Optional[float], Optional[float]]:
    cumN = sum((n for n, _, _ in statistics)) # type: int
    if cumN == 0:
        return cumN, None, None
    cumMean = sum((n * mean for n, mean, _ in statistics)) / cumN
    if cumN == 1:
        return cumN, cumMean, None
    a = cumN - 1
    b = sum(((n - 1) * v + n * m * m for n, m, v in statistics))
    c = cumN * cumMean * cumMean
    if b - c >= 0:
        cumVariance = (b - c) / a
    else:
        cumVariance = 0.0
    return cumN, cumMean, cumVariance


def confidenceInterval(
    n: int,
    mean: float,
    variance: float,
    alpha: float
) -> Tuple[float, float]:
    """
    Given a dictionary of samples lists, returns a two dictionaries with
    confidence interval lower and upper bounds respectively.
    :param n: size of the sample
    :param mean: mean of the sample
    :param variance: variance of the sample
    :param alpha: alpha value for the confidence interval (1-confidence)
    """
    if n >= 2:
        if variance == 0:
            return mean, mean
        else:
            return t.interval(1-alpha, n-1, mean, np.sqrt(variance/n))
    else:
        return float("-inf"), float("+inf")


def confidenceIntervalFromSample(
    sample: List[float],
    alpha: float
) -> Tuple[float, float]:
    """
    Given a dictionary of samples lists, returns a two dictionaries with
    confidence interval lower and upper bounds respectively.
    :param sample:
    :param alpha: alpha value for the confidence interval (1-confidence)
    """
    n = len(sample)
    mean = np.mean(sample)
    variance = np.var(sample,ddof=1)
    return t.interval(1-alpha, n-1, mean, np.sqrt(variance/n))
                 
def allPairsShortestPath(
        graph: Dict[Hashable, Dict[Hashable, float]]
) -> Tuple[Dict[Hashable, float], Dict[Hashable, Hashable]]:
    """
    Implements the Floyd-Warshall algorithm for all-pairs distance calculation.
    :param graph:
    """

    # Initialize dist and pred:
    # copy graph into dist, but add infinite where there is
    # no edge, and 0 in the diagonal
    dist = {}
    pred = {}
    for u in graph:
        dist[u] = {}
        pred[u] = {}
        for v in graph:
            dist[u][v] = float("inf")
            pred[u][v] = -1
        dist[u][u] = 0
        for neighbor in graph[u]:
            dist[u][neighbor] = graph[u][neighbor]
            pred[u][neighbor] = u

    for w in graph:
        # given dist u to v, check if path u - w - v is shorter
        for u in graph:
            for v in graph:
                new_dist = dist[u][w] + dist[w][v]
                if new_dist < dist[u][v]:
                    dist[u][v] = new_dist
                    pred[u][v] = pred[w][v]  # route new path through w

    return dist, pred


def cmp(x, y):
    if x < y:
        return -1
    elif x > y:
        return +1
    else:
        return 0


def weightedChoice(
    choices: Dict[Hashable, float]
):
    if any((c for c, w in choices.items() if isinf(w))):
        raise ValueError("Cannot use infinite weights")
    total = sum(choices.values())
    r = random.uniform(0, total)
    for c, w in choices.items():
        r -= w
        if r <= 0:
            return c
    raise ValueError("Not sure what happened... here is what you passed me", choices)


def weightedChoicesNoRepetition(choices, n):
    if n > len(choices):
        raise Exception("Can not select %d items from %s" % (n, choices))
    choices = copy.deepcopy(choices)
    selected = []
    for i in range(n):
        sel = weightedChoice(choices)
        selected.append(sel)
        del choices[sel]
    return selected


def sigmoid(
    mid: float,
    width: float,
    minV: float,
    maxV: float,
    descending: bool,
    x: np.ndarray
):
    """
    Returns a sigmoid function that is centered in mid, has maximum amplitude maxV
    and qualitatively reaches the top value at width.
    :param mid: mid point of the sigmoid
    :param width: approximate width of the sigmoid
    :param minV: minimum value
    :param maxV: maximum value
    :param descending: whether or not the sigmoid should be descending
    :param x: x to compute sigmoid(d)
    """
    d = 1 if descending else -1
    np.seterr(over='ignore')
    s = lambda _x: 1 / (1 + np.exp(d * _x))
    v = minV + (maxV - minV) * s((x - mid) * (3 / width))  # type: np.ndarray
    above = v > maxV  # type: np.ndarray
    if np.any(above):
        v[above] = maxV
    below = v < minV  # type: np.ndarray
    if np.any(below):
        v[below] = minV
    return v

def pickle_save(path, data, mode='wb'):
    if path and dirname(path) != '' and not exists(dirname(path)):
        makedirs(dirname(path))
    with open(path, mode) as f:
        pickle.dump(data, f)


def pickle_load(path):
    with open(path, 'rb') as f:
        data = pickle.load(f)
    return data


def loadChunk(f):
    while True:
        try:
            yield pickle.load(f)
        except EOFError:
            pass


def matchTuples(
    match: Tuple[Any, Any],
    tuples: Iterable[Tuple[Any, Any]]
) -> List[Tuple[Any, Any]]:
    """
    Returns all the tuples that match every field of the given
    argument.
    :param match: tuple to match
    :param tuples: tuples to check for matching
    """
    matches = []
    for candidate in tuples:
        if matchTuple(match, candidate):
            matches.append(candidate)
    return matches

def matchTuple(
    match: Tuple[Any, Any],
    candidate: Tuple[Any, Any]
) -> bool:
    """
    Returns if the candidate tuple contains all the fields of the match
    tuple and if they have the same value.
    :param match: tuple to match
    :param candidate: tuple to check for matching
    """
    for mf, mv in match:
        featurePresent = False
        for cf, cv in candidate:
            if cf == mf:
                if cv != mv:
                    return False
                featurePresent = True
        if not featurePresent:
            return False
    return True




def load_agents(
    folder: str
):
    groupedAgents = {}
    for subFolder in os.listdir(folder):
        if not os.path.isdir(folder + subFolder):
            continue
        if not subFolder.endswith("/"):
            subFolder += "/"
        label = subFolder[:-1]

        groupedAgents[label] = []
        for file in os.listdir(folder + subFolder):
            if not file.endswith(".agent"):
                continue
            agent = pickle_load(folder + subFolder + file)
            groupedAgents[label].append(agent)
    return groupedAgents

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

def numify(value):
    if isint(value):
        return int(value)
    elif isfloat(value):
        return float(value)
    else:
        return value

K = TypeVar('K')
V = TypeVar('V')
def project(
    dictionary: Dict[K,V],
    keys: Iterable[Hashable]
) -> Dict[K,V]:
    """
    Filters entries in a dictionary returning only those whose key is in the keys parameter.
    :param dictionary: the dictionary to filter
    :param keys: the keys of the entry to be preserved
    :return: a dictionary containing only entries of the original dictionary whose key is in the keys parameter
    """
    return {f: v for f, v in dictionary.items() if f in keys}

K = TypeVar('K')
V = TypeVar('V')
def hash_dictionary(
    dictionary: Dict[K,V],
) -> FrozenSet[Tuple[K,V]]:
    return frozenset(dictionary.items())

class KeyedDefaultDict(defaultdict):
    def __missing__(self, key):
        if self.default_factory is None: raise KeyError((key,))
        self[key] = value = self.default_factory(key)
        return value

# from http://stackoverflow.com/a/33002123
def cohen_d(x,y):
    nx = len(x)
    ny = len(y)
    dof = nx + ny - 2
    return (np.mean(x) - np.mean(y)) / np.sqrt(((nx-1)*np.std(x, ddof=1) ** 2 + (ny-1)*np.std(y, ddof=1) ** 2) / dof)

def camel_case_split(identifier):
    matches = finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]