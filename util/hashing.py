from abc import abstractmethod
import numpy as np

class Hashing:
    """
    Hashing an array of indices to a single index. Mostly used for tile coding.
    """
    def __init__(self):
        """
        Hash several indices (typically, one per dimension) onto one index (typically, index of a tile). This could be a
        simple cartesian-product, i.e., unique index for every combination, or some sort of randomized hash function,
        e.g., UNH.

        Must be able to deal with 2D arrays of indices.
        """
        pass

    @abstractmethod
    def hash(self, indices: np.array) -> np.ndarray:
        """
        Hashes the given array column by column, returning a row of hashes.
        :type indices: np.array(shape=(n_dimensions,n_tilings) or shape=(n_dimensions))
        :return: np.array(shape=(n_tilings))
        """
        pass

    @abstractmethod
    def memory_size(self) -> int:
        pass


class UNH(Hashing):
    # constants were taken from rlpark's implementation.

    increment = 470

    def __init__(self, memory):
        super(UNH, self).__init__()
        self.rnd_seq = np.zeros(16384, dtype='int')
        self.memory = int(memory)
        for i in range(4):
            self.rnd_seq[:] = self.rnd_seq << 8 | np.random.randint(np.iinfo('int16').min,
                                                                    np.iinfo('int16').max,
                                                                    16384) & 0xff

    def hash(self, indices):
        if len(indices.shape) == 1:
            indices = indices[:,np.newaxis]
        rnd_seq = self.rnd_seq
        a = self.increment * np.arange(indices.shape[1])
        index = indices + a[np.newaxis, :]
        index %= rnd_seq.size
        hashed_index = (np.sum(rnd_seq[index], axis=0)).astype(np.int)
        hashed_index %= self.memory
        return hashed_index

    def memory_size(self):
        return self.memory


class GoedelHashing(Hashing):
    def __init__(self, dims, wrap=False):
        super().__init__()
        self.memory = np.prod(dims)
        self.dims = dims.astype(int)
        self.wrap = wrap
        self.dim_offset = np.cumprod(np.hstack(([1], self.dims[:-1]))).astype(int)[::-1, np.newaxis]

    def hash(self, indices):
        if len(indices.shape) == 1:
            indices = indices[:,np.newaxis]
        if self.wrap:
            indices = np.remainder(indices, self.dims[:, np.newaxis])
        else:
            indices = np.clip(indices, 0, self.dims[:, np.newaxis] - 1)
        return np.sum(indices * self.dim_offset, axis=0)

    def memory_size(self):
        return self.memory
