from typing import Optional, Any

def load_class(class_name: str, package_name: str) -> Optional[Any]:
    """
    Returns a class.
    :param class_name: the class name
    :param package_name: the package where the module of the class is implemented
    :return: the specified class if it was found, None otherwise
    """
    from importlib import import_module
    from pkgutil import iter_modules

    package = import_module(package_name)
    for _, module_name, is_a_package in iter_modules(package.__path__):
        if not is_a_package:
            module = import_module("%s.%s" % (package_name, module_name))
            if hasattr(module, class_name):
                return getattr(module, class_name)
    raise ModuleNotFoundError('No module in package %s implements class %s.' % (package_name, class_name))
