This is the code I used for work on Reinforcement Learning during my PhD.

Requirements
-

To succesfully run the software, you should first run:
```
apt-get install python3 python3-pip gfortran libblas-dev liblapack-dev python3-tk
pip3 install numpy scipy tabulate mypy-lang tk dropbox
(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - http://pi.dk/3) | bash
```




To allow for non-interactive ssh login:
-Public keys have to be setup in workers
   `ssh-copy-id -i $PUBLIC_KEY_FILE $SERVER_USER@$SERVER_HOST`
-Private key has to be setup in the client
   `cp $IDENTITY_FILE ~/.ssh/id_rsa`

Useful ssh commands:
-

- Generate keys pair: `ssh-keygen -t rsa`

- Extract public key from identity file: `ssh-keygen -y -f $IDENTITY_FILE > $PUBLIC_KEY_FILE`

- Copy public key to a host: `ssh-copy-id -i $PUBLIC_KEY_FILE $SERVER_USER@$SERVER_HOST`

- Setup public key for SSH user: `cp $PUBLIC_KEY_FILE ~/.ssh/id_rsa.pub`

- Setup identity for SSH user: `cp $IDENTITY_FILE ~/.ssh/id_rsa`


Tweak ssh configs
-

To make sessions more stable run
-on the server(s)
```
echo "ClientAliveInterval 30" | sudo tee -a /etc/ssh/sshd_config
echo "ClientAliveMaxCount 1200" | sudo tee -a /etc/ssh/sshd_config
```
-on the client
```
echo "Host *" >> ~/.ssh/config
echo "  ServerAliveInterval 30" >> ~/.ssh/config
echo "  ServerAliveCountMax 1200" >> ~/.ssh/config
```
(source: http://askubuntu.com/a/354245)


PyInstaller instructions
-

If building with PyInstaller, `rest.py` needs to be modified. Specifically:
1) Open (with root privileges) `/usr/local/lib/python3.4/dist-packages/dropbox/rest.py`
2) Find the line containing:
  `TRUSTED_CERT_FILE = pkg_resources.resource_filename(__name__, 'trusted-certs.crt')`
3) Comment it and add another line containing:
  `TRUSTED_CERT_FILE = 'trusted-certs.crt'`
4) Save and close
5) Build with: `pyinstaller -F record_pacman_game.spec record_pacman_game.py`
