from util.hashing import UNH
from util.tiles import TileCoder
import numpy as np

import unittest

class TestTileCoder(unittest.TestCase):

    def test_tile_coder_2_tilings(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 2)
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],11)  # (1,1) * (10,1) = 10+1 = 11
        self.assertEqual(idx[1],100)  # 100 + (0,0) * (10,1) = 100+0 = 100

    def test_tile_coder_2_tilings_unequal_resolution(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), [10,5], 2)
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],10)  # (1,0) * (10,1) = 10+0 = 10
        self.assertEqual(idx[1],50)  # 50 + (0,0) * (10,1) = 50+0 = 50

    def test_tile_coder_3_tilings(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 3)
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),3)
        self.assertEqual(idx[0],11)
        self.assertEqual(idx[1],111)
        self.assertEqual(idx[2],200)

    def test_tile_coder_with_uhn_hashing(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 3, UNH(1000))
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),3)
        self.assertTrue(0 <= idx[0] < 1000)
        self.assertTrue(1000 <= idx[1] < 2000)
        self.assertTrue(2000 <= idx[2] < 3000)

    def test_tile_coder_with_uhn_hashing_consistent(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 3, UNH(1000))
        idx1 = tc.get_indices(np.array([0.14, 0.14]))
        idx2 = tc.get_indices(np.array([0.14, 0.14]))
        # noinspection PyTypeChecker
        self.assertTrue(np.all(idx1==idx2))

    def test_tile_coder_with_uhn_hashing_non_constant(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 3, UNH(1000))
        idx1 = tc.get_indices(np.array([0.14, 0.14]))
        idx2 = tc.get_indices(np.array([0.54, 0.14]))
        # noinspection PyTypeChecker
        self.assertTrue(np.all(idx1!=idx2))

    def test_tile_coder_with_all_nullable_values(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 2, nullable=[True, True])
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],24)  # (1+1,1+1) * (11,1) = 22+2 = 24
        self.assertEqual(idx[1],133)  # 121 + (1+0,1+0) * (11,1) = 121 + 11+1 = 133
        idx = tc.get_indices(np.array([np.nan, 0.14]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],2)  # (0,1+1) * (11,1) = 0+2 = 2
        self.assertEqual(idx[1],122)  # 121 + (0,1+0) * (11,1) = 121 + 0+1 = 122

    def test_tile_coder_with_some_nullable_values(self):
        tc = TileCoder((np.array([0, 0]), np.array([+1, +1])), 10, 2, nullable=[False, True])
        idx = tc.get_indices(np.array([0.14, 0.14]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],12)  # (1,1+1) * (10,1) = 10+2 = 12
        self.assertEqual(idx[1],111)  # 110 + (0,1+0) * (10,1) = 110 + 0+1 = 111
        idx = tc.get_indices(np.array([0.14,np.nan]))
        self.assertEqual(len(idx),2)
        self.assertEqual(idx[0],10)  # (1,0) * (10,1) = 10+0 = 10
        self.assertEqual(idx[1],110)  # 110 + (0,0) * (10,1) = 110 + 0+0 = 110

    def test_tile_coder_log_scales(self):
        tc = TileCoder((np.array([0]), np.array([16])), 4, 1, scales='log')
        tc.bases = np.array([2.0])
        tc.ln_bases = np.log(tc.bases)

        idx = tc.get_indices(np.array([0.0]))
        self.assertEqual(idx[0],0)
        idx = tc.get_indices(np.array([0.99]))
        self.assertEqual(idx[0],0)
        idx = tc.get_indices(np.array([1.0]))
        self.assertEqual(idx[0],1)
        idx = tc.get_indices(np.array([2.99]))
        self.assertEqual(idx[0],1)
        idx = tc.get_indices(np.array([3.0]))
        self.assertEqual(idx[0],2)
        idx = tc.get_indices(np.array([6.99]))
        self.assertEqual(idx[0],2)
        idx = tc.get_indices(np.array([7.0]))
        self.assertEqual(idx[0],3)
        idx = tc.get_indices(np.array([14.99]))
        self.assertEqual(idx[0],3)

    def test_tile_coder_log_scales_with_a_nullable_value(self):
        tc = TileCoder((np.array([0]), np.array([16])), 4, 1, scales='log', nullable=[True])
        tc.bases = np.array([2.0])
        tc.ln_bases = np.log(tc.bases)
        idx = tc.get_indices(np.array([np.nan]))
        self.assertEqual(idx[0],0)
        idx = tc.get_indices(np.array([0.0]))
        self.assertEqual(idx[0],1)
        idx = tc.get_indices(np.array([0.99]))
        self.assertEqual(idx[0],1)
        idx = tc.get_indices(np.array([1.0]))
        self.assertEqual(idx[0],2)
        idx = tc.get_indices(np.array([2.99]))
        self.assertEqual(idx[0],2)
        idx = tc.get_indices(np.array([3.0]))
        self.assertEqual(idx[0],3)
        idx = tc.get_indices(np.array([6.99]))
        self.assertEqual(idx[0],3)
        idx = tc.get_indices(np.array([7.0]))
        self.assertEqual(idx[0],4)
        idx = tc.get_indices(np.array([14.99]))
        self.assertEqual(idx[0],4)


if __name__ == '__main__':
    unittest.main()
