from util.hashing import UNH, GoedelHashing
import numpy as np

import unittest

class TestHashing(unittest.TestCase):

    def test_goedel(self):
        unh = GoedelHashing(np.array([2,2,2]))
        idx = np.array([[1],[1],[0]],dtype=int)
        h1 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==np.array([6])))
        self.assertTrue(h1.shape==(1,))

    def test_goedel_one_dim(self):
        unh = GoedelHashing(np.array([2,2,2]))
        idx = np.array([1,1,0],dtype=int)
        h1 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==np.array([6])))
        self.assertTrue(h1.shape==(1,))

    def test_goedel_multi_input(self):
        unh = GoedelHashing(np.array([2,2,2]))
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==np.array([6,6])))
        self.assertTrue(h1.shape==(2,))

    def test_goedel_subsequent_inputs_are_equal(self):
        unh = GoedelHashing(np.array([2,2,2]))
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==np.array([6,6])))
        h1 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==np.array([6,6])))

    def test_unh(self):
        unh = UNH(100)
        idx = np.array([[1],[1],[0]],dtype=int)
        h1 = unh.hash(idx)
        self.assertTrue(h1.shape==(1,))

    def test_unh_one_dim(self):
        unh = UNH(100)
        idx = np.array([1,1,0],dtype=int)
        h1 = unh.hash(idx)
        self.assertTrue(h1.shape==(1,))

    def test_unh_multi_input(self):
        unh = UNH(100)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh.hash(idx)
        self.assertTrue(h1.shape==(2,))

    def test_unh_different_memories_are_different(self):
        unh1 = UNH(100)
        unh2 = UNH(101)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh1.hash(idx)
        h2 = unh2.hash(idx)
        # noinspection PyTypeChecker
        # non=deterministic
        # self.assertTrue(np.all(h1!=h2))

    def test_unh_equal_memories_are_different(self):
        unh1 = UNH(100)
        unh2 = UNH(101)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh1.hash(idx)
        h2 = unh2.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.any(h1!=h2))

    def test_unh_different_inputs_are_different(self):
        unh1 = UNH(100)
        unh2 = UNH(100)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh1.hash(idx)
        h2 = unh2.hash(idx)
        # noinspection PyTypeChecker
        # non=deterministic
        # self.assertTrue(np.all(h1!=h2))

    def test_unh_subsequent_inputs_are_equal(self):
        unh = UNH(100)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh.hash(idx)
        h2 = unh.hash(idx)
        # noinspection PyTypeChecker
        self.assertTrue(np.all(h1==h2))

    def test_unh_uniform_coverage(self):
        unh = UNH(100)
        idx = np.array([[1,1],[1,1],[0,0]],dtype=int)
        h1 = unh.hash(idx)
        h2 = unh.hash(idx)
        # noinspection PyTypeChecker
        # non=deterministic
        # self.assertTrue(np.all(h1==h2))


if __name__ == '__main__':
    unittest.main()
