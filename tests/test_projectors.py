from util.projector import TileCoderProjector
from util.vector import DenseVector
import numpy as np

import unittest

class TestProjectors(unittest.TestCase):

    def test_tile_coder_projector_basic(self):
        tcp = TileCoderProjector(10, 2, (np.array([0, 0]), np.array([+1, +1])), None, 0, False)
        out = tcp.project(DenseVector(data=[0.14, 0.14]))
        self.assertEqual(len(out),200)
        self.assertEqual(out[11],1)
        self.assertEqual(out[100],1)
        self.assertEqual(sum(out),2)

    def test_tile_coder_projector_3_additional_features_all_zeroes(self):
        tcp = TileCoderProjector(10, 2, (np.array([0, 0]), np.array([+1, +1])), None, 3, False)
        out = tcp.project(DenseVector(data=[0.14, 0.14]))
        self.assertEqual(len(out),203)
        self.assertEqual(out[11],1)
        self.assertEqual(out[100],1)
        self.assertEqual(sum(out),2)

    def test_tile_coder_projector_3_additional_features_two_set_to_one(self):
        tcp = TileCoderProjector(10, 2, (np.array([0, 0]), np.array([+1, +1])), None, 3, False)
        out = tcp.project(DenseVector(data=[0.14, 0.14]), xb=DenseVector(data=[0, 1, 1]))
        self.assertEqual(len(out),203)
        self.assertEqual(out[11],1)
        self.assertEqual(out[100],1)
        self.assertEqual(sum(out),4)

    def test_tile_coder_projector_3_additional_features_and_bias_term(self):
        tcp = TileCoderProjector(10, 2, (np.array([0, 0]), np.array([+1, +1])), None, 3, True)
        out = tcp.project(DenseVector(data=[0.14, 0.14]))
        self.assertEqual(len(out),204)
        self.assertEqual(out[11],1)
        self.assertEqual(out[100],1)
        self.assertEqual(sum(out),3)

    def test_tile_coder_projector_3_additional_features_two_set_to_one_and_bias_term(self):
        tcp = TileCoderProjector(10, 2, (np.array([0, 0]), np.array([+1, +1])), None, 3, True)
        out = tcp.project(DenseVector(data=[0.14, 0.14]), xb=DenseVector(data=[0, 1, 1]))
        self.assertEqual(len(out),204)
        self.assertEqual(out[11],1)
        self.assertEqual(out[100],1)
        self.assertEqual(sum(out),5)