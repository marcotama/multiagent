from util.state_action_encoder import StateActionTabularEncoder, StateActionAppendingEncoder
from util.vector import SparseVector

import unittest

class TestStateActionEncoders(unittest.TestCase):

    def test_state_action_tabular_encoder_basic(self):
        sate = StateActionTabularEncoder(5,2,[0,1,2,3],include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),3)
        self.assertEqual(len(sa),20)
        self.assertEqual(sa[16],1)
        self.assertEqual(sa[19],1)
        self.assertEqual(sum(sa),2)

    def test_state_action_tabular_encoder_non_consecutive_actions(self):
        sate = StateActionTabularEncoder(5,2,[0,2,3,77],include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),77)
        self.assertEqual(len(sa),20)
        self.assertEqual(sa[16],1)
        self.assertEqual(sa[19],1)
        self.assertEqual(sum(sa),2)

    def test_state_action_tabular_encoder_mixed_action_types(self):
        sate = StateActionTabularEncoder(5,2,[0,2,3,'banana'],include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),'banana')
        self.assertEqual(len(sa),20)
        self.assertEqual(sa[16],1)
        self.assertEqual(sa[19],1)
        self.assertEqual(sum(sa),2)

    def test_state_action_tabular_encoder_bias_term(self):
        sate = StateActionTabularEncoder(5,2,[0,2,3,'banana'],include_bias=True)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),'banana')
        self.assertEqual(len(sa),21)
        self.assertEqual(sa[16],1)
        self.assertEqual(sa[19],1)
        self.assertEqual(sum(sa),3)

    def test_state_action_tilings_encoder_basic(self):
        sate = StateActionAppendingEncoder(5, 2, [0, 1, 2, 3], include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),3)
        self.assertEqual(len(sa),9)
        self.assertEqual(sa[1],1)
        self.assertEqual(sa[4],1)
        self.assertEqual(sa[8],1)
        self.assertEqual(sum(sa),3)

    def test_state_action_tilings_encoder_non_consecutive_actions(self):
        sate = StateActionAppendingEncoder(5, 2, [0, 2, 3, 77], include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),77)
        self.assertEqual(len(sa),9)
        self.assertEqual(sa[1],1)
        self.assertEqual(sa[4],1)
        self.assertEqual(sa[8],1)
        self.assertEqual(sum(sa),3)

    def test_state_action_tilings_encoder_mixed_action_types(self):
        sate = StateActionAppendingEncoder(5, 2, [0, 2, 3, 'banana'], include_bias=False)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),'banana')
        self.assertEqual(len(sa),9)
        self.assertEqual(sa[1],1)
        self.assertEqual(sa[4],1)
        self.assertEqual(sa[8],1)
        self.assertEqual(sum(sa),3)

    def test_state_action_tilings_encoder_bias_term(self):
        sate = StateActionAppendingEncoder(5, 2, [0, 2, 3, 'banana'], include_bias=True)
        sa = sate.state_action(SparseVector(data=[0,1,0,0,1]),'banana')
        self.assertEqual(len(sa),10)
        self.assertEqual(sa[1],1)
        self.assertEqual(sa[4],1)
        self.assertEqual(sa[8],1)
        self.assertEqual(sa[9],1)
        self.assertEqual(sum(sa),4)


if __name__ == '__main__':
    unittest.main()
