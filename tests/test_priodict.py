from util.priodict import PriorityDictionary

import unittest

class TestPriorityDictionary(unittest.TestCase):

    def test_priority_dict(self):
        pd = PriorityDictionary()
        pd['a'] = 1
        pd['b'] = 7
        pd['c'] = 31
        pd['d'] = 5
        l = [k for k in pd]
        self.assertEqual(l, ['a','d','b','c'])