import numpy as np
from util.data_manipulation import get_averages, normalize

import unittest

class TestPriorityDictionary(unittest.TestCase):

    def test_get_averages(self):
        d_in = {
            'a': {
                'x': [+1,+2,+3,+4],
                'y': [+3,+4,+5,+6],
                'z': [+5,+6,+7,+8],
            },
            'b': {
                'x': [-1,-2,-3,-4],
                'y': [-3,-4,-5,-6],
                'z': [-5,-6,-7,-8],
            },
        }
        d_expected_out = {
            'a': {
                'x': +2.5,
                'y': +4.5,
                'z': +6.5,
            },
            'b': {
                'x': -2.5,
                'y': -4.5,
                'z': -6.5,
            }
        }
        d_out = dict(get_averages(d_in).items())
        self.assertEqual(d_out, d_expected_out)

    def test_get_averages_with_deeper_keys(self):
        d_in = {
            'a': {
                'x': [+1,+2,+3,+4],
                'y': [+3,+4,+5,+6],
                'z': [+5,+6,+7,+8],
                'd': {
                    '1': [+1,+2,+3,+4],
                    '2': [-3,+4,+5,-6],
                    '3': [-5,-6,-7,-8],
                },
            },
            'b': {
                'x': [-1,-2,-3,-4],
                'y': [-3,-4,-5,-6],
                'z': [-5,-6,-7,-8],
                'd': {
                    '1': [-1,-2,-3,-4],
                    '2': [+3,-4,-5,+6],
                    '3': [+5,+6,+7,+8],
                },
            },
        }
        d_expected_out = {
            'a': {
                'x': +2.5,
                'y': +4.5,
                'z': +6.5,
                'd': {
                    '1': +2.5,
                    '2': 0.0,
                    '3': -6.5,
                },
            },
            'b': {
                'x': -2.5,
                'y': -4.5,
                'z': -6.5,
                'd': {
                    '1': -2.5,
                    '2': 0.0,
                    '3': +6.5,
                },
            },
        }
        d_out = dict(get_averages(d_in, deeper_keys=['d']).items())
        self.assertEqual(d_out, d_expected_out)

    def test_get_averages_with_copy_keys(self):
        d_in = {
            'a': {
                'x': [+1,+2,+3,+4],
                'y': [+3,+4,+5,+6],
                'z': [+5,+6,+7,+8],
                'c': 4,
            },
            'b': {
                'x': [-1,-2,-3,-4],
                'y': [-3,-4,-5,-6],
                'z': [-5,-6,-7,-8],
                'c': 4,
            },
        }
        d_expected_out = {
            'a': {
                'x': +2.5,
                'y': +4.5,
                'z': +6.5,
                'c': 4,
            },
            'b': {
                'x': -2.5,
                'y': -4.5,
                'z': -6.5,
                'c': 4,
            }
        }
        d_out = dict(get_averages(d_in, copy_keys=['c']).items())
        self.assertEqual(d_out, d_expected_out)

    def test_normalize(self):
        d_in = {
            'x': [+1,+2,+6,+60],
            'y': [+1,+4,+1,+10],
            'z': [+2,+2,+1,+10],
        }
        d_expected_out = {
            'x': np.asarray([+0.25,+0.25,+0.750,+0.750]),
            'y': np.asarray([+0.25,+0.50,+0.125,+0.125]),
            'z': np.asarray([+0.50,+0.25,+0.125,+0.125]),
        }
        d_out = dict(normalize(d_in).items())
        for key in ['x','y','z']:
            self.assertTrue(np.all(d_out[key] == d_expected_out[key]))