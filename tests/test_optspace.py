# Sample Code to test the OptSpace program

from math import log10, sqrt
from numpy import eye, ceil
from numpy.random import randn, rand
from numpy.linalg import norm
from util.opt_space import OptSpace
import unittest

class TestOptSpace(unittest.TestCase):

    def test_optspace_without_noise(self):
        return
        n = 1000
        m = 1000
        r = 3

        eps = 10 * r * log10(n)
        # Generate the matrix with the given parameters
        U = randn(n,r)
        V = randn(m,r)
        Sig = eye(r)
        M0 = U @ Sig @ V.T

        # Select the entries independently with prob. eps/sqrt(mn)
        E = 1 - ceil(rand(n,m) - eps / sqrt(m*n))
        M_E = M0 @ E

        # Call the OptSpace function
        X, S, Y, _ = OptSpace(M_E)

        # Compute the Frobenius norm
        err_nonoise = norm(X @ S @ Y.T - M0, 'fro') / sqrt(m*n)
        print('RMSE (without noise): %e\n' % err_nonoise)


    def test_optspace_with_noise(self):
        return
        print('Generating Matrix...\n')

        n = 1000
        m = 1000
        r = 3
        tol = 1e-8

        eps = 10 * r * log10(n)
        # Generate the matrix with the given parameters
        U = randn(n,r)
        V = randn(m,r)

        Sig = eye(r)
        M0 = U @ Sig @ V.T

        # Select the entries independently with prob. eps/sqrt(mn)
        E = 1 - ceil(rand(n,m) - eps / sqrt(m*n))

        # Add noise
        sig = .01
        M = M0 + sig*randn(n,m)
        M_E = M * E

        # Call the OptSpace function
        X, S, Y, _ = OptSpace(M_E,niter=20,tol=tol)

        # Compute the Frobenius norm
        err_noise = norm(X @ S @ Y.T - M0, 'fro')/sqrt(m*n)

        print('RMSE (with noise var %f) : %e\n' % (sig, err_noise))

