from pacman.layout import Layout
from pacman.text_display import NullGraphics
from pacman.game import ClassicGameRules, Direction
from pacman.agents.simple_ghost_agents import RandomGhost
from pacman.agents.keyboard_agents import KeyboardAgent
from pacman.features_extractor import PacmanFeaturesExtractor

import unittest

class TestPacmanFeaturesExtractor(unittest.TestCase):

    def setUp(self):
        mediumClassicLayoutStr = (
            """%%%%%%%%%%%%%%%%%%%%
            %o...%........%....%
            %.%%.%.%%%%%%.%.%%.%
            %.%..............%.%
            %.%.%%.%%  %%.%%.%.%
            %......%G  G%......%
            %.%.%%.%%%%%%.%%.%.%
            %.%..............%.%
            %.%%.%.%%%%%%.%.%%.%
            %....%   P  ..%...o%
            %%%%%%%%%%%%%%%%%%%%""")
        layout = Layout([line.strip() for line in mediumClassicLayoutStr.splitlines()])
        display = NullGraphics()
        rules = ClassicGameRules()
        pacman = [KeyboardAgent(), KeyboardAgent()]
        ghosts = [RandomGhost(i + 1) for i in range(layout.getNumGhosts())]
        game = rules.newGame(layout, pacman, ghosts, display, quiet=True)
        extractor = PacmanFeaturesExtractor([], agent_index=0)
        extractor.set_game_state(game.state)
        extractor.set_layout(layout)
        self.extractor = extractor  # type: PacmanFeaturesExtractor


    def test_nearest_ghost_x(self):
        f = self.extractor.extract_feature('nearest[1]-ghost-x')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 8)


    def test_nearest_ghost_y(self):
        f = self.extractor.extract_feature('nearest[1]-ghost-y')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 5)


    def test_nearest_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('nearest[1]-ghost-shortest_path_distance')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 15)


    def test_nearest_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('nearest[1]-ghost-shortest_path_direction')
        self.assertIsInstance(f, Direction)
        self.assertEqual(f, Direction.west)


    def test_nearest_ghost_shortest_scared_timer(self):
        f = self.extractor.extract_feature('nearest[1]-ghost-scared_timer')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 0)




    def test_nearest_scared_ghost_spawn_point_sp_distance(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-spawn_point_sp_distance')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 15)


    def test_nearest_scared_ghost_x(self):
        f = self.extractor.extract_feature('nearest[1]-scared_ghost-x')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertTrue(f is None)


    def test_nearest_scared_ghost_y(self):
        f = self.extractor.extract_feature('nearest[1]-scared_ghost-y')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertTrue(f is None)


    def test_nearest_scared_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('nearest[1]-scared_ghost-shortest_path_distance')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertTrue(f is None)


    def test_nearest_scared_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('nearest[1]-scared_ghost-shortest_path_direction')
        self.assertTrue(f is None or isinstance(f, Direction))
        self.assertTrue(f is None)


    def test_nearest_scared_ghost_shortest_scared_timer(self):
        f = self.extractor.extract_feature('nearest[1]-scared_ghost-scared_timer')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertTrue(f is None)



    def test_nearest_non_scared_ghost_x(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-x')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 8)


    def test_nearest_non_scared_ghost_y(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-y')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 5)


    def test_nearest_non_scared_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-shortest_path_distance')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 15)


    def test_nearest_non_scared_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-shortest_path_direction')
        self.assertTrue(f is None or isinstance(f, Direction))
        self.assertEqual(f, Direction.west)


    def test_nearest_non_scared_ghost_shortest_non_scared_timer(self):
        f = self.extractor.extract_feature('nearest[1]-non_scared_ghost-scared_timer')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 0)



    def test_nearest_capsule_x(self):
        f = self.extractor.extract_feature('nearest[1]-capsule-x')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 18)


    def test_nearest_capsule_y(self):
        f = self.extractor.extract_feature('nearest[1]-capsule-y')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 1)


    def test_nearest_capsule_shortest_path_distance(self):
        f = self.extractor.extract_feature('nearest[1]-capsule-shortest_path_distance')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 13)


    def test_nearest_capsule_shortest_path_direction(self):
        f = self.extractor.extract_feature('nearest[1]-capsule-shortest_path_direction')
        self.assertTrue(f is None or isinstance(f, Direction))
        self.assertEqual(f, Direction.east)


    def test_nearest_capsule_shortest_scared_timer(self):
        f = self.extractor.extract_feature('nearest[1]-capsule-scared_timer')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 0)




    def test_nearest_food_x(self):
        f = self.extractor.extract_feature('nearest[1]-food-x')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 12)


    def test_nearest_food_y(self):
        f = self.extractor.extract_feature('nearest[1]-food-y')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 1)


    def test_nearest_food_shortest_path_distance(self):
        f = self.extractor.extract_feature('nearest[1]-food-shortest_path_distance')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 3)


    def test_nearest_food_shortest_path_direction(self):
        f = self.extractor.extract_feature('nearest[1]-food-shortest_path_direction')
        self.assertTrue(f is None or isinstance(f, Direction))
        self.assertIn(f, [Direction.east, Direction.west])


    def test_nearest_food_shortest_scared_timer(self):
        f = self.extractor.extract_feature('nearest[1]-food-scared_timer')
        self.assertTrue(f is None or isinstance(f, int))
        self.assertEqual(f, 0)




    def test_ghost_x(self):
        f = self.extractor.extract_feature('ghost-x')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_ghost_y(self):
        f = self.extractor.extract_feature('ghost-y')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('ghost-shortest_path_distance')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('ghost-shortest_path_direction')
        self.assertTrue(all(isinstance(v, Direction) for v in f))


    def test_ghost_shortest_scared_timer(self):
        f = self.extractor.extract_feature('ghost-scared_timer')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v == 0 for v in f))




    def test_scared_ghost_x(self):
        f = self.extractor.extract_feature('scared_ghost-x')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_scared_ghost_y(self):
        f = self.extractor.extract_feature('scared_ghost-y')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_scared_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('scared_ghost-shortest_path_distance')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_scared_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('scared_ghost-shortest_path_direction')
        self.assertTrue(all(isinstance(v, Direction) for v in f))


    def test_scared_ghost_shortest_scared_timer(self):
        f = self.extractor.extract_feature('scared_ghost-scared_timer')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v > 0 for v in f))



    def test_non_scared_ghost_x(self):
        f = self.extractor.extract_feature('non_scared_ghost-x')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_non_scared_ghost_y(self):
        f = self.extractor.extract_feature('non_scared_ghost-y')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_non_scared_ghost_shortest_path_distance(self):
        f = self.extractor.extract_feature('non_scared_ghost-shortest_path_distance')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_non_scared_ghost_shortest_path_direction(self):
        f = self.extractor.extract_feature('non_scared_ghost-shortest_path_direction')
        self.assertTrue(all(isinstance(v, Direction) for v in f))


    def test_non_scared_ghost_shortest_non_scared_timer(self):
        f = self.extractor.extract_feature('non_scared_ghost-scared_timer')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v == 0 for v in f))




    def test_capsule_x(self):
        f = self.extractor.extract_feature('capsule-x')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_capsule_y(self):
        f = self.extractor.extract_feature('capsule-y')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_capsule_shortest_path_distance(self):
        f = self.extractor.extract_feature('capsule-shortest_path_distance')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_capsule_shortest_path_direction(self):
        f = self.extractor.extract_feature('capsule-shortest_path_direction')
        self.assertTrue(all(isinstance(v, Direction) for v in f))


    def test_capsule_shortest_scared_timer(self):
        f = self.extractor.extract_feature('capsule-scared_timer')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v == 0 for v in f))




    def test_food_x(self):
        f = self.extractor.extract_feature('food-x')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_food_y(self):
        f = self.extractor.extract_feature('food-y')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_food_shortest_path_distance(self):
        f = self.extractor.extract_feature('food-shortest_path_distance')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v <= max(self.extractor.maze.geom) for v in f))


    def test_food_shortest_path_direction(self):
        f = self.extractor.extract_feature('food-shortest_path_direction')
        self.assertTrue(all(isinstance(v, Direction) for v in f))


    def test_food_shortest_scared_timer(self):
        f = self.extractor.extract_feature('food-scared_timer')
        self.assertTrue(all(isinstance(v, int) for v in f))
        self.assertTrue(all(v == 0 for v in f))




    def test_self_x (self):
        f = self.extractor.extract_feature('self_x')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 9)

    def test_self_y (self):
        f = self.extractor.extract_feature('self_y')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 1)

    def test_self_score (self):
        f = self.extractor.extract_feature('self_score')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 0)

    def test_legal_west (self):
        f = self.extractor.extract_feature('legal-west')
        self.assertIsInstance(f, bool)
        self.assertEqual(f, True)

    def test_legal_east (self):
        f = self.extractor.extract_feature('legal-east')
        self.assertIsInstance(f, bool)
        self.assertEqual(f, True)

    def test_legal_south (self):
        f = self.extractor.extract_feature('legal-south')
        self.assertIsInstance(f, bool)
        self.assertEqual(f, False)

    def test_legal_north (self):
        f = self.extractor.extract_feature('legal-north')
        self.assertIsInstance(f, bool)
        self.assertEqual(f, False)





    def test_shortest_path_distance_to_nearest_ghost_via_north (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-ghost-via-north')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_ghost_via_south (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-ghost-via-south')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_ghost_via_east (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-ghost-via-east')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 16)

    def test_shortest_path_distance_to_nearest_ghost_via_west (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-ghost-via-west')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 15)



    def test_shortest_path_distance_to_nearest_food_via_north (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-food-via-north')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_food_via_south (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-food-via-south')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_food_via_east (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-food-via-east')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 3)

    def test_shortest_path_distance_to_nearest_food_via_west (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-food-via-west')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 4)



    def test_shortest_path_distance_to_nearest_capsule_via_north (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-capsule-via-north')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_capsule_via_south (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-capsule-via-south')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_capsule_via_east (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-capsule-via-east')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 13)

    def test_shortest_path_distance_to_nearest_capsule_via_west (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-capsule-via-west')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 16)



    def test_shortest_path_distance_to_nearest_scared_ghost_via_north (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-scared_ghost-via-north')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_scared_ghost_via_south (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-scared_ghost-via-south')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_scared_ghost_via_east (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-scared_ghost-via-east')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_scared_ghost_via_west (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-scared_ghost-via-west')
        self.assertTrue(f is None)



    def test_shortest_path_distance_to_nearest_non_scared_ghost_via_north (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-non_scared_ghost-via-north')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_non_scared_ghost_via_south (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-non_scared_ghost-via-south')
        self.assertTrue(f is None)

    def test_shortest_path_distance_to_nearest_non_scared_ghost_via_east (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-non_scared_ghost-via-east')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 16)

    def test_shortest_path_distance_to_nearest_non_scared_ghost_via_west (self):
        f = self.extractor.extract_feature('shortest_path_distance_to_nearest-non_scared_ghost-via-west')
        self.assertIsInstance(f, int)
        self.assertEqual(f, 15)


    def test_agent_0_shortest_path_distance_wrt_agent_1 (self):
        f = self.extractor.extract_feature('agent[0]-shortest_path_distance', 1)
        self.assertTrue(isinstance(f, int))
        self.assertEqual(f, 15)