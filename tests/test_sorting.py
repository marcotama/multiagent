from util.sorting import arg_max, arg_min, arg_sort

import unittest

class TestPriorityDictionary(unittest.TestCase):

    def test_arg_sort_list(self):
        input_list = [1,4,3]
        expected_output = [0,2,1]
        actual_output = arg_sort(input_list)
        self.assertEqual(expected_output, actual_output)

    def test_arg_sort_dict(self):
        input_list = {'a': 1, 'b': 4, 'c': 3}
        expected_output = ['a','c','b']
        actual_output = arg_sort(input_list)
        self.assertEqual(expected_output, actual_output)

    def test_arg_min_list(self):
        input_list = [5,1,4,3]
        expected_output = 1
        actual_output = arg_min(input_list)
        self.assertEqual(expected_output, actual_output)

    def test_arg_min_dict(self):
        input_list = {'0': 5, 'a': 1, 'b': 4, 'c': 3}
        expected_output = 'a'
        actual_output = arg_min(input_list)
        self.assertEqual(expected_output, actual_output)

    def test_arg_max_list(self):
        input_list = [5,1,4,3]
        expected_output = 0
        actual_output = arg_max(input_list)
        self.assertEqual(expected_output, actual_output)

    def test_arg_max_dict(self):
        input_list = {'0': 5, 'a': 1, 'b': 4, 'c': 3}
        expected_output = '0'
        actual_output = arg_max(input_list)
        self.assertEqual(expected_output, actual_output)