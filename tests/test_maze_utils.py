from pacman.layout import Layout
from util.maze_utils import Maze

import unittest

class TestPacman2PFeaturesExtractor(unittest.TestCase):

    def setUp(self):
        mediumClassicLayoutStr = (
            """%%%%%%%%%%%%%%%%%%%%
            %o...%........%....%
            %.%%.%.%%%%%%.%.%%.%
            %.%..............%.%
            %.%.%%.%%  %%.%%.%.%
            %......%G  G%......%
            %.%.%%.%%%%%%.%%.%.%
            %.%..............%.%
            %.%%.%.%%%%%%.%.%%.%
            %....%...P....%...o%
            %%%%%%%%%%%%%%%%%%%%""")
        layout = Layout([line.strip() for line in mediumClassicLayoutStr.splitlines()])

        w, h = layout.width, layout.height
        from itertools import product
        self.maze = Maze(
            blocks=[(x,y) for x, y in product(range(w), range(h)) if layout.walls[x][y]],
            walls=[],
            height=layout.height,
            width=layout.width
        )


    def test_walkable(self):
        self.assertTrue(self.maze.walkable(9, 1))
        self.assertTrue(self.maze.walkable(9, 3))
        self.assertTrue(self.maze.walkable(9, 5))
        self.assertTrue(self.maze.walkable(8, 1))
        self.assertFalse(self.maze.walkable(8, 0))
        self.assertFalse(self.maze.walkable(7, 0))
        self.assertFalse(self.maze.walkable(0, 0))
        self.assertFalse(self.maze.walkable(19, 10))
        self.assertFalse(self.maze.walkable(0, 10))
        self.assertFalse(self.maze.walkable(19, 0))
        self.assertFalse(self.maze.walkable(9, 2))
        self.assertFalse(self.maze.walkable(9, 4))


    def test_shortest_path_distance(self):
        self.assertEqual(self.maze.shortest_path_distance(9, 1, 18, 3), 15)
        self.assertEqual(self.maze.shortest_path_distance(9, 1, 18, 5), 13)
        self.assertEqual(self.maze.shortest_path_distance(9, 1, 6, 2), 4)


    def test_shortest_path_direction(self):
        self.assertEqual(self.maze.shortest_path_direction(9, 1, 18, 3), (1, 0))
        self.assertEqual(self.maze.shortest_path_direction(9, 1, 18, 5), (1, 0))
        self.assertEqual(self.maze.shortest_path_direction(9, 1, 6, 2), (-1, 0))


    def test_shortest_path_includes(self):
        self.assertTrue(self.maze.shortest_path_includes(9, 1, 18, 3, 10, 1))
        self.assertTrue(self.maze.shortest_path_includes(9, 1, 18, 5, 11, 1))
        self.assertTrue(self.maze.shortest_path_includes(9, 1, 6, 2, 8, 1))
        self.assertFalse(self.maze.shortest_path_includes(9, 1, 18, 3, 19, 1))
        self.assertFalse(self.maze.shortest_path_includes(9, 1, 18, 5, 11, 4))
        self.assertFalse(self.maze.shortest_path_includes(9, 1, 6, 2, 10, 1))


    def test_junctions(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%.*.*..**..*.*.%.%
           %.%.%%.%%**%%.%%.%.%
           %*.*..*%G**G%*..*.*%
           %.%.%%.%%%%%%.%%.%.%
           %.%.*.*......*.*.%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = sorted(self.maze._get_junctions())
        jns2 = [(4,3),(6,3),(13,3),(15,3),(1,5),(3,5),(6,5),(9,5),(10,5),(13,5),(16,5),(18,5),(9,6),(10,6),(9,7),(10,7),(4,7),(6,7),(13,7),(15,7)]
        jns2 = sorted(self.maze._encode_locations(jns2))
        self.assertEqual(jns1, jns2)


    def test_adjacent_junctions(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%G  G%......%
           %.%.%%.%%%%%%.%%.%.%
           %.%...*......*...%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze._get_adjacent_junctions(9, 1)
        jns1 = sorted([node for node, paths in jns1])
        jns2 = [(6,3),(13,3)]
        jns2 = sorted(self.maze._encode_locations(jns2))
        self.assertEqual(jns1, jns2)


    def test_escape_junctions(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%G%%.%.%
           %.%..G*G....G.G..%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze._get_escape_junctions(9, 1, [(5, 3), (7, 3), (12, 3), (14, 3), (13, 4)])
        jns1 = sorted([node for node, paths, threats_path in jns1])
        jns2 = [(6,3)]
        jns2 = sorted([self.maze._encode_location(x,y) for x,y in jns2])
        self.assertEqual(jns1, jns2)


    def test_safe_junctions(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%.G......%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%...*........G.%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [(7,9),(14,3)])
        jns1 = sorted(jns1)
        jns2 = sorted([(6,3)])
        self.assertEqual(jns1, jns2)


    def test_safe_junctions_no_threats(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%.G......%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%...*........G.%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [])
        jns1 = sorted(jns1)
        jns2 = sorted([(6,3),(13,3)])
        self.assertEqual(jns1, jns2)


    def test_safe_junctions_no_safe_junction(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%..G........G..%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%...P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [(4,3),(13,3)])
        jns1 = sorted(jns1)
        jns2 = []
        self.assertEqual(jns1, jns2)


    def test_safe_junctions_no_safe_junction_2(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%...........G..%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%.G.P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [(4,3),(13,3)])
        jns1 = sorted(jns1)
        jns2 = []
        self.assertEqual(jns1, jns2)


    def test_safe_junctions_no_safe_junction_3(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%..............%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%.G.P.G..%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [(4,3),(13,3)])
        jns1 = sorted(jns1)
        jns2 = []
        self.assertEqual(jns1, jns2)


    def test_safe_junctions_no_safe_junction_4(self):
        """%%%%%%%%%%%%%%%%%%%%
           %o...%........%....%
           %.%%.%.%%%%%%.%.%%.%
           %.%..............%.%
           %.%.%%.%%  %%.%%.%.%
           %......%    %......%
           %.%.%%.%%%%%%.%%.%.%
           %.%..........G...%.%
           %.%%.%.%%%%%%.%.%%.%
           %....%.G.P....%...o%
           %%%%%%%%%%%%%%%%%%%%"""
        jns1 = self.maze.get_safe_junctions(9, 1, [(4,3),(13,3)])
        jns1 = sorted(jns1)
        jns2 = []
        self.assertEqual(jns1, jns2)
