import math
import numpy as np
import unittest
from util.vector import DenseVector #, SparseVector

np.seterr(divide='ignore', invalid='ignore')

d1 = DenseVector(10)
d2 = DenseVector(10)
f1 = 5.0
f2 = 2.0
f3 = 8.0
zr = 0.0

d1[1] = 1.0
d1[4] = 4.0
d2[2] = 1.0
d2[4] = 2.0
# d1 = [0 1 0 0 4 ... ]
# d2 = [0 0 1 0 2 ... ]


class TestOperators(unittest.TestCase):
    def test_add(self):
        # add
        res = d1+d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 1.0
        assert res[3] == 0.0
        assert res[4] == 6.0

        res = d2 + f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 6.0
        assert res[3] == 5.0
        assert res[4] == 7.0

    def test_sub(self):
        res = d1-d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == -1.0
        assert res[3] == 0.0
        assert res[4] == 2.0

        res = d2 - f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == -4.0
        assert res[3] == -5.0
        assert res[4] == -3.0

    def test_mul(self):
        res = d1*d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 0.0
        assert res[3] == 0.0
        assert res[4] == 8.0

        res = d2 * f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 5.0
        assert res[3] == 0.0
        assert res[4] == 10.0

    def test_truediv(self):
        res = d1/d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert math.isinf(res[1])
        assert res[2] == 0.0
        assert math.isnan(res[3])
        assert res[4] == 2.0

        res = d2 / f2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 0.5
        assert res[3] == 0.0
        assert res[4] == 1.0

        # matmul
        res = d1 @ d2
        assert isinstance(res, (float, int))
        assert res == 8.0


class TestReverseOperators(unittest.TestCase):
    def test_radd(self):
        res = f1 + d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 6.0
        assert res[3] == 5.0
        assert res[4] == 7.0

    def test_rsub(self):
        res = f1 - d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 4.0
        assert res[3] == 5.0
        assert res[4] == 3.0

    def test_rmul(self):
        res = f1 * d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 5.0
        assert res[3] == 0.0
        assert res[4] == 10.0

    def test_rtruediv(self):
        res = f3 / d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert math.isinf(res[1])
        assert res[2] == 8.0
        assert res[4] == 4.0


class TestInplaceOperators(unittest.TestCase):
    def test_iadd(self):
        res = d2.copy()
        res += d1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 1.0
        assert res[3] == 0.0
        assert res[4] == 6.0

        res = d2.copy()
        res += f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 6.0
        assert res[3] == 5.0
        assert res[4] == 7.0

    def test_isub(self):
        res = d1.copy()
        res -= d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == -1.0
        assert res[3] == 0.0
        assert res[4] == 2.0

        res = d2.copy()
        res -= f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == -4.0
        assert res[3] == -5.0
        assert res[4] == -3.0

    def test_imul(self):
        res = d1.copy()
        res *= d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 0.0
        assert res[3] == 0.0
        assert res[4] == 8.0

        res = d2.copy()
        res *= f1
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 5.0
        assert res[3] == 0.0
        assert res[4] == 10.0

    def test_itruediv(self):
        res = d1.copy()
        res /= d2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert math.isinf(res[1])
        assert res[2] == 0.0
        assert math.isnan(res[3])
        assert res[4] == 2.0

        res = d2.copy()
        res /= f2
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[2] == 0.5
        assert res[3] == 0.0
        assert res[4] == 1.0


class TestExponentiation(unittest.TestCase):
    def test_exp(self):
        res = DenseVector.exp(d1)
        assert isinstance(res, DenseVector)
        assert len(res) == 10
        assert res[1] == math.e
        assert res[2] == 1.0
        assert res[4] == math.exp(4.0)

# TODO tests with mixed types
